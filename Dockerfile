# grab env variables from CI/CD pipeline
ARG CERNGETDP_VERSION

# load cerngetdp-image 
FROM gitlab-registry.cern.ch/steam/cerngetdp:${CERNGETDP_VERSION}

# fiqus python requirements 
COPY setup.py .
COPY README.md .
COPY requirements.txt .

# create python venv
RUN python3 -m venv /fiqus_venv

# install requirements in python 3.11.8
ENV SETUPTOOLS_USE_DISTUTILS=stdlib
RUN /fiqus_venv/bin/pip3 install --upgrade pip
RUN /fiqus_venv/bin/pip3 install .["all"]