![FiQuS logo](https://gitlab.cern.ch/steam/fiqus/-/raw/master/docs/images/FiQuS_name_logo.svg)

# Introduction
Source code for STEAM **FiQuS** (**Fi**nite Element **Qu**ench **S**imulator).

# Publications

## Describing FiQuS Modules
- S. Atalay et al, "An open-source 3D FE quench simulation tool for no-insulation HTS pancake coils", in Superconductor Science and Technology, doi: [10.1088/1361-6668/ad3f83](https://doi.org/10.1088/1361-6668/ad3f83)
- J. Dular et al, "Coupled Axial and Transverse Currents Method for Finite Element Modelling of Periodic Superconductors", in Superconductor Science and Technology, doi: [10.1088/1361-6668/ad650d](https://doi.org/10.1088/1361-6668/ad650d).
- E. Schnaubelt et al, "Transient Finite Element Simulation of Accelerator Magnets Using Thermal Thin Shell Approximation", arXiV preprint, doi: [10.48550/arXiv.2501.15871](https://doi.org/10.48550/arXiv.2501.15871).
- A. Vitrano et al, "An Open-Source Finite Element Quench Simulation Tool for Superconducting Magnets," in IEEE Transactions on Applied Superconductivity, vol. 33, no. 5, pp. 1-6, Aug. 2023, Art no. 4702006, doi: [10.1109/TASC.2023.3259332](https://ieeexplore.ieee.org/abstract/document/10077402).

## Using FiQuS

- J. Dular et al, "Simulation of Rutherford Cable AC Loss and Magnetization with the Coupled Axial and Transverse Currents Method", in IEEE Transactions on Applied Superconductivity, [10.1109/TASC.2024.3520941](https://doi.org/10.1109/TASC.2024.3520941).
- M. Wozniak et al, "Influence of Critical Current Distribution on Operation, Quench Detection and Protection of HTS Pancake Coils, in IEEE Transactions on Applied Superconductivity, [10.1109/TASC.2025.3532246](https://doi.org/10.1109/TASC.2025.3532246).
- Laura AM D’Angelo et al, "Efficient Reduced Magnetic Vector Potential Formulation for the Magnetic Field Simulation of Accelerator Magnets", in IEEE Transactions on Magnetics, vol. 60, no. 3, pp. 1-8, Jan. 2024, Art no. 7000808, doi: [10.1109/TMAG.2024.3352113](https://ieeexplore.ieee.org/abstract/document/10387412).
- M. Wozniak et al, "Quench Co-Simulation of Canted Cos-Theta Magnets," in IEEE Transactions on Applied Superconductivity, vol. 34, no. 3, pp. 1-5, Dec. 2023, Art no. 4900105, doi: [10.1109/TASC.2023.3338142](https://ieeexplore.ieee.org/document/10337614).
- M. Wozniak et al, "Fast Quench Propagation Conductor for Protecting Canted Cos-Theta Magnets," in IEEE Transactions on Applied Superconductivity, vol. 33, no. 5, pp. 1-5, Aug. 2023, Art no. 4701705, doi: [10.1109/TASC.2023.3247997](https://ieeexplore.ieee.org/document/10050158).
- E. Schnaubelt et al, "Parallel-in-Time Integration of Transient Phenomena in No-Insulation Superconducting Coils Using Parareal", accepted for publication in the proceedings of the 
Scientific Computing in Electrical Engineering (SCEE) 2024 conference, [arXiv:2404.13333](https://arxiv.org/abs/2404.13333).

## Mathematical Formulations Forming the Basis of Some FiQuS Modules

- E. Schnaubelt et al, "Magneto-Thermal Thin Shell Approximation for 3D Finite Element Analysis of No-Insulation Coils", in IEEE Transactions on Applied Superconductivity, vol. 34, no. 3, pp. 1-6, Dec. 2023, Art no. 4700406, doi: [10.1109/TASC.2023.3340648](https://ieeexplore.ieee.org/document/10349801).
- E. Schnaubelt et al, "Electromagnetic Simulation of No-Insulation Coils Using H−φ Thin Shell Approximation", in IEEE Transactions on Applied Superconductivity, vol. 33, no. 5, pp. 1-6, Mar. 2023, Art no. 4900906, doi: [10.1109/TASC.2023.3258905](https://ieeexplore.ieee.org/document/10076826).
- E. Schnaubelt et al, "Thermal thin shell approximation towards finite element quench simulation", in Superconductor Science and Technology, vol. 36, no. 4, Art no. 044004, doi: [10.1088/1361-6668/acbeea](https://iopscience.iop.org/article/10.1088/1361-6668/acbeea).


# Folder Structure
![FiQuS folder structure](https://gitlab.cern.ch/steam/fiqus/-/raw/master/docs/images/FiQuS_folder_structure.svg)

# Installation

## Released version:
```pip install fiqus```

# Links
STEAM website: http://cern.ch/steam

STEAM-FiQuS website: https://steam.docs.cern.ch/tools/fiqus/

STEAM-FiQuS documentation: https://steam-fiqus.docs.cern.ch/ 

Coverage report: https://steam-fiqus.docs.cern.ch/htmlcov/ 

# Contact
steam-team@cern.ch

# STEAM User Agreement
By using any software of the STEAM framework, users agree with this document:
https://edms.cern.ch/document/2024516

(Copyright © 2022, CERN, Switzerland. All rights reserved.)

# License
This project is licensed under the GNU General Public License v3.0. See the LICENSE file for details.