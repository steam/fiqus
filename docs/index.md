<!--- {!README.md!} --->

# Installation

## Released version:
```pip install fiqus```

# Links
[STEAM Website](https://cern.ch/steam)

[STEAM-FiQuS website](https://cern.ch/fiqus)

[Coverage report](https://steam-fiqus-dev.docs.cern.ch/htmlcov/) 

# Contact
[steam-team@cern.ch](mailto:steam-team@cern.ch)

# STEAM User Agreement
By using any software of the STEAM framework, users agree with [this document](https://edms.cern.ch/document/2024516). 


(Copyright © 2022, CERN, Switzerland. All rights reserved.)
