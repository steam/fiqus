# Installation

These videos document a very basic environment setup for working with FiQuS. First all the needed software is installed and then last video shows how to run a simple FiQuS model.  
It is done with Windows 11 and PyCharm. Windows 10 works in the same way. An equivalent setup for Visual Studio Code would also work.

## 1. Python installation
This video shows how to install Python (3.11.8).  
Link to: [python.org](https://python.org/)  
Direct download of Python 3.11.8 64-bit: [Windows installer (64-bit)](https://www.python.org/ftp/python/3.11.2/python-3.11.2-amd64.exe)
<video width="720" height="400" controls>
    <source src="/video/Python.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 2. PyCharm installation
This video shows how to install PyCharm (2024.2.3).  
Link to: [jetbrains.com/pycharm](https://www.jetbrains.com/pycharm)  
Direct download of PyCharm 64-bit: [.exe (Windows)](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC)
<video width="720" height="400" controls>
    <source src="/video/PyCharm.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 3. Git installation
This video shows how to install Git (2.46.2).  
Link to: [git-scm.com](https://git-scm.com)  
Direct download of PyCharm 64-bit: [64-bit Git for Windows Setup](https://github.com/git-for-windows/git/releases/download/v2.46.2.windows.1/Git-2.46.2-64-bit.exe)
<video width="720" height="400" controls>
    <source src="/video/Git.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 4. FiQuS installation
This video shows how to install (git clone) FiQuS from CERN Gitlab repository.  
Link to: [cern.ch/fiqus](https://cern.ch/fiqus)  
Direct repository link: [gitlab.cern.ch/steam/fiqus](https://gitlab.cern.ch/steam/fiqus)  
Clone with HTTPS link: [https://gitlab.cern.ch/steam/fiqus.git](https://gitlab.cern.ch/steam/fiqus.git)
<video width="720" height="400" controls>
    <source src="/video/Cloning.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 5. CERNGetDP installation
This video shows how to download CERNGetDP from CERN Gitlab repository and how to set settings in FiQuS for the executable location.  
Link to: [cern.ch/cerngetdp](https://cern.ch/cerngetdp)  
Direct repository link: [gitlab.cern.ch/steam/cerngetdp](https://gitlab.cern.ch/steam/cerngetdp)  
Direct download link CERNGetDP(2024.8.2): [cerngetdp_2024.8.2.zip](https://cernbox.cern.ch/remote.php/dav/public-files/n6BIGib9k2OKA2B/Windows/cerngetdp_2024.8.2.zip)
<video width="720" height="400" controls>
    <source src="/video/CERNGetDP.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 6. Gmsh installation
This video shows how to download Gmsh and what to do afterwards.  
Link to: [gmsh.info](https://gmsh.info)  
Direct download link Gmsh(4.13.1): [gmsh-4.13.1-Windows64.zip](https://gmsh.info/bin/Windows/gmsh-4.13.1-Windows64.zip)
<video width="720" height="400" controls>
    <source src="/video/Gmsh.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## 7. FiQuS run
This video shows how to run a FiQuS. The code used in the video is below. Video also shows how to set up Gmsh to open geometry, mesh and result files.

```python
import os
from fiqus import MainFiQuS as mf

magnet_name = 'MQXA'

input_file = os.path.join(os.getcwd(), 'tests', '_inputs', magnet_name, f'{magnet_name}.yaml')
output_folder = os.path.join(os.getcwd(), 'tests', '_outputs', magnet_name)

mf.MainFiQuS(input_file_path=input_file, model_folder=output_folder)
```
<video width="720" height="400" controls>
    <source src="/video/FiQuS.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

