# Geometry Multipole

Implements geometry generation class and methods for the Multipole magnets.

::: fiqus.geom_generators.GeometryMultipole