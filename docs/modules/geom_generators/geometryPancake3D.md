# Geometry Pancake3D

Implements geometry generation class and methods for the Pancake3D magnets.

::: fiqus.geom_generators.GeometryPancake3D