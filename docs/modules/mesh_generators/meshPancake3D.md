# Mesh Pancake3D

Implements mesh generation class and methods for the Pancake3D magnets.

::: fiqus.mesh_generators.MeshPancake3D
