import os

class PlotPythonMultipole:
    def __init__(self, fdm, verbose=True):
        """
        Class for making python plots of multipole magnets
        :param fdm: FiQuS data model
        :param verbose: If True more information is printed in python console.
        """
        self.fdm = fdm
        self.verbose = verbose

    def dummy_plot_func(self):
        """
        not implemented yet
        :return:
        :rtype:
        """
        pass