# Post Processor

The post processor will take output files from GetDP and manipulate, e.g.,
by parsing them to different file types or carrying out additional operations
on the file (e.g., finding maximum value over time, time-integration, ...). 
