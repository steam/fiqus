# Pro File Assembler

The pro assembler creates the final `.pro` that is executed by GetDP. To this
end, it combines `pro_templates` and fills them with data according to the input
YAML file. 
