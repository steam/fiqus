{% import "materials.pro" as materials %}
{% import "TSA_materials.pro" as TSA_materials %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FUNCTIONSPACE_TSABasisFunctions(type) %}
// TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// TSA contributions following special indexing to restrict them to one side of the thin
// layer split in plus and minus side.
{% for i in range(1, NofSets+1) %}
    {% if (type == "electromagnetic") or (type == "thermal") %}
{
    Name BASISFUN_snMinus_<<i>>;
    NameOfCoef BASISFUN_snMinus_coeff_<<i>>;
    {% if type == "electromagnetic" %}
    Function BF_Edge;
    {% elif type == "thermal" %}
    Function BF_Node;
    {% endif %}
    Support Region[
                {   
                    {% if (dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" and type == "electromagnetic") %}
                    DOM_terminalContactLayerSurface_<<i>>,
                    DOM_terminalContactLayerSurface_<<i+1>>,
                    {% else %}
                    DOM_allInsulationSurface_<<i>>,
                    DOM_allInsulationSurface_<<i+1>>,
                    {% endif %}
                    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient and type == "thermal" %}
                    DOM_windingSurfaceMinus_<<i>>,
                    DOM_windingSurfaceMinus_<<i+1>>,
                    {% endif %}
                    DOM_windingMinus_<<i>>,
                    DOM_windingMinus_<<i+1>>
                }
            ];
    {% if type == "electromagnetic" %}
    Entity EdgesOf[
        {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
        DOM_terminalContactLayerSurface_<<i>>,
        {% else %}
        DOM_allInsulationSurface_<<i>>,
        {% endif %}
        Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal, {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %} DOM_terminalContactLayerSurface_<<i-1>>, DOM_insulationSurface {% else %} DOM_allInsulationSurface_<<i-1>> {% endif %}}
        ];
    {% elif type == "thermal" %}
    Entity NodesOf[
            DOM_allInsulationSurface_<<i>>,
            Not { DOM_allInsulationSurface_<<i-1>>, DOM_insulationBoundaryCurvesTerminal 
            }
                ];
    {% endif %}
}

{
    Name BASISFUN_snPlus_<<i>>;
    NameOfCoef BASISFUN_snPlus_coeff_<<i>>;
    {% if type == "electromagnetic" %}
    Function BF_Edge;
    {% elif type == "thermal" %}
    Function BF_Node;
    {% endif %}
    Support Region[
                {   
                    {% if (dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" and type == "electromagnetic") %}
                    DOM_terminalContactLayerSurface_<<i>>,
                    DOM_terminalContactLayerSurface_<<i+1>>,
                    {% else %}
                    DOM_allInsulationSurface_<<i>>,
                    DOM_allInsulationSurface_<<i+1>>,
                    {% endif %}
                    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient and type == "thermal" %}
                    DOM_windingSurfacePlus_<<i>>,
                    DOM_windingSurfacePlus_<<i+1>>,
                    {% endif %}
                    DOM_windingPlus_<<i>>,
                    DOM_windingPlus_<<i+1>>
                }
            ];
    {% if type == "electromagnetic" %}
    Entity EdgesOf[
        {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
        DOM_terminalContactLayerSurface_<<i>>,
        {% else %}
        DOM_allInsulationSurface_<<i>>,
        {% endif %}
        Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal, {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %} DOM_terminalContactLayerSurface_<<i-1>>, DOM_insulationSurface {% else %} DOM_allInsulationSurface_<<i-1>> {% endif %}}
        ];
    {% elif type == "thermal" %}
    Entity NodesOf[
            DOM_allInsulationSurface_<<i>>,
            Not { DOM_allInsulationSurface_<<i-1>>, DOM_insulationBoundaryCurvesTerminal            
        }
            ];
    {% endif %}
}
    {% elif type == "thermal_adiabatic_half_tsa_minus" %}
    
    {
        Name BASISFUN_snMinus_not_connected_<<i>>;
        NameOfCoef BASISFUN_snMinus_not_connected_coeff_<<i>>;
        Function BF_Node;
        Support Region[
                    {   
                        DOM_insulationSurface_<<i>>,
                        DOM_insulationSurface_<<i+1>>
                    }
                ];
        Entity NodesOf[
                    DOM_insulationSurface_<<i>>,
                    Not DOM_insulationSurface_<<i-1>>
                    ];
    }
    
    {% elif type == "thermal_adiabatic_half_tsa_plus" %}
    {
        Name BASISFUN_snPlus_not_connected_<<i>>;
        NameOfCoef BASISFUN_snPlus_not_connected_coeff_<<i>>;
        Function BF_Node;
        Support Region[
                    {   
                        DOM_insulationSurface_<<i>>,
                        DOM_insulationSurface_<<i+1>>
                    }
                ];
        Entity NodesOf[
            DOM_insulationSurface_<<i>>,
            Not DOM_insulationSurface_<<i-1>>
            ];
    }
    {% endif %}
{% endfor %}

// TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FUNCTIONSPACE_CryocoolerTSABasisFunctions() %}
// Cryocooler TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{
    Name BASISFUN_snOutside;
    NameOfCoef BASISFUN_snOutside_coeff;
    Function BF_Node;
    Support DOM_terminalSurfaces;
    Entity NodesOf[ DOM_terminalSurfaces ];
}

// Cryocooler TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FUNCTIONSPACE_TSASubSpaces(aditionalBasisFunctions=[], additional_bf_for_adiabatic_tsa=false) %}
// TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// identification of the positive and negative side of the contact layer:
{
    Name SUBSPACE_insulationSurface_down;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last and len(aditionalBasisFunctions)==0 %}
        BASISFUN_snMinus_<<i>>
    {% else %}
        BASISFUN_snMinus_<<i>>,
    {% endif %}
{% endfor %}
{% for bf in aditionalBasisFunctions %}
        {% if loop.last %}
        <<bf>>
        {% else %}
        <<bf>>,
        {% endif %}
{% endfor %}
    };
}
{
    Name SUBSPACE_insulationSurface_up;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last and len(aditionalBasisFunctions)==0 %}
        BASISFUN_snPlus_<<i>>
    {% else %}
        BASISFUN_snPlus_<<i>>,
    {% endif %}
{% endfor %}
{% for bf in aditionalBasisFunctions %}
        {% if loop.last %}
        <<bf>>
        {% else %}
        <<bf>>,
        {% endif %}
{% endfor %}
    };
}

{% if additional_bf_for_adiabatic_tsa %}
{
    Name SUBSPACE_adiabatic_minus_up;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last %}
    BASISFUN_snMinus_<<i>>
    {% else %}
    BASISFUN_snMinus_<<i>>,
    {% endif %}
{% endfor %}
    };
}
{
    Name SUBSPACE_adiabatic_minus_down;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last %}
    BASISFUN_snMinus_not_connected_<<i>>
    {% else %}
    BASISFUN_snMinus_not_connected_<<i>>,
    {% endif %}
{% endfor %}
    };
}

{
    Name SUBSPACE_adiabatic_plus_up;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last %}
    BASISFUN_snPlus_<<i>>
    {% else %}
    BASISFUN_snPlus_<<i>>,
    {% endif %}
{% endfor %}
    };
}
{
    Name SUBSPACE_adiabatic_plus_down;
    NameOfBasisFunction{
{% for i in range(1, NofSets+1) %}
    {% if loop.last %}
    BASISFUN_snPlus_not_connected_<<i>>
    {% else %}
    BASISFUN_snPlus_not_connected_<<i>>,
    {% endif %}
{% endfor %}
    };
}
{% endif %}


For i In {1:INPUT_NumOfTSAElements - 1}
    {
        Name SUBSPACE_tsa~{i}; 
        NameOfBasisFunction {
{% if len(aditionalBasisFunctions)==0 %}
            BASISFUN_sn~{i}
{% else %}
            BASISFUN_sn~{i},
{% endif %}
{% for bf in aditionalBasisFunctions %}
        {% if loop.last %}
            <<bf>>
        {% else %}
            <<bf>>,
        {% endif %}
{% endfor %}
        };
    }

{% if additional_bf_for_adiabatic_tsa %}
{
    Name SUBSPACE_adiabatic_plus_tsa~{i}; 
    NameOfBasisFunction {
        BASISFUN_snPlus_adiabatic~{i}
    };
}
{
    Name SUBSPACE_adiabatic_minus_tsa~{i}; 
    NameOfBasisFunction {
        BASISFUN_snMinus_adiabatic~{i}
    };
}
{% endif %}
EndFor

// TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FUNCTIONSPACE_CryocoolerTSASubSpaces() %}
// Cryocooler TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// identification of the positive and negative side of the cryocooler lumped mass:
{
    Name SUBSPACE_cryocooler_inside;
    NameOfBasisFunction{
        BASISFUN_sn
    };
}
{
    Name SUBSPACE_cryocooler_outside;
    NameOfBasisFunction{
        BASISFUN_snOutside
    };
}

For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
    {
        Name SUBSPACE_cryocooler~{i}; 
        NameOfBasisFunction {
            BASISFUN_cryocooler_sn~{i}
        };
    }
EndFor

// Cryocooler TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_VolumetricQuantities(type) %}
// Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% if type == "electromagnetic" %}
{
    Name LOCALQUANT_h;
    Type Local;
    NameOfSpace SPACE_hPhi;
}
{
    // Different test function is needed for non-symmetric tensors, otherwise, getdp
    // assumes the tensors are symmetric and the result is wrong.
    Name LOCALQUANT_h_Derivative;
    Type Local;
    NameOfSpace SPACE_hPhi;
}
{
    Name GLOBALQUANT_I;
    Type Global;
    NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
}
{
    Name GLOBALQUANT_V;
    Type Global;
    NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
}
{% elif type == "electricScalarPotential" %}
{
    Name LOCALQUANT_electricScalarPotential;
    Type Local;
    NameOfSpace SPACE_electricScalarPotential;
}
{
    Name LOCALQUANT_h;
    Type Local;
    NameOfSpace SPACE_hPhi;
}
{% if dm.magnet.solve.type == "weaklyCoupled" or dm.magnet.solve.type == "stronglyCoupled" %}
{
    Name LOCALQUANT_T;
    Type Local;
    NameOfSpace SPACE_temperature;
}
{% endif %}
{% elif type == "thermal" %}
{
    Name LOCALQUANT_T;
    Type Local;
    NameOfSpace SPACE_temperature;
}
{% else %}
<<0/0>>
// ERROR: wrong type for FORMULATION_VolumetricQuantities!
{% endif %}
// Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_EECircuitQuantities() %}
// EECircuit quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{   
    Name CIRCUITQUANT_ICircuit; 
    Type Global; 
    NameOfSpace SPACE_circuit[ICircuit]; 
}
{   
    Name CIRCUITQUANT_UCircuit; 
    Type Global; 
    NameOfSpace SPACE_circuit[UCircuit]; 
}		   

// EECircuit quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_TSAQuantities(type) %}
// TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% if type == "electromagnetic" %}
{
    Name LOCALQUANT_hThinShell~{0};
    Type Local;
    NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_down];
}

For i In {1:INPUT_NumOfTSAElements-1}
    {
        Name LOCALQUANT_hThinShell~{i};
        Type Local;
        NameOfSpace SPACE_hPhi[SUBSPACE_tsa~{i}];
    }
EndFor

{
    Name LOCALQUANT_hThinShell~{INPUT_NumOfTSAElements};
    Type Local;
    NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_up];
}
{% elif type == "electricScalarPotential" %}
{
    Name LOCALQUANT_electricScalarPotentialThinShell~{0};
    Type Local;
    NameOfSpace SPACE_electricScalarPotential[SUBSPACE_insulationSurface_up];
}

For i In{1 : INPUT_NumOfTSAElements - 1}
    {
        Name LOCALQUANT_electricScalarPotentialThinShell~{i};
        Type Local;
        NameOfSpace SPACE_electricScalarPotential[SUBSPACE_tsa~{i}];
    }
EndFor

{
    Name LOCALQUANT_electricScalarPotentialThinShell~{INPUT_NumOfTSAElements};
    Type Local;
    NameOfSpace SPACE_electricScalarPotential[SUBSPACE_insulationSurface_down];
}
{% elif type == "thermal" %}
{
    Name LOCALQUANT_TThinShell~{0};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_up];
}

For i In{1 : INPUT_NumOfTSAElements - 1}
    {
        Name LOCALQUANT_TThinShell~{i};
        Type Local;
        NameOfSpace SPACE_temperature[SUBSPACE_tsa~{i}];
    }
EndFor

{
    Name LOCALQUANT_TThinShell~{INPUT_NumOfTSAElements};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_down];
}
{% elif type == "thermal_adiabatic_half_tsa_minus" %}
{
    Name LOCALQUANT_TThinShell_minus~{0};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_up];
}

For i In{1 : INPUT_NumOfTSAElements - 1}
    {
        Name LOCALQUANT_TThinShell_minus~{i};
        Type Local;
        NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_tsa~{i}];
    }
EndFor

{
    Name LOCALQUANT_TThinShell_minus~{INPUT_NumOfTSAElements};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_down];
}
{% elif type == "thermal_adiabatic_half_tsa_plus" %}
{
    Name LOCALQUANT_TThinShell_plus~{0};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_up];
}

For i In{1 : INPUT_NumOfTSAElements - 1}
    {
        Name LOCALQUANT_TThinShell_plus~{i};
        Type Local;
        NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_tsa~{i}];
    }
EndFor

{
    Name LOCALQUANT_TThinShell_plus~{INPUT_NumOfTSAElements};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_down];
}
{% else %}
<<0/0>>
// ERROR: wrong type for FORMULATION_TSAQuantities!
{% endif %}
// TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_CryocoolerTSAQuantities() %}
// Cryocooler TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{
    Name LOCALQUANT_cryocooler_TThinShell~{0};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_inside];
}

For i In {1:INPUT_NumOfCryocoolerTSAElements-1}
    {
        Name LOCALQUANT_cryocooler_TThinShell~{i};
        Type Local;
        NameOfSpace SPACE_temperature[SUBSPACE_cryocooler~{i}];
    }
EndFor

{
    Name LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements};
    Type Local;
    NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_outside];
}

// Cryocooler TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_VolumetricIntegrals(type) %}
{% if type == "electromagnetic" %}
Integral{
    // note that it is only defined in DOM_allConducting, not all domain
    [ rho[<<rhoArguments>>] * Dof{d LOCALQUANT_h}, {d LOCALQUANT_h} ];
    In DOM_allConducting;
    Jacobian JAC_vol;
    Integration Int;
}

Integral{
    DtDof[mu[] * Dof{LOCALQUANT_h}, {LOCALQUANT_h}];
    In DOM_total;
    Jacobian JAC_vol;
    Integration Int;
}
    {% if dm.magnet.solve.winding.superConductor and not dm.magnet.solve.winding.resistivity and type == "electromagnetic" %}
Integral
{
    JacNL[d_of_rho_wrt_j_TIMES_j[<<rhoArguments>>] * Dof{d LOCALQUANT_h} , {d LOCALQUANT_h_Derivative} ];
    In DOM_allWindings; 
    Jacobian JAC_vol;
    Integration Int; 
}
    {% endif %}
// the global term allows to link current and voltage in the cuts
GlobalTerm{
    [ Dof{GLOBALQUANT_V}, {GLOBALQUANT_I} ];
    In DOM_airCuts;
}
{% elif type == "electricScalarPotential" %}

Integral { [electricScalarPotential_scalingFactor/rho[<<rhoArguments>>] * Dof{d LOCALQUANT_electricScalarPotential} , {d LOCALQUANT_electricScalarPotential} ];
In DOM_allConducting; Jacobian JAC_vol; Integration Int; }

{% elif type == "thermal" %}
Integral {
    [ kappa[<<kappaArguments>>] * Dof{d LOCALQUANT_T}, {d LOCALQUANT_T} ];
    In DOM_thermal;
    Jacobian JAC_vol;
    Integration Int;
}
Integral {
    DtDof[ Cv[<<CvArguments>>] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
    In DOM_thermal;
    Jacobian JAC_vol;
    Integration Int;
}
{% if dm.magnet.solve.imposedPowerDensity.power %}
Integral {
    [ -imposedPowerDensity[XYZ[], $Time], {LOCALQUANT_T} ];
    In DOM_allWindings;
    Jacobian JAC_vol;
    Integration Int;
}
{% endif %}

{% if dm.magnet.solve.terminals.cooling == "cryocooler" %}
{% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
{% set cryocooler_quantity = "LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}" %}
{% else %}
{% set cryocooler_quantity = "LOCALQUANT_T" %}
{% endif %}
Integral {
    [ cryocoolerCoolingPower[{<<cryocooler_quantity>>}], {<<cryocooler_quantity>>} ];
    In DOM_terminalSurfaces;
    Jacobian JAC_sur;
    Integration Int;
}

// This Jacobian entry is extremely important for the convergence of the nonlinear solver
Integral {
    JacNL [ cryocoolerCoolingPowerDerivativeT[{<<cryocooler_quantity>>}] * Dof{<<cryocooler_quantity>>}, {<<cryocooler_quantity>>} ];
    In DOM_terminalSurfaces;
    Jacobian JAC_sur;
    Integration Int;
}
{% endif %}

{% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient == "nitrogenBath" %}
Integral {
    [ CFUN_hN_Tdiff[{LOCALQUANT_T} - <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
Integral {
    [ -CFUN_hN_Tdiff[{LOCALQUANT_T} - <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>] * <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
Integral {
    JacNL [ CFUN_hN_dTdiff_Tdiff[{LOCALQUANT_T} - <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
Integral {
    JacNL [ -CFUN_hN_dTdiff_Tdiff[{LOCALQUANT_T} - <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>] * <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
    {% else %}
Integral {
    [ <<dm.magnet.solve.convectiveCooling.heatTransferCoefficient>> * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
Integral {
    [ - <<dm.magnet.solve.convectiveCooling.heatTransferCoefficient>> * <<dm.magnet.solve.convectiveCooling.exteriorBathTemperature>>, {LOCALQUANT_T} ];
    In DOM_allConvectiveSurface;
    Jacobian JAC_sur;
    Integration Int;
}
    {% endif %}
{% endif %}

{% elif type == "resistiveHeating" %}
Integral {
    [ -(rho[<<rhoArguments>>] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}, {LOCALQUANT_T} ];
    In DOM_resistiveHeating;
    Jacobian JAC_vol;
    Integration Int;
}
{% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_EECircuitIntegrals() %}
    // U = R * I for resistor as global terms
    GlobalTerm {  [Dof{CIRCUITQUANT_UCircuit}, {CIRCUITQUANT_ICircuit}] ; In DOM_circuitResistance; }
    GlobalTerm { [Resistance[] * Dof{CIRCUITQUANT_ICircuit}, {CIRCUITQUANT_ICircuit} ]; In DOM_circuitResistance; }

    // U = L * dI/dt for inductor as global terms
    GlobalTerm{ [ Dof{CIRCUITQUANT_UCircuit}, {CIRCUITQUANT_ICircuit} ];  In DOM_circuitInductance; }
    GlobalTerm{ DtDof[ Inductance[] * Dof{CIRCUITQUANT_ICircuit}, {CIRCUITQUANT_ICircuit} ];  In DOM_circuitInductance; }

    // Circuital equations related to circuit netlist
    GlobalEquation{
        Type Network; NameOfConstraint EECircuit_Netlist;
        { Node {GLOBALQUANT_I}; Loop {GLOBALQUANT_V}; Equation {GLOBALQUANT_V}; In DOM_terminalCut; }
        { Node {CIRCUITQUANT_ICircuit}; Loop {CIRCUITQUANT_UCircuit}; Equation {CIRCUITQUANT_UCircuit}; In DOM_circuit; }
    }
  
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_TSAIntegrals(type) %}
{% if type == "electromagnetic" %}
    {% set quantityName = "LOCALQUANT_hThinShell" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell" %}
    {% set functionKey = "electromagnetic" %}
    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
        {% set integrationDomain = "DOM_terminalContactLayerSurface" %}
    {% else %}
        {% set integrationDomain = "DOM_allInsulationSurface" %}
    {% endif %}
{% elif  type == "thermal" %}
    {% set quantityName = "LOCALQUANT_TThinShell" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell" %}
    {% set functionKey = "thermal" %}
    {% if dm.magnet.solve.heatFlowBetweenTurns %}
    {% set integrationDomain = "DOM_allInsulationSurface" %}
    {% else %}
    {% set integrationDomain = "DOM_terminalContactLayerSurface" %}
    {% endif %}
{% elif  type == "electricScalarPotential" %}
    {% set quantityName = "LOCALQUANT_electricScalarPotentialThinShell" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell" %}
    {% set functionKey = "electricScalarPotential" %}
    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    {% set integrationDomain = "DOM_terminalContactLayerSurface_WithoutNotch" %}
    {% else %}
    {% set integrationDomain = "DOM_allInsulationSurface_WithoutNotch" %}
    {% endif %}
{% elif type == "resistiveHeating" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell" %}
    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    {% set integrationDomain = "DOM_terminalContactLayerSurface" %}
    {% else %}
    {% set integrationDomain = "DOM_allInsulationSurface" %}
    {% endif %}
{% elif type == "thermal_adiabatic_half_tsa_minus" %}
    {% set quantityName = "LOCALQUANT_TThinShell_minus" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell_minus" %}
    {% set functionKey = "thermal" %}
    {% set integrationDomain = "DOM_insulationSurface" %}
{% elif type == "thermal_adiabatic_half_tsa_plus" %}
    {% set quantityName = "LOCALQUANT_TThinShell_plus" %}
    {% set temperatureArgument = "LOCALQUANT_TThinShell_plus" %}
    {% set functionKey = "thermal" %}
    {% set integrationDomain = "DOM_insulationSurface" %}
{% else %}
    <<0/0>>
    // ERROR: wrong type for FORMULATION_TSAIntegrals!
{% endif %}

{% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled", "thermal"] %}
    {% set temperatureArgument1 = "{" + temperatureArgument + "~{i}}" %}
    {% set temperatureArgument2 = "{" + temperatureArgument + "~{i+1}}" %}
{% elif dm.magnet.solve.type == "electromagnetic" %}
    {% set temperatureArgument1 = "INPUT_initialTemperature" %}
    {% set temperatureArgument2 = "INPUT_initialTemperature" %}
{% endif %}

{% if type == "electromagnetic" or type == "thermal" or type == "thermal_adiabatic_half_tsa_minus" or type == "thermal_adiabatic_half_tsa_plus" or type == "electricScalarPotential"%}
For i In {0:INPUT_NumOfTSAElements-1}
    {% for a in range(1,3) %}
        {% for b in range(1,3) %}
        Integral {
            [   {% if type == "electricScalarPotential" %} electricScalarPotential_scalingFactor * {% endif %}
                <<functionKey>>MassFunctionNoDta<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{d <<quantityName>>~{i + <<a>> - 1}},
                {d <<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }

        Integral {
            [   {% if type == "electricScalarPotential" %} electricScalarPotential_scalingFactor * {% endif %}
                <<functionKey>>StiffnessFunctiona<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{<<quantityName>>~{i + <<a>> - 1}},
                {<<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }

        {% if not type == "electricScalarPotential" %}
        Integral {
            DtDof[
                <<functionKey>>MassFunctionDta<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{<<quantityName>>~{i + <<a>> - 1}},
                {<<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }
        {% endif %}
        {% endfor %}
    {% endfor %}
EndFor
{% elif type == "resistiveHeating" %}
For i In {0:INPUT_NumOfTSAElements-1}
    {% for k in range(1,3) %} // row of the 1D FE matrix
    Integral {
        [
            - electromagneticRHSFunctionk<<k>>[
                <<temperatureArgument1>>,
                <<temperatureArgument2>>
            ] * SquNorm[
                ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
            ],
            {LOCALQUANT_TThinShell~{i + <<k>> - 1}}
        ];
        In <<integrationDomain>>;
        Integration Int;
        Jacobian JAC_sur;
    } 

        {% for a in range(1,3) %}
            {% for b in range(1,3) %}
    Integral {
        [
            -electromagneticTripleFunctionk<<k>>a<<a>>b<<b>>[
                <<temperatureArgument1>>,
                <<temperatureArgument2>>
            ] * {d LOCALQUANT_hThinShell~{i + <<a>> - 1}} * {d LOCALQUANT_hThinShell~{i + <<b>> - 1}},
            {LOCALQUANT_TThinShell~{i + <<k>> - 1}}
        ];
        In <<integrationDomain>>;
        Integration Int;
        Jacobian JAC_sur;
    }
            {% endfor %}
        {% endfor %}
    {% endfor %}
EndFor
{% else %}
<<0/0>>
// ERROR: Wrong type for FORMULATION_TSAIntegrals!
{% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro FORMULATION_CryocoolerTSAIntegrals(type) %}

    {% set quantityName = "LOCALQUANT_cryocooler_TThinShell" %}
    {% set temperatureArgument = "LOCALQUANT_cryocooler_TThinShell" %}
    {% set functionKey = "thermal" %}
    {% set integrationDomain = "DOM_terminalSurfaces" %}
    {% set temperatureArgument1 = "{" + temperatureArgument + "~{i}}" %}
    {% set temperatureArgument2 = "{" + temperatureArgument + "~{i+1}}" %}

For i In {0:INPUT_NumOfCryocoolerTSAElements-1}
    {% for a in range(1,3) %}
        {% for b in range(1,3) %}
        Integral {
            [   
                <<functionKey>>MassFunctionNoDta<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{d <<quantityName>>~{i + <<a>> - 1}},
                {d <<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }

        Integral {
            [   
                <<functionKey>>StiffnessFunctiona<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{<<quantityName>>~{i + <<a>> - 1}},
                {<<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }

        Integral {
            DtDof[
                <<functionKey>>MassFunctionDta<<a>>b<<b>>[
                    <<temperatureArgument1>>,
                    <<temperatureArgument2>>
                ] * Dof{<<quantityName>>~{i + <<a>> - 1}},
                {<<quantityName>>~{i + <<b>> - 1}}
            ];
            In <<integrationDomain>>;
            Integration Int;
            Jacobian JAC_sur;
        }
        {% endfor %}
    {% endfor %}
EndFor
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_tolerances(systemTolerances, postOperationTolerances, type) %}
{% set map_quantity_to_system_name = {
    "coupledSolutionVector": "SYSTEM_stronglyCoupled",
    "thermalSolutionVector": "SYSTEM_thermal",
    "electromagneticSolutionVector": "SYSTEM_electromagnetic",
  }
%}
{% for tolerance in systemTolerances %}
    {% if loop.first %}
System{
    {% endif %}
    {
        <<map_quantity_to_system_name[tolerance["quantity"]]>>,
        <<tolerance["relative"]>>,
        <<tolerance["absolute"]>>,
        {% if type == "nonlinearSolver" %}
        Solution <<tolerance["normType"]>>
        {% elif type == "timeLoop" %}
        <<tolerance["normType"]>>
        {% else %}
        <<0/0>>
        // ERROR: wrong type for RESOLUTION_tolerances!
        {% endif %}
    }
    {% if loop.last %}
}
    {% endif %}
{% endfor %}
{% for tolerance in postOperationTolerances %}
    {% if loop.first %}
PostOperation{
    {% endif %}
    {
        POSTOP_CONV_<<tolerance["quantity"]>>,
        <<tolerance["relative"]>>,
        <<tolerance["absolute"]>>,
        <<tolerance["normType"]>>
    }
    {% if loop.last %}
}
    {% endif %}
{% endfor %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_systemsOfEquationsSolver(systemNames, type, solveAfterThisTimes=[0, 0]) %}
{#
solveAfterThisTimes and systemNames are lists of equal lengths, and each entry
corresponds to the other. This is used because sometimes, in coupled problems, the
thermal system is started being solved right before exciting things (like local defects)
begin in order to speed up the program.
#}
{% if type == "nonlinear" %}
// Nonlinear solver starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
IterativeLoopN[
    INPUT_NLSMaxNumOfIter,
    INPUT_NLSRelaxFactor,
    <<RESOLUTION_tolerances(
        dm.magnet.solve.nonlinearSolver.systemTolerances,
        dm.magnet.solve.nonlinearSolver.postOperationTolerances,
        type="nonlinearSolver")|indent(4)>>
]{
    {% for systemName, solveAfterThisTime in zip(systemNames, solveAfterThisTimes) %}
        {% if solveAfterThisTime == 0 %}
    GenerateJac <<systemName>>;
    SolveJac <<systemName>>;

        {% else %}
    Test[$Time >= <<solveAfterThisTime>>]{
        GenerateJac <<systemName>>;
        SolveJac <<systemName>>;
    }
        {% endif %}
    {% endfor %}
}
// Check if the solution is NaN and remove it
Test[$KSPResidual != $KSPResidual]{
    Print["Critical: Removing NaN solution from the solution vector."];
    {% for systemName in systemNames %}
    RemoveLastSolution[<<systemName>>];
    {% endfor %}
}
// Nonlinear solver ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% elif type == "linear" %}
// Linear solver starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    {% for systemName in systemNames %}
Generate <<systemName>>;
Solve <<systemName>>;
    {% endfor %}
// Linear solver ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% else %}
<<0/0>>
// ERROR: wrong for RESOLUTION_systemsOfEquationsSolver!
{% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_saveSpecificTimes(postOperationName, timesToBeSaved) %}
// Saving quantities at specific times starts ++++++++++++++++++++++++++++++++++++++++++
{% for time in timesToBeSaved %}
    {% set timeMax = time+1e-6 %}
    {% set timeMin = time-1e-6 %}
Test[$Time < <<timeMax>> && $Time > <<timeMin>>]{
    PostOperation[<<postOperationName>>];
}
{% endfor %}
// Saving quantities at specific times ends ++++++++++++++++++++++++++++++++++++++++++++
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_InitializeSolutions(systemNames) %}
{% for systemName in systemNames %}
InitSolution[<<systemName>>];
SaveSolution[<<systemName>>];
{% endfor %}
SetExtrapolationOrder[INPUT_extrapolationOrder];
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_SaveSolutions(systemNames, solveAfterThisTimes) %}
{% if dm.magnet.postproc.timeSeriesPlots %}
    {% for quantity in dm.magnet.postproc.timeSeriesPlots %}
        {% if (quantity.quantity == "maximumTemperature") and (not dm.magnet.solve.stopWhenTemperatureReaches) %}
    PostOperation[POSTOP_maximumTemperature];
        {% endif %}
    {% endfor %}
{% endif %}
{% if dm.magnet.solve.stopWhenTemperatureReaches %}
PostOperation[POSTOP_maximumTemperature];
Test[#999 > <<dm.magnet.solve.stopWhenTemperatureReaches>>] {
    Print["Critical: stop simulation since maximum temperature surpassed threshold"];
    Break[];
}
{% endif %}
{% if dm.magnet.solve.quantitiesToBeSaved %}
    {% for quantity in dm.magnet.solve.quantitiesToBeSaved %}
        {% if quantity.timesToBeSaved %}
<<RESOLUTION_saveSpecificTimes(
    postOperationName = quantity.getdpPostOperationName,
    timesToBeSaved=quantity.timesToBeSaved)>>
        {% endif %}
    {% endfor %}
{% endif %}
{% for systemName, solveAfterThisTime in zip(systemNames, solveAfterThisTimes) %}
    {% if solveAfterThisTime == 0 %}
SaveSolution[<<systemName>>];
    {% else %}
        {% if 'solutionVector' in dm.magnet.solve.time.adaptiveSteppingSettings.tolerances|map(attribute="quantity") %}
SaveSolution[<<systemName>>];
        {% else %}
Test[$Time < <<solveAfterThisTime>>]{
    CopySolution[<<systemName>>, 'DummySolution'];
    CreateSolution[<<systemName>>];
    CopySolution['DummySolution', <<systemName>>];
    SaveSolution[<<systemName>>];
}
        {% endif %}
    {% endif %}
{% endfor %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_electricScalarPotentialSolver() %}
    Generate SYSTEM_electricScalarPotential;
    Solve SYSTEM_electricScalarPotential;
    SaveSolution SYSTEM_electricScalarPotential;

    {% if dm.quench_detection.voltage_tap_pairs and dm.magnet.solve.voltageTapPositions %}
        PostOperation[POSTOP_electricScalarPotential_<<dm.magnet.solve.type>>];
        PostOperation[POSTOP_electricScalarPotential];

        Test[$quenchDetected == 0] {
        {% for idx, _ in enumerate(dm.quench_detection.voltage_tap_pairs) %}

                Test[$surpassedVoltageThreshold_<<idx>> == 0 && Abs[$voltage_<<idx>>] > <<dm.quench_detection.voltage_thresholds[idx]>>]{
                    Evaluate[$surpassedVoltageThreshold_<<idx>> = 1];
                    Evaluate[$surpassedVoltageThresholdTime_<<idx>> = $Time];
                    Print[{$surpassedVoltageThresholdTime_<<idx>>}, Format "Critical: surpassed dection voltage in voltage tap pair no. <<idx>> at t = %.12g s"];
                }
    
                Test[Abs[$voltage_<<idx>>] <= <<dm.quench_detection.voltage_thresholds[idx]>>]{
                    Evaluate[$surpassedVoltageThreshold_<<idx>> = 0];
                }
    
                Test[$surpassedVoltageThreshold_<<idx>> == 1 && $Time - $surpassedVoltageThresholdTime_<<idx>> > <<dm.quench_detection.discrimination_times[idx]>>]{
                    Evaluate[$quenchDetected = 1];
                    Evaluate[$quenchDetectionTime = $Time];

                    Print[{$quenchDetectionTime}, Format "Critical: detected quench with voltage tap pair no. <<idx>> at t = %.12g s"];
                }
        {% endfor %}
        }

        Test[$quenchDetected == 1] {
            PostOperation[POSTOP_I];
            Test[$currentThresholdReached == 0 && $I < <<dm.magnet.solve.EECircuit.stopSimulationAtCurrent>>] {
                Evaluate[$currentThresholdReached = 1];
                Evaluate[$currentThresholdReachedTime = $Time];
                Print[{$currentThresholdReachedTime}, Format "Critical: below stop simulation current at t = %.12g s"];
            }

            Test[$currentThresholdReached == 1 && $Time - $currentThresholdReachedTime > <<dm.magnet.solve.EECircuit.stopSimulationWaitingTime>>] {
                Print["Critical: stop simulation due to low current"];
                Break[];
            }
        }
    {% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_PostOperations() %}
{% if dm.magnet.solve.quantitiesToBeSaved is not none %}
    {% for quantity in dm.magnet.solve.quantitiesToBeSaved %}
        {% if not quantity.timesToBeSaved %}
PostOperation[<<quantity.getdpPostOperationName>>];
        {% endif %}
    {% endfor %}
{% endif %}
{% if dm.magnet.postproc.timeSeriesPlots is not none %}
    {% for timeSeriesPlot in dm.magnet.postproc.timeSeriesPlots %}
PostOperation[POSTOP_timeSeriesPlot_<<timeSeriesPlot.quantity>>];
    {% endfor %}
{% endif %}
{% if dm.magnet.postproc.magneticFieldOnCutPlane is not none %}
PostOperation[POSTOP_magneticFieldOnCutPlaneVector];
PostOperation[POSTOP_magneticFieldOnCutPlaneMagnitude];
{% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_SolveTransientProblemFixedStepping(systemNames, solveAfterThisTimes=[0,0]) %}
<<RESOLUTION_InitializeSolutions(systemNames)>>
{% set intervals = dm.magnet.solve.time.fixedSteppingSettings %}
{% for interval in intervals %}
TimeLoopTheta[<<interval.startTime>>, <<interval.endTime>>, <<interval.step>>, 1]{
    <<RESOLUTION_systemsOfEquationsSolver(
        systemNames = systemNames,
        type=dm.magnet.solve.systemsOfEquationsType,
        solveAfterThisTimes = solveAfterThisTimes)|indent(4)>>
    {% if dm.magnet.solve.voltageTapPositions %}
    <<RESOLUTION_electricScalarPotentialSolver()|indent(4)>>
    {% endif %}
    <<RESOLUTION_SaveSolutions(systemNames, solveAfterThisTimes)|indent(4)>>
}
{% endfor %}
<<RESOLUTION_PostOperations()>>
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_SolveTransientProblemAdaptiveStepping(systemNames, solveAfterThisTimes=[0,0]) %}
<<RESOLUTION_InitializeSolutions(systemNames)>>

TimeLoopAdaptive[
    INPUT_tStart,
    INPUT_tEnd,
    INPUT_tAdaptiveInitStep,
    INPUT_tAdaptiveMinStep,
    INPUT_tAdaptiveMaxStep,
    "<<dm.magnet.solve.time.adaptiveSteppingSettings.integrationMethod>>",
    List[INPUT_tAdaptiveBreakPoints],
    <<RESOLUTION_tolerances(
        dm.magnet.solve.time.adaptiveSteppingSettings.systemTolerances,
        dm.magnet.solve.time.adaptiveSteppingSettings.postOperationTolerances,
        type="timeLoop")|indent(4)>>
]{
    <<RESOLUTION_systemsOfEquationsSolver(
        systemNames = systemNames,
        type=dm.magnet.solve.systemsOfEquationsType,
        solveAfterThisTimes = solveAfterThisTimes)|indent(4)>>
}{
    {% if dm.magnet.solve.voltageTapPositions %}
    <<RESOLUTION_electricScalarPotentialSolver()|indent(4)>>
    {% endif %}
    <<RESOLUTION_SaveSolutions(systemNames, solveAfterThisTimes)|indent(4)>>
}

<<RESOLUTION_PostOperations()>>
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro RESOLUTION_SolveTransientProblem(systemNames, solveAfterThisTimes=[0,0]) %}
{% if dm.magnet.solve.time.timeSteppingType == "adaptive" %}
<<RESOLUTION_SolveTransientProblemAdaptiveStepping(systemNames, solveAfterThisTimes)>>
{% elif dm.magnet.solve.time.timeSteppingType == "fixed" %}
<<RESOLUTION_SolveTransientProblemFixedStepping(systemNames, solveAfterThisTimes)>>
{% endif %}
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{% macro POSTOPERATION_printResults(
    quantity,
    onElementsOf="None",
    onPoint="None",
    onRegion="None",
    onGlobal="None",
    onSection="None",
    depth="None",
    format="Default",
    name="None",
    fileName="None",
    atGaussPoints=False,
    lastTimeStepOnly=False,
    appendToExistingFile=False,
    storeInVariable=False,
    noTitle=False) %}
    {% if dm.magnet.mesh.winding.elementType[0] == "prism" %}
        {% set noOfGaussPoints = 9 %}
    {% elif dm.magnet.mesh.winding.elementType[0] == "hexahedron" %}
        {% set noOfGaussPoints = 6 %}
    {% elif dm.magnet.mesh.winding.elementType[0] == "tetrahedron" %}
        {% set noOfGaussPoints = 4 %}
    {% endif %}

Print[
    <<quantity>>,
{% if onElementsOf != "None" %}
    OnElementsOf <<onElementsOf>>,
{% elif onPoint != "None" %}
    OnPoint {<<onPoint[0]>>, <<onPoint[1]>>, <<onPoint[2]>>},
{% elif onRegion != "None" %}
    OnRegion <<onRegion>>,
{% elif onGlobal != "None" %}
    OnGlobal,
{% elif onSection != "None" %}
    OnSection{
        {<<onSection[0][0]>>, <<onSection[0][1]>>, <<onSection[0][2]>>}
        {<<onSection[1][0]>>, <<onSection[1][1]>>, <<onSection[1][2]>>}
        {<<onSection[2][0]>>, <<onSection[2][1]>>, <<onSection[2][2]>>}
    },
{% else %}
    {# <<0/0>> #}
    // ERROR: No print region specified!
{% endif %}
{% if depth != "None" %}
    Depth <<depth>>,
{% endif %}
{% if atGaussPoints and noOfGaussPoints %}
    AtGaussPoints <<noOfGaussPoints>>,
{% endif %}
{% if storeInVariable %}
    StoreInVariable $<<storeInVariable>>,
{% endif %}
{% if fileName != "None" %}
    {% if format == "TimeTable" %}
    File "<<fileName>>-<<format>>Format.csv",
    {% elif format != "Default" %}
    File "<<fileName>>-<<format>>Format.txt",
    {% else %}
    File "<<fileName>>-<<format>>Format.pos",
    {% endif %}
{% else %}
    {% if format == "TimeTable" %}
    File "<<quantity>>-<<format>>Format.csv",
    {% elif format != "Default" %}
    File "<<quantity>>-<<format>>Format.txt",
    {% else %}
    File "<<quantity>>-<<format>>Format.pos",
    {% endif %}
{% endif %}
{% if format == "TimeTable" %}
    Format <<format>>,
    Comma,
{% elif format != "Default" %}
    Format <<format>>,
{% endif %}
{% if dm.magnet.solve.quantitiesToBeSaved is not none %}
    {% for quantityDict in dm.magnet.solve.quantitiesToBeSaved %}
        {% if quantityDict.getdpQuantityName == quantity %}
            {% if quantityDict.timesToBeSaved %}
    AppendToExistingFile 1,
    LastTimeStepOnly 1,
            {% endif %}
        {% endif %}
    {% endfor %}
{% endif %}
{% if lastTimeStepOnly == True %}
    LastTimeStepOnly 1,
{% endif %}
{% if appendToExistingFile == True %}
    AppendToExistingFile 1,
{% endif %}
{% if noTitle == True %}
    NoTitle,
{% endif %}
{% if name != "None" %}
    Name "<<name>>"
{% elif fileName != "None" %}
    Name "<<fileName>>"
{% else %}
    Name "<<quantity>>"
{% endif %}
];
{% endmacro %}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
{# ================================================================================== #}
//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    DOM_innerTerminalAndTransitionNotch_surface = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[0]>> }];
    DOM_outerTerminalAndTransitionNotch_surface = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[1]>> }];
    {% endif %}

    {% set NofSets = (len(rm.insulator.surf.numbers)/3)|int %}
    {% set HalfNofSets = (NofSets/2)|int %}
    DOM_allInsulationSurface_0 = Region[{ <<rm.insulator.surf.numbers[-3]>>, <<rm.insulator.surf.numbers[-2]>>, <<rm.insulator.surf.numbers[-1]>> }];

    DOM_terminalContactLayerSurface_WithoutNotch_0 = Region[{ <<rm.insulator.surf.numbers[-2]>> }];
    DOM_terminalContactLayerSurface_Notch_0 = Region[{ <<rm.insulator.surf.numbers[-1]>> }];

    DOM_terminalContactLayerSurface_0 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_0, DOM_terminalContactLayerSurface_Notch_0 }];
    {% if not dm.magnet.solve.heatFlowBetweenTurns %}
    DOM_insulationSurface_0 = Region[{ <<rm.insulator.surf.numbers[-3]>> }];
    {% endif %}

    {% for i in range(1, NofSets+2) %}
    DOM_windingMinus_<<i>> = Region[{ <<rm.powered["Pancake3D"].vol.numbers[:-2][(i-1)%NofSets]>> }];
    DOM_windingPlus_<<i>> = Region[{ <<rm.powered["Pancake3D"].vol.numbers[:-2][(i-1+HalfNofSets)%NofSets]>> }];
    {% if dm.magnet.solve.voltageTapPositions %}
    DOM_windingMinus_electricScalarPotential_<<i>> = Region[{ <<rm.powered["Pancake3D"].vol.numbers[:-2][(i-1)%NofSets]>> }];
    DOM_windingPlus_electricScalarPotential_<<i>> = Region[{ <<rm.powered["Pancake3D"].vol.numbers[:-2][(i-1+HalfNofSets)%NofSets]>> }];
    {% endif %}

    DOM_allInsulationSurface_<<i>> = Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets)]>> }];
    DOM_allInsulationSurface_<<i>> += Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets) + 1]>> }];
    DOM_allInsulationSurface_<<i>> += Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets) + 2]>> }];

    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" or not dm.magnet.solve.heatFlowBetweenTurns %}
    DOM_terminalContactLayerSurface_WithoutNotch_<<i>> = Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets) + 1]>> }];
    DOM_terminalContactLayerSurface_Notch_<<i>> = Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets) + 2]>> }];
    DOM_terminalContactLayerSurface_<<i>> = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_<<i>>, DOM_terminalContactLayerSurface_Notch_<<i>> }];
    {% endif %}
    {% if not dm.magnet.solve.heatFlowBetweenTurns %}
    DOM_insulationSurface_<<i>> = Region[{ <<rm.insulator.surf.numbers[(3 * (i-1))%(3 * NofSets)]>> }];
    {% endif %}
    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    DOM_windingSurfaceMinus_<<i>> = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[2::][(i-1)%NofSets]>> }];
    DOM_windingSurfacePlus_<<i>> = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[2::][(i-1+HalfNofSets)%NofSets]>> }];
    {% endif %}

    {% endfor %}


    // Add terminals to winding region logic:
    // <<rm.powered["Pancake3D"].vol_in.number>>: inner terminal
    // <<rm.powered["Pancake3D"].vol_out.number>>: outer terminal
    // <<rm.powered["Pancake3D"].vol.numbers[-2]>>: inner layer transition angle
    // <<rm.powered["Pancake3D"].vol.numbers[-1]>>: outer layer transition angle
    {% for i in range(1, NofSets+2) %}
    DOM_windingMinus_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol_in.number>> }];
    {% if dm.magnet.solve.voltageTapPositions %}
    DOM_windingMinus_electricScalarPotential_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol_in.number>> }];
    {% endif %}
    DOM_windingMinus_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol.numbers[-2]>> }];
    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    DOM_windingSurfaceMinus_<<i>> += Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[0]>> }];
    {% endif %}
    {% endfor %}

    {% for i in range(1, NofSets+1) %}
    DOM_windingPlus_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol_out.number>> }];
    {% if dm.magnet.solve.voltageTapPositions %}
    DOM_windingPlus_electricScalarPotential_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol_out.number>> }];
    {% endif %}
    DOM_windingPlus_<<i>> += Region[{ <<rm.powered["Pancake3D"].vol.numbers[-1]>> }];
    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    DOM_windingSurfacePlus_<<i>> += Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[1]>> }];
    {% endif %}
    {% endfor %}

    DOM_allInsulationSurface = Region[{ <<rm.insulator.surf.numbers|join(', ')>> }];
    DOM_insulationSurface = Region[{ <<rm.insulator.surf.numbers[::3]|join(', ')>> }];
    DOM_terminalContactLayerSurface_WithoutNotch = Region[{ <<rm.insulator.surf.numbers[1::3]|join(', ')>> }];
    DOM_terminalContactLayerSurface_Notch = Region[{ <<rm.insulator.surf.numbers[2::3]|join(', ')>> }];
    DOM_terminalContactLayerSurface = Region[{ DOM_terminalContactLayerSurface_WithoutNotch, DOM_terminalContactLayerSurface_Notch }];
    DOM_allInsulationSurface_WithoutNotch = Region[ {DOM_insulationSurface, DOM_terminalContactLayerSurface_WithoutNotch} ];

    {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
    DOM_allWindingSurface = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers[2::]|join(', ')>> }];
    DOM_allConvectiveSurface = Region[{ <<rm.powered["Pancake3D"].surf_th.numbers|join(', ')>> }];
    {% endif %}

    DOM_insulationBoundaryCurvesAir = Region[{ <<rm.insulator.curve.numbers[0]>> }];
    DOM_insulationBoundaryCurvesTerminal = Region[{ <<rm.insulator.curve.numbers[1]>> }];
{% else %}
    DOM_insulation = Region[ <<rm.insulator.vol.numbers[0]>> ];
    DOM_terminalContactLayer = Region[ <<rm.insulator.vol.numbers[1]>> ];
    DOM_allInsulations = Region[{ <<rm.insulator.vol.numbers|join(', ')>> }];
{% endif %}

    // create windings region:
    DOM_allWindings = Region[{ <<rm.powered["Pancake3D"].vol.numbers[:-2]|join(', ')>> }];

    // create terminals region:
    DOM_terminals = Region[{ <<rm.powered["Pancake3D"].vol_in.number>>, <<rm.powered["Pancake3D"].vol_out.number>>}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{<<rm.powered["Pancake3D"].vol.numbers[-2]>>, <<rm.powered["Pancake3D"].vol.numbers[-1]>>}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    {% if  dm.magnet.geometry.contactLayer.thinShellApproximation %}
    DOM_allConducting = Region[{ DOM_powered}];
        {% if dm.magnet.solve.resistiveHeatingTerminals %}
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
        {% else %}
    DOM_resistiveHeating = Region[{ DOM_allWindings }];
        {% endif %}
    DOM_thermal = Region[{ {% if dm.magnet.solve.solveHeatEquationTerminalsTransitionNotch %}DOM_powered{% else %}DOM_allWindings{% endif %}}];
    {% else %}
    DOM_thermal = Region[{ {% if dm.magnet.solve.solveHeatEquationTerminalsTransitionNotch %}DOM_powered, DOM_allInsulations{% else %}DOM_allWindings, DOM_allInsulations{% endif %}}];
    DOM_allConducting = Region[{ DOM_powered, DOM_terminalContactLayer}];
        {% if dm.magnet.solve.resistiveHeatingTerminals %}
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals, DOM_terminalContactLayer }];
        {% else %}
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminalContactLayer }];
        {% endif %}
    {% endif %}
{% else %}
    {% if  dm.magnet.geometry.contactLayer.thinShellApproximation %}
    DOM_thermal = Region[{ {% if dm.magnet.solve.solveHeatEquationTerminalsTransitionNotch %}DOM_powered{% else %}DOM_allWindings{% endif %}}];
    DOM_allConducting = Region[{ DOM_powered }];
        {% if dm.magnet.solve.resistiveHeatingTerminals %}
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
        {% else %}
    DOM_resistiveHeating = Region[{ DOM_allWindings }];
        {% endif %}
    {% else %}
    DOM_thermal = Region[{ {% if dm.magnet.solve.solveHeatEquationTerminalsTransitionNotch %}DOM_powered, DOM_allInsulations{% else %}DOM_allWindings, DOM_allInsulations{% endif %}}];
    DOM_allConducting = Region[{ DOM_powered, DOM_allInsulations }];
        {% if dm.magnet.solve.resistiveHeatingTerminals %}
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
        {% else %}
    DOM_resistiveHeating = Region[{ DOM_allWindings }];
        {% endif %}
    DOM_resistiveHeating += Region[{ DOM_allInsulations }];
    {% endif %}
{% endif %}
    DOM_air = Region[{ <<rm.air.vol.number>> }];
{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    DOM_airPoints = Region[{ <<rm.air.point.numbers[0]>> }];
{% else %}
    DOM_airPoints = Region[{ <<rm.air.point.numbers|join(', ')>> }];
{% endif %}

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];

{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" and
    not dm.magnet.geometry.contactLayer.thinShellApproximation %}
    DOM_air += Region[ DOM_insulation ];
{% endif %}

{% if dm.magnet.geometry.air.shellTransformation %}
    {% if dm.magnet.geometry.air.type == "cylinder" %}
    // add shell volume for shell transformation:
    DOM_air += Region[{ <<rm.air_far_field.vol.numbers|join(", ")>> }];
    DOM_airInf = Region[{ <<rm.air_far_field.vol.numbers|join(", ")>> }];
    {% elif dm.magnet.geometry.air.type == "cuboid" %}
    // add shell volume for shell transformation:
    DOM_air += Region[{ <<rm.air_far_field.vol.numbers|join(", ")>> }];
    DOM_airInfX = Region[{ <<rm.air_far_field.vol.numbers[0]>> }];
    DOM_airInfY = Region[{ <<rm.air_far_field.vol.numbers[1]>> }];
    {% endif %}
{% endif %}


    // boundary surface between the all conducting and non-conducting domains:
{% if dm.magnet.geometry.contactLayer.thinShellApproximation or dm.magnet.solve.contactLayer.resistivity != "perfectlyInsulating" %}
    DOM_pancakeBoundary = Region[{ <<rm.powered["Pancake3D"].surf.numbers[0]>> }];
{% else %}
    DOM_pancakeBoundary = Region[{ <<rm.powered["Pancake3D"].surf.numbers[1]>> }];
{% endif %}

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    DOM_terminalCut = Region[{ <<rm.air.cochain.numbers[1]>> }];
    DOM_airHoleCut = Region[{ <<rm.air.cochain.numbers[2:]|join(', ')>> }];
    DOM_airCuts = Region[{ DOM_terminalCut, DOM_airHoleCut }];
{% else %}
    DOM_terminalCut = Region[{ <<rm.air.cochain.numbers[0]>> }];
    DOM_airCuts = Region[{ DOM_terminalCut }];
{% endif %}

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ << rm.powered['Pancake3D'].surf_in.numbers | join(', ') >> }];
    DOM_topTerminalSurface = Region[{ << rm.powered['Pancake3D'].surf_out.numbers | join(', ') >> }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    {% if dm.quench_detection.voltage_tap_pairs and dm.magnet.solve.voltageTapPositions %}
    DOM_dummyPrintResistiveVoltages = Region[ 9999999 ] ;
    {% endif %}
    
    {% if dm.magnet.solve.EECircuit.enable %}
    DOM_currentSource = Region[ 9000000 ];
    DOM_resistanceCrowbar = Region[ 9000001 ];
    DOM_switchCrowbar = Region[ 9000002 ];
    DOM_resistanceEE = Region[ 9000003 ];
    DOM_switchEE = Region[ 9000004 ];
    DOM_inductanceEE = Region[ 9000005 ];

    DOM_circuitResistance = Region[{ DOM_resistanceCrowbar, DOM_resistanceEE, DOM_switchCrowbar, DOM_switchEE }];
    DOM_circuitInductance = Region[{ DOM_inductanceEE }];

    DOM_circuit = Region[{ DOM_currentSource, DOM_circuitResistance, DOM_circuitInductance }];
    {% endif %}
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {<<dm.power_supply.t_control_LUT|join(", ")>>};
    listOfCurrentValues = {<<dm.power_supply.I_control_LUT|join(", ")>>};
{% if dm.magnet.solve.EECircuit.enable %}
    {% if dm.magnet.solve.EECircuit.TurnOffDeltaTimePowerSupply == 0 %}
    current[] = $quenchDetected == 1? 0: InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};
    {% else %}
    current[] = $quenchDetected == 1? (
        $Time - $quenchDetectionTime > <<dm.magnet.solve.EECircuit.TurnOffDeltaTimePowerSupply>> ? 0 :
        InterpolationLinear[$quenchDetectionTime]{ListAlt[listOfTimeValues, listOfCurrentValues]} * (1 - ($Time - $quenchDetectionTime)/<<dm.magnet.solve.EECircuit.TurnOffDeltaTimePowerSupply>>) ) : 
        InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};
    {% endif %}
{% else %}
    current[] = InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};
{% endif %}

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    {% set windingThickness = dm.magnet.geometry.winding.thickness + dm.magnet.geometry.contactLayer.thickness * (dm.magnet.geometry.winding.numberOfTurns - 1) / dm.magnet.geometry.winding.numberOfTurns %}
    {# A scaling factor needs to be used for TSA because winding is slightly thicker in #}
    {# TSA compared to volume insulation. #}
    {% set scalingFactor = dm.magnet.geometry.winding.thickness / windingThickness %}
    {% set gapThickness = 0 %}
{% else %}
    {% set windingThickness = dm.magnet.geometry.winding.thickness %}
    {% set scalingFactor = 1 %}
    {% set gapThickness = dm.magnet.geometry.contactLayer.thickness %}
{% endif %}


    {% if dm.magnet.solve.imposedPowerDensity.power and not dm.magnet.solve.type == "electromagnetic" %}
    imposedPowerDensity[] = Pancake3DPowerDensity[$1, $2]
        {<<dm.magnet.solve.imposedPowerDensity.startTime>>,
        <<dm.magnet.solve.imposedPowerDensity.endTime>>, 
        <<dm.magnet.solve.imposedPowerDensity.startArcLength>>,
        <<dm.magnet.solve.imposedPowerDensity.endArcLength>>,
        <<dm.magnet.solve.imposedPowerDensity.power>>,
        <<dm.magnet.geometry.winding.innerRadius>>,
        <<windingThickness>>,
        <<gapThickness>>,
        <<dm.magnet.geometry.winding.theta_i>>,
        <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
        <<dm.magnet.geometry.numberOfPancakes>>,
        <<dm.magnet.geometry.winding.height>>,
        <<dm.magnet.geometry.gapBetweenPancakes>>
        };
    {% endif %}

    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = <<dm.magnet.solve.initialConditions.temperature>>; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = <<dm.magnet.solve.time.start>>; // start time, [s]
    INPUT_tEnd = <<dm.magnet.solve.time.end>>; // end time, [s]
    INPUT_extrapolationOrder = <<dm.magnet.solve.time.extrapolationOrder>>; // order of extrapolation for the time stepping scheme
{% if dm.magnet.solve.time.timeSteppingType == "adaptive" %}
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = <<dm.magnet.solve.time.adaptiveSteppingSettings.minimumStep>>; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = <<dm.magnet.solve.time.adaptiveSteppingSettings.maximumStep>>; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = <<dm.magnet.solve.time.adaptiveSteppingSettings.initialStep>>; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ << list(set(dm.magnet.solve.time.adaptiveSteppingSettings.breakPoints+dm.power_supply.t_control_LUT))|sort|join(', ') >> }; // force solution at these time points, [s]
{% endif %}

{% if dm.magnet.solve.systemsOfEquationsType == "nonlinear" %}
    // Nonlinear solver parameters:
    INPUT_NLSMaxNumOfIter = <<dm.magnet.solve.nonlinearSolver.maximumNumberOfIterations>>; // maximum number of iterations for the nonlinear solver
    INPUT_NLSRelaxFactor = <<dm.magnet.solve.nonlinearSolver.relaxationFactor>>; // relaxation factor for the nonlinear solver
{% endif %}

    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================
{% if dm.magnet.solve.type in ["electromagnetic", "weaklyCoupled", "stronglyCoupled"] %}
    // Air permeability starts =========================================================
    // Linear:
    INPUT_airMagneticPermeability = <<dm.magnet.solve.air.permeability>>;
    mu[] = INPUT_airMagneticPermeability;
    // Air permeability ends ===========================================================
{% endif %}

{%
if (
        (
            not (
                dm.magnet.solve.winding.resistivity and
                dm.magnet.solve.winding.thermalConductivity and
                dm.magnet.solve.winding.specificHeatCapacity
            ) and dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled"]
        )
        or (not(dm.magnet.solve.winding.resistivity) and dm.magnet.solve.type in ["electromagnetic"])
        or (not(
            dm.magnet.solve.winding.thermalConductivity and
            dm.magnet.solve.winding.specificHeatCapacity
        ) and dm.magnet.solve.type in ["thermal"])
    )
%}
    // Winding material combination parameters start ===================================
    {% for material in dm.magnet.solve.winding.normalConductors %}
    INPUT_relativeThickness<<material.getdpNormalMaterialGetDPName>> = <<material.relativeThickness>>;
    {% endfor %}

    INPUT_relativeThicknessOfSuperConductor = <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>;
    INPUT_relativeThicknessOfNormalConductor = <<dm.magnet.solve.winding.relativeThicknessOfNormalConductor>>;
    INPUT_relativeWidthOfPlating = <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>;
    // Factor 1.0/<<scalingFactor>> is used equate the scaling applied to Jcritical in the parallel direction 
    INPUT_jCriticalScalingNormalToWinding = 1.0/<<scalingFactor>> * <<dm.magnet.solve.winding.superConductor.jCriticalScalingNormalToWinding>>;
    // Winding material combination parameters end =====================================
{% endif %}

{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // TSA parameters start ============================================================
    INPUT_insulationThickness = <<dm.magnet.geometry.contactLayer.thickness>>; // thickness of the insulation, [m]
    INPUT_NumOfTSAElements = <<dm.magnet.solve.contactLayer.numberOfThinShellElements>>;
    th_terminal_k = INPUT_insulationThickness / (INPUT_NumOfTSAElements == 0 ? 1 : INPUT_NumOfTSAElements);
    {% if dm.magnet.solve.heatFlowBetweenTurns %}
    th_insulation_k = th_terminal_k;
    {% else %}
    th_insulation_k = 0.5 * th_terminal_k;
    {% endif %}
    // TSA parameters end ==============================================================
    
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
    // Cryocooler TSA parameters start ==================================================
    INPUT_NumOfCryocoolerTSAElements = <<dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.numberOfThinShellElements>>;
    cryocooler_thickness[] = <<dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume>>/ GetVariable[]{$areaCryocooler} ;
    th_cryocooler_k[] = cryocooler_thickness[]/INPUT_NumOfCryocoolerTSAElements;
    // Cryocooler TSA parameters end ====================================================
    {% endif %}
{% endif %}

{% set materialCodes = {
    "Copper": 0,
    "Hastelloy": 1,
    "Silver": 2,
    "Indium": 3,
    "Stainless Steel": 4,
    "HTSSuperPower": 5, 
    "HTSFujikura": 6,
    "HTSSucci": 7
} %}

{% if dm.magnet.solve.type in ["electromagnetic", "weaklyCoupled", "stronglyCoupled"] %}
    // Winding resistivity, Jc, and lambda starts ======================================================
    {% if dm.magnet.solve.winding.resistivity %}
    // Linear:
    INPUT_coilResistivity = <<dm.magnet.solve.winding.resistivity>>; // resistivity of the coil, [Ohm*m]
    rho[DOM_allWindings] = INPUT_coilResistivity;
    {% else %}
    
        {% if dm.magnet.solve.winding.superConductor.lengthValues is not none %}
            {% set numberOfIcValues = len(dm.magnet.solve.winding.superConductor.lengthValues) %}
            {% set listOfLengthValues = dm.magnet.solve.winding.superConductor.lengthValues %}
            {% set listOfIcValues = dm.magnet.solve.winding.superConductor.IcValues %}
        {% else %}
            {% set numberOfIcValues = 1 %}
            {% set listOfLengthValues = [1] %}
            {% set listOfIcValues = [dm.magnet.solve.winding.superConductor.IcAtTinit] %}
        {% endif %}
    rhoWindingAndDerivative[] = WindingWithSuperConductorRhoAndDerivativeV1[
        $Time,
        XYZ[],
        $3,
        $2,
        $1
    ]{  <<dm.magnet.solve.winding.superConductor.IcReferenceTemperature>>,
        <<dm.magnet.solve.winding.superConductor.IcReferenceBmagnitude>>,
        <<dm.magnet.solve.winding.superConductor.IcReferenceBangle>>,
        <<dm.magnet.solve.winding.superConductor.IcInArcLength>>,
        {% if dm.magnet.solve.winding.isotropic %} 
        1,
        {% else %} 
        0,
        {% endif %}
        <<numberOfIcValues>>, // N of Ic Values
        {% for lengthValue in listOfLengthValues %}
        <<lengthValue>>,
        {% endfor %}
        {% for IcValue in listOfIcValues %}
        <<IcValue>>,
        {% endfor %}
        <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<materialCodes[material.name]>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.relativeThickness>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.RRR>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.RRRRefTemp>>,
        {% endfor %}
        <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
        <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
        <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
        <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>,
        <<materialCodes[dm.magnet.solve.winding.superConductor.name]>>, // material integer: HTS
        <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>, // relative thickness of HTS
        <<dm.magnet.solve.winding.superConductor.electricFieldCriterion>>, // electric field criterion of HTS
        <<dm.magnet.solve.winding.superConductor.nValue>>, // n value of HTS
        <<dm.magnet.solve.winding.minimumPossibleResistivity>>, // winding minimum possible resistivity (or superconductor minimum? bug)
        <<dm.magnet.solve.winding.maximumPossibleResistivity>>, // winding maximum possible resistivity (or superconductor maximum? bug)
        <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect start turn
        <<dm.magnet.solve.localDefects.criticalCurrentDensity.endTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect end turn
        <<dm.magnet.solve.localDefects.criticalCurrentDensity.whichPancakeCoil if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "1">>, // local defect which pancake coil
        <<dm.magnet.solve.localDefects.criticalCurrentDensity.value if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect value
        <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTime if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "99999999">>, // local defect start time
        <<dm.magnet.geometry.winding.innerRadius>>,
        <<windingThickness>>,
        <<gapThickness>>,
        <<dm.magnet.geometry.winding.theta_i>>,
        <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
        <<dm.magnet.geometry.numberOfPancakes>>,
        <<dm.magnet.geometry.winding.height>>,
        <<dm.magnet.geometry.gapBetweenPancakes>>,
        <<scalingFactor>>,
        1 // arbitrary jCritical scaling normal to winding
    };
    rho[DOM_allWindings] = {% if dm.magnet.solve.winding.isotropic %}
    	CompXX[
            GetFirstTensor[
                SetVariable[
                    rhoWindingAndDerivative[$1, $2, $3],
                    ElementNum[],
                    QuadraturePointIndex[],
                    $NLIteration
                ]{
                    $rhoWindingAndDerivative
                }
            ]
        ];
    {% else %}
    GetFirstTensor[
        SetVariable[
            rhoWindingAndDerivative[$1, $2, $3],
            ElementNum[],
            QuadraturePointIndex[],
            $NLIteration
        ]{
            $rhoWindingAndDerivative
        }
    ];
    {% endif %}

    
    d_of_rho_wrt_j_TIMES_j[DOM_allWindings] = GetSecondTensor[
        GetVariable[ElementNum[], QuadraturePointIndex[], $NLIteration]{$rhoWindingAndDerivative}
    ];

    Jcritical[] = Pancake3DCriticalCurrentDensity[
        $Time,
        XYZ[],
        $2,
        $1
        ]{
            <<dm.magnet.solve.winding.superConductor.IcReferenceTemperature>>,
            <<dm.magnet.solve.winding.superConductor.IcReferenceBmagnitude>>,
            <<dm.magnet.solve.winding.superConductor.IcReferenceBangle>>,
            <<dm.magnet.solve.winding.superConductor.IcInArcLength>>,
            <<numberOfIcValues>>, // N of Ic Values
            {% for lengthValue in listOfLengthValues %}
            <<lengthValue>>,
            {% endfor %}
            {% for IcValue in listOfIcValues %}
            <<IcValue>>,
            {% endfor %}
            <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
            {% for material in dm.magnet.solve.winding.normalConductors %}
            <<materialCodes[material.name]>>,
            {% endfor %}
            {% for material in dm.magnet.solve.winding.normalConductors %}
            <<material.relativeThickness>>,
            {% endfor %}
            {% for material in dm.magnet.solve.winding.normalConductors %}
            <<material.RRR>>,
            {% endfor %}
            {% for material in dm.magnet.solve.winding.normalConductors %}
            <<material.RRRRefTemp>>,
            {% endfor %}
            <<materialCodes[dm.magnet.solve.winding.superConductor.name]>>, // material integer: HTS
            <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>, // relative thickness of HTS
            <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect start turn
            <<dm.magnet.solve.localDefects.criticalCurrentDensity.endTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect end turn
            <<dm.magnet.solve.localDefects.criticalCurrentDensity.whichPancakeCoil if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "1">>, // local defect which pancake coil
            <<dm.magnet.solve.localDefects.criticalCurrentDensity.value if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect value
            <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTime if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "99999999">>, // local defect start time
            <<windingThickness>>,
            <<dm.magnet.geometry.winding.height>>,
            <<scalingFactor>>,
            <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
            <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
            <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
            <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>
        };

        Icritical[] = Pancake3DCriticalCurrent[
            $Time,
            XYZ[],
            $2,
            $1
            ]{
                <<dm.magnet.solve.winding.superConductor.IcReferenceTemperature>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBmagnitude>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBangle>>,
                <<dm.magnet.solve.winding.superConductor.IcInArcLength>>,
                <<numberOfIcValues>>, // N of Ic Values
                {% for lengthValue in listOfLengthValues %}
                <<lengthValue>>,
                {% endfor %}
                {% for IcValue in listOfIcValues %}
                <<IcValue>>,
                {% endfor %}
                <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<materialCodes[material.name]>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.relativeThickness>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRR>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRRRefTemp>>,
                {% endfor %}
                <<materialCodes[dm.magnet.solve.winding.superConductor.name]>>, // material integer: HTS
                <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>, // relative thickness of HTS
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect start turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.endTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect end turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.whichPancakeCoil if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "1">>, // local defect which pancake coil
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.value if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect value
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTime if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "99999999">>, // local defect start time
                <<windingThickness>>,
                <<dm.magnet.geometry.winding.height>>,
                <<scalingFactor>>,
                <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
                <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>
            };

            lambda[] = Pancake3DHTSCurrentSharingIndex[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  <<dm.magnet.solve.winding.superConductor.IcReferenceTemperature>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBmagnitude>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBangle>>,
                <<dm.magnet.solve.winding.superConductor.IcInArcLength>>,
                {% if dm.magnet.solve.winding.isotropic %} 
                1,
                {% else %} 
                0,
                {% endif %}
                <<numberOfIcValues>>, // N of Ic Values
                {% for lengthValue in listOfLengthValues %}
                <<lengthValue>>,
                {% endfor %}
                {% for IcValue in listOfIcValues %}
                <<IcValue>>,
                {% endfor %}
                <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<materialCodes[material.name]>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.relativeThickness>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRR>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRRRefTemp>>,
                {% endfor %}
                <<materialCodes[dm.magnet.solve.winding.superConductor.name]>>, // material integer: HTS
                <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>, // relative thickness of HTS
                <<dm.magnet.solve.winding.superConductor.electricFieldCriterion>>, // electric field criterion of HTS
                <<dm.magnet.solve.winding.superConductor.nValue>>, // n value of HTS
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect start turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.endTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect end turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.whichPancakeCoil if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "1">>, // local defect which pancake coil
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.value if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect value
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTime if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "99999999">>, // local defect start time
                <<windingThickness>>,
                <<dm.magnet.geometry.winding.height>>,
                <<scalingFactor>>,
                <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
                <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>
            };

            jHTS[] = Pancake3DHTSCurrentDensity[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  <<dm.magnet.solve.winding.superConductor.IcReferenceTemperature>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBmagnitude>>,
                <<dm.magnet.solve.winding.superConductor.IcReferenceBangle>>,
                <<dm.magnet.solve.winding.superConductor.IcInArcLength>>,
                {% if dm.magnet.solve.winding.isotropic %} 
                1,
                {% else %} 
                0,
                {% endif %}
                <<numberOfIcValues>>, // N of Ic Values
                {% for lengthValue in listOfLengthValues %}
                <<lengthValue>>,
                {% endfor %}
                {% for IcValue in listOfIcValues %}
                <<IcValue>>,
                {% endfor %}
                <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<materialCodes[material.name]>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.relativeThickness>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRR>>,
                {% endfor %}
                {% for material in dm.magnet.solve.winding.normalConductors %}
                <<material.RRRRefTemp>>,
                {% endfor %}
                <<materialCodes[dm.magnet.solve.winding.superConductor.name]>>, // material integer: HTS
                <<dm.magnet.solve.winding.relativeThicknessOfSuperConductor>>, // relative thickness of HTS
                <<dm.magnet.solve.winding.superConductor.electricFieldCriterion>>, // electric field criterion of HTS
                <<dm.magnet.solve.winding.superConductor.nValue>>, // n value of HTS
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect start turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.endTurn if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect end turn
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.whichPancakeCoil if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "1">>, // local defect which pancake coil
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.value if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "0">>, // local defect value
                <<dm.magnet.solve.localDefects.criticalCurrentDensity.startTime if dm.magnet.solve.localDefects.criticalCurrentDensity is not none else "99999999">>, // local defect start time
                <<windingThickness>>,
                <<dm.magnet.geometry.winding.height>>,
                <<scalingFactor>>,
                <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
                <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
                <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>
            };
    {% endif %}
    // Winding resistivity, Jc, and lambda ends ========================================================

    // Terminals resistivity starts ====================================================
    {% if dm.magnet.solve.terminals.transitionNotch.resistivity %}
    rho[DOM_transitionNotchVolumes] = <<dm.magnet.solve.terminals.transitionNotch.resistivity>>;
    {% else %}
    rho[DOM_transitionNotchVolumes] = <<materials[dm.magnet.solve.terminals.transitionNotch.material.resistivityMacroName](RRR=dm.magnet.solve.terminals.transitionNotch.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.transitionNotch.material.RRRRefTemp)>>;
    {% endif %}

    {% if dm.magnet.solve.terminals.resistivity %}
    // Linear:
    INPUT_terminalResistivity = <<dm.magnet.solve.terminals.resistivity>>; // resistivity of the terminals, [Ohm*m]
    rho[DOM_terminals] = INPUT_terminalResistivity;
    {% else %}
    // Nonlinear:
    rho[DOM_terminals] = <<materials[dm.magnet.solve.terminals.material.resistivityMacroName](RRR=dm.magnet.solve.terminals.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.material.RRRRefTemp)>>;
    
    {% endif %}
    // Terminals resistivity ends ======================================================

    // Insulation resistivity starts ===================================================
    {% if dm.magnet.solve.contactLayer.resistivity != "perfectlyInsulating" %}
    {% if dm.magnet.solve.contactLayer.resistivity %}
    // Linear:
    INPUT_insulationResistivity =<<dm.magnet.solve.contactLayer.resistivity>>; // resistivity of the insulation, [Ohm*m]

        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
        electromagneticOnlyFunction[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_fct_only[]{
            th_terminal_k, INPUT_insulationResistivity
        };
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    electromagneticMassFunctionNoDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationResistivity, <<a>>, <<b>>
        };
    
    electromagneticStiffnessFunctiona<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationResistivity, <<a>>, <<b>>
        };

    electromagneticMassFunctionDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, <<a>>, <<b>>
        };

                {% endfor %}
        {% endfor %}
        
            {% for k in range(1,3) %}
    electromagneticRHSFunctionk<<k>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_insulationResistivity
        };

                {% for a in range(1,3) %}
                    {% for b in range(1,3) %}
    electromagneticTripleFunctionk<<k>>a<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, <<k>>, <<a>>, <<b>>
        };

                    {% endfor %}
                {% endfor %}
            {% endfor %}

        {% else %}
    // Volume insulation:
    rho[DOM_insulation] = INPUT_insulationResistivity;
        {% endif %}
    {% elif not dm.magnet.solve.contactLayer.resistivity %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
        electromagneticOnlyFunction[DOM_insulationSurface] = <<dm.magnet.solve.contactLayer.material.getdpTSAOnlyResistivityFunction>>[$1, $2]{
            oneDGaussianOrder, th_terminal_k
        };
    // Thin-shell insulation:
            {% for a in range(1,3) %}   
                {% for b in range(1,3) %}
    electromagneticMassFunctionNoDta<<a>>b<<b>>[DOM_insulationSurface] = <<TSA_materials[dm.magnet.solve.contactLayer.material.getdpTSAMassResistivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_insulation_k")>>;
    
    electromagneticStiffnessFunctiona<<a>>b<<b>>[DOM_insulationSurface] = <<TSA_materials[dm.magnet.solve.contactLayer.material.getdpTSAStiffnessResistivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_insulation_k")>>;
    

    electromagneticMassFunctionDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
    
            {% for k in range(1,3) %}
    electromagneticRHSFunctionk<<k>>[DOM_insulationSurface] = <<dm.magnet.solve.contactLayer.material.getdpTSARHSFunction>>[$1, $2]{
            <<k>>, oneDGaussianOrder, th_terminal_k
        };

                {% for a in range(1,3) %}
                    {% for b in range(1,3) %}
    electromagneticTripleFunctionk<<k>>a<<a>>b<<b>>[DOM_insulationSurface] = <<dm.magnet.solve.contactLayer.material.getdpTSATripleFunction>>[$1, $2]{
            <<k>>, <<a>>, <<b>>, oneDGaussianOrder, th_terminal_k
        };

                    {% endfor %}
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    rho[DOM_insulation] = <<materials[dm.magnet.solve.contactLayer.material.resistivityMacroName](RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp)>>;

        {% endif %}
    {% endif %}
    {% endif %}
    // Insulation resistivity ends =====================================================
    
    {% if dm.magnet.solve.voltageTapPositions %}
    // Insulation conductivity starts ===================================================
    {% if dm.magnet.solve.terminals.terminalContactLayer.resistivity and dm.magnet.geometry.contactLayer.thinShellApproximation and   dm.magnet.solve.contactLayer.resistivity != "perfectlyInsulating"  %}
    // Linear:
    INPUT_insulationConductivity = 1./<<dm.magnet.solve.contactLayer.resistivity>>; // conductivity of the insulation, [S/m]

    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
        electricScalarPotentialMassFunctionNoDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationConductivity, <<a>>, <<b>>
        };
    
        electricScalarPotentialStiffnessFunctiona<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationConductivity, <<a>>, <<b>>
        };
                {% endfor %}
            {% endfor %}



    {% elif not dm.magnet.solve.contactLayer.resistivity %}
    // Nonlinear:

    // Thin-shell insulation:
    {# Non-linear TSA conductivity not considered yet. #}
    <<0/0>>;
    
    {% endif %}
    // Insulation conductivity ends =====================================================
    {% endif %}

    // Transition layer resistivity starts =============================================
    {% if dm.magnet.solve.terminals.terminalContactLayer.resistivity %}
    // Linear:
    INPUT_terminalContactLayerResistivity = <<dm.magnet.solve.terminals.terminalContactLayer.resistivity>>; // resistivity of the insulation, [Ohm*m]

    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    electromagneticOnlyFunction[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_fct_only[]{
        th_terminal_k, INPUT_terminalContactLayerResistivity
    };
    // Thin-shell insulation:
        {% for a in range(1,3) %}
            {% for b in range(1,3) %}
    electromagneticMassFunctionNoDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, <<a>>, <<b>>
        };

    electromagneticStiffnessFunctiona<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, <<a>>, <<b>>
        };

    electromagneticMassFunctionDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, <<a>>, <<b>>
        };

                {% endfor %}
        {% endfor %}
        
            {% for k in range(1,3) %}
    electromagneticRHSFunctionk<<k>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity
        };

                {% for a in range(1,3) %}
                    {% for b in range(1,3) %}
    electromagneticTripleFunctionk<<k>>a<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, <<k>>, <<a>>, <<b>>
        };

                    {% endfor %}
                {% endfor %}
            {% endfor %}

        {% else %}
    rho[DOM_terminalContactLayer] = INPUT_terminalContactLayerResistivity;
        {% endif %}
    {% elif not dm.magnet.solve.terminals.terminalContactLayer.resistivity %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
        electromagneticOnlyFunction[DOM_terminalContactLayerSurface] = <<dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAOnlyResistivityFunction>>[$1, $2]{
            oneDGaussianOrder, th_terminal_k
        };
    // Thin-shell insulation:
            {% for a in range(1,3) %}   
                {% for b in range(1,3) %}
    electromagneticMassFunctionNoDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = <<TSA_materials[dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAMassResistivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_terminal_k")>>;
    
    electromagneticStiffnessFunctiona<<a>>b<<b>>[DOM_terminalContactLayerSurface] = <<TSA_materials[dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAStiffnessResistivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_terminal_k")>>;

    electromagneticMassFunctionDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
    
            {% for k in range(1,3) %}
    electromagneticRHSFunctionk<<k>>[DOM_terminalContactLayerSurface] = <<dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSARHSFunction>>[$1, $2]{
            <<k>>, oneDGaussianOrder, th_terminal_k
        };

                {% for a in range(1,3) %}
                    {% for b in range(1,3) %}
    electromagneticTripleFunctionk<<k>>a<<a>>b<<b>>[DOM_terminalContactLayerSurface] = <<dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSATripleFunction>>[$1, $2]{
            <<k>>, <<a>>, <<b>>, oneDGaussianOrder, th_terminal_k
        };

                    {% endfor %}
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    rho[DOM_terminalContactLayer] = <<materials[dm.magnet.solve.terminals.terminalContactLayer.material.resistivityMacroName](RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp)>>;

        {% endif %}
    {% endif %}
    // Transition layer resistivity ends ===============================================
    {% if dm.magnet.solve.voltageTapPositions %}
    // the electric scalar potential formulation has to deal with very large conductivity values
    // to yield an equivalent system with better conditioning, it is scaled with this factor
    electricScalarPotential_scalingFactor = 1E-10; 
    // Transition layer conductivity starts =============================================
        {% if dm.magnet.solve.terminals.terminalContactLayer.resistivity and dm.magnet.geometry.contactLayer.thinShellApproximation  %}
    
    // Linear:
    INPUT_terminalContactLayerConductivity = 1./<<dm.magnet.solve.terminals.terminalContactLayer.resistivity>>; // resistivity of the insulation, [S/m]

    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    electricScalarPotentialMassFunctionNoDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, <<a>>, <<b>>
        };

    electricScalarPotentialStiffnessFunctiona<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}

        {% elif not dm.magnet.solve.terminals.terminalContactLayer.resistivity %}
        // Thin-shell insulation:
            {# Non-linear TSA conductivity not considered yet. #}
            <<0/0>>;

    {% endif %}
    // Transition layer conductivity ends ===============================================

    {% endif %}
{% endif %}

{% if dm.magnet.solve.type in ["thermal", "weaklyCoupled", "stronglyCoupled"] %}
    // Winding thermal conductivity starts =============================================
    {% if dm.magnet.solve.winding.thermalConductivity %}
    // Linear:
    INPUT_windingThermalConductivity = <<dm.magnet.solve.winding.thermalConductivity>>; // thermal conductivity of the winding, [W/*(m*K)]
    kappa[DOM_allWindings] = INPUT_windingThermalConductivity;
    {% else %}
    // Nonlinear:
    kappa[DOM_allWindings] = WindingThermalConductivityV1[
        XYZ[],
        $2,
        $1
    ]{  {% if dm.magnet.solve.winding.isotropic %} 1 {% else %} 0 {% endif %},
        <<len(dm.magnet.solve.winding.normalConductors)>>, // N of materials,
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<materialCodes[material.name]>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.relativeThickness>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.RRR>>,
        {% endfor %}
        {% for material in dm.magnet.solve.winding.normalConductors %}
        <<material.RRRRefTemp>>,
        {% endfor %}
        <<materialCodes[dm.magnet.solve.winding.shuntLayer.material.name]>>,
        <<dm.magnet.solve.winding.shuntLayer.material.relativeHeight>>,
        <<dm.magnet.solve.winding.shuntLayer.material.RRR>>,
        <<dm.magnet.solve.winding.shuntLayer.material.RRRRefTemp>>,
        <<dm.magnet.geometry.winding.innerRadius>>,
        <<windingThickness>>,
        <<gapThickness>>,
        <<dm.magnet.geometry.winding.theta_i>>,
        <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
        <<dm.magnet.geometry.numberOfPancakes>>,
        <<dm.magnet.geometry.winding.height>>,
        <<dm.magnet.geometry.gapBetweenPancakes>>,
        <<scalingFactor>>,
        1 // arbitrary jCritical scaling normal to winding
    };
    {% endif %}
    // Winding thermal conductivity ends ===============================================

    // Terminals thermal conductivity starts ===========================================
    {% if dm.magnet.solve.terminals.transitionNotch.thermalConductivity %}
    kappa[DOM_transitionNotchVolumes] = <<dm.magnet.solve.terminals.transitionNotch.thermalConductivity>>;
    {% else %}
    kappa[DOM_transitionNotchVolumes] = <<materials[dm.magnet.solve.terminals.transitionNotch.material.thermalConductivityMacroName](RRR=dm.magnet.solve.terminals.transitionNotch.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.transitionNotch.material.RRRRefTemp)>>;
    {% endif %}

    {% if dm.magnet.solve.terminals.thermalConductivity %}
    // Linear:
    INPUT_terminalThermalConductivity = <<dm.magnet.solve.terminals.thermalConductivity>>; // thermal conductivity of the terminal, [W/*(m*K)]
    kappa[DOM_terminals] = INPUT_terminalThermalConductivity;
    {% else %}
    // Nonlinear:
    kappa[DOM_terminals] = <<materials[dm.magnet.solve.terminals.material.thermalConductivityMacroName](RRR=dm.magnet.solve.terminals.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.material.RRRRefTemp)>>;
    {% endif %}
    // Terminals thermal conductivity ends =============================================

    // Insulation thermal conductivity starts ==========================================
    {% if dm.magnet.solve.contactLayer.thermalConductivity %}
    // Linear:
    INPUT_insulationThermalConductivity = <<dm.magnet.solve.contactLayer.thermalConductivity>>; // thermal conductivity of the insulation, [W/*(m*K)]

        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_insulation_k, INPUT_insulationThermalConductivity, <<a>>, <<b>>
        };

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_insulation_k, INPUT_insulationThermalConductivity, <<a>>, <<b>>
        };
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    kappa[DOM_insulation] = INPUT_insulationThermalConductivity;
        {% endif %}
    {% else %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_insulationSurface] = <<TSA_materials[dm.magnet.solve.contactLayer.material.getdpTSAMassThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", thickness_TSA="th_insulation_k", constantThickness=True)>>;

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_insulationSurface] = <<TSA_materials[dm.magnet.solve.contactLayer.material.getdpTSAStiffnessThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", thickness_TSA="th_insulation_k", constantThickness=True)>>;
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    kappa[DOM_insulation] = <<materials[dm.magnet.solve.contactLayer.material.thermalConductivityMacroName](RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp)>>;
        {% endif %}
    {% endif %}
    // Insulation thermal conductivity ends ============================================

    // Transition layer thermal conductivity starts ====================================
    {% if dm.magnet.solve.terminals.terminalContactLayer.thermalConductivity %}
    // Linear:
    INPUT_terminalContactLayerThermalConductivity = <<dm.magnet.solve.terminals.terminalContactLayer.thermalConductivity>>; // thermal conductivity of the insulation, [W/*(m*K)]

        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerThermalConductivity, <<a>>, <<b>>
        };

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerThermalConductivity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    kappa[DOM_terminalContactLayer] = INPUT_terminalContactLayerThermalConductivity;
        {% endif %}
    {% else %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = <<TSA_materials[dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAMassThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_terminal_k")>>;

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_terminalContactLayerSurface] = <<TSA_materials[dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAStiffnessThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_terminal_k")>>;
    
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    kappa[DOM_terminalContactLayer] = <<materials[dm.magnet.solve.terminals.terminalContactLayer.material.thermalConductivityMacroName](RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp)>>;
        {% endif %}
    {% endif %}
    // Transition layer thermal conductivity ends ======================================
    
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
    // Cryocooler thermal conductivity starts ====================================
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.thermalConductivity %}
    // Linear:
    INPUT_cryocoolerThermalConductivity = <<dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.thermalConductivity>>; // thermal conductivity of the insulation, [W/*(m*K)]

            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, <<a>>, <<b>>
        };

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
    {% else %}
    // Nonlinear:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionNoDta<<a>>b<<b>>[DOM_terminalSurfaces] = <<TSA_materials[dm.magnet.solve.cryocooler.lumpedMass.material.getdpTSAMassThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", thickness_TSA="th_cryocooler_k[]")>>;

    thermalStiffnessFunctiona<<a>>b<<b>>[DOM_terminalSurfaces] = <<TSA_materials[dm.magnet.solve.cryocooler.lumpedMass.material.getdpTSAStiffnessThermalConductivityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", thickness_TSA="th_cryocooler_k[]")>>;
    
                {% endfor %}
            {% endfor %}

    {% endif %}
    // Cryocooler thermal conductivity ends ======================================
    {% endif %}

    // Winding specific heat capacity starts ===========================================
    {% if dm.magnet.solve.winding.specificHeatCapacity %}
    // Linear:
    INPUT_coilSpecificHeatCapacity = <<dm.magnet.solve.winding.specificHeatCapacity>>; // specific heat capacity of the coil, [J/(kg*K)]
    Cv[DOM_allWindings] = INPUT_coilSpecificHeatCapacity;
    {% else %}
    // Nonlinear:
        {% for material in dm.magnet.solve.winding.normalConductors %}
    Cv<<material.getdpNormalMaterialGetDPName>>[] =  <<materials[material.heatCapacityMacroName]()>>;
        {% endfor %}

    Cv[DOM_allWindings] = <<scalingFactor>> * RuleOfMixtures[
        {% for material in dm.magnet.solve.winding.normalConductors %}
            {% if loop.last %}
        Cv<<material.getdpNormalMaterialGetDPName>>[$1]
            {% else %}
        Cv<<material.getdpNormalMaterialGetDPName>>[$1],
            {% endif %}
        {% endfor %}
    ]{
        {% for material in dm.magnet.solve.winding.normalConductors %}
            {% if loop.last %}
        INPUT_relativeThickness<<material.getdpNormalMaterialGetDPName>>
            {% else %}
        INPUT_relativeThickness<<material.getdpNormalMaterialGetDPName>>,
            {% endif %}
        {% endfor %}
    };
    {% endif %}
    // Winding specific heat capacity ends =============================================

    // Terminals specific heat capacity starts =========================================
    {% if dm.magnet.solve.terminals.transitionNotch.specificHeatCapacity %}
    Cv[DOM_transitionNotchVolumes] = <<dm.magnet.solve.terminals.transitionNotch.specificHeatCapacity>>;
    {% else %}
    Cv[DOM_transitionNotchVolumes] = <<materials[dm.magnet.solve.terminals.transitionNotch.material.heatCapacityMacroName]()>>;
    {% endif %}

    {% if dm.magnet.solve.terminals.specificHeatCapacity %}
    // Linear:
    INPUT_terminalSpecificHeatCapacity = <<dm.magnet.solve.terminals.specificHeatCapacity>>; // specific heat capacity of the terminal, [J/(kg*K)]
    Cv[DOM_terminals] = INPUT_terminalSpecificHeatCapacity;
    {% else %}
    // Nonlinear:
    Cv[DOM_terminals] =  <<materials[dm.magnet.solve.terminals.material.heatCapacityMacroName]()>>;
    {% endif %}
    // Terminals specific heat capacity ends ===========================================
    
    // Insulation specific heat capacity starts ========================================
    {% if dm.magnet.solve.contactLayer.specificHeatCapacity %}
    // Linear:
    INPUT_insulationSpecificHeatCapacity = <<dm.magnet.solve.contactLayer.specificHeatCapacity>>; // specific heat capacity of the terminal, [J/(kg*K)]

        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_insulation_k, INPUT_insulationSpecificHeatCapacity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    Cv[DOM_insulation] = INPUT_insulationSpecificHeatCapacity;
        {% endif %}

    {% else %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_insulationSurface] = <<TSA_materials[dm.magnet.solve.contactLayer.material.getdpTSAMassHeatCapacityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.contactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.contactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_insulation_k")>>;

                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    Cv[DOM_insulation] = <<materials[dm.magnet.solve.contactLayer.material.heatCapacityMacroName]()>>;
        {% endif %}
    {% endif %}
    // Insulation specific heat capacity ends ==========================================

    // Transition layer specific heat capacity starts ==================================
    {% if dm.magnet.solve.terminals.terminalContactLayer.specificHeatCapacity %}
    // Linear:
    INPUT_terminalContactLayerSpecificHeatCapacity = <<dm.magnet.solve.terminals.terminalContactLayer.specificHeatCapacity>>; // specific heat capacity of the terminal, [J/(kg*K)]

        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerSpecificHeatCapacity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    Cv[DOM_terminalContactLayer] = INPUT_terminalContactLayerSpecificHeatCapacity;
        {% endif %}

    {% else %}
    // Nonlinear:
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    // Thin-shell insulation:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_terminalContactLayerSurface] =  <<TSA_materials[dm.magnet.solve.terminals.terminalContactLayer.material.getdpTSAMassHeatCapacityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.terminalContactLayer.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.terminalContactLayer.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", constantThickness=True, thickness_TSA="th_terminal_k")>>;
                {% endfor %}
            {% endfor %}
        {% else %}
    // Volume insulation:
    Cv[DOM_terminalContactLayer] = <<materials[dm.magnet.solve.terminals.terminalContactLayer.material.heatCapacityMacroName]()>>;
        {% endif %}
    {% endif %}
    // Transition layer specific heat capacity ends ====================================
    
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
    // Cryocooler specific heat capacity starts ==================================
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.specificHeatCapacity %}
    // Linear:
    INPUT_cryocoolerSpecificHeatCapacity = <<dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.specificHeatCapacity>>; // specific heat capacity of the terminal, [J/(kg*K)]

            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerSpecificHeatCapacity, <<a>>, <<b>>
        };

                {% endfor %}
            {% endfor %}

    {% else %}
    // Nonlinear:
            {% for a in range(1,3) %}
                {% for b in range(1,3) %}
    thermalMassFunctionDta<<a>>b<<b>>[DOM_terminalSurfaces] =  <<TSA_materials[dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.material.getdpTSAMassHeatCapacityMacroName](T_i="$1", T_iPlusOne="$2", RRR=dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.material.RRR, RRRRefTemp=dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.material.RRRRefTemp, BMagnitude="0", k=a, l=b, GaussianPoints="oneDGaussianOrder", thickness_TSA="th_cryocooler_k[]")>>;
                {% endfor %}
            {% endfor %}

    {% endif %}
    // Cryocooler specific heat capacity ends ====================================
    {% endif %}
{% endif %}
{% if dm.magnet.geometry.air.shellTransformation %}

    // Shell transformation parameters:
    {% if dm.magnet.geometry.air.type == "cylinder" %}
    INPUT_shellInnerRadius = <<dm.magnet.geometry.air.radius>>; // inner radius of the shell, [m]
    INPUT_shellOuterRadius = <<dm.magnet.geometry.air.shellOuterRadius>>; // outer radius of the shell, [m]
    {% elif dm.magnet.geometry.air.type == "cuboid" %}
        {% set shellInnerDistance = dm.magnet.geometry.air.a/2 %}
        {% set shellOuterDistance = dm.magnet.geometry.air.shellSideLength/2 %}
    INPUT_shellInnerDistance = <<shellInnerDistance>>; // inner radius of the shell, [m]
    INPUT_shellOuterDistance = <<shellOuterDistance>>; // outer radius of the shell, [m]
    <<0/0>>
    // ERROR: Shell transformation is not implemented for cuboid type of air yet!
    {% endif %}
{% endif %}
{% if dm.magnet.solve.systemsOfEquationsType == "nonlinear" %}
    {% if dm.magnet.solve.boundaryConditions == "vanishingTangentialElectricField" %}
        {% if dm.magnet.solve.materialParametersUseCoilField and dm.magnet.solve.type != "thermal" %}
            {% set Bfield = "mu[] * {LOCALQUANT_h}" %}
        {% else %}
            {% set Bfield = "0" %}
        {% endif %}
    {% else %}
        {% if dm.magnet.solve.materialParametersUseCoilField and dm.magnet.solve.type != "thermal"  %}
            {% set Bfield = "mu[] * {LOCALQUANT_h} + UnitVectorZ[] * " + str(dm.magnet.solve.boundaryConditions.imposedAxialField) %}
        {% else %}
            {% set Bfield = "UnitVectorZ[] * " + str(dm.magnet.solve.boundaryConditions.imposedAxialField) %}
        {% endif %}
    {% endif %}
    {% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled"] %}
        {% set rhoArguments = "{LOCALQUANT_T}, " + Bfield + ", {d LOCALQUANT_h}" %}
        {% set kappaArguments = "{LOCALQUANT_T}, " + Bfield %}
        {% set CvArguments = "{LOCALQUANT_T}, Norm[" + Bfield + "]" %}
    {% elif dm.magnet.solve.type == "electromagnetic" %}
        {% set rhoArguments = "INPUT_initialTemperature, " + Bfield + ", {d LOCALQUANT_h}" %}
        {% set kappaArguments = "INPUT_initialTemperature, " + Bfield %}
        {% set CvArguments = "INPUT_initialTemperature, Norm[" + Bfield + "]" %}
    {% elif dm.magnet.solve.type == "thermal" %}
        {% set rhoArguments = "{LOCALQUANT_T}, " + Bfield + ", 0, 0" %}
        {% set kappaArguments = "{LOCALQUANT_T}, " + Bfield + ", 0, 0" %}
        {% set CvArguments = "{LOCALQUANT_T}, " + Bfield + ", 0, 0" %}
    {% endif %}
{% elif dm.magnet.solve.systemsOfEquationsType == "linear" %}
    {% set rhoArguments = "" %}
    {% set kappaArguments = "" %}
    {% set CvArguments = "" %}
{% endif %}

{% if dm.magnet.solve.EECircuit.enable %}
    // EE circuit quantities
    Resistance[DOM_resistanceCrowbar] = <<dm.power_supply.R_crowbar>>;
    Resistance[DOM_resistanceEE] = <<dm.quench_protection.energy_extraction.R_EE>>;

    Resistance[DOM_switchCrowbar] = $quenchDetected == 0? <<dm.magnet.solve.EECircuit.ResistanceCrowbarOpenSwitch>>: <<dm.magnet.solve.EECircuit.ResistanceCrowbarClosedSwitch>>;

    Resistance[DOM_switchEE] = $quenchDetected == 0? <<dm.magnet.solve.EECircuit.ResistanceEnergyExtractionClosedSwitch>>: <<dm.magnet.solve.EECircuit.ResistanceEnergyExtractionOpenSwitch>>;

    Inductance[DOM_inductanceEE] = <<dm.magnet.solve.EECircuit.inductanceInSeriesWithPancakeCoil>>;
{% endif %}
{% if dm.magnet.solve.terminals.cooling == "cryocooler" %}
    // Cryocooler quantities

    // Division by area to compute Watts per meter squared
    // SurfaceArea function does not allow DOM_*** as argument, so we need to use
    // the actual ids
    cryocoolerCoolingPower[] = 
        (<<dm.magnet.solve.terminals.cryocoolerOptions.coolingPowerMultiplier>> * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_T[$1] - <<dm.magnet.solve.terminals.cryocoolerOptions.staticHeatLoadPower>>)/ GetVariable[]{$areaCryocooler} ; 

    cryocoolerCoolingPowerDerivativeT[] =      
        <<dm.magnet.solve.terminals.cryocoolerOptions.coolingPowerMultiplier>> * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_dT_T[$1]/ GetVariable[]{$areaCryocooler}; 
{% endif %}
}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
{% if dm.magnet.geometry.air.shellTransformation %}
    {% if dm.magnet.geometry.air.type == "cylinder" %}
            {
                Region DOM_airInf;
                Jacobian VolCylShell {INPUT_shellInnerRadius, INPUT_shellOuterRadius};
            }
    {% elif dm.magnet.geometry.air.type == "cuboid" %}
            {
                Region DOM_airInfX;
                Jacobian VolRectShell {INPUT_shellInnerDistance, INPUT_shellOuterDistance, 1};
            }
            {
                Region DOM_airInfY;
                Jacobian VolRectShell {INPUT_shellInnerDistance, INPUT_shellOuterDistance, 2};
            }
    {% endif %}
{% endif %}
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
            {% if dm.magnet.solve.EECircuit.enable %}
                Region DOM_currentSource;
            {% else %}
                Region DOM_terminalCut;
            {% endif %}
                Type Assign;
                Value 1;

                {% set t = dm.power_supply.t_control_LUT%}
                {% set I = dm.power_supply.I_control_LUT%}

                TimeFunction current[$Time];
            }
        }
    }
    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
    {
        // Impose current:
        Name CONSTRAINT_voltage;
        Case{
            {
                Region DOM_airHoleCut;
                Type Assign;
                Value 0;
            }
        }
    }
    {% endif %}
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
        {% if dm.magnet.solve.terminals.cooling == "fixedTemperature" %}
            {
                Region DOM_bottomTerminalSurface;
                Type Assign;
                Value <<dm.magnet.solve.initialConditions.temperature>>;
            }
            {
                Region DOM_topTerminalSurface;
                Type Assign;
                Value <<dm.magnet.solve.initialConditions.temperature>>;
            }
        {% endif %}
        {% if dm.magnet.solve.isothermalInAxialDirection %}
            {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            {
                Region Region[{DOM_allWindings, DOM_allInsulationSurface}];
                Type Link;
                RegionRef Region[{DOM_allWindings, DOM_allInsulationSurface}];
                Coefficient 1;
                Function Vector[X[], Y[], <<dm.magnet.geometry.winding.height/2>>];
            }
            {% else %}
            {
                Region Region[{DOM_allWindings, DOM_allInsulations}];
                Type Link;
                RegionRef Region[{DOM_allWindings, DOM_allInsulations}];
                Coefficient 1;
                Function Vector[X[], Y[], <<dm.magnet.geometry.winding.height/2>>];
            }
            {% endif %}
        {% endif %}
            {
                {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                Region Region[
                            {
                                DOM_powered, 
                                DOM_allInsulationSurface
                            }
                        ];
                {% else %}
                Region Region[{DOM_powered, DOM_allInsulations}];
                {% endif %}
                Type Init;
                Value <<dm.magnet.solve.initialConditions.temperature>>;
            }
        }
    }
    {% if dm.magnet.solve.voltageTapPositions %}
    {
        Name CONSTRAINT_electricScalarPotential;
        Case{
            {
                Region DOM_topTerminalSurface;
                Type Assign;
                Value 1;
            }
            {
                Region DOM_bottomTerminalSurface;
                Type Assign;
                Value 0;
            }
        }
    }
    {% endif %}

    {% if dm.magnet.solve.EECircuit.enable %}
    { Name EECircuit_Netlist; Type Network;
        Case Circuit1 { // Describes node connection of branches
        // power source
        { Region DOM_currentSource;  Branch {1, 2} ; }

        // crowbar resistance and switch in series
        { Region DOM_resistanceCrowbar; Branch {2, 3} ; }
        { Region DOM_switchCrowbar; Branch {3, 1} ; }

        // energy extraction and switch in parallel
        { Region DOM_resistanceEE;  Branch {2, 4} ; }
        { Region DOM_switchEE;  Branch {2, 4} ; }

        // magnet current via terminal cut
        { Region DOM_terminalCut; Branch {4, 5} ; }

        // inductance in series with pancake coil
        { Region DOM_inductanceEE; Branch {5, 1} ; }
        }
    }
    {% endif %}
}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                Support Region[{DOM_total, DOM_allInsulationSurface}];
                Entity NodesOf[{DOM_Phi, DOM_insulationSurface}, Not {DOM_insulationBoundaryCurvesAir}];
{% else %}
                Support DOM_total;
                Entity NodesOf[{DOM_Phi}, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal}];
{% endif %}
{% else %}
                Support DOM_total;
                Entity NodesOf[DOM_Phi];
{% endif %}
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                Entity EdgesOf[All, Not {DOM_pancakeBoundary, DOM_allInsulationSurface}];
{% else %}
                Entity EdgesOf[All, Not DOM_pancakeBoundary];
{% endif %}
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                Support Region[{DOM_total, DOM_terminalContactLayerSurface}];
    {% else %}
                Support Region[{DOM_total, DOM_allInsulationSurface}];
    {% endif %}
{% else %}
                Support DOM_total;
{% endif %}
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
{% if dm.magnet.geometry.contactLayer.thinShellApproximation  %}
            <<FUNCTIONSPACE_TSABasisFunctions(type="electromagnetic")|indent(12)>>
            // TSA contribution on boundary of the thin layer
            {
                Name BASISFUN_gradpsinBnd;
                NameOfCoef phin_bnd;
                Function BF_GradNode;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_Phi,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity NodesOf[DOM_insulationBoundaryCurvesAir];
            }
            {
                Name BASISFUN_psieBnd;
                NameOfCoef psie_bnd;
                Function BF_Edge;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity EdgesOf[DOM_insulationBoundaryCurvesTerminal];
            }
            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef he~{i};
                    Function BF_Edge;
                    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                    Support DOM_terminalContactLayerSurface;
                    {% else %}
                    Support DOM_allInsulationSurface;
                    {% endif %}
                    Entity EdgesOf[ All, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal{% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}, DOM_insulationSurface {% endif %}} ];
                }
            EndFor
        }

        {% if dm.magnet.solve.contactLayer.resistivity != "perfectlyInsulating" %}
        SubSpace{
            <<FUNCTIONSPACE_TSASubSpaces(["BASISFUN_sc", "BASISFUN_gradpsinBnd", "BASISFUN_psieBnd"])|indent(12)>>
        }
        {% else %}
        SubSpace{
            <<FUNCTIONSPACE_TSASubSpaces(["BASISFUN_sc", "BASISFUN_gradpsinBnd", "BASISFUN_psieBnd", "BASISFUN_gradpsin"])|indent(12)>>
        }
        {% endif %}
{% else %}
        }
{% endif %}
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {% if not dm.magnet.solve.EECircuit.enable %}
            {
                NameOfCoef GLOBALQUANT_I;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_current;
            }
            {% endif %}
            {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
            {
                NameOfCoef GLOBALQUANT_V;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_voltage;
            }
            {% endif %}
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
            {% if dm.magnet.geometry.contactLayer.thinShellApproximation and dm.magnet.solve.contactLayer.resistivity != "perfectlyInsulating" %}
            {
                NameOfCoef phin_bnd;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
            {% endif %}
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
            {% if dm.magnet.solve.terminals.cooling == "cryocooler" %}
                Support Region[{DOM_thermal, DOM_terminalSurfaces 
                {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface
                {% endif %}
                }];
            {% else %}
                Support Region[{DOM_thermal 
                {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface
                {% endif %}
                }];
            {% endif %}
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                Entity NodesOf[DOM_thermal, 
                    Not {DOM_allInsulationSurface}];
{% else %}
                Entity NodesOf[DOM_thermal];
{% endif %}
            }
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FUNCTIONSPACE_TSABasisFunctions(type="thermal")|indent(12)>>

            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FUNCTIONSPACE_TSABasisFunctions(type="thermal_adiabatic_half_tsa_plus")|indent(12)>>
            <<FUNCTIONSPACE_TSABasisFunctions(type="thermal_adiabatic_half_tsa_minus")|indent(12)>>
            {% endif %}

            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FUNCTIONSPACE_CryocoolerTSABasisFunctions()|indent(12)>>
            {% endif %}

            {
                Name BASISFUN_snBndTerminal;
                NameOfCoef Tn_Bnd;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_allInsulationSurface
                {% if dm.magnet.solve.convectiveCooling.heatTransferCoefficient %}
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface, DOM_allWindingSurface
                {% endif %}
                }];
                Entity NodesOf[DOM_insulationBoundaryCurvesTerminal];
            }

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    {% if dm.magnet.solve.heatFlowBetweenTurns %}
                    Support DOM_allInsulationSurface;
                    {% else %}
                    Support DOM_terminalContactLayerSurface;
                    {% endif %}
                    Entity NodesOf[ All, Not DOM_insulationBoundaryCurvesTerminal ];
                }

                {% if not dm.magnet.solve.heatFlowBetweenTurns %}
                    {
                        Name BASISFUN_snMinus_adiabatic~{i};
                        NameOfCoef snMinus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
                    {
                        Name BASISFUN_snPlus_adiabatic~{i};
                        NameOfCoef snPlus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
                {% endif %}
            EndFor

            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
                {
                    Name BASISFUN_cryocooler_sn~{i};
                    NameOfCoef sn_cryocooler~{i};
                    Function BF_Node;
                    Support DOM_terminalSurfaces;
                    Entity NodesOf[ All ];
                }
            EndFor
            {% endif %}
            
{% endif %}
        }
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
        SubSpace {
            {% if dm.magnet.solve.heatFlowBetweenTurns %}
            <<FUNCTIONSPACE_TSASubSpaces(["BASISFUN_snBndTerminal"])|indent(12)>>
            {% else %}
            <<FUNCTIONSPACE_TSASubSpaces(["BASISFUN_snBndTerminal"], additional_bf_for_adiabatic_tsa=true)|indent(12)>>
            {% endif %}
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FUNCTIONSPACE_CryocoolerTSASubSpaces()|indent(12)>>
            {% endif %}
        }
{% endif %}
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    {% for i in range(1, NofSets+1) %}
            {
                NameOfCoef BASISFUN_snMinus_coeff_<<i>>;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_<<i>>;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

        {% if not dm.magnet.solve.heatFlowBetweenTurns %}
    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_<<i>>;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_<<i>>;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
        {% endif %}
    {% endfor %}

    For i In {1:INPUT_NumOfTSAElements - 1}
        {
            NameOfCoef sn~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }

        {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            {
                NameOfCoef snMinus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef snPlus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
        {% endif %}
    EndFor
    
    {
        NameOfCoef Tn_Bnd;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
    {
        NameOfCoef BASISFUN_snOutside_coeff;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
        {
            NameOfCoef sn_cryocooler~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }
    EndFor
    {% endif %}
{% endif %}
        }
    }

    {% if dm.magnet.solve.voltageTapPositions %}
    {
        Name SPACE_electricScalarPotential;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef sn;
                Function BF_Node;
                Support Region[{DOM_terminals, DOM_allWindings
                {% if not dm.magnet.geometry.contactLayer.thinShellApproximation %}
                    , DOM_insulation
                {% endif %} 
                }];
                Entity NodesOf[All
                    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                    , Not DOM_allInsulationSurface
                    {% endif %}
                ];
            }
        {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            {% for i in range(1, NofSets+1) %}
            {
                Name BASISFUN_snMinus_<<i>>;
                NameOfCoef snMinus_<<i>>;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_<<i>>,
                                DOM_allInsulationSurface_<<i+1>>,
                                DOM_windingMinus_electricScalarPotential_<<i>>,
                                DOM_windingMinus_electricScalarPotential_<<i+1>>
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_<<i>>,
                        Not { DOM_allInsulationSurface_<<i-1>> }
                ];
            }

            {
                Name BASISFUN_snPlus_<<i>>;
                NameOfCoef snPlus_<<i>>;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_<<i>>,
                                DOM_allInsulationSurface_<<i+1>>,
                                DOM_windingPlus_electricScalarPotential_<<i>>,
                                DOM_windingPlus_electricScalarPotential_<<i+1>>
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_<<i>>,
                        Not { DOM_allInsulationSurface_<<i-1>> }
                ];
            }
            {% endfor %}

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                    Support DOM_terminalContactLayerSurface;
                    {% else %}
                    Support DOM_allInsulationSurface;
                    {% endif %}
                    Entity NodesOf[ All ];
                }
            EndFor

    
        {% endif %}   
            
        }

        Constraint {
            { NameOfCoef sn ;
                EntityType NodesOf ; NameOfConstraint CONSTRAINT_electricScalarPotential ; }
        }

        SubSpace {
            <<FUNCTIONSPACE_TSASubSpaces([])|indent(12)>>
        }
    }
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
    { Name SPACE_circuit; Type Scalar;
        BasisFunction {
            {
                Name BASISFUN_ICircuit ; NameOfCoef ICircuit_coeff ; Function BF_Region ;
                    Support DOM_circuit ; Entity DOM_circuit ;
          }
        }

        GlobalQuantity {
            { Name ICircuit; Type AliasOf       ; NameOfCoef ICircuit_coeff; }
            { Name UCircuit; Type AssociatedWith; NameOfCoef ICircuit_coeff; }
        }

        Constraint {
             { NameOfCoef ICircuit; EntityType Region; NameOfConstraint CONSTRAINT_current; }
        }
    }
    {% endif %}

}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
{% if dm.magnet.solve.type == "stronglyCoupled" %}
    {
        Name FORMULATION_<<dm.magnet.solve.type>>;
        Type FemEquation;
        Quantity{
            <<FORMULATION_VolumetricQuantities("electromagnetic")|indent(12)>>
            <<FORMULATION_VolumetricQuantities("thermal")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("electromagnetic")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAQuantities()|indent(12)>>
            {% endif %}
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
        <<FORMULATION_EECircuitQuantities()|indent(12)>>
    {% endif %}
        }

        Equation{
            <<FORMULATION_VolumetricIntegrals("electromagnetic")|indent(12)>>
            <<FORMULATION_VolumetricIntegrals("thermal")|indent(12)>> 
            <<FORMULATION_VolumetricIntegrals("resistiveHeating")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("electromagnetic")|indent(12)>>
            <<FORMULATION_TSAIntegrals("resistiveHeating")|indent(12)>>
            <<FORMULATION_TSAIntegrals("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAIntegrals()|indent(12)>>
            {% endif %}
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
        <<FORMULATION_EECircuitIntegrals()|indent(12)>>
    {% endif %}
        }
    }
{% elif dm.magnet.solve.type == "weaklyCoupled" %}
    {
        Name FORMULATION_electromagnetic;
        Type FemEquation;
        Quantity{
            <<FORMULATION_VolumetricQuantities("thermal")|indent(12)>>
            <<FORMULATION_VolumetricQuantities("electromagnetic")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            <<FORMULATION_TSAQuantities("electromagnetic")|indent(12)>>
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
        <<FORMULATION_EECircuitQuantities()|indent(12)>>
    {% endif %}
    {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
    <<FORMULATION_CryocoolerTSAQuantities()|indent(12)>>
    {% endif %}
        }

        Equation{
            <<FORMULATION_VolumetricIntegrals("electromagnetic")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("electromagnetic")|indent(12)>>
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
            <<FORMULATION_EECircuitIntegrals()|indent(12)>>
    {% endif %}
        }
    }

    {
        Name FORMULATION_thermal;
        Type FemEquation;
        Quantity {
            <<FORMULATION_VolumetricQuantities("thermal")|indent(12)>>
            <<FORMULATION_VolumetricQuantities("electromagnetic")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            <<FORMULATION_TSAQuantities("electromagnetic")|indent(12)>>
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAQuantities()|indent(12)>>
            {% endif %}
    {% endif %}
        }
        
        Equation{
            <<FORMULATION_VolumetricIntegrals("thermal")|indent(12)>>
            <<FORMULATION_VolumetricIntegrals("resistiveHeating")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            <<FORMULATION_TSAIntegrals("resistiveHeating")|indent(12)>>
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAIntegrals()|indent(12)>>
            {% endif %}
    {% endif %}
        }
    }

{% elif dm.magnet.solve.type == "electromagnetic" %}
    {
        Name FORMULATION_<<dm.magnet.solve.type>>;
        Type FemEquation;
        Quantity{
            <<FORMULATION_VolumetricQuantities("electromagnetic")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("electromagnetic")|indent(12)>>
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
        <<FORMULATION_EECircuitQuantities()|indent(12)>>
    {% endif %}
        }

        Equation{
            <<FORMULATION_VolumetricIntegrals("electromagnetic")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("electromagnetic")|indent(12)>>
    {% endif %}
    {% if dm.magnet.solve.EECircuit.enable %}
        <<FORMULATION_EECircuitIntegrals()|indent(12)>>
    {% endif %}
        }
    }
{% elif dm.magnet.solve.type == "thermal" %}
    {
        Name FORMULATION_<<dm.magnet.solve.type>>;
        Type FemEquation;
        Quantity {
            <<FORMULATION_VolumetricQuantities("thermal")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAQuantities()|indent(12)>>
            {% endif %}
    {% endif %}
        }
        
        Equation{
            <<FORMULATION_VolumetricIntegrals("thermal")|indent(12)>>
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("thermal")|indent(12)>>
            {% if not dm.magnet.solve.heatFlowBetweenTurns %}
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_minus")|indent(12)>>
            <<FORMULATION_TSAIntegrals("thermal_adiabatic_half_tsa_plus")|indent(12)>>
            {% endif %}
            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
            <<FORMULATION_CryocoolerTSAIntegrals()|indent(12)>>
            {% endif %}
    {% endif %}
        }
    }
{% endif %}

{% if dm.magnet.solve.voltageTapPositions %}
      { Name FORMULATION_electricScalarPotential; Type FemEquation;
        Quantity {
            <<FORMULATION_VolumetricQuantities("electricScalarPotential")|indent(12)>>
           {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAQuantities("electricScalarPotential")|indent(12)>>
            <<FORMULATION_TSAQuantities("thermal")|indent(12)>>
            {% endif %}
        }
  
        Equation {
            <<FORMULATION_VolumetricIntegrals("electricScalarPotential")|indent(12)>>
           {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            <<FORMULATION_TSAIntegrals("electricScalarPotential")|indent(12)>>
            {% endif %}
        }
      }
{% endif %}


}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_<<dm.magnet.solve.type>>;
        System{
{% if dm.magnet.solve.type == "weaklyCoupled" %}
            {
                Name SYSTEM_thermal;
                NameOfFormulation FORMULATION_thermal;
            }
            {
                Name SYSTEM_electromagnetic;
                NameOfFormulation FORMULATION_electromagnetic;
            }
{% else%}
            {
                Name SYSTEM_<<dm.magnet.solve.type>>;
                NameOfFormulation FORMULATION_<<dm.magnet.solve.type>>;
            }
{% endif %}
{% if dm.magnet.solve.voltageTapPositions %}
            {
                Name SYSTEM_electricScalarPotential;
                NameOfFormulation FORMULATION_electricScalarPotential;
            }
{% endif %}
        }

        Operation{
        {% if dm.magnet.solve.terminals.cooling == "cryocooler" %}
            Evaluate[SetVariable[SurfaceArea[]{<< rm.powered['Pancake3D'].surf_in.numbers | join(', ') >>, << rm.powered['Pancake3D'].surf_out.numbers | join(', ') >> }]{$areaCryocooler}];
        {% endif %}
{% if dm.magnet.solve.type == "weaklyCoupled" %}
    {% set systemNames = ["SYSTEM_electromagnetic", "SYSTEM_thermal"] %}
    {% set solveAfterThisTimes = [0, 0]%}
{% else %}
    {% set systemNames = ["SYSTEM_"+dm.magnet.solve.type] %}
    {% set solveAfterThisTimes = [0]%}
{% endif %}
            <<RESOLUTION_SolveTransientProblem(
                systemNames = systemNames,
                solveAfterThisTimes = solveAfterThisTimes)|indent(12)>>
        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_<<dm.magnet.solve.type>>;
{% if dm.magnet.solve.type == "weaklyCoupled" %}
        NameOfFormulation FORMULATION_electromagnetic;
        NameOfSystem SYSTEM_electromagnetic;
{% else %}
        NameOfFormulation FORMULATION_<<dm.magnet.solve.type>>;
        NameOfSystem SYSTEM_<<dm.magnet.solve.type>>;
{% endif %}
        Quantity{
{% if dm.magnet.solve.type == "electromagnetic" or dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled"] %}
            {
                Name RESULT_magneticField; // magnetic flux density (magnetic field)
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}{% if dm.magnet.solve.boundaryConditions != "vanishingTangentialElectricField" %} + UnitVectorZ[] * <<dm.magnet.solve.boundaryConditions.imposedAxialField>> {% endif %}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }
            {% if dm.magnet.solve.boundaryConditions != "vanishingTangentialElectricField" %}
            {
                Name RESULT_imposedAxialField; // axial field
                Value{
                    Local{
                        [UnitVectorZ[] * <<dm.magnet.solve.boundaryConditions.imposedAxialField>>];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_reactionField; // axial field
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }
            {% endif %}

            {
                Name RESULT_magnitudeOfMagneticField; // magnetic flux density magnitude
                Value{
                    Local{
                        [Norm[mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentDensity; // current density
                Value{
                    Local{
                        [{d LOCALQUANT_h}];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfCurrentDensity; // current density magnitude
                Value{
                    Local{
                        [Norm[{d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_resistivity; // current density magnitude
                Value{
                    Local{
                        [rho[<<rhoArguments>>]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_arcLength;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength[XYZ[]]{
                            <<dm.magnet.geometry.winding.innerRadius>>,
                            <<windingThickness>>,
                            <<gapThickness>>,
                            <<dm.magnet.geometry.winding.theta_i>>,
                            <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
                            <<dm.magnet.geometry.numberOfPancakes>>,
                            <<dm.magnet.geometry.winding.height>>,
                            <<dm.magnet.geometry.gapBetweenPancakes>>
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }
            {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
            {
                Name RESULT_arcLengthContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength_contactLayer[XYZ[]]{
                            <<dm.magnet.geometry.winding.innerRadius>>,
                            <<windingThickness>>,
                            <<gapThickness>>,
                            <<dm.magnet.geometry.winding.theta_i>>,
                            <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
                            <<dm.magnet.geometry.numberOfPancakes>>,
                            <<dm.magnet.geometry.winding.height>>,
                            <<dm.magnet.geometry.gapBetweenPancakes>>
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {
                Name RESULT_turnNumberContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber_contactLayer[XYZ[]]{
                            <<dm.magnet.geometry.winding.innerRadius>>,
                            <<windingThickness>>,
                            <<gapThickness>>,
                            <<dm.magnet.geometry.winding.theta_i>>,
                            <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
                            <<dm.magnet.geometry.numberOfPancakes>>,
                            <<dm.magnet.geometry.winding.height>>,
                            <<dm.magnet.geometry.gapBetweenPancakes>>
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {% endif %}
            {
                Name RESULT_turnNumber;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber[XYZ[]]{
                            <<dm.magnet.geometry.winding.innerRadius>>,
                            <<windingThickness>>,
                            <<gapThickness>>,
                            <<dm.magnet.geometry.winding.theta_i>>,
                            <<dm.magnet.mesh.winding.azimuthalNumberOfElementsPerTurn[0]>>,
                            <<dm.magnet.geometry.numberOfPancakes>>,
                            <<dm.magnet.geometry.winding.height>>,
                            <<dm.magnet.geometry.gapBetweenPancakes>>
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }

    {% if dm.magnet.solve.winding.superConductor and not dm.magnet.solve.winding.resistivity and type != "thermal" %}
            {
                Name RESULT_criticalCurrentDensity; // critical current density of the winding
                Value{
                    Local{
                        [Jcritical[<<rhoArguments>>]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrent;
                Value{
                    Local{
                        [Icritical[<<rhoArguments>>]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentSharingIndex;
                Value{
                    Local{
                        [lambda[<<rhoArguments>>]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTS;
                Value{
                    Local{
                        [jHTS[<<rhoArguments>>]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTSOverjCritical;
                Value{
                    Local{
                        // add small epsilon to avoid division by zero
                        [jHTS[<<rhoArguments>>]/(Jcritical[<<rhoArguments>>] + 1e-10)];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }
    {% endif %}

            {
                Name RESULT_magneticEnergy;
                Value{
                    Integral{
                        Type Global;
                        [1/2 * mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_inductance;
                Value{
                    Integral{
                        Type Global;
                        [mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_resistiveHeating; // resistive heating
                Value{
                    Local{
                        [(rho[<<rhoArguments>>] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In Region[{ DOM_resistiveHeating }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_voltageBetweenTerminals; // voltages in cuts
                Value{
                    Local{
                        [ - {GLOBALQUANT_V} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_currentThroughCoil; // currents in cuts
                Value{
                    Local{
                        [ {GLOBALQUANT_I} ];
                        In DOM_terminalCut;
                    }
                }
            }

        {% if dm.magnet.solve.EECircuit.enable %}
            {
                Name RESULT_current_DOM_currentSource; // currents in cuts
                Value{
                    Local{
                        [ {CIRCUITQUANT_ICircuit} ];
                        In DOM_currentSource;
                    }
                }
            }

            {
                Name RESULT_resistance_DOM_switches; // currents in cuts
                Value{
                    Term {
                        Type Global;
                        [ Resistance[] ];
                        In DOM_switchCrowbar;
                    }
                    Term {
                        Type Global;
                        [ Resistance[] ];
                        In DOM_switchEE;
                    }
                }
            }
        {% endif %}

            {
                Name RESULT_axialComponentOfTheMagneticField; // axial magnetic flux density
                Value{
                    Local{
                        [ CompZ[mu[] * {LOCALQUANT_h}] ];
                        In DOM_total;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_totalResistiveHeating; // total resistive heating for convergence
                Value{
                    Integral{
                        Type Global;
                        [(rho[<<rhoArguments>>] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In DOM_resistiveHeating;
                        Jacobian JAC_vol;
                        Integration Int;
                    }

                    {% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled", "thermal"] %}
                        {% set temperatureArgument1 = "{LOCALQUANT_TThinShell~{i}}" %}
                        {% set temperatureArgument2 = "{LOCALQUANT_TThinShell~{i+1}}" %}
                    {% elif dm.magnet.solve.type == "electromagnetic" %}
                        {% set temperatureArgument1 = "INPUT_initialTemperature" %}
                        {% set temperatureArgument2 = "INPUT_initialTemperature" %}
                    {% endif %}
                    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                        For i In {0:INPUT_NumOfTSAElements-1}
                            Integral {
                                Type Global;
                                [
                                    electromagneticOnlyFunction[
                                        <<temperatureArgument1>>,
                                        <<temperatureArgument2>>
                                    ] * SquNorm[
                                        ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
                                    ]
                                ];
                                {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                                In DOM_terminalContactLayerSurface;
                                {% else %}
                                In DOM_allInsulationSurface;
                                {% endif %}
                                Integration Int;
                                Jacobian JAC_sur;
                            } 
                        
                                {% for a in range(1,3) %}
                                    {% for b in range(1,3) %}
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta<<a>>b<<b>>[
                                        <<temperatureArgument1>>,
                                        <<temperatureArgument2>>
                                    ] * {d LOCALQUANT_hThinShell~{i + <<a>> - 1}} * {d LOCALQUANT_hThinShell~{i + <<b>> - 1}}
                                ];
                                {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                                In DOM_terminalContactLayerSurface;
                                {% else %}
                                In DOM_allInsulationSurface;
                                {% endif %}
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                                    {% endfor %}
                                {% endfor %}
                        EndFor
                    {% endif %} 
                }
            }
{% endif %}
{% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled", "thermal"]  %}
            {
                Name RESULT_temperature;
                Value {
                    Local{
                        [{LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_maximumTemperature; // maximum temperature
                Value{
                    Term {
                        Type Global;
                        [ #999 ];
                        In DOM_powered;
                    }
                }
            }

            {
                Name RESULT_heatFlux;
                Value {
                    Local{
                        [-kappa[<<kappaArguments>>] * {d LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfHeatFlux;
                Value {
                    Local{
                        [Norm[-kappa[<<kappaArguments>>] * {d LOCALQUANT_T}]];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_thermalConductivity; // current density magnitude
                Value{
                    Local{
                        [kappa[<<kappaArguments>>]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_specificHeatCapacity; // current density magnitude
                Value{
                    Local{
                        [Cv[<<CvArguments>>]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

        {% if dm.magnet.solve.terminals.cooling == "cryocooler" %}
            {
                Name RESULT_cryocoolerAveragePower; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;
                            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
                            {% set cryocooler_quantity = "LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}" %}
                            {% else %}
                            {% set cryocooler_quantity = "LOCALQUANT_T" %}
                            {% endif %}

                            [ cryocoolerCoolingPower[{<<cryocooler_quantity>>}] ];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_cryocoolerAverageTemperature; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;
                            // Division by area to compute Watts per meter squared
                            // SurfaceArea function does not allow DOM_*** as argument, so we need to use
                            // the actual ids
                            {% if dm.magnet.solve.terminals.cryocoolerOptions.lumpedMass.volume %}
                            {% set cryocooler_quantity = "LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}" %}
                            {% else %}
                            {% set cryocooler_quantity = "LOCALQUANT_T" %}
                            {% endif %}

                            [  {<<cryocooler_quantity>>}/ GetVariable[]{$areaCryocooler}];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }
        {% endif %}
{% endif %}
{% if dm.magnet.geometry.contactLayer.thinShellApproximation %} {
                Name RESULT_debug;
                Value{
                    Local{
                        [ 5 ];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
{% endif %}
        }
    }

{% if dm.quench_detection.voltage_tap_pairs and dm.magnet.solve.voltageTapPositions and not dm.magnet.solve.type == "thermal" %}
    {
        Name POSTPRO_electricScalarPotential;
        NameOfFormulation FORMULATION_electricScalarPotential;
        NameOfSystem SYSTEM_electricScalarPotential;
        Quantity{
            {
                Name RESULT_conductance;
                Value{
                    Integral{
                        Type Global;
                        [1./rho[<<rhoArguments>>] * {d LOCALQUANT_electricScalarPotential} * {d LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings}];
                        Jacobian JAC_vol;
                        Integration Int;
                    }

                {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
                    For i In {0:INPUT_NumOfTSAElements-1}

                    {% for a in range(1,3) %}
                        {% for b in range(1,3) %}

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialMassFunctionNoDta<<a>>b<<b>>[
                                    <<temperatureArgument1>>,
                                    <<temperatureArgument2>>
                                ] * {d LOCALQUANT_electricScalarPotentialThinShell~{i + <<a>> - 1}} * {d LOCALQUANT_electricScalarPotentialThinShell~{i + <<b>> - 1}}
                            ];
                            {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            {% else %}
                            In DOM_allInsulationSurface_WithoutNotch;
                            {% endif %}
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialStiffnessFunctiona<<a>>b<<b>>[
                                    <<temperatureArgument1>>,
                                    <<temperatureArgument2>>
                                ] * {LOCALQUANT_electricScalarPotentialThinShell~{i + <<a>> - 1}} * {LOCALQUANT_electricScalarPotentialThinShell~{i + <<b>> - 1}}
                            ];
                            {% if dm.magnet.solve.contactLayer.resistivity == "perfectlyInsulating" %}
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            {% else %}
                            In DOM_allInsulationSurface_WithoutNotch;
                            {% endif %}
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        {% endfor %}
                    {% endfor %}
                    EndFor

                {% endif %}
                }
            }
        
            {
                Name RESULT_electricScalarPotential;
                Value{
                    Local{
                        [{LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings {% if not dm.magnet.geometry.contactLayer.thinShellApproximation %}, DOM_insulation{% endif %} }];
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_currentFromElectricScalarPotential;
                Value{
                    Local{
                        [1./rho[<<rhoArguments>>] * {d LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings {% if not dm.magnet.geometry.contactLayer.thinShellApproximation %}, DOM_insulation{% endif %} }];
                        Jacobian JAC_vol;
                    }
                }
            }

        {% for idx, voltage_tap_pair in enumerate(dm.quench_detection.voltage_tap_pairs) %}

            {
                Name RESULT_resistiveVoltage_<<idx>>; // voltages in cuts
                Value{
                    Term {
                    Type Global;
                        [ $currentThroughCoil/$conductance * ($potential_<<voltage_tap_pair[0]>> - $potential_<<voltage_tap_pair[1]>>) ];
                        In DOM_dummyPrintResistiveVoltages;
                    }
                }
            }

        {% endfor %}
        }
    }
{% endif %}
}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation {
        }
    }
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %} {
        Name POSTOP_debug;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // 3D magnetic field vector field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_debug",
                onElementsOf="DOM_allInsulationSurface",
                name="Debug Variable",
                fileName="debug"
            )|indent(12)>>
        }
    }
    {% endif %}
{% if dm.magnet.solve.type in ["electromagnetic", "weaklyCoupled", "stronglyCoupled"] %}
    {
        Name POSTOP_magneticField;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // 3D magnetic field vector field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_magneticField",
                onElementsOf="DOM_total",
                name="Magnetic Field [T]",
                fileName="MagneticField"
            )|indent(12)>>
        {% if dm.magnet.solve.boundaryConditions != "vanishingTangentialElectricField" %}
        <<POSTOPERATION_printResults(
            quantity="RESULT_imposedAxialField",
            onElementsOf="DOM_total",
            name="Imposed Magnetic Field [T]",
            fileName="ImposedMagneticField"
        )|indent(12)>>

        <<POSTOPERATION_printResults(
            quantity="RESULT_reactionField",
            onElementsOf="DOM_total",
            name="Magnetic Reaction Field [T]",
            fileName="MagneticReactionField"
        )|indent(12)>>
        {% endif %}
        }
    }
    {
        Name POSTOP_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // 3D magnetic field magnitude scalar field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_magnitudeOfMagneticField",
                onElementsOf="DOM_total",
                name="The Magnitude of the Magnetic Field [T]",
                fileName="MagneticFieldMagnitude"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_currentDensity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // 3D current density vector field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_currentDensity",
                onElementsOf="DOM_allConducting",
                name="Current Density [A/m^2]",
                fileName="CurrentDensity"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // 3D current density vector field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_magnitudeOfCurrentDensity",
                onElementsOf="DOM_allConducting",
                name="The Magnitude of the Current Density [A/m^2]",
                fileName="CurrentDensityMagnitude"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_arcLength;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_arcLength",
                onElementsOf="DOM_allWindings",
                name="Arc Length [m]",
                fileName="arcLength",
                atGaussPoints=True,
                lastTimeStepOnly=True
            )|indent(12)>>
        }
    }
    {% if dm.magnet.geometry.contactLayer.thinShellApproximation %}
    {
        Name POSTOP_arcLengthContactLayer;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_arcLengthContactLayer",
                onElementsOf="DOM_allInsulationSurface",
                name="Arc Length Contact Layer [m]",
                fileName="arcLengthContactLayer",
                lastTimeStepOnly=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_turnNumberContactLayer;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_turnNumberContactLayer",
                onElementsOf="DOM_allInsulationSurface",
                name="Turn number contact layer [m]",
                fileName="turnNumberContactLayer",
                lastTimeStepOnly=True
            )|indent(12)>>
        }
    }
    {% endif %}
    {
        Name POSTOP_turnNumber;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_turnNumber",
                onElementsOf="DOM_allWindings",
                name="Turn number [m]",
                fileName="turnNumber",
                atGaussPoints=True,
                lastTimeStepOnly=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_resistiveHeating;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Resistive heating:
            <<POSTOPERATION_printResults(
                quantity="RESULT_resistiveHeating",
                onElementsOf="DOM_allWindings",
                name="Resistive Heating Windings [W/m^3]",
                fileName="ResistiveHeating_Windings",
                atGaussPoints=True
            )|indent(12)>>

            <<POSTOPERATION_printResults(
                quantity="RESULT_resistiveHeating",
                onElementsOf="DOM_allConductingWithoutWindings",
                name="Resistive Heating Without Windings [W/m^3]",
                fileName="ResistiveHeating"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_resistivity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Resistive heating:
            <<POSTOPERATION_printResults(
                quantity="RESULT_resistivity",
                onElementsOf="DOM_allWindings",
                name="Resistivity Windings [Ohm*m]",
                fileName="Resistivity_Windings",
                atGaussPoints=True
            )|indent(12)>>

            <<POSTOPERATION_printResults(
                quantity="RESULT_resistivity",
                onElementsOf="DOM_allConductingWithoutWindings",
                name="Resistivity Without Windings [Ohm*m]",
                fileName="Resistivity_ConductingWithoutWindings"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_inductance;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Current at the cut:
            <<POSTOPERATION_printResults(
                quantity="RESULT_inductance",
                onGlobal=True,
                format="TimeTable",
                name="Inductance [H]",
                fileName="Inductance",
                lastTimeStepOnly=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_timeConstant;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Current at the cut:
            <<POSTOPERATION_printResults(
                quantity="RESULT_axialComponentOfTheMagneticField",
                onPoint=[0, 0, 0],
                name="The Magnitude of the Magnetic Field [T]",
                fileName="axialComponentOfTheMagneticFieldForTimeConstant",
                format="TimeTable"
            )|indent(12)>>
        }
    }
    {% if dm.magnet.solve.winding.superConductor and not dm.magnet.solve.winding.resistivity %}
    {
        Name POSTOP_criticalCurrentDensity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Critical current density:
            <<POSTOPERATION_printResults(
                quantity="RESULT_criticalCurrentDensity",
                onElementsOf="DOM_allWindings",
                name="Critical Current Density [A/m^2]",
                fileName="CriticalCurrentDensity",
                atGaussPoints=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_criticalCurrent;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Critical current:
            <<POSTOPERATION_printResults(
                quantity="RESULT_criticalCurrent",
                onElementsOf="DOM_allWindings",
                name="Critical Current [A]",
                fileName="CriticalCurrent",
                atGaussPoints=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_currentSharingIndex;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Critical current:
            <<POSTOPERATION_printResults(
                quantity="RESULT_currentSharingIndex",
                onElementsOf="DOM_allWindings",
                name="Current Sharing Index [-]",
                fileName="currentSharingIndex",
                atGaussPoints=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_jHTS;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Current density in HTS layer:
            <<POSTOPERATION_printResults(
                quantity="RESULT_jHTS",
                onElementsOf="DOM_allWindings",
                name="Current Density in HTS Layer [A/m^2]",
                fileName="jHTS",
                atGaussPoints=True
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_jHTSOverjCritical;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Normalized HTS current density:
            <<POSTOPERATION_printResults(
                quantity="RESULT_jHTSOverjCritical",
                onElementsOf="DOM_allWindings",
                name="(HTS Current Density)/(Critical Current Density)",
                fileName="HTSCurrentDensityOverCriticalCurrentDensity",
                atGaussPoints=True
            )|indent(12)>>
        }
    }
    // {
    //     Name POSTOP_Ic;
    //     NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
    //     LastTimeStepOnly 1;
    //     Operation {
    //         Print[
    //             RESULT_criticalCurrent,
    //             OnElementsOf DOM_allWindings,
    //             StoreMinInRegister 1
    //         ];
    //     }
    // }
    {
        Name POSTOP_I;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                Format Table,
                StoreInVariable $I
            ];
        }
    }
    {% endif %}
    {
        Name POSTOP_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Voltage at the cut:
            <<POSTOPERATION_printResults(
                quantity="RESULT_voltageBetweenTerminals",
                onRegion="DOM_terminalCut",
                format="TimeTable",
                name="Voltage [V]",
                fileName="VoltageBetweenTerminals"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_currentThroughCoil;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Current at the cut:
            <<POSTOPERATION_printResults(
                quantity="RESULT_currentThroughCoil",
                onRegion="DOM_terminalCut",
                format="TimeTable",
                name="Current [A]",
                fileName="CurrentThroughCoil"
            )|indent(12)>>
        }
    }
{% endif %}
{% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled", "thermal"] %}
    {
        Name POSTOP_maximumTemperature;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation {
            Print[ RESULT_temperature, 
                    OnElementsOf DOM_powered, 
                    StoreMaxInRegister 999, 
                    Format Table,
                    LastTimeStepOnly 1, 
                    SendToServer "No",
                    File "maximumTemperature_dump.txt"
                ] ;
            // We can print the maximum temperature at any region that is part
            // of the thermal domain since the `StoreMaxInRegister` command
            // already searches all of the thermal region for the maximum and
            //populates the same value for all physical regions of the thermal 
            // domain.
            // Printing in just one domain makes the parsing of the output easier.
            <<POSTOPERATION_printResults(
                quantity="RESULT_maximumTemperature",
                onRegion="Region[%d]" % rm.powered['Pancake3D'].vol.numbers[0],
                format="TimeTable",
                name="Maximum temperature [K]",
                appendToExistingFile=True,
                lastTimeStepOnly=True,
                fileName="maximumTemperature(TimeSeriesPlot)",
                noTitle=True
            )|indent(12)>>
        }
    }
{% endif %}
{% if dm.magnet.solve.type in ["thermal", "weaklyCoupled", "stronglyCoupled"] %}
    {
        Name POSTOP_temperature;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation {
            // 3D temperature scalar field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_temperature",
                onElementsOf="DOM_thermal",
                name="Temperature [K]",
                fileName="Temperature"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_heatFlux;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation {
            // 3D temperature scalar field:
            <<POSTOPERATION_printResults(
                quantity="RESULT_heatFlux",
                onElementsOf="DOM_thermal",
                name="Heat Flux [W/m^2]",
                fileName="HeatFlux"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_thermalConductivity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Thermal conductivity:
            <<POSTOPERATION_printResults(
                quantity="RESULT_thermalConductivity",
                onElementsOf="DOM_allWindings",
                name="Thermal Conductivity Windings [W/(m*K)]",
                fileName="thermalConductivity_Windings",
                atGaussPoints=True
            )|indent(12)>>

            <<POSTOPERATION_printResults(
                quantity="RESULT_thermalConductivity",
                onElementsOf="DOM_allConductingWithoutWindings",
                name="Thermal Conductivity Without Windings [W/(m*K)]",
                fileName="thermalConductivity_ConductingWithoutWindings"
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_specificHeatCapacity;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            // Specific heat:
            <<POSTOPERATION_printResults(
                quantity="RESULT_specificHeatCapacity",
                onElementsOf="DOM_thermal",
                name="Specific Heat Capacity [J/(kg*K)]",
                fileName="specificHeatCapacity"
            )|indent(12)>>
        }
    }
{% endif %}
{% if dm.magnet.postproc.timeSeriesPlots is not none %}
    {% for timeSeriesPlot in dm.magnet.postproc.timeSeriesPlots %}
    {
        Name POSTOP_timeSeriesPlot_<<timeSeriesPlot.quantity>>;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
    {% if timeSeriesPlot.quantity == "maximumTemperature" %}
            
    {% elif timeSeriesPlot.quantity in ["currentThroughCoil", "voltageBetweenTerminals"] %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onRegion="DOM_terminalCut",
                format="TimeTable",
                fileName=timeSeriesPlot.fileName
            )|indent(12)>>
    {% elif timeSeriesPlot.quantity in ["totalResistiveHeating", "magneticEnergy"] %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onGlobal=True,
                format="TimeTable",
                fileName=timeSeriesPlot.fileName
            )|indent(12)>>
    {% elif timeSeriesPlot.quantity == "cryocoolerAveragePower" %}
        {% if dm.magnet.solve.type != "electromagnetic" %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onGlobal=True,
                format="TimeTable",
                fileName=timeSeriesPlot.fileName
            )|indent(12)>>
        {% endif %}
    {% elif timeSeriesPlot.quantity == "cryocoolerAverageTemperature" %}
        {% if dm.magnet.solve.type != "electromagnetic" %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onGlobal=True,
                format="TimeTable",
                fileName=timeSeriesPlot.fileName,
            )|indent(12)>>
        {% endif %}
    {% elif timeSeriesPlot.position.x is none %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onGlobal=True,
                format="TimeTable",
                fileName=timeSeriesPlot.fileName
            )|indent(12)>>
    {% else %}
            <<POSTOPERATION_printResults(
                quantity=timeSeriesPlot.getdpQuantityName,
                onPoint=[timeSeriesPlot.position.x, timeSeriesPlot.position.y, timeSeriesPlot.position.z],
                format="TimeTable",
                fileName=timeSeriesPlot.fileName
            )|indent(12)>>
    {% endif %}
        }
    }
    {% endfor %}
{% endif %}
{% if dm.magnet.postproc is not none %}
{% if dm.magnet.postproc.magneticFieldOnCutPlane is not none %}
    {
        Name POSTOP_magneticFieldOnCutPlaneVector;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_magneticField",
                onSection=dm.magnet.postproc.magneticFieldOnCutPlane.onSection,
                fileName="magneticFieldOnCutPlaneVector",
                depth=1
            )|indent(12)>>
        }
    }
    {
        Name POSTOP_magneticFieldOnCutPlaneMagnitude;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation{
            <<POSTOPERATION_printResults(
                quantity="RESULT_magnitudeOfMagneticField",
                onSection=dm.magnet.postproc.magneticFieldOnCutPlane.onSection,
                fileName="magneticFieldOnCutPlaneMagnitude",
                depth=1
            )|indent(12)>>
        }
    }
{% endif %}
{% endif %}
    // convergence criteria as postoperations:
{% for tolerance in (dm.magnet.solve.nonlinearSolver.postOperationTolerances + dm.magnet.solve.time.adaptiveSteppingSettings.postOperationTolerances)|unique(attribute="quantity") %}
    {% if tolerance.quantity in ["totalResistiveHeating", "magneticEnergy"] %}
    {
        // Does this work properly without explicitly specifying AtGaussPoints? Expectation: yes, since it should do the integration over the whole by Gaussian Integration
        Name POSTOP_CONV_<<tolerance.quantity>>;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_<<tolerance.quantity>>, OnGlobal];
        }
    }
    {% elif tolerance.quantity in ["voltageBetweenTerminals", "currentThroughCoil"] %}
    {
        Name POSTOP_CONV_<<tolerance.quantity>>;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_<<tolerance.quantity>>, OnRegion DOM_terminalCut];
        }
    }
    {% elif tolerance.quantity == "maximumTemperature" %}
        {% if dm.magnet.solve.type in ["weaklyCoupled", "stronglyCoupled", "thermal"] %}
    {
        Name POSTOP_CONV_maximumTemperature;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        Operation {
            Print[ RESULT_temperature,
                OnElementsOf DOM_powered,
                StoreMaxInRegister 999,
                Format Table,
                LastTimeStepOnly 1,
                SendToServer "No"
             ] ;

            <<POSTOPERATION_printResults(
                quantity="RESULT_maximumTemperature",
                onRegion="DOM_powered",
                format="TimeTable",
                name="Maximum temperature [K]",
                appendToExistingFile=True,
                lastTimeStepOnly=True,
                )|indent(12)>>
        } 
    }
        {% endif %}
    {% else %}
    {
        Name POSTOP_CONV_<<tolerance.quantity>>;
        NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_<<tolerance.quantity>>,
                OnPoint {<<tolerance.position.x>>, <<tolerance.position.y>>, <<tolerance.position.z>>},
                StoreInVariable $test
            ];
        }
    }
    {% endif %}
{% endfor %}

{% if dm.quench_detection.voltage_tap_pairs and dm.magnet.solve.voltageTapPositions and not dm.magnet.solve.type == "thermal" %}
{
    Name POSTOP_electricScalarPotential_<<dm.magnet.solve.type>>;
    NameOfPostProcessing POSTPRO_<<dm.magnet.solve.type>>;
    Operation {
        <<POSTOPERATION_printResults(
            quantity="RESULT_currentThroughCoil",
            onRegion="DOM_terminalCut",
            format="TimeTable",
            name="Current [A]",
            fileName="CurrentThroughCoil_quenchDetection",
            lastTimeStepOnly=True,
            appendToExistingFile=True,
            storeInVariable="currentThroughCoil",
            noTitle=True
        )|indent(12)>>

    {% if dm.magnet.solve.EECircuit.enable %}
        <<POSTOPERATION_printResults(
            quantity="RESULT_current_DOM_currentSource",
            onRegion="DOM_currentSource",
            format="TimeTable",
            name="Current [A]",
            fileName="Current_DOM_currentSource",
            lastTimeStepOnly=True,
            appendToExistingFile=True,
            noTitle=True
        )|indent(12)>>

        <<POSTOPERATION_printResults(
            quantity="RESULT_resistance_DOM_switches",
            onRegion="Region[{DOM_switchCrowbar, DOM_switchEE}]",
            format="TimeTable",
            name="Current [A]",
            fileName="Resistance_DOM_switches",
            lastTimeStepOnly=True,
            appendToExistingFile=True,
            noTitle=True
        )|indent(12)>>
    {% endif %}
    }
}

{
    Name POSTOP_electricScalarPotential;
    NameOfPostProcessing POSTPRO_electricScalarPotential;
    Operation {

        <<POSTOPERATION_printResults(
            quantity="RESULT_conductance",
            onGlobal=True,
            format="TimeTable",
            fileName="conductance",
            name="Conductance [S]",
            lastTimeStepOnly=True,
            appendToExistingFile=True,
            storeInVariable="conductance"
            )|indent(12)>>


        {% for idx, potential_point in enumerate(dm.magnet.solve.voltageTapPositions) %}
            <<POSTOPERATION_printResults(
                quantity="RESULT_electricScalarPotential",
                onPoint=[potential_point.x, potential_point.y, potential_point.z],
                name="Potential at point [" + str(potential_point.x)  + ", " + str(potential_point.y) + ", " + str(potential_point.z) + "] [V]",
                fileName="electricScalarPotential_" + str(idx),
                format="TimeTable",
                lastTimeStepOnly=True,
                appendToExistingFile=True,
                storeInVariable="potential_" + str(idx)
            )|indent(12)>>
        {% endfor %}

        // loop over voltage_tap_pairs to write voltage
        {% for idx, voltage_tap_pair in enumerate(dm.quench_detection.voltage_tap_pairs) %}
            <<POSTOPERATION_printResults(
                quantity="RESULT_resistiveVoltage_" + str(idx),
                onRegion="DOM_dummyPrintResistiveVoltages",
                name="Resistive voltage between voltage tap " + str(voltage_tap_pair[0])  + " and " + str(voltage_tap_pair[1]) + " [V]",
                fileName="resistiveVoltage_" + str(idx),
                format="TimeTable",
                lastTimeStepOnly=True,
                appendToExistingFile=True,
                storeInVariable="voltage_" + str(idx),
                noTitle=True
            )|indent(12)>>
        {% endfor %}

        Print[RESULT_electricScalarPotential, OnElementsOf Region[{DOM_terminals, DOM_allWindings {% if not dm.magnet.geometry.contactLayer.thinShellApproximation %}, DOM_insulation{% endif %} }], File "electricScalarPotential.pos"];
        //Print[RESULT_currentFromElectricScalarPotential, OnElementsOf DOM_allWindings, File "currentFromElectricScalarPotential.pos", AtGaussPoints 6];
    }
}
{% endif %}
}
