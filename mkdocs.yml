site_name: FiQuS
site_description: Documentation for STEAM FiQuS
site_author: STEAM Team
site_url: https://steam-fiqus.docs.cern.ch/
repo_name: GitLab
repo_url: https://gitlab.cern.ch/steam/fiqus
edit_uri: -/tree/master/docs
theme:
  name: material
  font: false
  palette: 
    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 
        name: Switch to dark mode

    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
plugins:
  - git-revision-date-localized
  - search
  - mkdocstrings:
      default_handler: python
      handlers:
        python:
          options:
            docstring_style: sphinx
extra_javascript: 
  - javascripts/katex.js
  - https://cdn.jsdelivr.net/npm/katex@0.16.7/dist/katex.min.js
  - https://cdn.jsdelivr.net/npm/katex@0.16.7/dist/contrib/auto-render.min.js

extra_css:
  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/katex.min.css
  - stylesheets/fonts.css

markdown_extensions:
  - markdown_include.include
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - admonition
  - pymdownx.inlinehilite
  - pymdownx.superfences
nav:
  - "Introduction": index.md
  - "Installation": installation.md
  - "Source code":
      - "Main FiQuS": modules/mainfiqus.md
      - "Main Magnets":
          - "CCT": modules/mains/maincct.md
          - "Multipole": modules/mains/mainmultipole.md
          - "Pancake3D": modules/mains/mainPancake3D.md
      - "Geometry Generators":
          - "CCT": modules/geom_generators/geometryCCT.md
          - "Multipole": modules/geom_generators/geometryMultipole.md
          - "Pancake3D": modules/geom_generators/geometryPancake3D.md
      - "Mesh Generators":
          - "Pancake3D": modules/mesh_generators/meshPancake3D.md
      - " Templates for .pro files":
          - "Overview": modules/pro_templates//overview.md
          - "Combined templates":
              - "CCT": modules/pro_templates/combined/cct_template.md
              - "Multipole": modules/pro_templates/combined/multipole_template.md
          - "Separated templates":
              - "Readme": modules/pro_templates/separated/separated_readme.md
