# Wire-in-Channel Simulation Model

This model simulates a wire-in-channel geometry. 

## Wire Dimensions

- Channel height: 2.59mm
- Channel width: 3.88mm
- Central wire diameter: 1.9mm
- 84 filaments of diameter 0.156mm
- Filament center-to-center distance: 0.167mm

## Running the Model

1. Clone the fiqus repository (https://gitlab.cern.ch/steam/fiqus).
2. Download the CERNGetDP 2024.5.0 release from (https://gitlab.cern.ch/steam/cerngetdp/-/releases/2024.5.0).
    - Create a settings file in the fiqus directory with the full path to the CERNGetDP executable. The settings file should be named `settings.{your username here}.yaml` and should contain something like the following:
    ```yaml
    GetDP_path: C:\...\getdp_2024.5.0.exe # full path to GetDP executable
    ```
    - The username in the filename is the one returned by `getpass.getuser()` in Python.
3. Copy the `run_fiqus.py` script to the fiqus directory.
    - Running the `run_fiqus.py` script will generate output files according to the run-type specified in the input `.yaml` file.
    - Output files will be saved in the output directory: `tests/_outputs/CAC_WireInChannel/...`

## Input Parameters

The input parameters are specified in the input `.yaml` file located in `tests/_inputs/CAC_WireInChannel/CAC_WireInChannel.yaml`. The input parameters are structured into geometry, mesh, solve and postproc sections, corresponding to the main run-types.

The main run-types are: 'geometry_only', 'mesh_only', 'solve_only'. Geometry and mesh parameters can be left as default, or modified as needed.

### Important Solve Parameters

- The main excitation types are: 'sine', 'piecewise_linear' or 'sine_with_DC'. 'sine_with_DC' can only be used if initializing from a previous solution ramped up to the DC component.
- The magnetic field at the last timestep can be saved to a `.pos` file by setting `save_last_magnetic_field` to a 'filename' (None if the solution should not be saved). This can be used to initialize a new simulation and is useful, for example, to simulate sine excitations superimposed on a DC excitation. In this case we would first ramp up the DC component and save the magnetic field at the last timestep, then use this field to initialize the simulation with the sine + DC component.
- Initial conditions can be read from a previous solution by setting `init_from_pos_file` to True, along with the .pos filename to init from.