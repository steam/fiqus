general:
  magnet_name: TEST_CAC_Strand_adaptiveMesh

run:
  type: mesh_only
  geometry: 1
  mesh: 1 
  solution: 1
  launch_gui: False
  overwrite: True

magnet:
  type: CACStrand  

  geometry:
    io_settings:
      save:
        save_to_yaml: True
        filename: GeometryModel.yaml
    # filament_radius: 4.5e-05  # Radius of the filaments (m).
    hexagonal_filaments: False # True for hexagonal filaments, False for circular filaments.
    # number_of_filaments: 18 # Number of filaments.
    filament_circular_distribution: True # Filament geometric distribution. True for circular, False for hexagonal.
    air_radius: 0.005 # Radius of the air region (m).
    # matrix:
    #   outer_radius: 0.0005  # Radius of the matrix (m).
    #   middle_radius: 0.00044611064 # Radius of middle partition of the matrix (m).
    #   inner_radius: 2e-05 # Radius of inner partition of the matrix (m).

  mesh:
    scaling_global: 2  # Global scaling factor for mesh size.
    filaments:
      boundary_mesh_size_ratio: 0.2  # Mesh size at filament boundaries, relative to filament radius.
      center_mesh_size_ratio: 0.45 # Mesh size at filament center, relative to filament radius.
      amplitude_dependent_scaling: True # Amplitude dependent scaling uses the expected field penetration distance in the filaments to determine the filament mesh. If the field penetration distance is low (i.e. for low field amplitudes) this feature increases mesh density in the region where the field is expected to penetrate.
      field_penetration_depth_scaling_factor: 3.0 # Scaling factor for estimate of field penetration depth, used for amplitude dependent scaling.
      desired_elements_in_field_penetration_region: 3.25 # Desired number of elements in the field penetration region.
    matrix:
      mesh_size_matrix_ratio_inner: 1.3  # Mesh size at matrix center, relative to filament radius.
      mesh_size_matrix_ratio_middle: 0.9 # Mesh size at matrix middle partition, relative to filament radius.
      mesh_size_matrix_ratio_outer: 1.0 # Mesh size at matrix outer boundary, relative to filament radius.
      interpolation_distance_from_filaments_ratio: 1.5 # The mesh size is interpolated from the filament boundaries, into the matrix, over a given distance. This parameter determines the distance over which the mesh size is interpolated, relative to the filament radius.
      rate_dependent_scaling_matrix: True # Rate dependent scaling uses the expected skin depth in the matrix to determine the matrix mesh. If the skin depth is low (i.e. for high frequencies) this feature increases mesh density in the region where the current is expected to flow.
      skindepth_scaling_factor: 2.5 # Scaling factor for estimate of skin depth, used for rate dependent scaling.
      desired_elements_in_skindepth: 6.0 # Desired number of elements in the skin depth region.
    air:
      max_mesh_size_ratio: 10.0  # Mesh size at the outer boundary of the air region, relative to filament radius.


  solve:
    pro_template: "ConductorAC_template.pro"
    conductor_name: conductor_1
    
    formulation_parameters:
      formulation: voltage_based # Currently, voltage_based is the only supported formulation
      dynamic_correction: True # In the voltage_based case, do we activate the dynamic correction? (if True, more expensive simulations!)
      compute_temperature: False # Do we compute the temperature?
      two_ell_periodicity: True

    general_parameters:
      temperature: 1.9
      superconductor_linear: False # To set the superconductor as linear (for debugging purposes)
      
    initial_conditions:
      init_from_pos_file: False
      pos_file_to_init_from: last_magnetic_field_4T_500A_dynCorMatrixOnly #init_linear_1000A_1T #init_1000A_1T

    source_parameters:
      source_type: sine #sine #piecewise
      boundary_condition_type: Natural

      sine:
        frequency: 40000
        field_amplitude: 0.01
        current_amplitude: 0
        field_angle: 90
        superimposed_DC:
          field_magnitude: 0
          current_magnitude: 0

      piecewise:
        source_csv_file: 
        times: [0, 1, 5]
        applied_fields_relative: [0, 1, 1]
        transport_currents_relative: [0, 1, 1]
        time_multiplier: 1
        applied_field_multiplier: 4
        transport_current_multiplier: 500

    numerical_parameters:
      sine:
        timesteps_per_period: 100
        number_of_periods_to_simulate: 0.5
      piecewise:
        time_to_simulate: 5
        timesteps_per_time_to_simulate: 500
        force_stepping_at_times_piecewise_linear: True


  postproc:
    save_last_magnetic_field: None # "last_magnetic_field_4T_500A_dynCorMatrixOnly"
    generate_pos_files : True

    plot_instantaneous_power:
      show: False
      title: 'Instantaneous Power (f=<<solve.source_parameters.sine.frequency>>, b=<<solve.source_parameters.sine.field_amplitude>>)'
      save: False
      save_file_name: 'instantaneous_power'

conductors:
  conductor_1:
    strand:
      type: Round
      fil_twist_pitch: 0.019
      number_of_filaments: 18
      filament_diameter: 9e-05 # Diameter of the filaments (m).
      diameter_core: 4.0e-05 # Diameter of the core (m).
      diameter_filamentary: 0.00089222128 # Diameter of the filamentary region (m).
      diameter: 0.001 # Diameter of the strand (m).
      
      material_superconductor: NbTi
      n_value_superconductor: 30
      ec_superconductor: 1e-4
      Cv_material_superconductor: CFUN_CvNbTi

      rho_material_stabilizer: CFUN_rhoCu
      RRR: 100
      Cv_material_stabilizer: CFUN_CvCu
    Jc_fit:
      type: Ic_A_NbTi
      Jc_5T_4_2K: 1.70732393e9 #3e9