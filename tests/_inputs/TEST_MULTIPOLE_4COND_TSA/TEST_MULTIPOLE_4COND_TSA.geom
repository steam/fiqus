Roxie_Data:
  iron:
    key_points: {}
    hyper_lines: {}
    hyper_areas: {}
    hyper_holes: {}
  coil:
    coils:
      1:
        type: common-block-coil
        poles:
          1:
            layers:
              1:
                windings:
                  1:
                    blocks:
                      1:
                        block_corners:
                          iH:
                            x: 0.0
                            y: 0.003142
                            z: null
                          iL:
                            x: 0.0
                            y: 0.0
                            z: null
                          oH:
                            x: 0.011
                            y: 0.003142
                            z: null
                          oL:
                            x: 0.011
                            y: 0.0
                            z: null
                        current_sign: 1
                        half_turns:
                          1:
                            corners:
                              insulated:
                                iH:
                                  x: -0.0001
                                  y: 0.001571
                                  z: null
                                iL:
                                  x: -0.0001
                                  y: -0.000084
                                  z: null
                                oH:
                                  x: 0.0111
                                  y: 0.001571
                                  z: null
                                oL:
                                  x: 0.0111
                                  y: -0.000084
                                  z: null
                              bare:
                                iH:
                                  x: 0.0
                                  y: 0.001487
                                  z: null
                                iL:
                                  x: 0.0
                                  y: 0.0
                                  z: null
                                oH:
                                  x: 0.011
                                  y: 0.001487
                                  z: null
                                oL:
                                  x: 0.011
                                  y: 0.0
                                  z: null
                            strand_groups:
                              1:
                                strand_positions:
                                  1:
                                    x: 0.00042308
                                    y: 0.00037175
                                    z: null
                                  2:
                                    x: 0.00126923
                                    y: 0.00037175
                                    z: null
                                  3:
                                    x: 0.00211538
                                    y: 0.00037175
                                    z: null
                                  4:
                                    x: 0.00296154
                                    y: 0.00037175
                                    z: null
                                  5:
                                    x: 0.00380769
                                    y: 0.00037175
                                    z: null
                                  6:
                                    x: 0.00465385
                                    y: 0.00037175
                                    z: null
                                  7:
                                    x: 0.0055
                                    y: 0.00037175
                                    z: null
                                  8:
                                    x: 0.00634615
                                    y: 0.00037175
                                    z: null
                                  9:
                                    x: 0.00719231
                                    y: 0.00037175
                                    z: null
                                  10:
                                    x: 0.00803846
                                    y: 0.00037175
                                    z: null
                                  11:
                                    x: 0.00888462
                                    y: 0.00037175
                                    z: null
                                  12:
                                    x: 0.00973077
                                    y: 0.00037175
                                    z: null
                                  13:
                                    x: 0.01057692
                                    y: 0.00037175
                                    z: null
                              2:
                                strand_positions:
                                  1:
                                    x: 0.00042308
                                    y: 0.00111525
                                    z: null
                                  2:
                                    x: 0.00126923
                                    y: 0.00111525
                                    z: null
                                  3:
                                    x: 0.00211538
                                    y: 0.00111525
                                    z: null
                                  4:
                                    x: 0.00296154
                                    y: 0.00111525
                                    z: null
                                  5:
                                    x: 0.00380769
                                    y: 0.00111525
                                    z: null
                                  6:
                                    x: 0.00465385
                                    y: 0.00111525
                                    z: null
                                  7:
                                    x: 0.0055
                                    y: 0.00111525
                                    z: null
                                  8:
                                    x: 0.00634615
                                    y: 0.00111525
                                    z: null
                                  9:
                                    x: 0.00719231
                                    y: 0.00111525
                                    z: null
                                  10:
                                    x: 0.00803846
                                    y: 0.00111525
                                    z: null
                                  11:
                                    x: 0.00888462
                                    y: 0.00111525
                                    z: null
                                  12:
                                    x: 0.00973077
                                    y: 0.00111525
                                    z: null
                                  13:
                                    x: 0.01057692
                                    y: 0.00111525
                                    z: null
                          2:
                            corners:
                              insulated:
                                iH:
                                  x: -0.0001
                                  y: 0.003226
                                  z: null
                                iL:
                                  x: -0.0001
                                  y: 0.001571
                                  z: null
                                oH:
                                  x: 0.0111
                                  y: 0.003226
                                  z: null
                                oL:
                                  x: 0.0111
                                  y: 0.001571
                                  z: null
                              bare:
                                iH:
                                  x: 0.0
                                  y: 0.003142
                                  z: null
                                iL:
                                  x: 0.0
                                  y: 0.001655
                                  z: null
                                oH:
                                  x: 0.011
                                  y: 0.003142
                                  z: null
                                oL:
                                  x: 0.011
                                  y: 0.001655
                                  z: null
                            strand_groups:
                              1:
                                strand_positions:
                                  1:
                                    x: 0.00042308
                                    y: 0.00220175
                                    z: null
                                  2:
                                    x: 0.00126923
                                    y: 0.00220175
                                    z: null
                                  3:
                                    x: 0.00211538
                                    y: 0.00220175
                                    z: null
                                  4:
                                    x: 0.00296154
                                    y: 0.00220175
                                    z: null
                                  5:
                                    x: 0.00380769
                                    y: 0.00220175
                                    z: null
                                  6:
                                    x: 0.00465385
                                    y: 0.00220175
                                    z: null
                                  7:
                                    x: 0.0055
                                    y: 0.00220175
                                    z: null
                                  8:
                                    x: 0.00634615
                                    y: 0.00220175
                                    z: null
                                  9:
                                    x: 0.00719231
                                    y: 0.00220175
                                    z: null
                                  10:
                                    x: 0.00803846
                                    y: 0.00220175
                                    z: null
                                  11:
                                    x: 0.00888462
                                    y: 0.00220175
                                    z: null
                                  12:
                                    x: 0.00973077
                                    y: 0.00220175
                                    z: null
                                  13:
                                    x: 0.01057692
                                    y: 0.00220175
                                    z: null
                              2:
                                strand_positions:
                                  1:
                                    x: 0.00042308
                                    y: 0.00294525
                                    z: null
                                  2:
                                    x: 0.00126923
                                    y: 0.00294525
                                    z: null
                                  3:
                                    x: 0.00211538
                                    y: 0.00294525
                                    z: null
                                  4:
                                    x: 0.00296154
                                    y: 0.00294525
                                    z: null
                                  5:
                                    x: 0.00380769
                                    y: 0.00294525
                                    z: null
                                  6:
                                    x: 0.00465385
                                    y: 0.00294525
                                    z: null
                                  7:
                                    x: 0.0055
                                    y: 0.00294525
                                    z: null
                                  8:
                                    x: 0.00634615
                                    y: 0.00294525
                                    z: null
                                  9:
                                    x: 0.00719231
                                    y: 0.00294525
                                    z: null
                                  10:
                                    x: 0.00803846
                                    y: 0.00294525
                                    z: null
                                  11:
                                    x: 0.00888462
                                    y: 0.00294525
                                    z: null
                                  12:
                                    x: 0.00973077
                                    y: 0.00294525
                                    z: null
                                  13:
                                    x: 0.01057692
                                    y: 0.00294525
                                    z: null
                    conductor_name: KEKI3RD2
                    conductors_number: 2
              2:
                windings:
                  2:
                    blocks:
                      2:
                        block_corners:
                          iH:
                            x: 0.011475
                            y: 0.003142
                            z: null
                          iL:
                            x: 0.011475
                            y: 0.0
                            z: null
                          oH:
                            x: 0.022475
                            y: 0.003142
                            z: null
                          oL:
                            x: 0.022475
                            y: 0.0
                            z: null
                        current_sign: 1
                        half_turns:
                          3:
                            corners:
                              insulated:
                                iH:
                                  x: 0.011375
                                  y: 0.001571
                                  z: null
                                iL:
                                  x: 0.011375
                                  y: -0.000084
                                  z: null
                                oH:
                                  x: 0.022575
                                  y: 0.001571
                                  z: null
                                oL:
                                  x: 0.022575
                                  y: -0.000084
                                  z: null
                              bare:
                                iH:
                                  x: 0.011475
                                  y: 0.001487
                                  z: null
                                iL:
                                  x: 0.011475
                                  y: 0.0
                                  z: null
                                oH:
                                  x: 0.022475
                                  y: 0.001487
                                  z: null
                                oL:
                                  x: 0.022475
                                  y: 0.0
                                  z: null
                            strand_groups:
                              1:
                                strand_positions:
                                  1:
                                    x: 0.01189808
                                    y: 0.00037175
                                    z: null
                                  2:
                                    x: 0.01274423
                                    y: 0.00037175
                                    z: null
                                  3:
                                    x: 0.01359038
                                    y: 0.00037175
                                    z: null
                                  4:
                                    x: 0.01443654
                                    y: 0.00037175
                                    z: null
                                  5:
                                    x: 0.01528269
                                    y: 0.00037175
                                    z: null
                                  6:
                                    x: 0.01612885
                                    y: 0.00037175
                                    z: null
                                  7:
                                    x: 0.016975
                                    y: 0.00037175
                                    z: null
                                  8:
                                    x: 0.01782115
                                    y: 0.00037175
                                    z: null
                                  9:
                                    x: 0.01866731
                                    y: 0.00037175
                                    z: null
                                  10:
                                    x: 0.01951346
                                    y: 0.00037175
                                    z: null
                                  11:
                                    x: 0.02035962
                                    y: 0.00037175
                                    z: null
                                  12:
                                    x: 0.02120577
                                    y: 0.00037175
                                    z: null
                                  13:
                                    x: 0.02205192
                                    y: 0.00037175
                                    z: null
                              2:
                                strand_positions:
                                  1:
                                    x: 0.01189808
                                    y: 0.00111525
                                    z: null
                                  2:
                                    x: 0.01274423
                                    y: 0.00111525
                                    z: null
                                  3:
                                    x: 0.01359038
                                    y: 0.00111525
                                    z: null
                                  4:
                                    x: 0.01443654
                                    y: 0.00111525
                                    z: null
                                  5:
                                    x: 0.01528269
                                    y: 0.00111525
                                    z: null
                                  6:
                                    x: 0.01612885
                                    y: 0.00111525
                                    z: null
                                  7:
                                    x: 0.016975
                                    y: 0.00111525
                                    z: null
                                  8:
                                    x: 0.01782115
                                    y: 0.00111525
                                    z: null
                                  9:
                                    x: 0.01866731
                                    y: 0.00111525
                                    z: null
                                  10:
                                    x: 0.01951346
                                    y: 0.00111525
                                    z: null
                                  11:
                                    x: 0.02035962
                                    y: 0.00111525
                                    z: null
                                  12:
                                    x: 0.02120577
                                    y: 0.00111525
                                    z: null
                                  13:
                                    x: 0.02205192
                                    y: 0.00111525
                                    z: null
                          4:
                            corners:
                              insulated:
                                iH:
                                  x: 0.011375
                                  y: 0.003226
                                  z: null
                                iL:
                                  x: 0.011375
                                  y: 0.001571
                                  z: null
                                oH:
                                  x: 0.022575
                                  y: 0.003226
                                  z: null
                                oL:
                                  x: 0.022575
                                  y: 0.001571
                                  z: null
                              bare:
                                iH:
                                  x: 0.011475
                                  y: 0.003142
                                  z: null
                                iL:
                                  x: 0.011475
                                  y: 0.001655
                                  z: null
                                oH:
                                  x: 0.022475
                                  y: 0.003142
                                  z: null
                                oL:
                                  x: 0.022475
                                  y: 0.001655
                                  z: null
                            strand_groups:
                              1:
                                strand_positions:
                                  1:
                                    x: 0.01189808
                                    y: 0.00220175
                                    z: null
                                  2:
                                    x: 0.01274423
                                    y: 0.00220175
                                    z: null
                                  3:
                                    x: 0.01359038
                                    y: 0.00220175
                                    z: null
                                  4:
                                    x: 0.01443654
                                    y: 0.00220175
                                    z: null
                                  5:
                                    x: 0.01528269
                                    y: 0.00220175
                                    z: null
                                  6:
                                    x: 0.01612885
                                    y: 0.00220175
                                    z: null
                                  7:
                                    x: 0.016975
                                    y: 0.00220175
                                    z: null
                                  8:
                                    x: 0.01782115
                                    y: 0.00220175
                                    z: null
                                  9:
                                    x: 0.01866731
                                    y: 0.00220175
                                    z: null
                                  10:
                                    x: 0.01951346
                                    y: 0.00220175
                                    z: null
                                  11:
                                    x: 0.02035962
                                    y: 0.00220175
                                    z: null
                                  12:
                                    x: 0.02120577
                                    y: 0.00220175
                                    z: null
                                  13:
                                    x: 0.02205192
                                    y: 0.00220175
                                    z: null
                              2:
                                strand_positions:
                                  1:
                                    x: 0.01189808
                                    y: 0.00294525
                                    z: null
                                  2:
                                    x: 0.01274423
                                    y: 0.00294525
                                    z: null
                                  3:
                                    x: 0.01359038
                                    y: 0.00294525
                                    z: null
                                  4:
                                    x: 0.01443654
                                    y: 0.00294525
                                    z: null
                                  5:
                                    x: 0.01528269
                                    y: 0.00294525
                                    z: null
                                  6:
                                    x: 0.01612885
                                    y: 0.00294525
                                    z: null
                                  7:
                                    x: 0.016975
                                    y: 0.00294525
                                    z: null
                                  8:
                                    x: 0.01782115
                                    y: 0.00294525
                                    z: null
                                  9:
                                    x: 0.01866731
                                    y: 0.00294525
                                    z: null
                                  10:
                                    x: 0.01951346
                                    y: 0.00294525
                                    z: null
                                  11:
                                    x: 0.02035962
                                    y: 0.00294525
                                    z: null
                                  12:
                                    x: 0.02120577
                                    y: 0.00294525
                                    z: null
                                  13:
                                    x: 0.02205192
                                    y: 0.00294525
                                    z: null
                    conductor_name: KEKI3RD2
                    conductors_number: 2
        bore_center:
          x: 0.0
          y: 0.0
          z: null
    physical_order: [{coil: 1, pole: 1, layer: 1, winding: 1, block: 1}, {coil: 1, pole: 1, layer: 2, winding: 2, block: 2}]
  wedges: {}
