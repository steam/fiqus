general:
  magnet_name: TEST_MULTIPOLE_SMC_TSA_withQH
run:
  type: start_from_yaml
  geometry: null
  mesh: null
  solution: null
  launch_gui: false
  overwrite: false
  comments: ''
  verbosity_Gmsh: 5
  verbosity_GetDP: 5
  verbosity_FiQuS: true
magnet:
  type: multipole
  geometry:
    electromagnetics:
      with_iron_yoke: true
      with_wedges: true
    thermal:
      with_iron_yoke: false
      with_wedges: true
      use_TSA: true
      correct_block_coil_tsa_checkered_scheme: true
  mesh:
    electromagnetics:
      conductors:
        transfinite:
            enabled_for: null
            curve_target_size_height: 0.0125
            curve_target_size_width: 0.0125
        field:
          enabled: true
          SizeMin: 0.035
          SizeMax: 0.5
          DistMin: 0.05
          DistMax: 0.05
      wedges:
        transfinite:
            enabled_for: null
            curve_target_size_height: 0.005
            curve_target_size_width: 0.005
        field:
          enabled: true
          SizeMin: 0.035
          SizeMax: 0.5
          DistMin: 0.05
          DistMax: 0.05
      iron_field:
        enabled: true
        SizeMin: 0.05
        SizeMax: 0.25
        DistMin: 0.01
        DistMax: 0.25
      bore_field:
        enabled: true
        SizeMin: 0.05
        SizeMax: 0.5
        DistMin: 0.01
        DistMax: 0.025
    thermal:
      conductors:
        transfinite:
            enabled_for: curves_and_surfaces # none, curves, curves_and_surfaces
            curve_target_size_height: 1
            curve_target_size_width: 1
        field:
          enabled: false
          SizeMin: 0.0013
          SizeMax: 0.0013
          DistMin: 0.0026
          DistMax: 0.0052
      wedges:
        transfinite:
            enabled_for: curves_and_surfaces # none, curves, curves_and_surfaces
            curve_target_size_height: 1
            curve_target_size_width: 1
        field:
          enabled: false
          SizeMin: 0.0013
          SizeMax: 0.0013
          DistMin: 0.0026
          DistMax: 0.0052
      insulation:
        global_size: 0.00002 # replaces insulation constant size  
        TSA: 
          minimum_discretizations: 1
          global_size_QH: 0.00002
          minimum_discretizations_QH: 1
      isothermal_conductors: false
      isothermal_wedges: false
  solve:
    wedges:
      material: Cu
      RRR: 100.0
      T_ref_RRR_high: 273.0
    electromagnetics:
      solve_type: stationary
      non_linear_solver:
        rel_tolerance: 1E-6
        abs_tolerance: 1E-4
        relaxation_factor: 0.9
        max_iterations: 20
        norm_type: LinfNorm
    thermal:
      solve_type: transient
      insulation_TSA:
        block_to_block:
          material: kapton
      He_cooling:
        enabled: True
        sides: outer
        heat_transfer_coefficient: CFUN_hHe_T_THe
      non_linear_solver:
        rel_tolerance: 1E-3
        abs_tolerance: 1e-1
        relaxation_factor: 0.7
        max_iterations: 20
        norm_type: LinfNorm
      time_stepping:
        initial_time: 0.
        final_time: 0.005
        initial_time_step: 1E-10
        min_time_step: 1E-12
        max_time_step: 0.1
        breakpoints: [ ]
        integration_method: Euler
        rel_tol_time: 1E-3
        abs_tol_time: 1e-1
        norm_type: LinfNorm
        stop_temperature: 300
      init_temperature: 1.9
      enforce_init_temperature_as_minimum: false
  postproc:
    electromagnetics:
      output_time_steps_pos: true
      output_time_steps_txt: true
      save_pos_at_the_end: false
      save_txt_at_the_end: false
      compare_to_ROXIE: null
      plot_all: null
      variables: [ b ]
      volumes: [ omega ]
    thermal:
      output_time_steps_pos: true
      output_time_steps_txt: true
      save_pos_at_the_end: false
      save_txt_at_the_end: false
      take_average_conductor_temperature: true
      plot_all: null
      variables: [T]
      volumes: [conducting]
circuit:
  R_circuit: 0.0
  L_circuit: null
  R_parallel: null
power_supply:
  I_initial: 14500.0
  t_off: 0.0
  t_control_LUT: [-0.02, 0.0, 0.01]
  I_control_LUT: [14500.0, 14500.0, 0.0]
  R_crowbar: 0.0
  Ud_crowbar: 0.0
quench_protection:
  energy_extraction:
    t_trigger: 0.005
    R_EE: 0.03
    power_R_EE: null
    L: null
    C: null
  quench_heaters:
    N_strips: 4
    t_trigger: [0.0, 0.0, 0.0, 0.0]
    U0: [450.0, 450.0, 450.0, 450.0]
    C: [0.0141, 0.0141, 0.0141, 0.0141]
    R_warm: [2.94681, 2.94681, 2.94681, 2.94681]
    w: [0.01957, 0.01957, 0.01957, 0.01957]
    h: [2.5e-05, 2.5e-05, 2.5e-05, 2.5e-05]
    h_ins: [[5e-05], [5e-05], [5e-05], [5e-05]]
    type_ins: [[kapton], [kapton], [kapton], [kapton]]
    h_ground_ins: [[0.0005], [0.0005], [0.0005], [0.0005]]
    type_ground_ins: [[kapton], [kapton], [kapton], [kapton]]
    l: [0.3, 0.3, 0.3, 0.3]
    l_copper: [0.2641, 0.2641, 0.2641, 0.2641]
    l_stainless_steel: [0.05, 0.05, 0.05, 0.05]
    ids: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]
    turns: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121]
    turns_sides: [o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o]
  cliq:
    t_trigger: 99999.0
    current_direction: [1]
    sym_factor: null
    N_units: 1
    U0: 100.0
    C: 0.04
    R: 0.025
    L: null
    I0: null
  esc:
    t_trigger: [99999.0]
    U0: [0.0]
    C: [0.0]
    R_unit: [0.0]
    R_leads: [0.0]
    Ud_Diode: [0.0]
quench_detection:
  voltage_thresholds: null
  discrimination_times: null
  voltage_tap_pairs: null
conductors:
  SMC11T150:
    version: null
    case: null
    state: null
    cable:
      type: Rutherford
      n_strands: 40
      n_strand_layers: 2
      n_strands_per_layers: 20
      bare_cable_width: 0.014847
      bare_cable_height_low: 0.001305
      bare_cable_height_high: 0.001305
      bare_cable_height_mean: 0.001305
      th_insulation_along_width: 0.00015
      th_insulation_along_height: 0.00015
      width_core: null
      height_core: null
      strand_twist_pitch: 0.09
      strand_twist_pitch_angle: null
      Rc: 1e-05
      Ra: null
      f_superconductor: null
      f_stabilizer: null
      f_insulation: null
      f_inner_voids: null
      f_outer_voids: null
      f_core: null
      material_insulation: G10
      material_inner_voids: G10
      material_outer_voids: G10
      material_core: null
    strand:
      type: Round
      fil_twist_pitch: 0.019
      diameter: 0.0007
      diameter_core: null
      diameter_filamentary: null
      filament_diameter: 5.5e-05
      number_of_filaments: null
      f_Rho_effective: 1.0
      Cu_noCu_in_strand: 1.106
      material_superconductor: Nb3Sn
      n_value_superconductor: null
      ec_superconductor: null
      k_material_superconductor: null
      Cv_material_superconductor: null
      material_stabilizer: Cu
      rho_material_stabilizer: CFUN_rhoCu
      RRR: 100.0
      T_ref_RRR_high: 273.0
      T_ref_RRR_low: null
      k_material_stabilizer: CFUN_kCu
      Cv_material_stabilizer: CFUN_CvCu
    Jc_fit:
      type: Summers
      Tc0_Summers: 18.0
      Bc20_Summers: 29.0
      Jc0_Summers: 31500000000.0
