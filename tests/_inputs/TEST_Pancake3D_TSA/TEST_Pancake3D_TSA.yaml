general:
  magnet_name: TEST_Pancake3D_TSA

run:
  type: start_from_yaml
  geometry: a
  mesh: a
  solution: a
  launch_gui: true
  overwrite: true
  # comments: correct transformation of j, erik derivative

magnet:
  type: Pancake3D

  geometry:
    numberOfPancakes: 3
    gapBetweenPancakes: 0.01

    winding:
      innerRadius: 5.0e-3
      thickness: 240.0e-6
      height: 4.0e-3
      numberOfTurns: 5.3

    contactLayer:
      thinShellApproximation: True
      thickness: 40.0e-6

    terminals:
      inner:
        thickness: 1.0e-3

      outer:
        thickness: 1.0e-3

    air:
      type: cylinder
      radius: 15.0e-3
      axialMargin: 6.0e-3

  mesh:
    minimumElementSize: 0.989
    maximumElementSize: 3
    winding:
      axialNumberOfElements:
        - 3
      azimuthalNumberOfElementsPerTurn:
        - 26
      radialNumberOfElementsPerTurn:
        - 1
      axialDistributionCoefficient:
        - 1.0
      elementType:
        - hexahedron

    contactLayer:
      radialNumberOfElementsPerTurn:
        - 1

    terminals:
      structured: false
      radialElementSize: 7

    air:
      structured: false
      radialElementSize: 10

  solve:
    type: stronglyCoupled
    time:
      start: 0.0
      end: 10
      timeSteppingType: adaptive
      extrapolationOrder: 1
      
      adaptiveSteppingSettings:
        initialStep: 1 # 1 s
        minimumStep: 0.001 # 0.01 s
        maximumStep: 2 # 2 s
        integrationMethod: Euler

        tolerances:
          - quantity: voltageBetweenTerminals
            relative: 0.1 # 10 percent
            absolute: 0.05 # 0.05 V
            normType: LinfNorm

          - quantity: magnitudeOfCurrentDensity
            position:
              turnNumber: 3.7
              whichPancakeCoil: 1
            relative: 0.1 # 10 percent
            absolute: 16.0e+6 # 16.0e+6 A/m^2
            normType: LinfNorm

          - quantity: magnitudeOfMagneticField
            position:
              x: 0
              y: 0
              z: 0
            relative: 0.10 # 10 percent
            absolute: 0.002 # 0.002 T
            normType: LinfNorm

    nonlinearSolver:
      maximumNumberOfIterations: 50
      relaxationFactor: 0.7

      tolerances:
        - quantity: voltageBetweenTerminals
          relative: 0.1 # 10 percent
          absolute: 0.05 # 0.05 V
          normType: LinfNorm

        - quantity: magnitudeOfCurrentDensity
          position:
            turnNumber: 3.7
            whichPancakeCoil: 1
          relative: 0.1 # 10 percent
          absolute: 16.0e+6 # 16.0e+6 A/m^2
          normType: LinfNorm

        - quantity: magnitudeOfMagneticField
          position:
            x: 0
            y: 0
            z: 0
          relative: 0.10 # 10 percent
          absolute: 0.002 # 0.002 T
          normType: LinfNorm

    winding:
      material:
        - name: Copper
          relativeThickness: 0.2143
          RRR: 100.0
          RRRRefTemp: 295.0
        - name: Stainless Steel
          relativeThickness: 0.1
        - name: Hastelloy
          relativeThickness: 0.4715
        - name: Silver
          relativeThickness: 0.1428
          RRR: 100.0
          RRRRefTemp: 295.0
        - name: HTSSuperPower
          relativeThickness: 0.0714
          electricFieldCriterion: 0.0001
          nValue: 30.0
          IcAtTAndBref: 230
          IcReferenceTemperature: 77
          IcReferenceBmagnitude: 0
          IcReferenceBangle: 90
          
      minimumPossibleResistivity: 0
      maximumPossibleResistivity: 0.01

    contactLayer:
      numberOfThinShellElements: 2

      resistivity: 0.000112 # 0.000112 Ohm*m

      material:
        name: Stainless Steel

    terminals:
      material:
        name: Copper
        RRR: 100.0
        RRRRefTemp: 295.0
      
      transitionNotch:
        material:
          name: Copper
          RRR: 100.0
          RRRRefTemp: 295.0

      terminalContactLayer:
        resistivity: 0.000112
        material:
          name: Stainless Steel

    air:
      permeability: 1.2566e-06 # 1.2566e-06 H/m

    initialConditions:
      temperature: 77.0 # 77 K

    quantitiesToBeSaved:
      - quantity: temperature

power_supply:
  t_control_LUT:
    - 0
    - 5
    - 10
  I_control_LUT:
    - 0
    - 1
    - 1
