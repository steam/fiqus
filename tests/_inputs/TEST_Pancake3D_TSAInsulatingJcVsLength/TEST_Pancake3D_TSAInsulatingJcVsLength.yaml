general:
  magnet_name: TEST_Pancake3D_TSAInsulatingJcVsLength

run:
  type: start_from_yaml
  geometry: a
  mesh: a
  solution: a
  launch_gui: true
  overwrite: true
  verbosity_GetDP: 5
  # comments: correct transformation of j, erik derivative

magnet:
  type: Pancake3D

  geometry:
    numberOfPancakes: 1
    gapBetweenPancakes: 0.01

    winding:
      innerRadius: 5.0e-3
      thickness: 240.0e-6
      height: 4.0e-3
      numberOfTurns: 8

    contactLayer:
      thinShellApproximation: True
      thickness: 40.0e-6

    terminals:
      inner:
        thickness: 1.0e-3

      outer:
        thickness: 1.0e-3

    air:
      type: cylinder
      radius: 15.0e-3
      axialMargin: 6.0e-3
      shellTransformation: true
      shellTransformationMultiplier: 1.4


  mesh:
    minimumElementSize: 0.989
    maximumElementSize: 3
    winding:
      axialNumberOfElements:
        - 3
      azimuthalNumberOfElementsPerTurn:
        - 26
      radialNumberOfElementsPerTurn:
        - 1
      axialDistributionCoefficient:
        - 1.0
      elementType:
        - prism

    contactLayer:
      radialNumberOfElementsPerTurn:
        - 1

    terminals:
      structured: false
      radialElementSize: 7

    air:
      structured: false
      radialElementSize: 10

  solve:

    stopWhenTemperatureReaches: 200

    materialParametersUseCoilField: false
  
    boundaryConditions: 
      imposedAxialField: 0.001
    voltageTapPositions: 
      - turnNumber: 0
        whichPancakeCoil: 1
      - turnNumber: 8
        whichPancakeCoil: 1
      - turnNumber: 2.3
        whichPancakeCoil: 1
      - turnNumber: 5.7
        whichPancakeCoil: 1
    
    EECircuit:
      inductanceInSeriesWithPancakeCoil: 1E-3
      enable: True
      TurnOffDeltaTimePowerSupply: 1.5
      ResistanceEnergyExtractionOpenSwitch: 1E6
      ResistanceEnergyExtractionClosedSwitch: 1E-6
      ResistanceCrowbarOpenSwitch: 1E6
      ResistanceCrowbarClosedSwitch: 1E-6
      stopSimulationAtCurrent: 0.2
      stopSimulationWaitingTime: 1.5

    heatFlowBetweenTurns: false
    resistiveHeatingTerminals: false
    convectiveCooling:
      heatTransferCoefficient: 100000.0
      exteriorBathTemperature: 70.0
    imposedPowerDensity:
      power: 100
      startArcLength: 0.15
      endArcLength: 0.18
      startTime: 1
      endTime: 7
      
    type: stronglyCoupled
    time:
      start: 0.0
      end: 15
      timeSteppingType: adaptive
      extrapolationOrder: 1

      adaptiveSteppingSettings:
        initialStep: 0.1 # 1 s
        minimumStep: 0.001 # 0.01 s
        maximumStep: 1 # 2 s
        integrationMethod: Euler

        tolerances:
          - quantity: voltageBetweenTerminals
            relative: 0.1 # 10 percent
            absolute: 0.05 # 0.05 V
            normType: LinfNorm

          - quantity: magnitudeOfCurrentDensity
            position:
              turnNumber: 3.7
              whichPancakeCoil: 1
            relative: 0.1 # 10 percent
            absolute: 16.0e+6 # 16.0e+6 A/m^2
            normType: LinfNorm

          - quantity: magnitudeOfMagneticField
            position:
              x: 0
              y: 0
              z: 0
            relative: 0.10 # 10 percent
            absolute: 0.002 # 0.002 T
            normType: LinfNorm

    nonlinearSolver:
      maximumNumberOfIterations: 20
      relaxationFactor: 0.7

      tolerances:
        - quantity: voltageBetweenTerminals
          relative: 0.0001 # 1 percent
          absolute: 0.001 # 0.001 V
          normType: LinfNorm

        - quantity: magnitudeOfCurrentDensity
          position:
            turnNumber: 3.7
            whichPancakeCoil: 1
          relative: 0.0001 # 1 percent
          absolute: 16.0e+3 # 16.0e+6 A/m^2
          normType: LinfNorm

        - quantity: magnitudeOfMagneticField
          position:
            x: 0
            y: 0
            z: 0
          relative: 0.0001 # 1 percent
          absolute: 0.001 # 0.001 T
          normType: LinfNorm

    winding:
      shuntLayer: # Material properties of the shunt layer.
        material: # Material from STEAM material library.
          name: Copper
          RRR: 100 # Residual-resistivity ratio (also known as Residual-resistance ratio or just RRR) is the ratio of the resistivity of a material at reference temperature and at 0 K.
          RRRRefTemp: 295 # Reference temperature for residual resistance ratio
          relativeHeight: 0.02
      isotropic: true
      material:
        - name: Copper
          relativeThickness: 0.2143
          RRR: 100.0
          RRRRefTemp: 295.0
        - name: Hastelloy
          relativeThickness: 0.5715
        - name: Silver
          relativeThickness: 0.1428
          RRR: 100.0
          RRRRefTemp: 295.0
        - name: HTSSucci
          relativeThickness: 0.0714
          electricFieldCriterion: 0.0001
          nValue: 30.0
          IcAtTAndBref:
            csvFile: IcVsLength.csv
            lengthUnit: turnNumber
          IcReferenceTemperature: 77
          IcReferenceBmagnitude: 0
      
      minimumPossibleResistivity: 1E-20
      maximumPossibleResistivity: 0.01

    contactLayer:
      numberOfThinShellElements: 2

      resistivity: perfectlyInsulating

      material:
        name: Kapton

    terminals:
      cooling: cryocooler

      cryocoolerOptions:
        staticHeatLoadPower: 28
        coolingPowerMultiplier: 0.8
        lumpedMass:
          thermalConductivity: 100

          material:
            name: Copper
            RRR: 100.0
            RRRRefTemp: 295.0
        
          volume: 0.0001
          numberOfThinShellElements: 2

      material:
        name: Copper
        RRR: 100.0
        RRRRefTemp: 295.0

      transitionNotch:
        resistivity: 1.0e-2
        specificHeatCapacity: 150
        thermalConductivity: 100

      terminalContactLayer:
        resistivity: 0.000112
        material:
          name: G10

    air:
      permeability: 1.2566e-06 # 1.2566e-06 H/m

    initialConditions:
      temperature: 77.0 # 77 K

    quantitiesToBeSaved:
      - quantity: inductance
      - quantity: temperature
      - quantity: resistivity
      - quantity: thermalConductivity
      - quantity: resistiveHeating
      - quantity: currentDensity
      - quantity: magneticField
      - quantity: criticalCurrent
      - quantity: criticalCurrentDensity
      - quantity: jHTSOverjCritical
      - quantity: jHTS
      - quantity: currentSharingIndex
      - quantity: arcLength
      - quantity: turnNumber

  postproc:
    timeSeriesPlots:
      - quantity: voltageBetweenTerminals
      - quantity: currentThroughCoil
      - quantity: maximumTemperature
      - quantity: cryocoolerAveragePower
      - quantity: cryocoolerAverageTemperature
      - quantity: axialComponentOfTheMagneticField
        position:
          x: 0
          y: 0
          z: 0

power_supply:
  t_off:
  t_control_LUT:
    - 0
    - 5
    - 10
  I_control_LUT:
    - 0
    - 1
    - 1
  R_crowbar: 0.1

quench_detection:
  voltage_thresholds:
    - 1.5E-5
    - 0.1
  discrimination_times:
    - 0.1
    - 1.5
  voltage_tap_pairs:
    - [0, 1]
    - [2, 3]

quench_protection:
  energy_extraction:
    R_EE: 0.1