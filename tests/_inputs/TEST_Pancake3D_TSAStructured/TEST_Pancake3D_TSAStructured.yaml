general:
  magnet_name: TEST_Pancake3D_TSAStructured

run:
  type: geometry_and_mesh
  geometry: a
  mesh: a
  solution: a
  launch_gui: false
  overwrite: true
  # comments: correct transformation of j, erik derivative

magnet:
  type: Pancake3D

  geometry:
    numberOfPancakes: 3
    gapBetweenPancakes: 0.005

    winding:
      innerRadius: 5.0e-3 # 5 mm
      thickness: 240.0e-6 # 240 um
      height: 4.0e-3 # 4 mm
      numberOfTurns: 5

    contactLayer:
      thinShellApproximation: True
      thickness: 40.0e-6 # 40 um

    terminals:
      inner:
        thickness: 1.0e-3 # 1 mm

      outer:
        thickness: 1.0e-3 # 1 mm

    air:
      type: cylinder
      radius: 15.0e-3
      axialMargin: 6.0e-3

  mesh:
    minimumElementSize: 0.989
    maximumElementSize: 9
    winding:
      axialNumberOfElements:
        - 3
      azimuthalNumberOfElementsPerTurn:
        - 32
      radialNumberOfElementsPerTurn:
        - 1
      axialDistributionCoefficient:
        - 1.0
      elementType:
        - tetrahedron

    contactLayer:
      radialNumberOfElementsPerTurn:
        - 1
      
    terminals:
      structured: true
      radialElementSize: 12
    
    air:
      structured: true
      radialElementSize: 12

  solve:
    type: electromagnetic

    # startFromThisSolution: Solution_thisOne

    time:
      start: 0.0
      end: 10
      timeSteppingType: adaptive
      extrapolationOrder: 1

      adaptiveSteppingSettings:
        initialStep: 1 # 1 s
        minimumStep: 0.01 # 0.01 s
        maximumStep: 30 # 30 s
        integrationMethod: Euler

        tolerances:
          - quantity: voltageBetweenTerminals
            relative: 0.1 # 10 percent
            absolute: 0.05 # 0.05 V
            normType: LinfNorm

          - quantity: magnitudeOfCurrentDensity
            position:
              turnNumber: 3.7
              whichPancakeCoil: 1
            relative: 0.1 # 10 percent
            absolute: 16.0e+6 # 16.0e+6 A/m^2
            normType: LinfNorm

          - quantity: magnitudeOfMagneticField
            position:
              x: 0
              y: 0
              z: 0
            relative: 0.10 # 10 percent
            absolute: 0.002 # 0.002 T
            normType: LinfNorm

    nonlinearSolver:
      maximumNumberOfIterations: 50
      relaxationFactor: 0.7

      tolerances:
        - quantity: voltageBetweenTerminals
          relative: 0.1 # 10 percent
          absolute: 0.05 # 0.05 V
          normType: LinfNorm

        - quantity: magnitudeOfCurrentDensity
          position:
            turnNumber: 3.7
            whichPancakeCoil: 1
          relative: 0.1 # 10 percent
          absolute: 16.0e+6 # 16.0e+6 A/m^2
          normType: LinfNorm

        - quantity: magnitudeOfMagneticField
          position:
            x: 0
            y: 0
            z: 0
          relative: 0.10 # 10 percent
          absolute: 0.002 # 0.002 T
          normType: LinfNorm

    winding:
      resistivity: 1e-12
      thermalConductivity: 400
      specificHeatCapacity: 400

    contactLayer:
      numberOfThinShellElements: 2
      resistivity: 1e-3
      thermalConductivity: 400
      specificHeatCapacity: 400

    terminals:
      cooling: adiabatic
      resistivity: 1e-12
      thermalConductivity: 400
      specificHeatCapacity: 400

      transitionNotch:
        resistivity: 1e-2
        thermalConductivity: 400
        specificHeatCapacity: 400

      terminalContactLayer:
        resistivity: 1e-3
        thermalConductivity: 400
        specificHeatCapacity: 400

    air:
      permeability: 1.2566e-06 # 1.2566e-06 H/m

    initialConditions:
      temperature: 77.0 # 77 K

power_supply:
  t_control_LUT:
    - 0
    - 5
    - 10
  I_control_LUT:
    - 0
    - 1
    - 1
