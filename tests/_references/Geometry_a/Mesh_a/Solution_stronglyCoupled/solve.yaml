time:  # All the time related settings for transient analysis.
  start: 0.0  # Start time of the simulation.
  end: 10.0 # End time of the simulation.
  extrapolationOrder: 1 # Before solving for the next time steps, the previous solutions can be extrapolated for better convergence.
  timeSteppingType: adaptive
  adaptiveSteppingSettings:  # Adaptive time loop settings (only used if stepping type is adaptive).
    tolerances:  # Time steps or nonlinear iterations will be refined until the tolerances are satisfied.
      - quantity: voltageBetweenTerminals  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 0.05 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      - quantity: magnitudeOfCurrentDensity  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 16000000.0 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
        position: # Probing position of the quantity for tolerance.
          turnNumber: 3.7  # Winding turn number as a position input. It starts from 0 and it can be a float.
          whichPancakeCoil: 1 # The first pancake coil is 1, the second is 2, etc.
          x: -0.0018878758915098113
          y: -0.00581588253948834
          z: -0.009
      - quantity: magnitudeOfMagneticField  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 0.002 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
        position: # Probing position of the quantity for tolerance.
          x: 0.0  # x coordinate of the position.
          y: 0.0 # y coordinate of the position.
          z: 0.0 # z coordinate of the position.
    initialStep: 1.0 # Initial step for adaptive time stepping
    minimumStep: 0.01 # The simulation will be aborted if a finer time step is required than this minimum step value.
    maximumStep: 30.0 # Bigger steps than this won't be allowed
    integrationMethod: Euler # Integration method for transient analysis
    breakPoints: [] # Make sure to solve the system for these times.
    postOperationTolerances:
      - quantity: voltageBetweenTerminals
        relative: 0.1
        absolute: 0.05
        normType: LinfNorm
      - quantity: magnitudeOfCurrentDensity
        relative: 0.1
        absolute: 16000000.0
        normType: LinfNorm
        position:
          turnNumber: 3.7
          whichPancakeCoil: 1
          x: -0.0018878758915098113
          y: -0.00581588253948834
          z: -0.009
      - quantity: magnitudeOfMagneticField
        relative: 0.1
        absolute: 0.002
        normType: LinfNorm
        position:
          x: 0.0
          y: 0.0
          z: 0.0
    systemTolerances: []
nonlinearSolver: # All the nonlinear solver related settings.
  tolerances:  # Time steps or nonlinear iterations will be refined until the tolerances are satisfied.
    - quantity: voltageBetweenTerminals  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 0.05 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
    - quantity: magnitudeOfCurrentDensity  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 16000000.0 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      position: # Probing position of the quantity for tolerance.
        turnNumber: 3.7  # Winding turn number as a position input. It starts from 0 and it can be a float.
        whichPancakeCoil: 1 # The first pancake coil is 1, the second is 2, etc.
        x: -0.0018878758915098113
        y: -0.00581588253948834
        z: -0.009
    - quantity: magnitudeOfMagneticField  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 0.002 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      position: # Probing position of the quantity for tolerance.
        x: 0.0  # x coordinate of the position.
        y: 0.0 # y coordinate of the position.
        z: 0.0 # z coordinate of the position.
  maximumNumberOfIterations: 50 # Maximum number of iterations allowed for the nonlinear solver.
  relaxationFactor: 0.7 # Calculated step changes of the solution vector will be multiplied with this value for better convergence.
  postOperationTolerances:
    - quantity: voltageBetweenTerminals
      relative: 0.1
      absolute: 0.05
      normType: LinfNorm
    - quantity: magnitudeOfCurrentDensity
      relative: 0.1
      absolute: 16000000.0
      normType: LinfNorm
      position:
        turnNumber: 3.7
        whichPancakeCoil: 1
        x: -0.0018878758915098113
        y: -0.00581588253948834
        z: -0.009
    - quantity: magnitudeOfMagneticField
      relative: 0.1
      absolute: 0.002
      normType: LinfNorm
      position:
        x: 0.0
        y: 0.0
        z: 0.0
  systemTolerances: []
winding: # This dictionary contains the winding material properties.
  resistivity: 1e-12  # A scalar value. If this is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: null # List of materials of HTS CC.
  shuntLayer: # Material properties of the shunt layer.
    resistivity: null  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: null # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: null # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: # Material from STEAM material library.
      name: Copper
      residualResistanceRatio: 100 # Residual-resistivity ratio (also known as Residual-resistance ratio or just RRR) is the ratio of the resistivity of a material at reference temperature and at 0 K.
      residualResistanceRatioReferenceTemperature: 295 # Reference temperature for residual resistance ratio
      relativeHeight: 0.0 # HTS 2G coated conductor are typically plated, usually  using copper. The relative height of the shunt layer is the  width of the shunt layer divided by the width of the tape.  0 means no shunt layer.
      resistivityMacroName: MATERIAL_Resistivity_Copper_T_B
      thermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_T_B
      heatCapacityMacroName: MATERIAL_SpecificHeatCapacity_Copper_T
      getdpTSAOnlyResistivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAMassResistivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAStiffnessResistivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAMassThermalConductivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAStiffnessThermalConductivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAMassHeatCapacityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSARHSFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSATripleFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
  relativeThicknessOfNormalConductor: 0
  relativeThicknessOfSuperConductor: 0
  normalConductors: []
  superConductor: null
contactLayer: # This dictionary contains the contact layer material properties.
  resistivity: 0.001  # A scalar value or "perfectlyInsulating". If "perfectlyInsulating" is given, the contact layer will be perfectly insulating. If this value is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: null # Material from STEAM material library.
  numberOfThinShellElements: 2 # Number of thin shell elements in the FE formulation (GetDP related, not physical and only used when TSA is set to True)
terminals: # This dictionary contains the terminals material properties and cooling condition.
  resistivity: 1e-12  # A scalar value. If this is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: null # Material from STEAM material library.
  cooling: adiabatic # Cooling condition of the terminal. It can be either adiabatic, fixed temperature, or cryocooler.
  transitionNotch: # Material properties of the transition notch volume.
    resistivity: 0.01  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: null # Material from STEAM material library.
  terminalContactLayer: # Material properties of the transition layer between terminals and windings.
    resistivity: 0.001  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: null # Material from STEAM material library.
air: # This dictionary contains the air material properties.
  permeability: 1.2566e-06  # Permeability of air.
initialConditions: # Initial conditions of the problem.
  temperature: 77.0  # Initial temperature of the pancake coils.
quantitiesToBeSaved: # List of quantities to be saved.
  - quantity: magneticField  # Name of the quantity to be saved.
    timesToBeSaved: null # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_magneticField
    getdpPostOperationName: POSTOP_magneticField
  - quantity: currentDensity  # Name of the quantity to be saved.
    timesToBeSaved: null # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_currentDensity
    getdpPostOperationName: POSTOP_currentDensity
  - quantity: temperature  # Name of the quantity to be saved.
    timesToBeSaved: null # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_temperature
    getdpPostOperationName: POSTOP_temperature
type: stronglyCoupled # FiQuS/Pancake3D can solve only electromagnetics and thermal or electromagnetic and thermal coupled. In the weaklyCoupled setting, thermal and electromagnetics systems will be put into different matrices, whereas in the stronglyCoupled setting, they all will be combined into the same matrix. The solution should remain the same.
proTemplate: Pancake3D_template.pro # file name of the .pro template file
localDefects: # Local defects (like making a small part of the winding normal conductor at some time) can be introduced.
  criticalCurrentDensity: null  # Set critical current density locally.
initFromPrevious: '' # The simulation is continued from an existing .res file.  The .res file is from a previous computation on the same geometry and mesh. The .res file is taken from the folder Solution_<<initFromPrevious>>
isothermalInAxialDirection: false # If True, the DoF along the axial direction will be equated. This means that the temperature will be the same along the axial direction reducing the number of DoF. This is only valid for the thermal analysis.
systemsOfEquationsType: linear
