Group {
    // ------- PROBLEM DEFINITION -------
    // Filaments
    Filaments_SC = Region[{2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53}]; // Filament superconducting region
    Filament_holes = Region[{}]; // Filament holes

    Filaments = Region[{Filaments_SC, Filament_holes}]; // Filaments (including holes)


    BndFilaments = Region[{1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52}]; // Filament boundaries
    Cuts = Region[{68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85}]; // Cuts on filament boundaries

    // Define the filaments, boundaries and cuts according to their layer and index in the layer
 // There is no inner layer (of length 1)
 
                filament_1_1 = Region[{2}]; // Filament surface
                filamentBnd_1_1 = Region[{1}]; // Boundary
                Cut_1_1 = Region[{68}]; // Cut
                filament_1_2 = Region[{5}]; // Filament surface
                filamentBnd_1_2 = Region[{4}]; // Boundary
                Cut_1_2 = Region[{69}]; // Cut
                filament_1_3 = Region[{8}]; // Filament surface
                filamentBnd_1_3 = Region[{7}]; // Boundary
                Cut_1_3 = Region[{70}]; // Cut
                filament_1_4 = Region[{11}]; // Filament surface
                filamentBnd_1_4 = Region[{10}]; // Boundary
                Cut_1_4 = Region[{71}]; // Cut
                filament_1_5 = Region[{14}]; // Filament surface
                filamentBnd_1_5 = Region[{13}]; // Boundary
                Cut_1_5 = Region[{72}]; // Cut
                filament_1_6 = Region[{17}]; // Filament surface
                filamentBnd_1_6 = Region[{16}]; // Boundary
                Cut_1_6 = Region[{73}]; // Cut
 
                filament_2_1 = Region[{20}]; // Filament surface
                filamentBnd_2_1 = Region[{19}]; // Boundary
                Cut_2_1 = Region[{74}]; // Cut
                filament_2_2 = Region[{23}]; // Filament surface
                filamentBnd_2_2 = Region[{22}]; // Boundary
                Cut_2_2 = Region[{75}]; // Cut
                filament_2_3 = Region[{26}]; // Filament surface
                filamentBnd_2_3 = Region[{25}]; // Boundary
                Cut_2_3 = Region[{76}]; // Cut
                filament_2_4 = Region[{29}]; // Filament surface
                filamentBnd_2_4 = Region[{28}]; // Boundary
                Cut_2_4 = Region[{77}]; // Cut
                filament_2_5 = Region[{32}]; // Filament surface
                filamentBnd_2_5 = Region[{31}]; // Boundary
                Cut_2_5 = Region[{78}]; // Cut
                filament_2_6 = Region[{35}]; // Filament surface
                filamentBnd_2_6 = Region[{34}]; // Boundary
                Cut_2_6 = Region[{79}]; // Cut
 
                filament_3_1 = Region[{38}]; // Filament surface
                filamentBnd_3_1 = Region[{37}]; // Boundary
                Cut_3_1 = Region[{80}]; // Cut
                filament_3_2 = Region[{41}]; // Filament surface
                filamentBnd_3_2 = Region[{40}]; // Boundary
                Cut_3_2 = Region[{81}]; // Cut
                filament_3_3 = Region[{44}]; // Filament surface
                filamentBnd_3_3 = Region[{43}]; // Boundary
                Cut_3_3 = Region[{82}]; // Cut
                filament_3_4 = Region[{47}]; // Filament surface
                filamentBnd_3_4 = Region[{46}]; // Boundary
                Cut_3_4 = Region[{83}]; // Cut
                filament_3_5 = Region[{50}]; // Filament surface
                filamentBnd_3_5 = Region[{49}]; // Boundary
                Cut_3_5 = Region[{84}]; // Cut
                filament_3_6 = Region[{53}]; // Filament surface
                filamentBnd_3_6 = Region[{52}]; // Boundary
                Cut_3_6 = Region[{85}]; // Cut
    

    // To assign different material properties to each filament hole, we need to assign them separately.



    // Matrix partitions
        Matrix_0 = Region[{56}];
        Matrix_1 = Region[{58}];
        Matrix_2 = Region[{60}];

    // Matrix
    Matrix = Region[{56, 58, 60}];
    BndMatrix = Region[ 62 ]; // Strand outer boundary
    BndMatrixCut = Region[ 67 ]; // Strand outer boundary cut
    Cuts +=  Region[ BndMatrixCut ]; // important to add the matrix cut to the region of the cuts
    
    // Air
    Air = Region[ 63 ]; // Air surface
    BndAir = Region[ 61 ]; // Air outer boundary

    // Define a region for the matrix partitions to be included in the TI (in-plane) problem
    TI_adjacent_region = Region[ {Air} ]; // Define the regions adjacent to the region on which the TI problem is solved (used for defining h_perp_space_dynamic)
    Matrix_partitions_for_TI = Region[ {Matrix} ]; 
    

    // Split into conducting and non-conducting domains
    LinOmegaC = Region[ {Matrix, Filament_holes} ]; 
    NonLinOmegaC = Region[ {Filaments_SC} ]; 
    OmegaC = Region[ {LinOmegaC, NonLinOmegaC} ];
    BndOmegaC = Region[ {BndMatrix, BndFilaments} ];
    OmegaCC = Region[ {Air} ];
    Omega = Region[ {OmegaC, OmegaCC} ]; // the whole domain (only surfaces in 2D, or volumes in 3D)

    OmegaC_AndBnd = Region[{OmegaC, BndOmegaC}]; // useful for function space definition
    OmegaCC_AndBnd = Region[{OmegaCC, BndOmegaC, BndAir}]; // useful for function space definition
    
    // Here we define points on the boundaries of the filaments and the outer matrix boundary.
    // These points are used to fix the magnetic potential to zero on the boundaries.
    MatrixPointOnBoundary = Region[{64}];
    FilamentPointsOnBoundaries = Region[{3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54}];
    ArbitraryPoints = Region[{MatrixPointOnBoundary, FilamentPointsOnBoundaries}];

}

Function{
    // ------- GEOMETRY PARAMETERS -------
        number_of_layers = 18/6;
    // ------- MATERIAL PARAMETERS -------
    temperature = 1.9;
    T[] = temperature; // this can be made a function of time if needed. Later on, T may also be a field we solve for.
    RRR_matrix = 100.0;

    mu0 = Pi*4e-7; // [H/m]
    nu0 = 1.0/mu0; // [m/H]
    mu[Omega] = mu0;
    nu[Omega] = nu0;
    // Copper
 // If we don't load the geometry from a YAML file, we can assign the material properties directly
            rho[Matrix] = CFUN_rhoCu_T_B[T[], $1]{RRR_matrix};
    
    // Superconducting filaments (nonlinear now)
    ec = 0.0001; // [V/m], the value 1e-4 V/m is a common convention

        jc[] = 1707323930.0 / 1.70732393e9 * CFUN_IcNbTi_T_B_a[T[], $1, 1]; // [A/m2] critical current density as function of temperature and field amplitude


    n = 30.0; // [-] power law index, one key parameter for the power law
    
    rho_power[] = ec / jc[$2] * (Norm[$1] / jc[$2])^(n - 1); // [Ohm m] power law resistivity
    dedj_power[] = (
        ec / jc[$2] * (Norm[$1]/jc[$2])^(n - 1) * TensorDiag[1, 1, 1] +
        ec / jc[$2]^3 * (n - 1) * (Norm[$1]/jc[$2])^(n - 3) * SquDyadicProduct[$1]);

    outer_product[] = Tensor[CompX[$1]*CompX[$2], CompX[$1]*CompY[$2], CompX[$1]*CompZ[$2], CompY[$1]*CompX[$2], CompY[$1]*CompY[$2], CompY[$1]*CompZ[$2], CompZ[$1]*CompX[$2], CompZ[$1]*CompY[$2], CompZ[$1]*CompZ[$2]];
    dedb_power[] = -n*ec/jc[Norm[$2]]^2 * (Norm[$1]/jc[Norm[$2]])^(n-1) * ( jc[Norm[$2] + 0.005] - jc[Max[0, Norm[$2] - 0.005]] ) / (0.01) * outer_product[$1, $2]/(Norm[$2]+1e-10);


    rho[Filaments_SC] = rho_power[$1, $2];
    dedj[Filaments_SC] = dedj_power[$1, $2];
    dedb[Filaments_SC] = dedb_power[$1, $2];


    sigma[] = 1/rho[$1] ; // Can only be used in the matrix

    // HEAT APPROXIMATION
        filament_Cv[] = CFUN_CvNbTi_T_B[$1, $2]{0, 1, 0}; // Volumetric heat capacity [J/(m3 K)], as function of temperature and field magnitude.
    
        matrix_Cv[] = CFUN_CvCu_T[$1]; // Volumetric heat capacity [J/(m3 K)], as function of temperature


    // ------- SOURCE PARAMETERS -------
    bmax = 0.5; // Maximum applied magnetic induction [T]
    f = 10.0; // Frequency of applied field [Hz]
    // Direction and value of applied field

 // Sine wave source (potentially with DC component)
        time_multiplier = 1;

        I_transport[] = 0.0 + 1.0 * Sin[2*Pi*f * $Time];

        constant_field_direction[] = Vector[0., 1., 0.];
        directionApplied[] = Vector[Cos[90.0*Pi/180], Sin[90.0*Pi/180], 0.];

        hsVal[] = nu0 * 0.0 * constant_field_direction[] + nu0 * 0.5 * Sin[2*Pi*f * $Time] * directionApplied[];
        hsVal_prev[] = nu0 * 0.0 * constant_field_direction[] + nu0 * 0.5 * Sin[2*Pi*f * ($Time-$DTime)] * directionApplied[];


    // For the natural boundary condition (restricted to fields of constant direction for the moment, should be generalized)
    dbsdt[] = mu0 * (hsVal[] - hsVal_prev[]) / $DTime; // must be a finite difference to avoid error accumulation

    // ------- NUMERICAL PARAMETERS -------
    timeStart = 0.; // Initial time [s]

        timeFinal = 0.3/f; // Final time for source definition (s)
        dt = 1 / (f*100.0); // Time step (initial if adaptive) (s)
        dt_max = dt; // Fixed maximum time step
        dt_max_var[] = dt_max;

    // Set correction factor based on the periodicity length
        correctionFactor = 0.827; // to be automatized (equal to sin(x)/x with x = pi*ell/p, with ell = p/6 in the hexagonal lattice case)
        ell = 2*correctionFactor * 0.019 / 6;
    
    iter_max = 60; // Maximum number of iterations (after which we exit the iterative loop)
    extrapolationOrder = 1; // Extrapolation order for predictor
    tol_energy = 1e-6; // Tolerance on the relative change of the power indicator
    writeInterval = dt; // Time interval to save the solution [s]

    // ------- SIMULATION NAME -------
    name = "test_temporary";
    resDirectory = StrCat["./",name];
    infoResidualFile = StrCat[resDirectory,"/residual.txt"];
    outputPower = StrCat[resDirectory,"/power.txt"]; // File updated during runtime
    crashReportFile = StrCat[resDirectory,"/crash_report.txt"];
    outputTemperature = StrCat[resDirectory,"/temperature.txt"];

}

Constraint {
    { Name phi ;
        Case {
 // For natural boundary condition (in formulation)
            {Region ArbitraryPoints ; Value 0.0 ;} // Fix the magnetic potential to zero on the boundaries of the filaments and the outer matrix boundary
        }
    }
    { Name Current ;
        Case {
            {Region BndMatrixCut ; Type Assign ; Value 1.0 ; TimeFunction I_transport[] ;} // Contraint for the total transport current
            // {Region Cuts ; Type Assign ; Value 0. ;} // for debugging (or to model fully uncoupled filaments without transport current)
        }
    }
    { Name Voltage ; Case {} } // Empty to avoid warnings
    { Name Current_plane ; Case {} } // Empty to avoid warnings
    { Name Voltage_plane ;
        Case {
            // The constraint below can be useful for debugging (together with the Current one on Cuts, they uncouple the OOP and IP problems in the linked-flux formulation)
            //{Region filamentBnd_1_1 ; Type Assign ; Value 1. ;} // Put just one to non-zero voltage to see a non-trivial solution
            //{Region BndFilaments ; Type Assign ; Value 0. ;} // All the other ones are forced to be zero
        }
    }
    { Name v_plane ;
        Case {
            // {Region filamentBnd_1_1 ; Type Assign ; Value 1. ;}
            // {Region BndFilaments ; Type Assign ; Value 0. ;}
            {Region MatrixPointOnBoundary ; Type Assign ; Value 0. ;}
        }
    }

    // As only the curl of the field is projected, the absolute value is unknown. We need to symmetrize the field
    { Name hp_static ;
        Case {
            // {Region CenterPoint ; Value 0.0 ;} // replaced by a Lagrange multiplier in the formulation (more general, as there might be a filament at the center!)
        }
    }

    // This is the key constraint for coupling global quantities: it contains the links between the filaments
    {Name ElectricalCircuit ; Type Network ;
        Case circuit {
            // A filament in the center is treated separately. 
            
            { Region Cut_1_2 ; Branch { 101, 103 } ; }
            { Region filamentBnd_1_1 ; Branch {1000, 101} ; }
            { Region Cut_1_3 ; Branch { 102, 104 } ; }
            { Region filamentBnd_1_2 ; Branch {1000, 102} ; }
            { Region Cut_1_4 ; Branch { 103, 105 } ; }
            { Region filamentBnd_1_3 ; Branch {1000, 103} ; }
            { Region Cut_1_5 ; Branch { 104, 106 } ; }
            { Region filamentBnd_1_4 ; Branch {1000, 104} ; }
            { Region Cut_1_6 ; Branch { 105, 101 } ; }
            { Region filamentBnd_1_5 ; Branch {1000, 105} ; }
            { Region Cut_1_1 ; Branch { 106, 102 } ; }
            { Region filamentBnd_1_6 ; Branch {1000, 106} ; }
            { Region Cut_2_2 ; Branch { 201, 203 } ; }
            { Region filamentBnd_2_1 ; Branch {1000, 201} ; }
            { Region Cut_2_3 ; Branch { 202, 204 } ; }
            { Region filamentBnd_2_2 ; Branch {1000, 202} ; }
            { Region Cut_2_4 ; Branch { 203, 205 } ; }
            { Region filamentBnd_2_3 ; Branch {1000, 203} ; }
            { Region Cut_2_5 ; Branch { 204, 206 } ; }
            { Region filamentBnd_2_4 ; Branch {1000, 204} ; }
            { Region Cut_2_6 ; Branch { 205, 201 } ; }
            { Region filamentBnd_2_5 ; Branch {1000, 205} ; }
            { Region Cut_2_1 ; Branch { 206, 202 } ; }
            { Region filamentBnd_2_6 ; Branch {1000, 206} ; }
            { Region Cut_3_2 ; Branch { 301, 303 } ; }
            { Region filamentBnd_3_1 ; Branch {1000, 301} ; }
            { Region Cut_3_3 ; Branch { 302, 304 } ; }
            { Region filamentBnd_3_2 ; Branch {1000, 302} ; }
            { Region Cut_3_4 ; Branch { 303, 305 } ; }
            { Region filamentBnd_3_3 ; Branch {1000, 303} ; }
            { Region Cut_3_5 ; Branch { 304, 306 } ; }
            { Region filamentBnd_3_4 ; Branch {1000, 304} ; }
            { Region Cut_3_6 ; Branch { 305, 301 } ; }
            { Region filamentBnd_3_5 ; Branch {1000, 305} ; }
            { Region Cut_3_1 ; Branch { 306, 302 } ; }
            { Region filamentBnd_3_6 ; Branch {1000, 306} ; }

        }
    }
    { Name h ; Type Assign ;
        Case {
        }
    }
    
}

FunctionSpace {
    // Function space for magnetic field h in h-conform formulation. Main field for the magnetodynamic problem.
    //  h = sum phi_n * grad(psi_n)     (nodes in Omega_CC with boundary)
    //      + sum h_e * psi_e           (edges in Omega_C)
    //      + sum I_i * c_i             (cuts, global basis functions for net current intensity) not yet here
    { Name h_space; Type Form1;
        BasisFunction {
            { Name gradpsin; NameOfCoef phin; Function BF_GradNode;
                Support OmegaCC_AndBnd; Entity NodesOf[OmegaCC]; } // Extend support to boundary for surface integration (e.g. useful for weak B.C.)
            { Name gradpsin; NameOfCoef phin2; Function BF_GroupOfEdges;
                Support OmegaC; Entity GroupsOfEdgesOnNodesOf[BndOmegaC]; } // To treat properly the Omega_CC-Omega_C boundary
            { Name psie; NameOfCoef he; Function BF_Edge;
                Support OmegaC_AndBnd; Entity EdgesOf[All, Not BndOmegaC]; }
            { Name sc; NameOfCoef Ii; Function BF_GroupOfEdges;
                Support Omega; Entity GroupsOfEdgesOf[Cuts]; } // The region Cuts contains the union of all the relevant cuts (cohomology basis function support)
        }
        GlobalQuantity {
            { Name I ; Type AliasOf        ; NameOfCoef Ii ; }
            { Name V ; Type AssociatedWith ; NameOfCoef Ii ; }
        }
        
        Constraint {
            { NameOfCoef he; EntityType EdgesOf; NameOfConstraint h; }
            { NameOfCoef phin; EntityType NodesOf; NameOfConstraint phi; }
            { NameOfCoef phin2; EntityType NodesOf; NameOfConstraint phi; }
            { NameOfCoef Ii ;
                EntityType GroupsOfEdgesOf ; NameOfConstraint Current ; }
            { NameOfCoef V ;
                EntityType GroupsOfNodesOf ; NameOfConstraint Voltage ; }
        }
    }
    // Function space for the in-plane voltage field. Main field for the electrokinetics problem.
    // The (in-plane) coupling current derive from this voltage j_coupling = - sigma * grad(v).
    { Name v_space_elKin ; Type Form0 ;
        BasisFunction {
            { Name vn ; NameOfCoef vn ; Function BF_Node ;
                Support Matrix_partitions_for_TI ; Entity NodesOf[All, Not BndFilaments] ; }
            { Name vi; NameOfCoef vi; Function BF_GroupOfNodes;
                Support Matrix_partitions_for_TI; Entity GroupsOfNodesOf[BndFilaments]; }
        }
        GlobalQuantity {
            { Name V ; Type AliasOf        ; NameOfCoef vi ; }
            { Name I ; Type AssociatedWith ; NameOfCoef vi ; }
        }
        Constraint {
            { NameOfCoef vn ; EntityType NodesOf ; NameOfConstraint v_plane ; }
            { NameOfCoef V ;
                EntityType Region ; NameOfConstraint Voltage_plane ; }
            { NameOfCoef I ;
                EntityType Region ; NameOfConstraint Current_plane ; }
        }
    }
    // The curl of this space is the projection of the coupling current on this subspace (no net current from fil.)
    { Name h_perp_space_static; Type Form1P;
        BasisFunction {
            { Name sn; NameOfCoef hn; Function BF_PerpendicularEdge;
                Support Matrix_partitions_for_TI; Entity NodesOf[All]; }
        }
        Constraint {
            { NameOfCoef hn; EntityType NodesOf; NameOfConstraint hp_static; }
        }
    }
    // This is a Lagrange multplier for forcing the integral of hp_static to be zero in the matrix (avg field = 0)
    { Name h_perp_space_static_lagrange; Type Vector;
        BasisFunction {
            { Name sn_lag; NameOfCoef hn_lag; Function BF_RegionZ;
                Support Matrix_partitions_for_TI; Entity Matrix_partitions_for_TI; } // Ideally, should be only one function (not one per matrix part). To be double-checked!
        }
    }
    // This space will correct the static space above with dynamic effects
    { Name h_perp_space_dynamic; Type Form1P;
        BasisFunction {
            { Name sn; NameOfCoef hn; Function BF_PerpendicularEdge;
                Support Matrix_partitions_for_TI; Entity NodesOf[All, Not TI_adjacent_region ]; }
        }
    }
}

Jacobian {
    { Name Vol ;
        Case {
            {Region All ; Jacobian Vol ;}
        }
    }
    { Name Sur ;
        Case {
            { Region All ; Jacobian Sur ; }
        }
    }
}

Integration {
    { Name Int ;
        Case {
            { Type Gauss ;
                Case {
                    { GeoElement Point ; NumberOfPoints 1 ; }
                    { GeoElement Line ; NumberOfPoints 3 ; }
                    { GeoElement Triangle ; NumberOfPoints 3 ; }
                }
            }
        }
    }
}

Formulation{
    // h-formulation
    { Name MagDyn_hphi; Type FemEquation;
        Quantity {
            // Functions for the out-of-plane (OOP) problem
            { Name h; Type Local; NameOfSpace h_space; }
            { Name hp; Type Local; NameOfSpace h_space; }
            { Name I; Type Global; NameOfSpace h_space[I]; }
            { Name V; Type Global; NameOfSpace h_space[V]; }
            // Functions for the in-plane (IP) problem
            { Name v; Type Local; NameOfSpace v_space_elKin; }
            { Name Vp; Type Global; NameOfSpace v_space_elKin[V]; }
            { Name Ip; Type Global; NameOfSpace v_space_elKin[I]; }
        }
        Equation {
            // --- OOP problem ---
            // Time derivative of b (NonMagnDomain)
            Galerkin { [ ell* mu[] * Dof{h} / $DTime , {h} ];
                In Omega; Integration Int; Jacobian Vol;  }
            Galerkin { [ - ell*mu[] * {h}[1] / $DTime , {h} ];
                In Omega; Integration Int; Jacobian Vol;  }
            // Induced current (linear OmegaC)
            Galerkin { [ ell*rho[mu0*Norm[{h}]] * Dof{d h} , {d h} ];
                In LinOmegaC; Integration Int; Jacobian Vol;  }
            // Induced current (non-linear OmegaC)
            Galerkin { [ ell*rho[{d h}, mu0*Norm[{h}]] * {d h} , {d h} ];
                In NonLinOmegaC; Integration Int; Jacobian Vol;  }

            Galerkin { [ ell*dedj[{d h}, mu0*Norm[{h}]] * Dof{d h} , {d h} ];
                In NonLinOmegaC; Integration Int; Jacobian Vol;  } // For Newton-Raphson method
            Galerkin { [ - ell*dedj[{d h}, mu0*Norm[{h}]] * {d h} , {d h} ];
                In NonLinOmegaC; Integration Int; Jacobian Vol;  } // For Newton-Raphson method
            
            Galerkin { [ ell*dedb[{d h}, mu0*{h}] * mu0*Dof{h} , {d hp} ];
                In NonLinOmegaC; Integration Int; Jacobian Vol;  } // For Newton-Raphson method
            Galerkin { [ -ell*dedb[{d h}, mu0*{h}] * mu0*{h} , {d hp} ];
                In NonLinOmegaC; Integration Int; Jacobian Vol;  } // For Newton-Raphson method

            // Natural boundary condition for normal flux density (useful when transport current is an essential condition)
            Galerkin { [ - ell*dbsdt[] * Normal[] , {dInv h} ];
                In BndAir; Integration Int; Jacobian Sur;  }
            // Global term
            GlobalTerm { [ Dof{V} , {I} ] ; In Cuts ; }
            
            // --- IP problem ---
            Galerkin { [ ell * sigma[mu0*Norm[{h}]] * Dof{d v} , {d v} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  } // Matrix
            GlobalTerm { [ Dof{Ip} , {Vp} ] ; In BndFilaments ; }

            // --- Coupling between OOP and IP problems via circuit equations ---
            GlobalEquation {
                Type Network ; NameOfConstraint ElectricalCircuit ;
                { Node {I};  Loop {V};  Equation {V};  In Cuts ; }
                { Node {Ip}; Loop {Vp}; Equation {Ip}; In BndFilaments ; }
          	}
        }
    }
    // h-formulation
    { Name MagDyn_hphi_dynCorr; Type FemEquation;
        Quantity {
            // Functions for the OOP and IP problems, that are just used here (and not solved for)
            { Name h; Type Local; NameOfSpace h_space; }
            { Name v; Type Local; NameOfSpace v_space_elKin; }
            // Functions for the dynamic correction of the IP problem
            { Name hp_static; Type Local; NameOfSpace h_perp_space_static; }
            { Name hp_static_lagrange; Type Local; NameOfSpace h_perp_space_static_lagrange; }
            { Name hp_dynamic; Type Local; NameOfSpace h_perp_space_dynamic; }
        }
        Equation {
            // --- Dynamic correction of the IP problem ---
            // Projection of the static current flow on the curl of a "static" magnetic field
            Galerkin { [ sigma[mu0*Norm[{h}]] * {d v} , {d hp_static} ]; // No DOF! Just take the solution of the previously solved v-based formulation
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }
            Galerkin { [ Dof{d hp_static} , {d hp_static} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }
            Galerkin { [ Dof{hp_static_lagrange} , {hp_static} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }
            Galerkin { [ Dof{hp_static} , {hp_static_lagrange} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  } // ensure the magnetic field is averaged to zero over the matrix
            // Introduce a dynamic component to the magnetic field
            Galerkin { [ mu[] * Dof{hp_static} / $DTime , {hp_dynamic} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }   
            Galerkin { [ mu[] * Dof{hp_dynamic} / $DTime , {hp_dynamic} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }  
            Galerkin { [ - mu[] * {hp_static}[1] / $DTime , {hp_dynamic} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }   
            Galerkin { [ - mu[] * {hp_dynamic}[1] / $DTime , {hp_dynamic} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  }    
            //Galerkin { [ - Dof{d v} , {d hp_dynamic} ];
            //    In Matrix; Integration Int; Jacobian Vol;  } // Static field is curl-free so this contribution is unnecessary!
            Galerkin { [ rho[mu0*Norm[{h}]] * Dof{d hp_dynamic} , {d hp_dynamic} ];
                In Matrix_partitions_for_TI; Integration Int; Jacobian Vol;  } // Only the dynamic correction is an eddy current
        }
    }
}

Macro CustomIterativeLoop
    // Compute first solution guess and residual at step $TimeStep
    Generate[A];
    Solve[A]; Evaluate[ $syscount = $syscount + 1 ];
    Generate[A]; GetResidual[A, $res0];
    Evaluate[ $res = $res0 ];
    Evaluate[ $iter = 0 ];
    Evaluate[ $convCrit = 1e99 ];
    PostOperation[MagDyn_energy];
    Print[{$iter, $res, $res / $res0, $indicFilamentLoss},
        Format "%g %14.12e %14.12e %14.12e", File infoResidualFile];
    // ----- Enter the iterative loop (hand-made) -----
    While[$convCrit > 1 && $res / $res0 <= 1e10 && $iter < iter_max]{
        Solve[A]; Evaluate[ $syscount = $syscount + 1 ];
        Generate[A]; GetResidual[A, $res];
        Evaluate[ $iter = $iter + 1 ];
        Evaluate[ $indicFilamentLossOld = $indicFilamentLoss];
        PostOperation[MagDyn_energy];
        Print[{$iter, $res, $res / $res0, $indicFilamentLoss},
            Format "%g %14.12e %14.12e %14.12e", File infoResidualFile];
        // Evaluate the convergence indicator
        Evaluate[ $relChangeACLoss = Abs[($indicFilamentLossOld - $indicFilamentLoss)/((Abs[$indicFilamentLossOld]>1e-7 || $iter < 10) ? $indicFilamentLossOld:1e-7)] ];
        Evaluate[ $convCrit = $relChangeACLoss/tol_energy];
    }
Return

Resolution {
    { Name MagDyn;
        System {
            {Name A; NameOfFormulation MagDyn_hphi;}
            {Name A_dynCorr; NameOfFormulation MagDyn_hphi_dynCorr;}
        }
        Operation {
            // Initialize directories
            CreateDirectory[resDirectory];
            DeleteFile[outputPower];
            DeleteFile[infoResidualFile];
            // Initialize the solution (initial condition)
            SetTime[ timeStart ];
            SetDTime[ dt ];
            SetTimeStep[ 0 ];
            InitSolution[A];
            SaveSolution[A]; // Saves the solution x (from Ax = B) to .res file
            Evaluate[ $syscount = 0 ];
            Evaluate[ $saved = 1 ];
            Evaluate[ $elapsedCTI = 1 ]; // Number of control time instants already treated
            Evaluate[ $isCTI = 0 ];


            InitSolution[A_dynCorr];
            SaveSolution[A_dynCorr];
            // ----- Enter implicit Euler time integration loop (hand-made) -----
            // Avoid too close steps at the end. Stop the simulation if the step becomes ridiculously small
            SetExtrapolationOrder[ extrapolationOrder ];
            While[$Time < timeFinal] {
                SetTime[ $Time + $DTime ]; // Time instant at which we are looking for the solution
                SetTimeStep[ $TimeStep + 1 ];
                // Iterative loop defined as a macro above
                Print[{$Time, $DTime, $TimeStep}, Format "Start new time step. Time: %g s. Time step: %g s. Step: %g."];
                Call CustomIterativeLoop;
                // Has it converged? If yes, save solution and possibly increase the time step...
                Test[ $iter < iter_max && ($res / $res0 <= 1e10 || $res0 == 0)]{
                    Print[{$Time, $DTime, $iter}, Format "Converged time %g s with time step %g s in %g iterations."];
                    // Save the solution of few time steps (small correction to avoid bad rounding)
                    // Test[ $Time >= $saved * writeInterval - 1e-7 || $Time + $DTime >= timeFinal]{
                    Test[ 1 ]{
                    // Test[ $Time >= $saved * $DTime - 1e-7 || $Time + $DTime >= timeFinal]{
                        SaveSolution[A];
                        // post
                        PostOperation[test_Losses];
                        Generate[A_dynCorr]; Solve[A_dynCorr]; SaveSolution[A_dynCorr];
                        PostOperation[test_Losses_dynCorr];
                        Print[{$Time, $saved}, Format "Saved time %g s (saved solution number %g). Output power infos:"];
                        Print[{$Time, $indicFilamentLoss, $indicCouplingLoss, $indicEddyLoss, $indicTotalLoss, $indicCouplingLoss_dyn, $indicTotalLoss_dyn},
                            Format "%g %14.12e %14.12e %14.12e %14.12e %14.12e %14.12e", File outputPower];

                        // Compute the temperature

                        Evaluate[$saved = $saved + 1];
                    }
                    // Increase the step if we converged sufficiently "fast" (and not a control time instant)
                    Test[ $iter < iter_max / 4 && $DTime < dt_max_var[] && $isCTI == 0 ]{
                        Evaluate[ $dt_new = Min[$DTime * 2, dt_max_var[]] ];
                        Print[{$dt_new}, Format "*** Fast convergence: increasing time step to %g"];
                        SetDTime[$dt_new];
                    }
                    Test[ $DTime > dt_max_var[]]{
                        Evaluate[ $dt_new = dt_max_var[] ];
                        Print[{$dt_new}, Format "*** Variable maximum time-stepping: reducing time step to %g"];
                        SetDTime[$dt_new];
                    }
                }
                // ...otherwise, reduce the time step and try again
                {
                    Evaluate[ $dt_new = $DTime / 2 ];
                    Print[{$iter, $dt_new},
                        Format "*** Non convergence (iter %g): recomputing with reduced step %g"];
                    RemoveLastSolution[A];
                    SetTime[$Time - $DTime];
                    SetTimeStep[$TimeStep - 1];
                    SetDTime[$dt_new];
                    // If it gets ridicoulously small, end the simulation, and report the information in crash file.
                    Test[ $dt_new < dt_max/10000 ]{
                        Print[{$iter, $dt_new, $Time},
                            Format "*** Non convergence (iter %g): time step %g too small, stopping the simulation at time %g s.", File crashReportFile];
                        // Print[A];
                        Exit;
                    }
                }
            } // ----- End time loop -----
            // Print information about the resolution and the nonlinear iterations
            Print[{$syscount}, Format "Total number of linear systems solved: %g"];
        }
    }
}

PostProcessing {
    { Name MagDyn_hphi; NameOfFormulation MagDyn_hphi;
        Quantity {
            { Name phi; Value{ Local{ [ {dInv h} ] ;
                In OmegaCC; Jacobian Vol; } } }
            { Name h; Value{ Local{ [ {h} ] ;
                In Omega; Jacobian Vol; } } }
            { Name b; Value{ Local{ [ mu[] * {h} ] ;
                In Omega; Jacobian Vol; } } }
            { Name b_reaction; Value{ Local{ [ mu[] * ({h} - hsVal[]) ] ;
                In Omega; Jacobian Vol; } } }
            { Name j; Value{ Local{ [ {d h} ] ;
                In OmegaC; Jacobian Vol; } } }
            { Name jc; Value{ Local{ [ jc[mu0*Norm[{h}]] ] ;
                In NonLinOmegaC; Jacobian Vol; } } }
            { Name power_filaments; Value{ Local{ [ rho[{d h}, mu0*Norm[{h}]] * {d h} * {d h} ] ; // j*e : Power (only for filaments)
                In NonLinOmegaC; Jacobian Vol; } } }
            { Name sigma_matrix; Value{ Local{ [ sigma[mu0*Norm[{h}]]] ;
                In Matrix; Jacobian Vol; } } }
            { Name j_plane; Value{ Local{ [ -1/rho[mu0*Norm[{h}]] * {d v} ] ;
                In Matrix; Jacobian Vol; } } }
            { Name v_plane; Value{ Local{ [ {v} ] ;
                In Matrix; Jacobian Vol; } } }
            { Name power_matrix;
                Value{
                    Local{ [rho[mu0*Norm[{h}]] * {d h} * {d h}] ; // j*e = rho*j^2 in matrix (eddy)
                        In Matrix ; Integration Int ; Jacobian Vol; }
                    Local{ [ (sigma[mu0*Norm[{h}]] * {d v}) * {d v}] ; // j*e = sigma*e^2 in matrix (coupling)
                        In Matrix ; Integration Int ; Jacobian Vol; }
                }
            }
            { Name couplingLoss;
                Value{
                    Integral{ [ (sigma[mu0*Norm[{h}]] * {d v}) * {d v}] ; // j*e = sigma*e^2 in matrix
                        In Matrix ; Integration Int ; Jacobian Vol; }
                }
            }
            { Name eddyLoss; // NEW. Eddyloss was computed as totalLoss[matrix], which combines eddy and couplingLoss
                Value{
                    Integral{ [rho[mu0*Norm[{h}]] * {d h} * {d h}] ; // EddyLoss = rho*j^2 in matrix
                        In Matrix ; Integration Int ; Jacobian Vol; }
                }
            }
            { Name totalLoss;
                Value{
                    // Separate OmegaC into Matrix and nonlinear (resistivities take different argument types)
                    Integral{ [rho[{d h}, mu0*Norm[{h}]] * {d h} * {d h}] ; // j*e = rho*j^2 (filaments)
                        In NonLinOmegaC ; Integration Int ; Jacobian Vol; }
                    Integral{ [rho[mu0*Norm[{h}]] * {d h} * {d h}] ; // j*e = rho*j^2 (eddy)
                        In Matrix ; Integration Int ; Jacobian Vol; }
                    Integral{ [ (sigma[mu0*Norm[{h}]]*{d v}) * {d v}] ; // j*e = sigma*e^2 in matrix (coupling)
                        In Matrix ; Integration Int ; Jacobian Vol; }
                }
            }

            { Name heat_capacity;
                Value{
                    Integral{ [(filament_Cv[T[] + $cumulative_temperature, mu0*Norm[{h}]] )] ; // j*e = rho*j^2 in filaments (?)
                        In NonLinOmegaC ; Integration Int ; Jacobian Vol; }
                    Integral{ [(matrix_Cv[T[] + $cumulative_temperature]) ] ; // j*e = rho*j^2 in filaments (?)
                        In NonLinOmegaC ; Integration Int ; Jacobian Vol; }
                } 
            }

            { Name I; Value { Term{ [ {I} ] ; In Cuts; } } }
            { Name V; Value { Term{ [ {V} ] ; In Cuts; } } }
            { Name Ip; Value { Term{ [ {Ip} ] ; In BndFilaments; } } }
            { Name Vp; Value { Term{ [ {Vp} ] ; In BndFilaments; } } }
            { Name I_integral;
                Value{
                    Integral{ [ {d h} * Vector[0,0,1]] ;
                        In OmegaC ; Integration Int ; Jacobian Vol; }
                }
            }
            { Name I_abs_integral;
                Value{
                    Integral{ [ Fabs[{d h} * Vector[0,0,1]]] ;
                        In OmegaC ; Integration Int ; Jacobian Vol; }
                }
            }
            // Applied field (useful for magnetization plots)
            { Name hsVal; Value{ Term { [ hsVal[] ]; In Omega; } } }
            // Magnetization: integral of 1/2 * (r /\ j) in a conducting (sub-)domain
            { Name m_avg; Value{ Integral{ [ 0.5 * XYZ[] /\ {d h} ] ;
                In OmegaC; Integration Int; Jacobian Vol; } } }
            // Magnetic energy
            { Name magnetic_energy; Value{ Integral{ [ 0.5 * mu[] * {h} * {h} ] ;
                In Omega; Integration Int; Jacobian Vol; } } }
        }
    }
    { Name MagDyn_hphi_dynCorr; NameOfFormulation MagDyn_hphi_dynCorr;
        Quantity {
            { Name hp_static; Value{ Local{ [ {hp_static} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name hp_static_lagrange; Value{ Local{ [ {hp_static_lagrange} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name j_static; Value{ Local{ [ {d hp_static} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name hp_dynamic; Value{ Local{ [ {hp_dynamic} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name j_dynamic; Value{ Local{ [ {d hp_dynamic} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name j_stadyn; Value{ Local{ [ {d hp_static} + {d hp_dynamic} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name j_stadyn_mixed; Value{ Local{ [ - sigma[mu0*Norm[{h}]] * {d v} + {d hp_dynamic} ] ;
                In Matrix_partitions_for_TI; Jacobian Vol; } } }
            { Name totalLoss_dyn;
                Value{
                    // Integral{ [rho[{d h}, mu0*Norm[{h}]] * {d h} * {d h}] ;
                    //     In OmegaC ; Integration Int ; Jacobian Vol; }
                    // NEW separate OmegaC into Matrix and nonlinear
                    Integral{ [rho[{d h}, mu0*Norm[{h}]] * {d h} * {d h}] ; // j*e = rho*j^2 in filaments
                        In NonLinOmegaC ; Integration Int ; Jacobian Vol; }
                    Integral{ [rho[mu0*Norm[{h}]] * {d h} * {d h}] ; // Eddy
                        In Matrix ; Integration Int ; Jacobian Vol; }
                    Integral{ [ (- sigma[mu0*Norm[{h}]]*{d v} + {d hp_dynamic}) * (- {d v} + rho[mu0*Norm[{h}]] * {d hp_dynamic})] ;
                        In Matrix_partitions_for_TI ; Integration Int ; Jacobian Vol; } // Attention to signs!
                }
            }
            { Name couplingLoss_dyn;
                Value{
                    Integral{ [(- sigma[mu0*Norm[{h}]]*{d v} + {d hp_dynamic}) * (- {d v} + rho[mu0*Norm[{h}]] * {d hp_dynamic})] ;
                        In Matrix_partitions_for_TI ; Integration Int ; Jacobian Vol; } // Attention to signs!
                }
            }
        }
    }
}

PostOperation {   
    { Name MagDyn;
        NameOfPostProcessing MagDyn_hphi;
        Operation {
            // Local field solutions
            Print[ b, OnElementsOf Omega , File StrCat["b.pos"], Name "b [T]" ];
            Print[ b_reaction, OnElementsOf Omega , File StrCat["br.pos"], Name "br [T]" ];
            Print[ j, OnElementsOf OmegaC , File StrCat["j.pos"], Name "j [A/m2]" ];
            // Print[ jc, OnElementsOf NonLinOmegaC , File StrCat["jc.pos"], Name "jc [A/m2]" ];
            Print[ j_plane, OnElementsOf Matrix , File StrCat["jPlane.pos"], Name "j_plane [A/m2]" ];
            Print[ v_plane, OnElementsOf Matrix , File StrCat["vPlane.pos"], Name "v_plane [V/m]" ]; 
            // Print[ power_filaments, OnElementsOf NonLinOmegaC , File StrCat["powFil_f.pos"], Name "powerFilaments [W]" ];
            // Print[ power_matrix, OnElementsOf Matrix , File StrCat["powMat_f.pos"], Name "powerMatrix [W]" ];
            // Print[ sigma_matrix, OnElementsOf Matrix , File StrCat["sigmaMat_f.pos"], Name "sigmaMatrix [S/m]" ];
            Print[ phi, OnElementsOf OmegaCC , File StrCat["phi_f.pos"], Name "phi [A]" ];
            // Global solutions
            Print[ I, OnRegion BndMatrixCut, File StrCat[resDirectory,"/I_transport.txt"], Format SimpleTable];
            Print[ V, OnRegion BndMatrixCut, File StrCat[resDirectory,"/V_transport.txt"], Format SimpleTable];
            Print[ hsVal[Omega], OnRegion Matrix, Format TimeTable, File StrCat[resDirectory, "/hs_val.txt"]];
            Print[ m_avg[Filaments], OnGlobal, Format TimeTable, File StrCat[resDirectory, "/magn_fil.txt"]];
            Print[ m_avg[Matrix], OnGlobal, Format TimeTable, File StrCat[resDirectory, "/magn_matrix.txt"]];
            Print[ magnetic_energy[OmegaC], OnGlobal, Format TimeTable, File StrCat[resDirectory, "/magnetic_energy_internal.txt"]];
        }
    }
    { Name MagDyn_dynCorr;
        NameOfPostProcessing MagDyn_hphi_dynCorr;
        Operation {
            Print[ j_dynamic, OnElementsOf Matrix_partitions_for_TI , File StrCat["j_dynamic.pos"], Name "j_dynamic [A/m2]" ];
            Print[ j_stadyn_mixed, OnElementsOf Matrix_partitions_for_TI , File StrCat["j_stadyn_mixed.pos"], Name "j_stadyn_mixed [A/m2]" ];
            Print[ hp_dynamic, OnElementsOf Matrix_partitions_for_TI , File StrCat["hp_dynamic.pos"], Name "hp_dynamic [A/m]" ];        
            // Print[ hp_static, OnElementsOf Matrix , File StrCat["hp_static.pos"], Name "hp_static [A/m]" ];
            // Print[ hp_static_lagrange, OnElementsOf Matrix , File StrCat["hp_static_lagrange.pos"], Name "hp_static_lagrange [A/m]" ];
        }
    }
    { Name MagDyn_energy; LastTimeStepOnly 1 ;
        NameOfPostProcessing MagDyn_hphi;
        Operation {
            Print[ totalLoss[NonLinOmegaC], OnGlobal, Format Table, StoreInVariable $indicFilamentLoss, File StrCat[resDirectory,"/dummy.txt"] ];
        }
    }
    { Name test_Losses; LastTimeStepOnly 1 ;
        NameOfPostProcessing MagDyn_hphi;
        Operation {
            Print[ totalLoss[NonLinOmegaC], OnGlobal, Format Table, StoreInVariable $indicFilamentLoss, File StrCat[resDirectory,"/dummy.txt"] ];
            // Print[ totalLoss[Matrix], OnGlobal, Format Table, StoreInVariable $indicEddyLoss, File > StrCat[resDirectory,"/dummy.txt"] ];
            Print[ eddyLoss[Matrix], OnGlobal, Format Table, StoreInVariable $indicEddyLoss, File > StrCat[resDirectory,"/dummy.txt"] ];
            Print[ couplingLoss[Matrix], OnGlobal, Format Table, StoreInVariable $indicCouplingLoss, File > StrCat[resDirectory,"/dummy.txt"] ];
            Print[ totalLoss[OmegaC], OnGlobal, Format Table, StoreInVariable $indicTotalLoss, File > StrCat[resDirectory,"/dummy.txt"] ];
        }
    }

    { Name heat_capacity; LastTimeStepOnly 1 ;
        NameOfPostProcessing MagDyn_hphi;
        Operation {
            Print[ heat_capacity[OmegaC], OnGlobal, Format Table, StoreInVariable $heat_capacity_per_unit_length, File StrCat[resDirectory,"/dummy.txt"] ];
        }
    }

    { Name test_Losses_dynCorr; LastTimeStepOnly 1 ;
        NameOfPostProcessing MagDyn_hphi_dynCorr;
        Operation {
            Print[ totalLoss_dyn[OmegaC], OnGlobal, Format Table, StoreInVariable $indicTotalLoss_dyn, File > StrCat[resDirectory,"/dummy.txt"] ];
            Print[ couplingLoss_dyn[Matrix_partitions_for_TI], OnGlobal, Format Table, StoreInVariable $indicCouplingLoss_dyn, File > StrCat[resDirectory,"/dummy.txt"] ];
        }
    }
}