Include "d:\cernbox\Repositories\steam-fiqus-dev\fiqus\pro_material_functions\ironBHcurves.pro";



    stop_temperature = 500.0;
    /* -------------------------------------------------------------------------- */
    // Checkered support indexing for bare part
    // first index: neighboring information in azimuthal direction
    // second index: neighboring information in radial direction

    bare_1_1 = {1000000};
    bare_2_1 = {1000001};
    bare_1_2 = {1000010};
    bare_2_2 = {1000011};

    // Shell lines belonging to the bare parts as indexed above

    bare_layers_1_1() = {1000004, 1000002, 1000003, 1000005};
    bare_layers_2_1() = {1000008, 1000006, 1000007, 1000009};
    bare_layers_1_2() = {1000014, 1000012, 1000013, 1000015};
    bare_layers_2_2() = {1000018, 1000016, 1000017, 1000019};

    // ------------ BOUNDARY CONDITIONS --------------------------------------------
    // boundary shells where Dirichlet BC applied, there we need two Tdisc
    // indexing follows the one with the bares BUT we have to think of these lines
    // as neighbors belonging to the non-existing exterior bare part, i.e.,
    // the line touching bare_2_1 will then be bare_1_1
    bndDir_1_1() = {};
    bndDir_2_1() = {};
    bndDir_1_2() = {};
    bndDir_2_2() = {};

    // boundary shells where Neumann BC applied, there we need two Tdisc
    // indexing follows the one with the bares BUT we have to think of these lines
    // as neighbors belonging to the non-existing exterior bare part, i.e.,
    // the line touching bare_2_1 will then be bare_1_1
    bndNeu_1_1() = {1000009, 1000013};
    bndNeu_2_1() = {1000004, 1000017};
    bndNeu_1_2() = {1000002, 1000019};
    bndNeu_2_2() = {1000006, 1000014};

    // boundary shells where Robin BC applied, follows the same indexing scheme as
    // Dirichlet, i.e.,
    // indexing follows the one with the bares BUT we have to think of these lines
    // as neighbors belonging to the non-existing exterior bare part, i.e.,
    // the line touching bare_2_1 will then be bare_1_1
    bndRobin_1_1() = { };
    bndRobin_2_1() = { };
    bndRobin_1_2() = { };
    bndRobin_2_2() = { };

    // for Robin and Neumann, we also need to store some information for GetDP to know the
    // outer virtual shell element
    // first index: same as first index of horVer_layers of Robin (simplified) or midLayers (non-simplified)
    // second index: same as first index of bndRobin or bndNeumann
    // third index: same as second index of bndRobin or bndNeumann




    // Neumann




    // QH


    QH_1_1() = {1000013};
    QH_2_1() = {1000017};

    QH_1_2() = {};
    QH_2_2() = {};

    // midLayers
    midLayers_1_1() = {1000020, 1000022};
    midLayers_2_1() = {1000021, 1000022};
    midLayers_1_2() = {1000020, 1000023};
    midLayers_2_2() = {1000021, 1000023};
    midLayers() = {1000020, 1000021, 1000022, 1000023};


    // AUX GROUPS ------------------------------------------------------------------
    allLayers = {1000004, 1000002, 1000003, 1000005, 1000008, 1000006, 1000007, 1000009, 1000014, 1000012, 1000013, 1000015, 1000018, 1000016, 1000017, 1000019};


Group {

      Air = Region[ 4 ];  // Air
      AirInf = Region[ 3 ];  // AirInf
      ht1_EM = Region[ 5 ];
      ht2_EM = Region[ 6 ];
      ht3_EM = Region[ 7 ];
      ht4_EM = Region[ 8 ];



      Surface_Inf = Region[ 2 ];

      Omega_p_EM = Region[ {
ht1_EM, ht3_EM, ht2_EM, ht4_EM} ];



      Omega_aff_EM = Region[ AirInf ];
      Omega_a_EM = Region[ Air ];
      Omega_c_EM = Region[ {Omega_p_EM} ];
      Omega_EM = Region[ {Air, AirInf, Omega_p_EM} ];
      Bd_Omega = Region[ {Surface_Inf}];



    // --------------------- BARE ------------------------------------------------

    // physical regions of the bare blocks
    For i In {1:2}
      For j In {1:2}
        Bare~{i}~{j} = Region[ bare~{i}~{j} ];
        Omega_TH     += Region[ bare~{i}~{j} ];
      EndFor
    EndFor

    // ------------------- SHELLS ------------------------------------------------
    For i In {1:2}
      For j In {1:2}
        // integration domains
        Bare_Layers~{i}~{j}  = Region[ bare_layers~{i}~{j} ];
        Bare_Layers~{i}~{j} += Region[ bndDir~{i}~{j} ];
        Bare_Layers~{i}~{j} += Region[ bndNeu~{i}~{j} ];
        Bare_Layers~{i}~{j} += Region[ bndRobin~{i}~{j} ];

        Bare_Layers~{i}~{j} += Region[ QH~{i}~{j} ];

        Domain_Insulated_Str~{i}~{j} = Region[ { Bare~{i}~{j},
          Bare_Layers~{i}~{j} } ];

        midLayers~{i}~{j} = Region[midLayers~{i}~{j}];

      EndFor
    EndFor

    midLayers = Region[midLayers];


    ht1_TH = Region[ 1000000 ];
    ht2_TH = Region[ 1000001 ];
    ht3_TH = Region[ 1000010 ];
    ht4_TH = Region[ 1000011 ];




    Omega_p_KEKI3RD2_TH = Region[ {
ht1_TH, ht3_TH, ht2_TH, ht4_TH} ];
    Omega_p_TH = Region[ {Omega_p_KEKI3RD2_TH} ];
  Omega_c_TH = Region[ {Omega_p_TH} ];
    Omega_TH = Region[ {Omega_p_TH} ];

    jcZero_KEKI3RD2 = Region[{}];
    jcNonZero_KEKI3RD2 = Region[Omega_p_KEKI3RD2_TH];
    jcNonZero_KEKI3RD2 -= Region[jcZero_KEKI3RD2];

  //      //      //    
  //  //
  // // sum of all



    intDomain_1_1 = Region[{}];
    intDomain_2_1 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_1 += Region[{}];
    intDomain_2_1 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_1 += Region[{}];
    intDomain_2_1 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_1 += Region[{}];
    intDomain_2_1 += Region[{1000002, 1000006}];

    // Robin domains 
    bndRobinInt_1_1_1_1 = Region[{}];
    bndRobinInt_1_1_2_1 = Region[{}];
    bndRobinInt_1_2_1_1 = Region[{}];
    bndRobinInt_1_2_2_1 = Region[{}];
    bndRobinInt_2_1_1_1 = Region[{}];
    bndRobinInt_2_1_2_1 = Region[{}];
    bndRobinInt_2_2_1_1 = Region[{}];
    bndRobinInt_2_2_2_1 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_1 = Region[{}];
    bndNeuInt_1_1_2_1 = Region[{}];
    bndNeuInt_1_2_1_1 = Region[{}];
    bndNeuInt_1_2_2_1 = Region[{}];
    bndNeuInt_2_1_1_1 = Region[{}];
    bndNeuInt_2_1_2_1 = Region[{1000002}];
    bndNeuInt_2_2_1_1 = Region[{}];
    bndNeuInt_2_2_2_1 = Region[{1000006}];
    intDomain_1_2 = Region[{}];
    intDomain_2_2 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_2 += Region[{}];
    intDomain_2_2 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_2 += Region[{}];
    intDomain_2_2 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_2 += Region[{1000009}];
    intDomain_2_2 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_2 = Region[{}];
    bndRobinInt_1_1_2_2 = Region[{}];
    bndRobinInt_1_2_1_2 = Region[{}];
    bndRobinInt_1_2_2_2 = Region[{}];
    bndRobinInt_2_1_1_2 = Region[{}];
    bndRobinInt_2_1_2_2 = Region[{}];
    bndRobinInt_2_2_1_2 = Region[{}];
    bndRobinInt_2_2_2_2 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_2 = Region[{1000009}];
    bndNeuInt_1_1_2_2 = Region[{}];
    bndNeuInt_1_2_1_2 = Region[{}];
    bndNeuInt_1_2_2_2 = Region[{}];
    bndNeuInt_2_1_1_2 = Region[{}];
    bndNeuInt_2_1_2_2 = Region[{}];
    bndNeuInt_2_2_1_2 = Region[{}];
    bndNeuInt_2_2_2_2 = Region[{}];
    intDomain_1_3 = Region[{}];
    intDomain_2_3 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_3 += Region[{}];
    intDomain_2_3 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_3 += Region[{}];
    intDomain_2_3 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_3 += Region[{1000004}];
    intDomain_2_3 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_3 = Region[{}];
    bndRobinInt_1_1_2_3 = Region[{}];
    bndRobinInt_1_2_1_3 = Region[{}];
    bndRobinInt_1_2_2_3 = Region[{}];
    bndRobinInt_2_1_1_3 = Region[{}];
    bndRobinInt_2_1_2_3 = Region[{}];
    bndRobinInt_2_2_1_3 = Region[{}];
    bndRobinInt_2_2_2_3 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_3 = Region[{}];
    bndNeuInt_1_1_2_3 = Region[{}];
    bndNeuInt_1_2_1_3 = Region[{1000004}];
    bndNeuInt_1_2_2_3 = Region[{}];
    bndNeuInt_2_1_1_3 = Region[{}];
    bndNeuInt_2_1_2_3 = Region[{}];
    bndNeuInt_2_2_1_3 = Region[{}];
    bndNeuInt_2_2_2_3 = Region[{}];
    intDomain_1_4 = Region[{1000022}];
    intDomain_2_4 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_4 += Region[{}];
    intDomain_2_4 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_4 += Region[{}];
    intDomain_2_4 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_4 += Region[{}];
    intDomain_2_4 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_4 = Region[{}];
    bndRobinInt_1_1_2_4 = Region[{}];
    bndRobinInt_1_2_1_4 = Region[{}];
    bndRobinInt_1_2_2_4 = Region[{}];
    bndRobinInt_2_1_1_4 = Region[{}];
    bndRobinInt_2_1_2_4 = Region[{}];
    bndRobinInt_2_2_1_4 = Region[{}];
    bndRobinInt_2_2_2_4 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_4 = Region[{}];
    bndNeuInt_1_1_2_4 = Region[{}];
    bndNeuInt_1_2_1_4 = Region[{}];
    bndNeuInt_1_2_2_4 = Region[{}];
    bndNeuInt_2_1_1_4 = Region[{}];
    bndNeuInt_2_1_2_4 = Region[{}];
    bndNeuInt_2_2_1_4 = Region[{}];
    bndNeuInt_2_2_2_4 = Region[{}];
    intDomain_1_5 = Region[{}];
    intDomain_2_5 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_5 += Region[{}];
    intDomain_2_5 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_5 += Region[{}];
    intDomain_2_5 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_5 += Region[{1000019}];
    intDomain_2_5 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_5 = Region[{}];
    bndRobinInt_1_1_2_5 = Region[{}];
    bndRobinInt_1_2_1_5 = Region[{}];
    bndRobinInt_1_2_2_5 = Region[{}];
    bndRobinInt_2_1_1_5 = Region[{}];
    bndRobinInt_2_1_2_5 = Region[{}];
    bndRobinInt_2_2_1_5 = Region[{}];
    bndRobinInt_2_2_2_5 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_5 = Region[{}];
    bndNeuInt_1_1_2_5 = Region[{1000019}];
    bndNeuInt_1_2_1_5 = Region[{}];
    bndNeuInt_1_2_2_5 = Region[{}];
    bndNeuInt_2_1_1_5 = Region[{}];
    bndNeuInt_2_1_2_5 = Region[{}];
    bndNeuInt_2_2_1_5 = Region[{}];
    bndNeuInt_2_2_2_5 = Region[{}];
    intDomain_1_6 = Region[{}];
    intDomain_2_6 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_6 += Region[{}];
    intDomain_2_6 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_6 += Region[{}];
    intDomain_2_6 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_6 += Region[{1000014}];
    intDomain_2_6 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_6 = Region[{}];
    bndRobinInt_1_1_2_6 = Region[{}];
    bndRobinInt_1_2_1_6 = Region[{}];
    bndRobinInt_1_2_2_6 = Region[{}];
    bndRobinInt_2_1_1_6 = Region[{}];
    bndRobinInt_2_1_2_6 = Region[{}];
    bndRobinInt_2_2_1_6 = Region[{}];
    bndRobinInt_2_2_2_6 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_6 = Region[{}];
    bndNeuInt_1_1_2_6 = Region[{}];
    bndNeuInt_1_2_1_6 = Region[{}];
    bndNeuInt_1_2_2_6 = Region[{1000014}];
    bndNeuInt_2_1_1_6 = Region[{}];
    bndNeuInt_2_1_2_6 = Region[{}];
    bndNeuInt_2_2_1_6 = Region[{}];
    bndNeuInt_2_2_2_6 = Region[{}];
    intDomain_1_7 = Region[{1000023}];
    intDomain_2_7 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_7 += Region[{}];
    intDomain_2_7 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_7 += Region[{}];
    intDomain_2_7 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_7 += Region[{}];
    intDomain_2_7 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_7 = Region[{}];
    bndRobinInt_1_1_2_7 = Region[{}];
    bndRobinInt_1_2_1_7 = Region[{}];
    bndRobinInt_1_2_2_7 = Region[{}];
    bndRobinInt_2_1_1_7 = Region[{}];
    bndRobinInt_2_1_2_7 = Region[{}];
    bndRobinInt_2_2_1_7 = Region[{}];
    bndRobinInt_2_2_2_7 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_7 = Region[{}];
    bndNeuInt_1_1_2_7 = Region[{}];
    bndNeuInt_1_2_1_7 = Region[{}];
    bndNeuInt_1_2_2_7 = Region[{}];
    bndNeuInt_2_1_1_7 = Region[{}];
    bndNeuInt_2_1_2_7 = Region[{}];
    bndNeuInt_2_2_1_7 = Region[{}];
    bndNeuInt_2_2_2_7 = Region[{}];
    intDomain_1_8 = Region[{}];
    intDomain_2_8 = Region[{1000020}];

    // add Robin boundary conditions
    intDomain_1_8 += Region[{}];
    intDomain_2_8 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_8 += Region[{}];
    intDomain_2_8 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_8 += Region[{}];
    intDomain_2_8 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_8 = Region[{}];
    bndRobinInt_1_1_2_8 = Region[{}];
    bndRobinInt_1_2_1_8 = Region[{}];
    bndRobinInt_1_2_2_8 = Region[{}];
    bndRobinInt_2_1_1_8 = Region[{}];
    bndRobinInt_2_1_2_8 = Region[{}];
    bndRobinInt_2_2_1_8 = Region[{}];
    bndRobinInt_2_2_2_8 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_8 = Region[{}];
    bndNeuInt_1_1_2_8 = Region[{}];
    bndNeuInt_1_2_1_8 = Region[{}];
    bndNeuInt_1_2_2_8 = Region[{}];
    bndNeuInt_2_1_1_8 = Region[{}];
    bndNeuInt_2_1_2_8 = Region[{}];
    bndNeuInt_2_2_1_8 = Region[{}];
    bndNeuInt_2_2_2_8 = Region[{}];
    intDomain_1_9 = Region[{}];
    intDomain_2_9 = Region[{1000021}];

    // add Robin boundary conditions
    intDomain_1_9 += Region[{}];
    intDomain_2_9 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_9 += Region[{}];
    intDomain_2_9 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_9 += Region[{}];
    intDomain_2_9 += Region[{}];

    // Robin domains 
    bndRobinInt_1_1_1_9 = Region[{}];
    bndRobinInt_1_1_2_9 = Region[{}];
    bndRobinInt_1_2_1_9 = Region[{}];
    bndRobinInt_1_2_2_9 = Region[{}];
    bndRobinInt_2_1_1_9 = Region[{}];
    bndRobinInt_2_1_2_9 = Region[{}];
    bndRobinInt_2_2_1_9 = Region[{}];
    bndRobinInt_2_2_2_9 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_9 = Region[{}];
    bndNeuInt_1_1_2_9 = Region[{}];
    bndNeuInt_1_2_1_9 = Region[{}];
    bndNeuInt_1_2_2_9 = Region[{}];
    bndNeuInt_2_1_1_9 = Region[{}];
    bndNeuInt_2_1_2_9 = Region[{}];
    bndNeuInt_2_2_1_9 = Region[{}];
    bndNeuInt_2_2_2_9 = Region[{}];
    intDomain_1_10 = Region[{}];
    intDomain_2_10 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_10 += Region[{}];
    intDomain_2_10 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_10 += Region[{}];
    intDomain_2_10 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_10 += Region[{}];
    intDomain_2_10 += Region[{1000013}];

    // Robin domains 
    bndRobinInt_1_1_1_10 = Region[{}];
    bndRobinInt_1_1_2_10 = Region[{}];
    bndRobinInt_1_2_1_10 = Region[{}];
    bndRobinInt_1_2_2_10 = Region[{}];
    bndRobinInt_2_1_1_10 = Region[{}];
    bndRobinInt_2_1_2_10 = Region[{}];
    bndRobinInt_2_2_1_10 = Region[{}];
    bndRobinInt_2_2_2_10 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_10 = Region[{}];
    bndNeuInt_1_1_2_10 = Region[{}];
    bndNeuInt_1_2_1_10 = Region[{}];
    bndNeuInt_1_2_2_10 = Region[{}];
    bndNeuInt_2_1_1_10 = Region[{1000013}];
    bndNeuInt_2_1_2_10 = Region[{}];
    bndNeuInt_2_2_1_10 = Region[{}];
    bndNeuInt_2_2_2_10 = Region[{}];
    intDomain_1_11 = Region[{}];
    intDomain_2_11 = Region[{}];

    // add Robin boundary conditions
    intDomain_1_11 += Region[{}];
    intDomain_2_11 += Region[{}];

    // add Dirichlet boundary conditions
    intDomain_1_11 += Region[{}];
    intDomain_2_11 += Region[{}];

    // add Neumann boundary conditions
    intDomain_1_11 += Region[{}];
    intDomain_2_11 += Region[{1000017}];

    // Robin domains 
    bndRobinInt_1_1_1_11 = Region[{}];
    bndRobinInt_1_1_2_11 = Region[{}];
    bndRobinInt_1_2_1_11 = Region[{}];
    bndRobinInt_1_2_2_11 = Region[{}];
    bndRobinInt_2_1_1_11 = Region[{}];
    bndRobinInt_2_1_2_11 = Region[{}];
    bndRobinInt_2_2_1_11 = Region[{}];
    bndRobinInt_2_2_2_11 = Region[{}];

    // Neumann domains
    bndNeuInt_1_1_1_11 = Region[{}];
    bndNeuInt_1_1_2_11 = Region[{}];
    bndNeuInt_1_2_1_11 = Region[{}];
    bndNeuInt_1_2_2_11 = Region[{}];
    bndNeuInt_2_1_1_11 = Region[{}];
    bndNeuInt_2_1_2_11 = Region[{}];
    bndNeuInt_2_2_1_11 = Region[{1000017}];
    bndNeuInt_2_2_2_11 = Region[{}];


    projection_points = Region[ 4000000 ];

}

Function {

      mu0 = 4.e-7 * Pi;
      nu [ Region[{Air, Omega_p_EM, AirInf}] ]  = 1. / mu0;


      js_fct[ ht1_EM ] = 7180.0/SurfaceArea[]{ 5 };
      js_fct[ ht3_EM ] = 7180.0/SurfaceArea[]{ 7 };
      js_fct[ ht2_EM ] = 7180.0/SurfaceArea[]{ 6 };
      js_fct[ ht4_EM ] = 7180.0/SurfaceArea[]{ 8 };



  area_fct[ ht1_TH ] = SurfaceArea[]{ 1000000 };
  area_fct[ ht3_TH ] = SurfaceArea[]{ 1000010 };
  area_fct[ ht2_TH ] = SurfaceArea[]{ 1000001 };
  area_fct[ ht4_TH ] = SurfaceArea[]{ 1000011 };

      bnd_dirichlet_1() = {};
      val_temperature_1 = 4.2;
      num_dirichlet = 1;  // number of different dirichlet boundary cond.

      bnd_neumann_1() = {1000002, 1000004, 1000006, 1000009, 1000013, 1000014, 1000017, 1000019};
      val_heatFlux_1 = 0.0;
      bnd_neumann_2() = {};
      val_heatFlux_2 = 100000.0;
      num_neumann = 2;  // number of different neumann boundary cond.

      bnd_robin_1() = {};
      val_heatExchCoeff_1[] = 10000.0;
      val_Tinf_1 = 4.2;
      num_robin = 1;  // number of different robin boundary cond.

      // time steps adaptive time stepping must hit
      Breakpoints = {0.01, 0.05};

        // first idx: 1 layers parallel to radial direction (== normal to phi unit vector)
        //            2 layers parallel to azimuthal direction (== normal to r unit vector)
        // second and third idx: same as bare layers
        // this gives the relation between radius/angle and index 0 to n_ele
      outerElem_1_1_1_1 = 0;
      outerElem_2_1_1_1 = 0;

      outerElem_1_2_1_1 = 1;
      outerElem_2_2_1_1 = 0;

      outerElem_1_1_2_1 = 0;
      outerElem_2_1_2_1 = 1;

      outerElem_1_2_2_1 = 1;
      outerElem_2_2_2_1 = 1;
      outerElem_1_1_1_2 = 0;
      outerElem_2_1_1_2 = 0;

      outerElem_1_2_1_2 = 1;
      outerElem_2_2_1_2 = 0;

      outerElem_1_1_2_2 = 0;
      outerElem_2_1_2_2 = 1;

      outerElem_1_2_2_2 = 1;
      outerElem_2_2_2_2 = 1;
      outerElem_1_1_1_3 = 0;
      outerElem_2_1_1_3 = 0;

      outerElem_1_2_1_3 = 3;
      outerElem_2_2_1_3 = 0;

      outerElem_1_1_2_3 = 0;
      outerElem_2_1_2_3 = 3;

      outerElem_1_2_2_3 = 3;
      outerElem_2_2_2_3 = 3;
      outerElem_1_1_1_4 = 0;
      outerElem_2_1_1_4 = 0;

      outerElem_1_2_1_4 = 2;
      outerElem_2_2_1_4 = 0;

      outerElem_1_1_2_4 = 0;
      outerElem_2_1_2_4 = 2;

      outerElem_1_2_2_4 = 2;
      outerElem_2_2_2_4 = 2;
      outerElem_1_1_1_5 = 0;
      outerElem_2_1_1_5 = 0;

      outerElem_1_2_1_5 = 3;
      outerElem_2_2_1_5 = 0;

      outerElem_1_1_2_5 = 0;
      outerElem_2_1_2_5 = 3;

      outerElem_1_2_2_5 = 3;
      outerElem_2_2_2_5 = 3;
      outerElem_1_1_1_6 = 0;
      outerElem_2_1_1_6 = 0;

      outerElem_1_2_1_6 = 1;
      outerElem_2_2_1_6 = 0;

      outerElem_1_1_2_6 = 0;
      outerElem_2_1_2_6 = 1;

      outerElem_1_2_2_6 = 1;
      outerElem_2_2_2_6 = 1;
      outerElem_1_1_1_7 = 0;
      outerElem_2_1_1_7 = 0;

      outerElem_1_2_1_7 = 2;
      outerElem_2_2_1_7 = 0;

      outerElem_1_1_2_7 = 0;
      outerElem_2_1_2_7 = 2;

      outerElem_1_2_2_7 = 2;
      outerElem_2_2_2_7 = 2;
      outerElem_1_1_1_8 = 0;
      outerElem_2_1_1_8 = 0;

      outerElem_1_2_1_8 = 7;
      outerElem_2_2_1_8 = 0;

      outerElem_1_1_2_8 = 0;
      outerElem_2_1_2_8 = 7;

      outerElem_1_2_2_8 = 7;
      outerElem_2_2_2_8 = 7;
      outerElem_1_1_1_9 = 0;
      outerElem_2_1_1_9 = 0;

      outerElem_1_2_1_9 = 7;
      outerElem_2_2_1_9 = 0;

      outerElem_1_1_2_9 = 0;
      outerElem_2_1_2_9 = 7;

      outerElem_1_2_2_9 = 7;
      outerElem_2_2_2_9 = 7;
      outerElem_1_1_1_10 = 0;
      outerElem_2_1_1_10 = 0;

      outerElem_1_2_1_10 = 6;
      outerElem_2_2_1_10 = 0;

      outerElem_1_1_2_10 = 0;
      outerElem_2_1_2_10 = 6;

      outerElem_1_2_2_10 = 6;
      outerElem_2_2_2_10 = 6;
      outerElem_1_1_1_11 = 0;
      outerElem_2_1_1_11 = 0;

      outerElem_1_2_1_11 = 6;
      outerElem_2_2_1_11 = 0;

      outerElem_1_1_2_11 = 0;
      outerElem_2_1_2_11 = 6;

      outerElem_1_2_2_11 = 6;
      outerElem_2_2_2_11 = 6;



      // --------------- MATERIAL FUNCTIONS ----------------------------------------



                                               
  	    f_inner_voids_KEKI3RD2 = 0.03;
        f_outer_voids_KEKI3RD2 = 0.094414900743883;
        f_strand_KEKI3RD2 = 1.0 - (0.03 + 0.094414900743883);

      f_stabilizer_KEKI3RD2 = f_strand_KEKI3RD2 * 1.17 / (1. + 1.17);
      f_sc_KEKI3RD2 = f_strand_KEKI3RD2 * (1.0 - 1.17 / (1. + 1.17));

      source_current = 7180.0;

        criticalCurrentDensity[jcNonZero_KEKI3RD2] = $Time > 1000000.0? 0: CFUN_Jc_NbTi_Cudi_fit1_T_B[$1, Norm[$2]]{9.2, 14.5, 84230.0, -6547.6, 0.004229668072083199, 1.17} * f_sc_KEKI3RD2;


      rho[Omega_p_KEKI3RD2_TH] = EffectiveResistivity[CFUN_rhoCu_T_B[$1, Norm[$2]]{179.8259386}]{f_stabilizer_KEKI3RD2};

      // effective thermal conductivity of the bare part
      kappa[Omega_p_KEKI3RD2_TH] = RuleOfMixtures[
        CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 179.8259386}, CFUN_rhoCu_T_B[$1, Norm[$2]]{179.8259386}]{179.8259386}
      , CFUN_kKapton_T[$1]
    ]
    {f_stabilizer_KEKI3RD2
      ,  f_outer_voids_KEKI3RD2
    };

      // heat capacity of bare part
      heatCap[Omega_p_KEKI3RD2_TH] = RuleOfMixtures[
        CFUN_CvCu_T[$1],
        CFUN_CvNbTi_T_B[$1, Norm[$2]]{7180.0, 84230.0, -6547.6}, 
        CFUN_CvHe_T[$1],
        CFUN_CvKapton_T[$1]
      ]
      {
        f_stabilizer_KEKI3RD2,
        f_sc_KEKI3RD2,
        f_inner_voids_KEKI3RD2,
        f_outer_voids_KEKI3RD2
      };

      // joule losses of bare part
      jouleLosses[] = CFUN_quenchState_Ic[criticalCurrentDensity[$1, $2] * area_fct[]]{source_current} * rho[$1, $2] * SquNorm[source_current/area_fct[]];



      For i In {1:num_dirichlet}
        // piece-wise defined const_temp
        const_temp[Region[bnd_dirichlet~{i}]] = val_temperature~{i};
      EndFor

      For n In {1:num_neumann}
        // piece-wise defined heatFlux
        heatFlux[Region[bnd_neumann~{n}]] = val_heatFlux~{n};
      EndFor

      For r In {1:num_robin}
        // piece-wise defined heatExchCoeff
        heatExchCoeff[Region[bnd_robin~{r}]] = val_heatExchCoeff~{r}[$1, $2];
        Tinf[Region[bnd_robin~{r}]] = val_Tinf~{r};
      EndFor

      // --------------- Thickness function ----------------------------------------
      // check if we need to flip the order of the layers
      // TODO: this is not very elegant, but it works
      // TODO: check if delta[i] instead of delta~{i} works

              delta_0[Region[1000002]] = 0.0001;
                  thermalConductivityMass_1_1_0[Region[1000002]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000002]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000002]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000002]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000002]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000002]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000002]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000002]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000002]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000002]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000002]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000002]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000006]] = 0.0001;
                  thermalConductivityMass_1_1_0[Region[1000006]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000006]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000006]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000006]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000006]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000006]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000006]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000006]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000006]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000006]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000006]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000006]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000009]] = 8.4e-05;
                  thermalConductivityMass_1_1_0[Region[1000009]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000009]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000009]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000009]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000009]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000009]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000009]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000009]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000009]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000009]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000009]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000009]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000004]] = 5e-05;
                  thermalConductivityMass_1_1_2[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_2[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_2[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_2[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_2[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_2[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_2[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_2[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_2[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_2[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_2[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_2[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000004]] = 0.0001;
                  thermalConductivityMass_1_1_1[Region[1000004]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_1[Region[1000004]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_1[Region[1000004]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_1[Region[1000004]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_1[Region[1000004]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_1[Region[1000004]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_1[Region[1000004]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_1[Region[1000004]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_1[Region[1000004]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_1[Region[1000004]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_1[Region[1000004]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_1[Region[1000004]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000004]] = 8.4e-05;
                  thermalConductivityMass_1_1_0[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000004]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000004]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000004]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000022]] = 8.4e-05;
            thermalConductivityMass_1_1_0[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_0[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_0[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_0[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_0[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_0[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_0[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_0[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_0[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_0[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_0[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_0[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000022]] = 8.4e-05;
            thermalConductivityMass_1_1_1[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_1[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_1[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_1[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_1[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_1[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_1[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_1[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_1[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_1[Region[1000022]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_1[Region[1000022]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_1[Region[1000022]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000019]] = 8.4e-05;
                  thermalConductivityMass_1_1_2[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_2[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_2[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_2[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_2[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_2[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_2[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_2[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_2[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_2[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_2[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_2[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000019]] = 0.0001;
                  thermalConductivityMass_1_1_1[Region[1000019]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_1[Region[1000019]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_1[Region[1000019]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_1[Region[1000019]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_1[Region[1000019]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_1[Region[1000019]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_1[Region[1000019]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_1[Region[1000019]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_1[Region[1000019]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_1[Region[1000019]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_1[Region[1000019]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_1[Region[1000019]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000019]] = 5e-05;
                  thermalConductivityMass_1_1_0[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000019]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000019]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000019]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000014]] = 8.4e-05;
                  thermalConductivityMass_1_1_0[Region[1000014]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000014]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000014]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000014]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000014]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000014]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000014]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000014]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000014]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000014]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000014]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000014]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000023]] = 8.4e-05;
            thermalConductivityMass_1_1_0[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_0[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_0[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_0[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_0[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_0[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_0[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_0[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_0[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_0[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_0[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_0[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000023]] = 8.4e-05;
            thermalConductivityMass_1_1_1[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_1[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_1[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_1[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_1[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_1[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_1[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_1[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_1[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_1[Region[1000023]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_1[Region[1000023]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_1[Region[1000023]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000020]] = 0.0001;
            thermalConductivityMass_1_1_0[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_0[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_0[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_0[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_0[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_0[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_0[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_0[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_0[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_0[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_0[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_0[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000020]] = 7e-05;
            thermalConductivityMass_1_1_1[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_1[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_1[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_1[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_1[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_1[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_1[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_1[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_1[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_1[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_1[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_1[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000020]] = 4e-05;
            thermalConductivityMass_1_1_2[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_2[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_2[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_2[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_2[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_2[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_2[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_2[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_2[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_2[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_2[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_2[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_3[Region[1000020]] = 2.5e-05;
            thermalConductivityMass_1_1_3[Region[1000020]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_3[Region[1000020]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_3[Region[1000020]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_3[Region[1000020]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_3[Region[1000020]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_3[Region[1000020]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_3[Region[1000020]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_3[Region[1000020]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_3[Region[1000020]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_3[Region[1000020]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_3[Region[1000020]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_3[Region[1000020]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 2, 2};
              delta_4[Region[1000020]] = 4e-05;
            thermalConductivityMass_1_1_4[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_4[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_4[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_4[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_4[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_4[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_4[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_4[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_4[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_4[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_4[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_4[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_5[Region[1000020]] = 0.0001;
            thermalConductivityMass_1_1_5[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_5[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_5[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_5[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_5[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_5[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_5[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_5[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_5[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_5[Region[1000020]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_5[Region[1000020]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_5[Region[1000020]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_6[Region[1000020]] = 0.0001;
            thermalConductivityMass_1_1_6[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_6[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_6[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_6[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_6[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_6[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_6[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_6[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_6[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_6[Region[1000020]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_6[Region[1000020]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_6[Region[1000020]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000021]] = 0.0001;
            thermalConductivityMass_1_1_0[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_0[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_0[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_0[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_0[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_0[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_0[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_0[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_0[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_0[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_0[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_0[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000021]] = 7e-05;
            thermalConductivityMass_1_1_1[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_1[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_1[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_1[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_1[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_1[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_1[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_1[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_1[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_1[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_1[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_1[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000021]] = 4e-05;
            thermalConductivityMass_1_1_2[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_2[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_2[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_2[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_2[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_2[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_2[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_2[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_2[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_2[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_2[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_2[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_3[Region[1000021]] = 2.5e-05;
            thermalConductivityMass_1_1_3[Region[1000021]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_3[Region[1000021]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_3[Region[1000021]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_3[Region[1000021]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_3[Region[1000021]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_3[Region[1000021]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_3[Region[1000021]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_3[Region[1000021]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_3[Region[1000021]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_3[Region[1000021]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_3[Region[1000021]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_3[Region[1000021]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 2, 2};
              delta_4[Region[1000021]] = 4e-05;
            thermalConductivityMass_1_1_4[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_4[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_4[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_4[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_4[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_4[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_4[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_4[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_4[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_4[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_4[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_4[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_5[Region[1000021]] = 0.0001;
            thermalConductivityMass_1_1_5[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_5[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_5[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_5[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_5[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_5[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_5[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_5[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_5[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_5[Region[1000021]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_5[Region[1000021]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_5[Region[1000021]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_6[Region[1000021]] = 0.0001;
            thermalConductivityMass_1_1_6[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

            thermalConductivityStiffness_1_1_6[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

            specificHeatCapacity_1_1_6[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
            thermalConductivityMass_1_2_6[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

            thermalConductivityStiffness_1_2_6[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

            specificHeatCapacity_1_2_6[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
            thermalConductivityMass_2_1_6[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

            thermalConductivityStiffness_2_1_6[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

            specificHeatCapacity_2_1_6[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
            thermalConductivityMass_2_2_6[Region[1000021]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

            thermalConductivityStiffness_2_2_6[Region[1000021]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

            specificHeatCapacity_2_2_6[Region[1000021]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_5[Region[1000013]] = 0.0001;
                  thermalConductivityMass_1_1_5[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_5[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_5[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_5[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_5[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_5[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_5[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_5[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_5[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_5[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_5[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_5[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_4[Region[1000013]] = 7e-05;
                  thermalConductivityMass_1_1_4[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_4[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_4[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_4[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_4[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_4[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_4[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_4[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_4[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_4[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_4[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_4[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_3[Region[1000013]] = 4e-05;
                  thermalConductivityMass_1_1_3[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_3[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_3[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_3[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_3[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_3[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_3[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_3[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_3[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_3[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_3[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_3[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000013]] = 2.5e-05;
                  thermalConductivityMass_1_1_2[Region[1000013]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_2[Region[1000013]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_2[Region[1000013]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_2[Region[1000013]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_2[Region[1000013]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_2[Region[1000013]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_2[Region[1000013]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_2[Region[1000013]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_2[Region[1000013]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_2[Region[1000013]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_2[Region[1000013]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_2[Region[1000013]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000013]] = 4e-05;
                  thermalConductivityMass_1_1_1[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_1[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_1[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_1[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_1[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_1[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_1[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_1[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_1[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_1[Region[1000013]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_1[Region[1000013]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_1[Region[1000013]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000013]] = 0.0001;
                  thermalConductivityMass_1_1_0[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000013]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000013]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000013]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_5[Region[1000017]] = 0.0001;
                  thermalConductivityMass_1_1_5[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_5[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_5[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_5[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_5[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_5[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_5[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_5[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_5[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_5[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_5[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_5[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_4[Region[1000017]] = 7e-05;
                  thermalConductivityMass_1_1_4[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_4[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_4[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_4[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_4[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_4[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_4[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_4[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_4[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_4[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_4[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_4[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};
              delta_3[Region[1000017]] = 4e-05;
                  thermalConductivityMass_1_1_3[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_3[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_3[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_3[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_3[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_3[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_3[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_3[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_3[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_3[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_3[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_3[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_2[Region[1000017]] = 2.5e-05;
                  thermalConductivityMass_1_1_2[Region[1000017]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_2[Region[1000017]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_2[Region[1000017]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_2[Region[1000017]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_2[Region[1000017]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_2[Region[1000017]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_2[Region[1000017]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_2[Region[1000017]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_2[Region[1000017]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_2[Region[1000017]] = TSA_CFUN_kSteel_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_2[Region[1000017]] = TSA_CFUN_kSteel_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_2[Region[1000017]] = TSA_CFUN_CvSteel_T_mass[$1, $2, $3]{2, 2, 2};
              delta_1[Region[1000017]] = 4e-05;
                  thermalConductivityMass_1_1_1[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_1[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_1[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_1[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_1[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_1[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_1[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_1[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_1[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_1[Region[1000017]] = TSA_CFUN_kKapton_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_1[Region[1000017]] = TSA_CFUN_kKapton_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_1[Region[1000017]] = TSA_CFUN_CvKapton_T_mass[$1, $2, $3]{2, 2, 2};
              delta_0[Region[1000017]] = 0.0001;
                  thermalConductivityMass_1_1_0[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 1, 2};

                  thermalConductivityStiffness_1_1_0[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 1, 2};

                  specificHeatCapacity_1_1_0[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 1, 2};
                  thermalConductivityMass_1_2_0[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{1, 2, 2};

                  thermalConductivityStiffness_1_2_0[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{1, 2, 2};

                  specificHeatCapacity_1_2_0[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{1, 2, 2};
                  thermalConductivityMass_2_1_0[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 1, 2};

                  thermalConductivityStiffness_2_1_0[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 1, 2};

                  specificHeatCapacity_2_1_0[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 1, 2};
                  thermalConductivityMass_2_2_0[Region[1000017]] = TSA_CFUN_kG10_T_mass[$1, $2, $3]{2, 2, 2};

                  thermalConductivityStiffness_2_2_0[Region[1000017]] = TSA_CFUN_kG10_T_stiffness[$1, $2, $3]{2, 2, 2};

                  specificHeatCapacity_2_2_0[Region[1000017]] = TSA_CFUN_CvG10_T_mass[$1, $2, $3]{2, 2, 2};


                powerDensity_1_0[Region[1000020]] = 0;
                powerDensity_2_0[Region[1000020]] = 0;

                powerDensity_1_1[Region[1000020]] = 0;
                powerDensity_2_1[Region[1000020]] = 0;

                powerDensity_1_2[Region[1000020]] = 0;
                powerDensity_2_2[Region[1000020]] = 0;

                powerDensity_1_3[Region[1000020]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{999.0, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 1, 2};
                powerDensity_2_3[Region[1000020]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{999.0, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 2, 2};

                powerDensity_1_4[Region[1000020]] = 0;
                powerDensity_2_4[Region[1000020]] = 0;

                powerDensity_1_5[Region[1000020]] = 0;
                powerDensity_2_5[Region[1000020]] = 0;

                powerDensity_1_6[Region[1000020]] = 0;
                powerDensity_2_6[Region[1000020]] = 0;

                powerDensity_1_0[Region[1000021]] = 0;
                powerDensity_2_0[Region[1000021]] = 0;

                powerDensity_1_1[Region[1000021]] = 0;
                powerDensity_2_1[Region[1000021]] = 0;

                powerDensity_1_2[Region[1000021]] = 0;
                powerDensity_2_2[Region[1000021]] = 0;

                powerDensity_1_3[Region[1000021]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{0.05, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 1, 2};
                powerDensity_2_3[Region[1000021]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{0.05, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 2, 2};

                powerDensity_1_4[Region[1000021]] = 0;
                powerDensity_2_4[Region[1000021]] = 0;

                powerDensity_1_5[Region[1000021]] = 0;
                powerDensity_2_5[Region[1000021]] = 0;

                powerDensity_1_6[Region[1000021]] = 0;
                powerDensity_2_6[Region[1000021]] = 0;

                powerDensity_1_5[Region[1000013]] = 0;
                powerDensity_2_5[Region[1000013]] = 0;

                powerDensity_1_4[Region[1000013]] = 0;
                powerDensity_2_4[Region[1000013]] = 0;

                powerDensity_1_3[Region[1000013]] = 0;
                powerDensity_2_3[Region[1000013]] = 0;

                powerDensity_1_2[Region[1000013]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{0.01, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 1, 2};
                powerDensity_2_2[Region[1000013]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{0.01, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 2, 2};

                powerDensity_1_1[Region[1000013]] = 0;
                powerDensity_2_1[Region[1000013]] = 0;

                powerDensity_1_0[Region[1000013]] = 0;
                powerDensity_2_0[Region[1000013]] = 0;

                powerDensity_1_5[Region[1000017]] = 0;
                powerDensity_2_5[Region[1000017]] = 0;

                powerDensity_1_4[Region[1000017]] = 0;
                powerDensity_2_4[Region[1000017]] = 0;

                powerDensity_1_3[Region[1000017]] = 0;
                powerDensity_2_3[Region[1000017]] = 0;

                powerDensity_1_2[Region[1000017]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{999.0, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 1, 2};
                powerDensity_2_2[Region[1000017]] = TSA_CFUN_QHCircuit_t_T_rhs[$Time, $1, $2, $3]{999.0, 225.0, 0.0282, 0.02123, 0.011, 2.5e-05, 3.185, 1, 2, 2};

                powerDensity_1_1[Region[1000017]] = 0;
                powerDensity_2_1[Region[1000017]] = 0;

                powerDensity_1_0[Region[1000017]] = 0;
                powerDensity_2_0[Region[1000017]] = 0;


}



    // split to avoid error for two touching lines in different intDomains

    Constraint {
      coordList_Python_1_1() = {0.0112375, 0.0, 0.0, 0.011, 0.0, 0.0, 0.0112375, 0.001487, 0.0, 0.011, 0.001487, 0.0, 0.011237500000000001, 0.0007434999999999994, 0.0, 0.011, 0.0007435000000000007, 0.0, 0.0, 0.001571, 0.0, 0.0, 0.001487, 0.0, 0.011, 0.001571, 0.0, 0.011, 0.001487, 0.0, 0.000785714285714285, 0.001571, 0.0, 0.000785714285714285, 0.001487, 0.0, 0.00157142857142857, 0.001571, 0.0, 0.00157142857142857, 0.001487, 0.0, 0.0023571428571428554, 0.001571, 0.0, 0.0023571428571428554, 0.001487, 0.0, 0.0031428571428571404, 0.001571, 0.0, 0.0031428571428571404, 0.001487, 0.0, 0.003928571428571427, 0.001571, 0.0, 0.003928571428571427, 0.001487, 0.0, 0.004714285714285712, 0.001571, 0.0, 0.004714285714285712, 0.001487, 0.0, 0.005499999999999997, 0.001571, 0.0, 0.005499999999999997, 0.001487, 0.0, 0.006285714285714283, 0.001571, 0.0, 0.006285714285714283, 0.001487, 0.0, 0.00707142857142857, 0.001571, 0.0, 0.00707142857142857, 0.001487, 0.0, 0.007857142857142856, 0.001571, 0.0, 0.007857142857142856, 0.001487, 0.0, 0.008642857142857141, 0.001571, 0.0, 0.008642857142857141, 0.001487, 0.0, 0.009428571428571429, 0.001571, 0.0, 0.009428571428571429, 0.001487, 0.0, 0.010214285714285716, 0.001571, 0.0, 0.010214285714285716, 0.001487, 0.0};
      coordList_Python_2_1() = {0.0112375, 0.001655, 0.0, 0.011, 0.001655, 0.0, 0.0112375, 0.003142, 0.0, 0.011, 0.003142, 0.0, 0.011237500000000001, 0.0023984999999999996, 0.0, 0.011, 0.0023985000000000005, 0.0, 0.0, 0.001571, 0.0, 0.0, 0.001655, 0.0, 0.011, 0.001571, 0.0, 0.011, 0.001655, 0.0, 0.000785714285714285, 0.001571, 0.0, 0.000785714285714285, 0.001655, 0.0, 0.00157142857142857, 0.001571, 0.0, 0.00157142857142857, 0.001655, 0.0, 0.0023571428571428554, 0.001571, 0.0, 0.0023571428571428554, 0.001655, 0.0, 0.0031428571428571404, 0.001571, 0.0, 0.0031428571428571404, 0.001655, 0.0, 0.003928571428571427, 0.001571, 0.0, 0.003928571428571427, 0.001655, 0.0, 0.004714285714285712, 0.001571, 0.0, 0.004714285714285712, 0.001655, 0.0, 0.005499999999999997, 0.001571, 0.0, 0.005499999999999997, 0.001655, 0.0, 0.006285714285714283, 0.001571, 0.0, 0.006285714285714283, 0.001655, 0.0, 0.00707142857142857, 0.001571, 0.0, 0.00707142857142857, 0.001655, 0.0, 0.007857142857142856, 0.001571, 0.0, 0.007857142857142856, 0.001655, 0.0, 0.008642857142857141, 0.001571, 0.0, 0.008642857142857141, 0.001655, 0.0, 0.009428571428571429, 0.001571, 0.0, 0.009428571428571429, 0.001655, 0.0, 0.010214285714285716, 0.001571, 0.0, 0.010214285714285716, 0.001655, 0.0};
      coordList_Python_1_2() = {0.0112375, 0.0, 0.0, 0.011475, 0.0, 0.0, 0.0112375, 0.001487, 0.0, 0.011475, 0.001487, 0.0, 0.011237500000000001, 0.0007434999999999994, 0.0, 0.011475, 0.0007435000000000007, 0.0, 0.011475, 0.001571, 0.0, 0.011475, 0.001487, 0.0, 0.022475, 0.001571, 0.0, 0.022475, 0.001487, 0.0, 0.012260714285714286, 0.001571, 0.0, 0.012260714285714286, 0.001487, 0.0, 0.01304642857142857, 0.001571, 0.0, 0.01304642857142857, 0.001487, 0.0, 0.013832142857142855, 0.001571, 0.0, 0.013832142857142855, 0.001487, 0.0, 0.01461785714285714, 0.001571, 0.0, 0.01461785714285714, 0.001487, 0.0, 0.015403571428571428, 0.001571, 0.0, 0.015403571428571428, 0.001487, 0.0, 0.01618928571428571, 0.001571, 0.0, 0.01618928571428571, 0.001487, 0.0, 0.016974999999999997, 0.001571, 0.0, 0.016974999999999997, 0.001487, 0.0, 0.017760714285714282, 0.001571, 0.0, 0.017760714285714282, 0.001487, 0.0, 0.01854642857142857, 0.001571, 0.0, 0.01854642857142857, 0.001487, 0.0, 0.019332142857142857, 0.001571, 0.0, 0.019332142857142857, 0.001487, 0.0, 0.020117857142857142, 0.001571, 0.0, 0.020117857142857142, 0.001487, 0.0, 0.02090357142857143, 0.001571, 0.0, 0.02090357142857143, 0.001487, 0.0, 0.021689285714285716, 0.001571, 0.0, 0.021689285714285716, 0.001487, 0.0};
      coordList_Python_2_2() = {0.0112375, 0.001655, 0.0, 0.011475, 0.001655, 0.0, 0.0112375, 0.003142, 0.0, 0.011475, 0.003142, 0.0, 0.011237500000000001, 0.0023984999999999996, 0.0, 0.011475, 0.0023985000000000005, 0.0, 0.011475, 0.001571, 0.0, 0.011475, 0.001655, 0.0, 0.022475, 0.001571, 0.0, 0.022475, 0.001655, 0.0, 0.012260714285714286, 0.001571, 0.0, 0.012260714285714286, 0.001655, 0.0, 0.01304642857142857, 0.001571, 0.0, 0.01304642857142857, 0.001655, 0.0, 0.013832142857142855, 0.001571, 0.0, 0.013832142857142855, 0.001655, 0.0, 0.01461785714285714, 0.001571, 0.0, 0.01461785714285714, 0.001655, 0.0, 0.015403571428571428, 0.001571, 0.0, 0.015403571428571428, 0.001655, 0.0, 0.01618928571428571, 0.001571, 0.0, 0.01618928571428571, 0.001655, 0.0, 0.016974999999999997, 0.001571, 0.0, 0.016974999999999997, 0.001655, 0.0, 0.017760714285714282, 0.001571, 0.0, 0.017760714285714282, 0.001655, 0.0, 0.01854642857142857, 0.001571, 0.0, 0.01854642857142857, 0.001655, 0.0, 0.019332142857142857, 0.001571, 0.0, 0.019332142857142857, 0.001655, 0.0, 0.020117857142857142, 0.001571, 0.0, 0.020117857142857142, 0.001655, 0.0, 0.02090357142857143, 0.001571, 0.0, 0.02090357142857143, 0.001655, 0.0, 0.021689285714285716, 0.001571, 0.0, 0.021689285714285716, 0.001655, 0.0};
      For i In {1:2}
        For j In {1:2}
          { Name Temperature~{i}~{j} ;
            Case {
                // Link DoF of auxiliary shells to actual temperature
              { Region midLayers~{i}~{j} ; Type Link;
                RegionRef Bare_Layers~{i}~{j} ; Coefficient 1;
                // coordList or coordList_Python
                Function shiftCoordinate[X[], Y[], Z[]]{coordList_Python~{i}~{j}()};
              }
              If (num_dirichlet > 0)
                // TODO: proper time dependent boundary conditions
                { Region Region[bndDir~{i}~{j}]; Type Assign;
                  Value const_temp[]; }
              EndIf
            }
          }
        EndFor
      EndFor

    }


Constraint {
  { Name Dirichlet_a_Mag;
    Case {
      { Region Bd_Omega ; Value 0.; }
    }
  }
  { Name SourceCurrentDensityZ;
    Case {
      { Region Omega_p_EM ; Value js_fct[]; }
    }
  }

  { Name initTemp ;
    Case {
{ Region Region[{allLayers, midLayers}] ; Value 4.2 ; Type Init; }        { Region Omega_TH ; Value 4.2 ; Type Init; } // init. condition
    }
  }
  { Name Dirichlet_a_projection;
    Case {
      { Region projection_points ; Value 0; Type Assign; }
    }
  }
}

FunctionSpace {
  { Name Hcurl_a_Mag_2D; Type Form1P; // Magnetic vector potential a
    BasisFunction {
      { Name se; NameOfCoef ae; Function BF_PerpendicularEdge;
        Support Omega_EM ; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef ae; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_Mag; }
    }
  }

  { Name Hregion_j_Mag_2D; Type Vector; // Electric current density js
    BasisFunction {
      { Name sr; NameOfCoef jsr; Function BF_RegionZ;
        Support Omega_p_EM; Entity Omega_p_EM; }
    }
    Constraint {
      { NameOfCoef jsr; EntityType Region;
        NameOfConstraint SourceCurrentDensityZ; }
    }
  }

  { Name H_curl_a_artificial_dof; Type Form1P;  
    BasisFunction {
      { Name se_after_projection; NameOfCoef ae_after_projection; Function BF_PerpendicularEdge;
        Support Omega_TH ; Entity NodesOf[ All ]; }
    }
    // not needed since boundary is not part of Omega_TH
    Constraint {
      { NameOfCoef ae_after_projection; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_projection; }     
    }
  }                                               

  { Name Hgrad_T; Type Form0;
    BasisFunction {
      { Name un;  NameOfCoef ui;  Function BF_Node;
          Support Region[ Omega_TH ]; Entity NodesOf[All, Not allLayers];
      }

      // temperature on shells following checkered support idea as indicated
      // by two indices
      // FYI: another possibility would be to treat the extremity points of
      // the shells separately
      For i In {1:2}
        For j In {1:2}
          { Name udn~{i}~{j}; NameOfCoef udi~{i}~{j}; Function BF_Node;
            Support Region[{midLayers~{i}~{j}, Domain_Insulated_Str~{i}~{j}}];
            Entity NodesOf[{midLayers~{i}~{j}, Bare_Layers~{i}~{j}}]; }
        EndFor
      EndFor
    }

    SubSpace {
      // "vertical" subspaces, up and down are connected via thin shell
      // vertical thin shells
      { Name Shell_Up_1;   NameOfBasisFunction {udn_1_1, udn_1_2};}
      { Name Shell_Down_1; NameOfBasisFunction {udn_2_2, udn_2_1};}

      // "horizontal" subspaces, up and down are connected via thin shell
      { Name Shell_Up_2;   NameOfBasisFunction {udn_1_1, udn_2_1}; }
      { Name Shell_Down_2; NameOfBasisFunction {udn_2_2, udn_1_2}; }
    }

    Constraint {
      For i In {1:2}
        For j In {1:2}
          { NameOfCoef udi~{i}~{j};  EntityType NodesOf;
            NameOfConstraint Temperature~{i}~{j}; }
          { NameOfCoef udi~{i}~{j};  EntityType NodesOf;
            NameOfConstraint initTemp; }
        EndFor
      EndFor
      { NameOfCoef ui; EntityType NodesOf; NameOfConstraint initTemp; }
      // do not constraint second order basis function as it's already covered by ui
    }
  }

      For i In {1:1-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{1}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{1}; NameOfCoef Tn~{i}~{j}~{1} ; Function BF_Node ;
                Support intDomain~{j}~{1} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{1};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:1-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{2}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{2}; NameOfCoef Tn~{i}~{j}~{2} ; Function BF_Node ;
                Support intDomain~{j}~{2} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{2};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:3-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{3}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{3}; NameOfCoef Tn~{i}~{j}~{3} ; Function BF_Node ;
                Support intDomain~{j}~{3} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{3};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:2-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{4}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{4}; NameOfCoef Tn~{i}~{j}~{4} ; Function BF_Node ;
                Support intDomain~{j}~{4} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{4};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:3-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{5}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{5}; NameOfCoef Tn~{i}~{j}~{5} ; Function BF_Node ;
                Support intDomain~{j}~{5} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{5};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:1-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{6}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{6}; NameOfCoef Tn~{i}~{j}~{6} ; Function BF_Node ;
                Support intDomain~{j}~{6} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{6};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:2-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{7}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{7}; NameOfCoef Tn~{i}~{j}~{7} ; Function BF_Node ;
                Support intDomain~{j}~{7} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{7};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:7-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{8}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{8}; NameOfCoef Tn~{i}~{j}~{8} ; Function BF_Node ;
                Support intDomain~{j}~{8} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{8};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:7-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{9}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{9}; NameOfCoef Tn~{i}~{j}~{9} ; Function BF_Node ;
                Support intDomain~{j}~{9} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{9};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:6-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{10}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{10}; NameOfCoef Tn~{i}~{j}~{10} ; Function BF_Node ;
                Support intDomain~{j}~{10} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{10};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
      For i In {1:6-1}
        For j In {1:2}
          { Name Hgrad_T~{i}~{j}~{11}; Type Form0 ;
            BasisFunction {
              { Name sn~{i}~{j}~{11}; NameOfCoef Tn~{i}~{j}~{11} ; Function BF_Node ;
                Support intDomain~{j}~{11} ; Entity NodesOf[ All ] ; }
            }
            Constraint {
              { NameOfCoef Tn~{i}~{j}~{11};  EntityType NodesOf;
                NameOfConstraint initTemp; }
            }
          }
        EndFor
      EndFor
}

Jacobian {
  { Name Jac_Vol_EM ;
    Case {
      { Region Omega_aff_EM ;
        Jacobian VolSphShell {0.13485, 0.1798} ; }
      { Region All ; Jacobian Vol ; }
    }
  }

  { Name Jac_Vol_TH ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Jac_Sur_TH ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int_EM ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point ; NumberOfPoints 1 ; }
          { GeoElement Line ; NumberOfPoints 2 ; }
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_line_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Line ; NumberOfPoints 2 ; }
        }
      }
    }
  }

  { Name Int_conducting_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_insulating_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }
}

Formulation {
  { Name Magnetostatics_a_2D; Type FemEquation;
    Quantity {
      { Name a ; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      { Name js; Type Local; NameOfSpace Hregion_j_Mag_2D; }
    }
    Equation {
      Integral { [ nu[{d a}] * Dof{d a} , {d a} ];
        In Omega_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }


      Integral { [ -Dof{js} , {a} ];
        In Omega_p_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }
    }
  }

  // Dummy formulation just to save the values of the norm of B from the EM mesh on the Gaussian points of
  // the thermal mesh. Alternatively, a Galerkin projection could be used.
  { Name Projection_EM_to_TH; Type FemEquation;
    Quantity {
      {Name a_before_projection; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      {Name a_artificial_dof; Type Local; NameOfSpace H_curl_a_artificial_dof; }
    }
    Equation {
      Integral { [ - SetVariable[Norm[{d a_before_projection}], ElementNum[], QuadraturePointIndex[]]{$Bnorm}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }

        Integral { [ Dof{d a_artificial_dof}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }
    }
  }                                              

  { Name Thermal_T;   Type FemEquation;
    Quantity {
      // cont temperature
      { Name T; Type Local; NameOfSpace Hgrad_T; }
        For j In {1:2} // "vertical" and "horizontal" separated

            // outer temp up
            { Name Ti~{0}~{j}~{1}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:1-1}
                  { Name Ti~{i}~{j}~{1} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{1}; }
              EndFor
            //outer temp down
            { Name Ti~{1}~{j}~{1}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{2}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:1-1}
                  { Name Ti~{i}~{j}~{2} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{2}; }
              EndFor
            //outer temp down
            { Name Ti~{1}~{j}~{2}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{3}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:3-1}
                  { Name Ti~{i}~{j}~{3} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{3}; }
              EndFor
            //outer temp down
            { Name Ti~{3}~{j}~{3}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{4}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:2-1}
                  { Name Ti~{i}~{j}~{4} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{4}; }
              EndFor
            //outer temp down
            { Name Ti~{2}~{j}~{4}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{5}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:3-1}
                  { Name Ti~{i}~{j}~{5} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{5}; }
              EndFor
            //outer temp down
            { Name Ti~{3}~{j}~{5}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{6}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:1-1}
                  { Name Ti~{i}~{j}~{6} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{6}; }
              EndFor
            //outer temp down
            { Name Ti~{1}~{j}~{6}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{7}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:2-1}
                  { Name Ti~{i}~{j}~{7} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{7}; }
              EndFor
            //outer temp down
            { Name Ti~{2}~{j}~{7}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{8}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:7-1}
                  { Name Ti~{i}~{j}~{8} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{8}; }
              EndFor
            //outer temp down
            { Name Ti~{7}~{j}~{8}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{9}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:7-1}
                  { Name Ti~{i}~{j}~{9} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{9}; }
              EndFor
            //outer temp down
            { Name Ti~{7}~{j}~{9}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{10}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:6-1}
                  { Name Ti~{i}~{j}~{10} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{10}; }
              EndFor
            //outer temp down
            { Name Ti~{6}~{j}~{10}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }

            // outer temp up
            { Name Ti~{0}~{j}~{11}; Type Local;
                NameOfSpace Hgrad_T[Shell_Up~{j}]; }
            // auxiliary shells in between
              For i In {1:6-1}
                  { Name Ti~{i}~{j}~{11} ; Type Local ;
                    NameOfSpace Hgrad_T~{i}~{j}~{11}; }
              EndFor
            //outer temp down
            { Name Ti~{6}~{j}~{11}; Type Local;
              NameOfSpace Hgrad_T[Shell_Down~{j}]; }
        EndFor
    }

    Equation {
      Integral { [ kappa[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{d T} , {d T} ] ;
        In Region[ {Omega_p_TH } ]; Integration Int_conducting_TH ; Jacobian Jac_Vol_TH ; }

      Integral { DtDof[ heatCap[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{T}, {T} ];
        In Region[ {Omega_p_TH } ]; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }


    // TODO: implement derivatives, missing copper for example
    /*   Integral { JacNL[ dkappadT[{T}, {d a}] * {d T} * Dof{T} , {d T} ] ;
         In Omega_TH; Integration Int<> ; Jacobian Jac_Vol_TH ; } */

      Integral { [ - jouleLosses[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}], {T}];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }

          For i In {0:0} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{1}} , {d Ti~{i + 1 - 1}~{j}~{1}}];
                    In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{1}} , {Ti~{i + 1 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{1}} , {Ti~{i + 1 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{1}} , {d Ti~{i + 2 - 1}~{j}~{1}}];
                    In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{1}} , {Ti~{i + 2 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{1}} , {Ti~{i + 2 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{1}} , {d Ti~{i + 1 - 1}~{j}~{1}}];
                    In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{1}} , {Ti~{i + 1 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{1}} , {Ti~{i + 1 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{1}} , {d Ti~{i + 2 - 1}~{j}~{1}}];
                    In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{1}} , {Ti~{i + 2 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{1}}, {Ti~{i+1}~{j}~{1}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{1}} , {Ti~{i + 2 - 1}~{j}~{1}} ];
                  In intDomain~{j}~{1}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:0} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{2}} , {d Ti~{i + 1 - 1}~{j}~{2}}];
                    In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{2}} , {Ti~{i + 1 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{2}} , {Ti~{i + 1 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{2}} , {d Ti~{i + 2 - 1}~{j}~{2}}];
                    In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{2}} , {Ti~{i + 2 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{2}} , {Ti~{i + 2 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{2}} , {d Ti~{i + 1 - 1}~{j}~{2}}];
                    In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{2}} , {Ti~{i + 1 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{2}} , {Ti~{i + 1 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{2}} , {d Ti~{i + 2 - 1}~{j}~{2}}];
                    In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{2}} , {Ti~{i + 2 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{2}}, {Ti~{i+1}~{j}~{2}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{2}} , {Ti~{i + 2 - 1}~{j}~{2}} ];
                  In intDomain~{j}~{2}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:2} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{3}} , {d Ti~{i + 1 - 1}~{j}~{3}}];
                    In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{3}} , {Ti~{i + 1 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{3}} , {Ti~{i + 1 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{3}} , {d Ti~{i + 2 - 1}~{j}~{3}}];
                    In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{3}} , {Ti~{i + 2 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{3}} , {Ti~{i + 2 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{3}} , {d Ti~{i + 1 - 1}~{j}~{3}}];
                    In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{3}} , {Ti~{i + 1 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{3}} , {Ti~{i + 1 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{3}} , {d Ti~{i + 2 - 1}~{j}~{3}}];
                    In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{3}} , {Ti~{i + 2 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{3}}, {Ti~{i+1}~{j}~{3}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{3}} , {Ti~{i + 2 - 1}~{j}~{3}} ];
                  In intDomain~{j}~{3}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:1} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{4}} , {d Ti~{i + 1 - 1}~{j}~{4}}];
                    In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{4}} , {Ti~{i + 1 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{4}} , {Ti~{i + 1 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{4}} , {d Ti~{i + 2 - 1}~{j}~{4}}];
                    In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{4}} , {Ti~{i + 2 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{4}} , {Ti~{i + 2 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{4}} , {d Ti~{i + 1 - 1}~{j}~{4}}];
                    In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{4}} , {Ti~{i + 1 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{4}} , {Ti~{i + 1 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{4}} , {d Ti~{i + 2 - 1}~{j}~{4}}];
                    In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{4}} , {Ti~{i + 2 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{4}}, {Ti~{i+1}~{j}~{4}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{4}} , {Ti~{i + 2 - 1}~{j}~{4}} ];
                  In intDomain~{j}~{4}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:2} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{5}} , {d Ti~{i + 1 - 1}~{j}~{5}}];
                    In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{5}} , {Ti~{i + 1 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{5}} , {Ti~{i + 1 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{5}} , {d Ti~{i + 2 - 1}~{j}~{5}}];
                    In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{5}} , {Ti~{i + 2 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{5}} , {Ti~{i + 2 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{5}} , {d Ti~{i + 1 - 1}~{j}~{5}}];
                    In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{5}} , {Ti~{i + 1 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{5}} , {Ti~{i + 1 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{5}} , {d Ti~{i + 2 - 1}~{j}~{5}}];
                    In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{5}} , {Ti~{i + 2 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{5}}, {Ti~{i+1}~{j}~{5}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{5}} , {Ti~{i + 2 - 1}~{j}~{5}} ];
                  In intDomain~{j}~{5}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:0} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{6}} , {d Ti~{i + 1 - 1}~{j}~{6}}];
                    In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{6}} , {Ti~{i + 1 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{6}} , {Ti~{i + 1 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{6}} , {d Ti~{i + 2 - 1}~{j}~{6}}];
                    In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{6}} , {Ti~{i + 2 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{6}} , {Ti~{i + 2 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{6}} , {d Ti~{i + 1 - 1}~{j}~{6}}];
                    In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{6}} , {Ti~{i + 1 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{6}} , {Ti~{i + 1 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{6}} , {d Ti~{i + 2 - 1}~{j}~{6}}];
                    In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{6}} , {Ti~{i + 2 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{6}}, {Ti~{i+1}~{j}~{6}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{6}} , {Ti~{i + 2 - 1}~{j}~{6}} ];
                  In intDomain~{j}~{6}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:1} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{7}} , {d Ti~{i + 1 - 1}~{j}~{7}}];
                    In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{7}} , {Ti~{i + 1 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{7}} , {Ti~{i + 1 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{7}} , {d Ti~{i + 2 - 1}~{j}~{7}}];
                    In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{7}} , {Ti~{i + 2 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{7}} , {Ti~{i + 2 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{7}} , {d Ti~{i + 1 - 1}~{j}~{7}}];
                    In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{7}} , {Ti~{i + 1 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{7}} , {Ti~{i + 1 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{7}} , {d Ti~{i + 2 - 1}~{j}~{7}}];
                    In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{7}} , {Ti~{i + 2 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{7}}, {Ti~{i+1}~{j}~{7}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{7}} , {Ti~{i + 2 - 1}~{j}~{7}} ];
                  In intDomain~{j}~{7}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:6} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{8}} , {d Ti~{i + 1 - 1}~{j}~{8}}];
                    In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{8}} , {Ti~{i + 1 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{8}} , {Ti~{i + 1 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{8}} , {d Ti~{i + 2 - 1}~{j}~{8}}];
                    In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{8}} , {Ti~{i + 2 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{8}} , {Ti~{i + 2 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{8}} , {d Ti~{i + 1 - 1}~{j}~{8}}];
                    In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{8}} , {Ti~{i + 1 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{8}} , {Ti~{i + 1 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{8}} , {d Ti~{i + 2 - 1}~{j}~{8}}];
                    In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{8}} , {Ti~{i + 2 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{8}} , {Ti~{i + 2 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:6} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{9}} , {d Ti~{i + 1 - 1}~{j}~{9}}];
                    In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{9}} , {Ti~{i + 1 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{9}} , {Ti~{i + 1 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{9}} , {d Ti~{i + 2 - 1}~{j}~{9}}];
                    In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{9}} , {Ti~{i + 2 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{9}} , {Ti~{i + 2 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{9}} , {d Ti~{i + 1 - 1}~{j}~{9}}];
                    In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{9}} , {Ti~{i + 1 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{9}} , {Ti~{i + 1 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{9}} , {d Ti~{i + 2 - 1}~{j}~{9}}];
                    In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{9}} , {Ti~{i + 2 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{9}} , {Ti~{i + 2 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:5} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{10}} , {d Ti~{i + 1 - 1}~{j}~{10}}];
                    In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{10}} , {Ti~{i + 1 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{10}} , {Ti~{i + 1 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{10}} , {d Ti~{i + 2 - 1}~{j}~{10}}];
                    In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{10}} , {Ti~{i + 2 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{10}} , {Ti~{i + 2 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{10}} , {d Ti~{i + 1 - 1}~{j}~{10}}];
                    In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{10}} , {Ti~{i + 1 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{10}} , {Ti~{i + 1 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{10}} , {d Ti~{i + 2 - 1}~{j}~{10}}];
                    In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{10}} , {Ti~{i + 2 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{10}} , {Ti~{i + 2 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i
          For i In {0:5} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal
                Integral {
                  [  thermalConductivityMass_1_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{11}} , {d Ti~{i + 1 - 1}~{j}~{11}}];
                    In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{11}} , {Ti~{i + 1 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{11}} , {Ti~{i + 1 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_1_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{d Ti~{i + 1 - 1}~{j}~{11}} , {d Ti~{i + 2 - 1}~{j}~{11}}];
                    In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_1_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{11}} , {Ti~{i + 2 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_1_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 1 - 1}~{j}~{11}} , {Ti~{i + 2 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


                Integral {
                  [  thermalConductivityMass_2_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{11}} , {d Ti~{i + 1 - 1}~{j}~{11}}];
                    In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{11}} , {Ti~{i + 1 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{11}} , {Ti~{i + 1 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [  thermalConductivityMass_2_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{d Ti~{i + 2 - 1}~{j}~{11}} , {d Ti~{i + 2 - 1}~{j}~{11}}];
                    In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  [thermalConductivityStiffness_2_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{11}} , {Ti~{i + 2 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }

                Integral {
                  DtDof[ specificHeatCapacity_2_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]] *
                    Dof{Ti~{i + 2 - 1}~{j}~{11}} , {Ti~{i + 2 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH;
                }


            EndFor  // j
          EndFor  // i


          For i In {0:6} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal

                Integral { [- powerDensity_1~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]], {Ti~{i + 1 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }


                Integral { [- powerDensity_2~{i}[{Ti~{i}~{j}~{8}}, {Ti~{i+1}~{j}~{8}}, delta~{i}[]], {Ti~{i + 2 - 1}~{j}~{8}} ];
                  In intDomain~{j}~{8}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }

            EndFor  // j
          EndFor  // i

          For i In {0:6} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal

                Integral { [- powerDensity_1~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]], {Ti~{i + 1 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }


                Integral { [- powerDensity_2~{i}[{Ti~{i}~{j}~{9}}, {Ti~{i+1}~{j}~{9}}, delta~{i}[]], {Ti~{i + 2 - 1}~{j}~{9}} ];
                  In intDomain~{j}~{9}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }

            EndFor  // j
          EndFor  // i

          For i In {0:5} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal

                Integral { [- powerDensity_1~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]], {Ti~{i + 1 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }


                Integral { [- powerDensity_2~{i}[{Ti~{i}~{j}~{10}}, {Ti~{i+1}~{j}~{10}}, delta~{i}[]], {Ti~{i + 2 - 1}~{j}~{10}} ];
                  In intDomain~{j}~{10}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }

            EndFor  // j
          EndFor  // i

          For i In {0:5} // loop over 1D FE elements
            For j In {1:2} // separation between vertical and horizontal

                Integral { [- powerDensity_1~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]], {Ti~{i + 1 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }


                Integral { [- powerDensity_2~{i}[{Ti~{i}~{j}~{11}}, {Ti~{i+1}~{j}~{11}}, delta~{i}[]], {Ti~{i + 2 - 1}~{j}~{11}} ];
                  In intDomain~{j}~{11}; Integration Int_line_TH; Jacobian Jac_Sur_TH; }

            EndFor  // j
          EndFor  // i

        // one fewer for loop cause no horVerLayers --> but one more bc of function for N_eleL
        If (num_robin > 0)
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}},
                   {Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{1}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{1}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}},
                   {Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{2}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{2}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}},
                   {Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{3}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{3}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}},
                   {Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{4}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{4}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}},
                   {Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{5}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{5}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}},
                   {Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{6}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{6}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}},
                   {Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{7}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{7}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}},
                   {Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{8}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{8}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}},
                   {Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{9}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{9}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}},
                   {Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{10}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{10}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- ROBIN -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                 Integral { [heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}}, Tinf[]] * Dof{Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}},
                   {Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}} ] ;
                   In bndRobinInt~{j}~{x}~{a}~{11}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

                  Integral { [-heatExchCoeff[{Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}}, Tinf[]] * Tinf[], {Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}} ] ;
                    In bndRobinInt~{j}~{x}~{a}~{11}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
        EndIf

        // ----------------- NEUMANN -----------------------------------------------
        // one fewer for loop cause no horVerLayers --> but one more bc of function for N_eleL
        If (num_neumann > 0)
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{1}}~{j}~{1}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{1}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{2}}~{j}~{2}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{2}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{3}}~{j}~{3}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{3}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{4}}~{j}~{4}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{4}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{5}}~{j}~{5}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{5}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{6}}~{j}~{6}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{6}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{7}}~{j}~{7}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{7}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{8}}~{j}~{8}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{8}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{9}}~{j}~{9}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{9}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{10}}~{j}~{10}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{10}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
            // ----------------- Neumann -----------------------------------------------
            For j In {1:2} // separation between vertical and horizontal
              For x In {1:2}
                For a In {1:2}
                  Integral { [-heatFlux[],
                    {Ti~{outerElem~{j}~{x}~{a}~{11}}~{j}~{11}} ] ;
                    In bndNeuInt~{j}~{x}~{a}~{11}; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }
                EndFor
              EndFor
            EndFor
        EndIf

    }
  }
}

Resolution {
  { Name resolution;
    System {
      { Name Sys_Mag; NameOfFormulation Magnetostatics_a_2D; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_4COND_TSA_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_4COND_TSA_EM.msh"; }
      { Name Sys_The; NameOfFormulation Thermal_T; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_4COND_TSA_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_4COND_TSA_TH.msh"; }
      { Name sys_Mag_projection; NameOfFormulation Projection_EM_to_TH; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_4COND_TSA_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_4COND_TSA_TH.msh";}
    }
    Operation {
      InitSolution[Sys_Mag];
      IterativeLoopN[20, 0.7,
        System { { Sys_Mag, 0.001, 0.1, Solution LinfNorm } }
      ] { GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
      PostOperation[Map_a];

      Generate[sys_Mag_projection]; Solve[sys_Mag_projection];
      SaveSolution[sys_Mag_projection]; //PostOperation[b_after_projection_pos];

      SetExtrapolationOrder[0];
      InitSolution Sys_The; // init. the solution using init. constraints

      //PostOperation[b_thermal];

      CreateDirectory["T_avg"];
      PostOperation[T_avg];

      PostOperation[Map_T];

      // initialized cumulate times to zero to avoid warning
      Evaluate[$tg_cumul_cpu = 0, $ts_cumul_cpu = 0, $tg_cumul_wall = 0, $ts_cumul_wall = 0];
      Print["timestep,gen_wall,gen_cpu,sol_wall,sol_cpu,pos_wall,pos_cpu,gen_wall_cumul,gen_cpu_cumul,sol_wall_cumul,sol_cpu_cumul,pos_wall_cumul,pos_cpu_cumul", File "computation_times.csv"];
      //PostOperation[b_after_projection_pos];
      

      Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

      TimeLoopAdaptive
      [ 0.0, 0.075, 1e-07, 1e-15, 0.1, "Euler", List[Breakpoints],
      System { { Sys_The, 0.001, 0.1, LinfNorm } } ]
      {
        IterativeLoopN[20, 0.7,
          System { { Sys_The, 0.001, 0.1, Solution LinfNorm } }]
        {
          Evaluate[ $tg1_wall = GetWallClockTime[], $tg1_cpu = GetCpuTime[] ];
          GenerateJac Sys_The ;
          Evaluate[ $tg2_wall = GetWallClockTime[], $tg2_cpu = GetCpuTime[] ];

          // add to generation times of previous rejected time steps
          Evaluate[ $tg_wall = $tg_wall + $tg2_wall - $tg1_wall, $tg_cpu = $tg_cpu + $tg2_cpu - $tg1_cpu ];

          Evaluate[ $ts1_wall = GetWallClockTime[], $ts1_cpu = GetCpuTime[] ];
          SolveJac Sys_The;
          Evaluate[ $ts2_wall = GetWallClockTime[], $ts2_cpu = GetCpuTime[] ];

          // add to solution times of previous rejected time steps
          Evaluate[ $ts_wall = $ts_wall + $ts2_wall - $ts1_wall, $ts_cpu = $ts_cpu + $ts2_cpu - $ts1_cpu ];

        }
      }
      {
        // save solution to .res file
        SaveSolution[Sys_The];

        Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];
        // print average temperature

        // print temperature map

        PostOperation[PrintMaxTemp]; // save maximum temperature in register 1
        Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

        Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

        // cumulated times
        Evaluate[ $tg_cumul_wall = $tg_cumul_wall + $tg_wall, $tg_cumul_cpu = $tg_cumul_cpu + $tg_cpu, $ts_cumul_wall = $ts_cumul_wall + $ts_wall, $ts_cumul_cpu = $ts_cumul_cpu + $ts_cpu, $tp_cumul_wall = $tp_cumul_wall + $tp2_wall - $tp1_wall, $tp_cumul_cpu = $tp_cumul_cpu + $tp2_cpu - $tp1_cpu];

        // print to file
        Print[{$TimeStep, $tg_wall, $tg_cpu, $ts_wall, $ts_cpu, $tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];

        // reset after accepted time step
        Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

        // check if maximum temperature is reached


        Print[{#1}, Format "Maximum temperature: %g "];
        Test[#1 > stop_temperature] {
          Break[];
        }
      }
    

      Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];
      PostOperation[T_avg];

      PostOperation[Map_T];
      Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

      Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

      Print[{$tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "-1,0,0,0,0,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];
    }
  }
}

PostProcessing {
  { Name MagSta_a_2D; NameOfFormulation Magnetostatics_a_2D; NameOfSystem Sys_Mag;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name b;
        Value {
          Term { [ {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[{d a}] * {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
    }
  }

  { Name Thermal_T ; NameOfFormulation Thermal_T ; NameOfSystem Sys_The ;
    PostQuantity {
      // Temperature
      { Name T ;
        Value {
          Local { [ {T} ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      { Name jOverJc ;
        Value {
          Term { [ source_current/area_fct[] * 1/(criticalCurrentDensity[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] + 1) ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      // Temperature average as integral quantity
      { Name T_avg ;
        Value {
          Integral {  [ {T} / area_fct[] ] ;
            In Region[ {Omega_p_TH } ] ; Jacobian Jac_Vol_TH ; Integration Int_conducting_TH; }

        }
      }

      { Name b_thermal ;
        Value {
          Local {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

    { Name rho ;
      Value {
          Term { [ rho[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ] ;
          In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
      }
    }
    }
  }

  { Name post_projection; NameOfFormulation Projection_EM_to_TH; NameOfSystem sys_Mag_projection;
    PostQuantity {
      { Name b_before_projection ;
        Value {
          Term {  [Norm[{d a_before_projection}]] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
      { Name b_after_projection ;
        Value {
          Term {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
    }
  }
}

PostOperation PrintMaxTemp UsingPost Thermal_T {
  // Get maximum in bare region and store in register 1
  Print[ T, OnElementsOf Omega_TH, StoreMaxInRegister 1, Format Table,
    LastTimeStepOnly 1, SendToServer "No"] ;
}

PostOperation {
  { Name Dummy; NameOfPostProcessing  Thermal_T ;
    Operation { }
  }

  { Name Map_a; NameOfPostProcessing MagSta_a_2D;
    Operation {
      Print[ b, OnElementsOf Omega_EM, File "b_Omega.pos"] ;
	  //Print [ b, OnLine {{List[{0,0,0}]}{List[{0.1798,0,0}]}} {1000}, Format SimpleTable, File "Center_line.csv"];
    }
  }

  { Name b_thermal; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ b_thermal, OnElementsOf Omega_p_TH, File "b_thermal.pos"] ;
    }
  }
  { Name b_after_projection_pos; NameOfPostProcessing post_projection;
    Operation {
      Print[ b_before_projection, OnElementsOf Omega_p_TH, File "b_before_projection_gmsh.pos"] ;
      Print[ b_after_projection, OnElementsOf Omega_p_TH, File "b_after_projection.pos"] ;
    }
  }

  { Name Map_T; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ T, OnElementsOf Omega_c_TH, File "T_Omega_c.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0 ] ;
      //Print[ JoverJc, OnElementsOf Omega_p_TH, File "JoverJc_Omega_p.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0, AtGaussPoints 4, Depth 0 ] ;
      //Print[ rho, OnElementsOf Omega_p_TH, File "rho_Omega_p.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0 ] ;
    }
  }

  { Name T_avg; NameOfPostProcessing Thermal_T;
    Operation {
    // writes pairs of time step and average temperature to file, one line for each time step
      Print[ T_avg[Region[1000000]], OnGlobal, File "T_avg/T_avg_0.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000001]], OnGlobal, File "T_avg/T_avg_1.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000010]], OnGlobal, File "T_avg/T_avg_2.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000011]], OnGlobal, File "T_avg/T_avg_3.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
    }
  }
}