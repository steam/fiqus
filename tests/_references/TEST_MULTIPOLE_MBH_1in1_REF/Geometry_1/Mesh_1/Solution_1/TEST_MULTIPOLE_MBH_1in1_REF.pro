Include "d:\cernbox\Repositories\steam-fiqus-dev\fiqus\pro_material_functions\ironBHcurves.pro";



    stop_temperature = 300.0;
    /* -------------------------------------------------------------------------- */


Group {

      Air = Region[ 5 ];  // Air
      AirInf = Region[ 4 ];  // AirInf
      ht1_EM = Region[ 6 ];
      ht3_EM = Region[ 8 ];
      ht5_EM = Region[ 10 ];
      ht7_EM = Region[ 12 ];
      ht9_EM = Region[ 14 ];
      ht10_EM = Region[ 15 ];
      ht12_EM = Region[ 17 ];
      ht14_EM = Region[ 19 ];
      ht16_EM = Region[ 21 ];
      ht19_EM = Region[ 24 ];
      ht22_EM = Region[ 27 ];
      ht133_EM = Region[ 29 ];
      ht132_EM = Region[ 30 ];
      ht130_EM = Region[ 32 ];
      ht129_EM = Region[ 33 ];
      ht127_EM = Region[ 35 ];
      ht125_EM = Region[ 37 ];
      ht123_EM = Region[ 39 ];
      ht120_EM = Region[ 42 ];
      ht118_EM = Region[ 44 ];
      ht116_EM = Region[ 46 ];
      ht114_EM = Region[ 48 ];
      ht57_EM = Region[ 50 ];
      ht59_EM = Region[ 52 ];
      ht61_EM = Region[ 54 ];
      ht63_EM = Region[ 56 ];
      ht65_EM = Region[ 58 ];
      ht66_EM = Region[ 59 ];
      ht68_EM = Region[ 61 ];
      ht70_EM = Region[ 63 ];
      ht72_EM = Region[ 65 ];
      ht75_EM = Region[ 68 ];
      ht78_EM = Region[ 71 ];
      ht189_EM = Region[ 73 ];
      ht188_EM = Region[ 74 ];
      ht186_EM = Region[ 76 ];
      ht185_EM = Region[ 77 ];
      ht183_EM = Region[ 79 ];
      ht181_EM = Region[ 81 ];
      ht179_EM = Region[ 83 ];
      ht176_EM = Region[ 86 ];
      ht174_EM = Region[ 88 ];
      ht172_EM = Region[ 90 ];
      ht170_EM = Region[ 92 ];
      ht2_EM = Region[ 7 ];
      ht4_EM = Region[ 9 ];
      ht6_EM = Region[ 11 ];
      ht8_EM = Region[ 13 ];
      ht11_EM = Region[ 16 ];
      ht13_EM = Region[ 18 ];
      ht15_EM = Region[ 20 ];
      ht17_EM = Region[ 22 ];
      ht18_EM = Region[ 23 ];
      ht20_EM = Region[ 25 ];
      ht21_EM = Region[ 26 ];
      ht134_EM = Region[ 28 ];
      ht131_EM = Region[ 31 ];
      ht128_EM = Region[ 34 ];
      ht126_EM = Region[ 36 ];
      ht124_EM = Region[ 38 ];
      ht122_EM = Region[ 40 ];
      ht121_EM = Region[ 41 ];
      ht119_EM = Region[ 43 ];
      ht117_EM = Region[ 45 ];
      ht115_EM = Region[ 47 ];
      ht113_EM = Region[ 49 ];
      ht58_EM = Region[ 51 ];
      ht60_EM = Region[ 53 ];
      ht62_EM = Region[ 55 ];
      ht64_EM = Region[ 57 ];
      ht67_EM = Region[ 60 ];
      ht69_EM = Region[ 62 ];
      ht71_EM = Region[ 64 ];
      ht73_EM = Region[ 66 ];
      ht74_EM = Region[ 67 ];
      ht76_EM = Region[ 69 ];
      ht77_EM = Region[ 70 ];
      ht190_EM = Region[ 72 ];
      ht187_EM = Region[ 75 ];
      ht184_EM = Region[ 78 ];
      ht182_EM = Region[ 80 ];
      ht180_EM = Region[ 82 ];
      ht178_EM = Region[ 84 ];
      ht177_EM = Region[ 85 ];
      ht175_EM = Region[ 87 ];
      ht173_EM = Region[ 89 ];
      ht171_EM = Region[ 91 ];
      ht169_EM = Region[ 93 ];
      ht23_EM = Region[ 94 ];
      ht25_EM = Region[ 96 ];
      ht27_EM = Region[ 98 ];
      ht29_EM = Region[ 100 ];
      ht31_EM = Region[ 102 ];
      ht33_EM = Region[ 104 ];
      ht35_EM = Region[ 106 ];
      ht37_EM = Region[ 108 ];
      ht40_EM = Region[ 111 ];
      ht42_EM = Region[ 113 ];
      ht44_EM = Region[ 115 ];
      ht46_EM = Region[ 117 ];
      ht48_EM = Region[ 119 ];
      ht50_EM = Region[ 121 ];
      ht52_EM = Region[ 123 ];
      ht54_EM = Region[ 125 ];
      ht56_EM = Region[ 127 ];
      ht167_EM = Region[ 129 ];
      ht165_EM = Region[ 131 ];
      ht163_EM = Region[ 133 ];
      ht161_EM = Region[ 135 ];
      ht159_EM = Region[ 137 ];
      ht157_EM = Region[ 139 ];
      ht155_EM = Region[ 141 ];
      ht153_EM = Region[ 143 ];
      ht151_EM = Region[ 145 ];
      ht150_EM = Region[ 146 ];
      ht148_EM = Region[ 148 ];
      ht146_EM = Region[ 150 ];
      ht144_EM = Region[ 152 ];
      ht142_EM = Region[ 154 ];
      ht140_EM = Region[ 156 ];
      ht138_EM = Region[ 158 ];
      ht136_EM = Region[ 160 ];
      ht79_EM = Region[ 162 ];
      ht81_EM = Region[ 164 ];
      ht83_EM = Region[ 166 ];
      ht85_EM = Region[ 168 ];
      ht87_EM = Region[ 170 ];
      ht89_EM = Region[ 172 ];
      ht91_EM = Region[ 174 ];
      ht93_EM = Region[ 176 ];
      ht96_EM = Region[ 179 ];
      ht98_EM = Region[ 181 ];
      ht100_EM = Region[ 183 ];
      ht102_EM = Region[ 185 ];
      ht104_EM = Region[ 187 ];
      ht106_EM = Region[ 189 ];
      ht108_EM = Region[ 191 ];
      ht110_EM = Region[ 193 ];
      ht112_EM = Region[ 195 ];
      ht223_EM = Region[ 197 ];
      ht221_EM = Region[ 199 ];
      ht219_EM = Region[ 201 ];
      ht217_EM = Region[ 203 ];
      ht215_EM = Region[ 205 ];
      ht213_EM = Region[ 207 ];
      ht211_EM = Region[ 209 ];
      ht209_EM = Region[ 211 ];
      ht207_EM = Region[ 213 ];
      ht206_EM = Region[ 214 ];
      ht204_EM = Region[ 216 ];
      ht202_EM = Region[ 218 ];
      ht200_EM = Region[ 220 ];
      ht198_EM = Region[ 222 ];
      ht196_EM = Region[ 224 ];
      ht194_EM = Region[ 226 ];
      ht192_EM = Region[ 228 ];
      ht24_EM = Region[ 95 ];
      ht26_EM = Region[ 97 ];
      ht28_EM = Region[ 99 ];
      ht30_EM = Region[ 101 ];
      ht32_EM = Region[ 103 ];
      ht34_EM = Region[ 105 ];
      ht36_EM = Region[ 107 ];
      ht38_EM = Region[ 109 ];
      ht39_EM = Region[ 110 ];
      ht41_EM = Region[ 112 ];
      ht43_EM = Region[ 114 ];
      ht45_EM = Region[ 116 ];
      ht47_EM = Region[ 118 ];
      ht49_EM = Region[ 120 ];
      ht51_EM = Region[ 122 ];
      ht53_EM = Region[ 124 ];
      ht55_EM = Region[ 126 ];
      ht168_EM = Region[ 128 ];
      ht166_EM = Region[ 130 ];
      ht164_EM = Region[ 132 ];
      ht162_EM = Region[ 134 ];
      ht160_EM = Region[ 136 ];
      ht158_EM = Region[ 138 ];
      ht156_EM = Region[ 140 ];
      ht154_EM = Region[ 142 ];
      ht152_EM = Region[ 144 ];
      ht149_EM = Region[ 147 ];
      ht147_EM = Region[ 149 ];
      ht145_EM = Region[ 151 ];
      ht143_EM = Region[ 153 ];
      ht141_EM = Region[ 155 ];
      ht139_EM = Region[ 157 ];
      ht137_EM = Region[ 159 ];
      ht135_EM = Region[ 161 ];
      ht80_EM = Region[ 163 ];
      ht82_EM = Region[ 165 ];
      ht84_EM = Region[ 167 ];
      ht86_EM = Region[ 169 ];
      ht88_EM = Region[ 171 ];
      ht90_EM = Region[ 173 ];
      ht92_EM = Region[ 175 ];
      ht94_EM = Region[ 177 ];
      ht95_EM = Region[ 178 ];
      ht97_EM = Region[ 180 ];
      ht99_EM = Region[ 182 ];
      ht101_EM = Region[ 184 ];
      ht103_EM = Region[ 186 ];
      ht105_EM = Region[ 188 ];
      ht107_EM = Region[ 190 ];
      ht109_EM = Region[ 192 ];
      ht111_EM = Region[ 194 ];
      ht224_EM = Region[ 196 ];
      ht222_EM = Region[ 198 ];
      ht220_EM = Region[ 200 ];
      ht218_EM = Region[ 202 ];
      ht216_EM = Region[ 204 ];
      ht214_EM = Region[ 206 ];
      ht212_EM = Region[ 208 ];
      ht210_EM = Region[ 210 ];
      ht208_EM = Region[ 212 ];
      ht205_EM = Region[ 215 ];
      ht203_EM = Region[ 217 ];
      ht201_EM = Region[ 219 ];
      ht199_EM = Region[ 221 ];
      ht197_EM = Region[ 223 ];
      ht195_EM = Region[ 225 ];
      ht193_EM = Region[ 227 ];
      ht191_EM = Region[ 229 ];

        w2_EM = Region[ 231 ];
        w3_EM = Region[ 232 ];
        w5_EM = Region[ 234 ];
        w10_EM = Region[ 237 ];
        w11_EM = Region[ 238 ];
        w13_EM = Region[ 240 ];
        w1_EM = Region[ 230 ];
        w4_EM = Region[ 233 ];
        w6_EM = Region[ 235 ];
        w9_EM = Region[ 236 ];
        w12_EM = Region[ 239 ];
        w14_EM = Region[ 241 ];
        w7_EM = Region[ 242 ];
        w15_EM = Region[ 244 ];
        w8_EM = Region[ 243 ];
        w16_EM = Region[ 245 ];

        BHiron2 = Region[ 1 ];

      Surface_Inf = Region[ 3 ];

      Omega_p_EM = Region[ {
ht1_EM, ht3_EM, ht5_EM, ht7_EM, ht9_EM, ht10_EM, ht12_EM, ht14_EM, ht16_EM, ht19_EM, ht22_EM, ht133_EM, ht132_EM, ht130_EM, ht129_EM, ht127_EM, ht125_EM, ht123_EM, ht120_EM, ht118_EM, ht116_EM, ht114_EM, ht57_EM, ht59_EM, ht61_EM, ht63_EM, ht65_EM, ht66_EM, ht68_EM, ht70_EM, ht72_EM, ht75_EM, ht78_EM, ht189_EM, ht188_EM, ht186_EM, ht185_EM, ht183_EM, ht181_EM, ht179_EM, ht176_EM, ht174_EM, ht172_EM, ht170_EM, ht23_EM, ht25_EM, ht27_EM, ht29_EM, ht31_EM, ht33_EM, ht35_EM, ht37_EM, ht40_EM, ht42_EM, ht44_EM, ht46_EM, ht48_EM, ht50_EM, ht52_EM, ht54_EM, ht56_EM, ht167_EM, ht165_EM, ht163_EM, ht161_EM, ht159_EM, ht157_EM, ht155_EM, ht153_EM, ht151_EM, ht150_EM, ht148_EM, ht146_EM, ht144_EM, ht142_EM, ht140_EM, ht138_EM, ht136_EM, ht79_EM, ht81_EM, ht83_EM, ht85_EM, ht87_EM, ht89_EM, ht91_EM, ht93_EM, ht96_EM, ht98_EM, ht100_EM, ht102_EM, ht104_EM, ht106_EM, ht108_EM, ht110_EM, ht112_EM, ht223_EM, ht221_EM, ht219_EM, ht217_EM, ht215_EM, ht213_EM, ht211_EM, ht209_EM, ht207_EM, ht206_EM, ht204_EM, ht202_EM, ht200_EM, ht198_EM, ht196_EM, ht194_EM, ht192_EM, ht2_EM, ht4_EM, ht6_EM, ht8_EM, ht11_EM, ht13_EM, ht15_EM, ht17_EM, ht18_EM, ht20_EM, ht21_EM, ht134_EM, ht131_EM, ht128_EM, ht126_EM, ht124_EM, ht122_EM, ht121_EM, ht119_EM, ht117_EM, ht115_EM, ht113_EM, ht58_EM, ht60_EM, ht62_EM, ht64_EM, ht67_EM, ht69_EM, ht71_EM, ht73_EM, ht74_EM, ht76_EM, ht77_EM, ht190_EM, ht187_EM, ht184_EM, ht182_EM, ht180_EM, ht178_EM, ht177_EM, ht175_EM, ht173_EM, ht171_EM, ht169_EM, ht24_EM, ht26_EM, ht28_EM, ht30_EM, ht32_EM, ht34_EM, ht36_EM, ht38_EM, ht39_EM, ht41_EM, ht43_EM, ht45_EM, ht47_EM, ht49_EM, ht51_EM, ht53_EM, ht55_EM, ht168_EM, ht166_EM, ht164_EM, ht162_EM, ht160_EM, ht158_EM, ht156_EM, ht154_EM, ht152_EM, ht149_EM, ht147_EM, ht145_EM, ht143_EM, ht141_EM, ht139_EM, ht137_EM, ht135_EM, ht80_EM, ht82_EM, ht84_EM, ht86_EM, ht88_EM, ht90_EM, ht92_EM, ht94_EM, ht95_EM, ht97_EM, ht99_EM, ht101_EM, ht103_EM, ht105_EM, ht107_EM, ht109_EM, ht111_EM, ht224_EM, ht222_EM, ht220_EM, ht218_EM, ht216_EM, ht214_EM, ht212_EM, ht210_EM, ht208_EM, ht205_EM, ht203_EM, ht201_EM, ht199_EM, ht197_EM, ht195_EM, ht193_EM, ht191_EM} ];

      Omega_bh_EM = Region[ {BHiron2} ];

      Omega_i_EM = Region[ {
w2_EM, w3_EM, w5_EM, w10_EM, w11_EM, w13_EM, w7_EM, w15_EM, w1_EM, w4_EM, w6_EM, w9_EM, w12_EM, w14_EM, w8_EM, w16_EM} ];

      Omega_aff_EM = Region[ AirInf ];
      Omega_a_EM = Region[ Air ];
      Omega_c_EM = Region[ {Omega_p_EM, Omega_bh_EM, Omega_i_EM} ];
      Omega_EM = Region[ {Air, AirInf, Omega_p_EM, Omega_bh_EM, Omega_i_EM} ];
      Bd_Omega = Region[ {Surface_Inf}];




    ht1_TH = Region[ 1000000 ];
    ht3_TH = Region[ 1000002 ];
    ht5_TH = Region[ 1000004 ];
    ht7_TH = Region[ 1000006 ];
    ht9_TH = Region[ 1000008 ];
    ht10_TH = Region[ 1000009 ];
    ht12_TH = Region[ 1000011 ];
    ht14_TH = Region[ 1000013 ];
    ht16_TH = Region[ 1000015 ];
    ht19_TH = Region[ 1000018 ];
    ht22_TH = Region[ 1000021 ];
    ht133_TH = Region[ 1000023 ];
    ht132_TH = Region[ 1000024 ];
    ht130_TH = Region[ 1000026 ];
    ht129_TH = Region[ 1000027 ];
    ht127_TH = Region[ 1000029 ];
    ht125_TH = Region[ 1000031 ];
    ht123_TH = Region[ 1000033 ];
    ht120_TH = Region[ 1000036 ];
    ht118_TH = Region[ 1000038 ];
    ht116_TH = Region[ 1000040 ];
    ht114_TH = Region[ 1000042 ];
    ht57_TH = Region[ 1000044 ];
    ht59_TH = Region[ 1000046 ];
    ht61_TH = Region[ 1000048 ];
    ht63_TH = Region[ 1000050 ];
    ht65_TH = Region[ 1000052 ];
    ht66_TH = Region[ 1000053 ];
    ht68_TH = Region[ 1000055 ];
    ht70_TH = Region[ 1000057 ];
    ht72_TH = Region[ 1000059 ];
    ht75_TH = Region[ 1000062 ];
    ht78_TH = Region[ 1000065 ];
    ht189_TH = Region[ 1000067 ];
    ht188_TH = Region[ 1000068 ];
    ht186_TH = Region[ 1000070 ];
    ht185_TH = Region[ 1000071 ];
    ht183_TH = Region[ 1000073 ];
    ht181_TH = Region[ 1000075 ];
    ht179_TH = Region[ 1000077 ];
    ht176_TH = Region[ 1000080 ];
    ht174_TH = Region[ 1000082 ];
    ht172_TH = Region[ 1000084 ];
    ht170_TH = Region[ 1000086 ];
    ht2_TH = Region[ 1000001 ];
    ht4_TH = Region[ 1000003 ];
    ht6_TH = Region[ 1000005 ];
    ht8_TH = Region[ 1000007 ];
    ht11_TH = Region[ 1000010 ];
    ht13_TH = Region[ 1000012 ];
    ht15_TH = Region[ 1000014 ];
    ht17_TH = Region[ 1000016 ];
    ht18_TH = Region[ 1000017 ];
    ht20_TH = Region[ 1000019 ];
    ht21_TH = Region[ 1000020 ];
    ht134_TH = Region[ 1000022 ];
    ht131_TH = Region[ 1000025 ];
    ht128_TH = Region[ 1000028 ];
    ht126_TH = Region[ 1000030 ];
    ht124_TH = Region[ 1000032 ];
    ht122_TH = Region[ 1000034 ];
    ht121_TH = Region[ 1000035 ];
    ht119_TH = Region[ 1000037 ];
    ht117_TH = Region[ 1000039 ];
    ht115_TH = Region[ 1000041 ];
    ht113_TH = Region[ 1000043 ];
    ht58_TH = Region[ 1000045 ];
    ht60_TH = Region[ 1000047 ];
    ht62_TH = Region[ 1000049 ];
    ht64_TH = Region[ 1000051 ];
    ht67_TH = Region[ 1000054 ];
    ht69_TH = Region[ 1000056 ];
    ht71_TH = Region[ 1000058 ];
    ht73_TH = Region[ 1000060 ];
    ht74_TH = Region[ 1000061 ];
    ht76_TH = Region[ 1000063 ];
    ht77_TH = Region[ 1000064 ];
    ht190_TH = Region[ 1000066 ];
    ht187_TH = Region[ 1000069 ];
    ht184_TH = Region[ 1000072 ];
    ht182_TH = Region[ 1000074 ];
    ht180_TH = Region[ 1000076 ];
    ht178_TH = Region[ 1000078 ];
    ht177_TH = Region[ 1000079 ];
    ht175_TH = Region[ 1000081 ];
    ht173_TH = Region[ 1000083 ];
    ht171_TH = Region[ 1000085 ];
    ht169_TH = Region[ 1000087 ];
    ht23_TH = Region[ 1000088 ];
    ht25_TH = Region[ 1000090 ];
    ht27_TH = Region[ 1000092 ];
    ht29_TH = Region[ 1000094 ];
    ht31_TH = Region[ 1000096 ];
    ht33_TH = Region[ 1000098 ];
    ht35_TH = Region[ 1000100 ];
    ht37_TH = Region[ 1000102 ];
    ht40_TH = Region[ 1000105 ];
    ht42_TH = Region[ 1000107 ];
    ht44_TH = Region[ 1000109 ];
    ht46_TH = Region[ 1000111 ];
    ht48_TH = Region[ 1000113 ];
    ht50_TH = Region[ 1000115 ];
    ht52_TH = Region[ 1000117 ];
    ht54_TH = Region[ 1000119 ];
    ht56_TH = Region[ 1000121 ];
    ht167_TH = Region[ 1000123 ];
    ht165_TH = Region[ 1000125 ];
    ht163_TH = Region[ 1000127 ];
    ht161_TH = Region[ 1000129 ];
    ht159_TH = Region[ 1000131 ];
    ht157_TH = Region[ 1000133 ];
    ht155_TH = Region[ 1000135 ];
    ht153_TH = Region[ 1000137 ];
    ht151_TH = Region[ 1000139 ];
    ht150_TH = Region[ 1000140 ];
    ht148_TH = Region[ 1000142 ];
    ht146_TH = Region[ 1000144 ];
    ht144_TH = Region[ 1000146 ];
    ht142_TH = Region[ 1000148 ];
    ht140_TH = Region[ 1000150 ];
    ht138_TH = Region[ 1000152 ];
    ht136_TH = Region[ 1000154 ];
    ht79_TH = Region[ 1000156 ];
    ht81_TH = Region[ 1000158 ];
    ht83_TH = Region[ 1000160 ];
    ht85_TH = Region[ 1000162 ];
    ht87_TH = Region[ 1000164 ];
    ht89_TH = Region[ 1000166 ];
    ht91_TH = Region[ 1000168 ];
    ht93_TH = Region[ 1000170 ];
    ht96_TH = Region[ 1000173 ];
    ht98_TH = Region[ 1000175 ];
    ht100_TH = Region[ 1000177 ];
    ht102_TH = Region[ 1000179 ];
    ht104_TH = Region[ 1000181 ];
    ht106_TH = Region[ 1000183 ];
    ht108_TH = Region[ 1000185 ];
    ht110_TH = Region[ 1000187 ];
    ht112_TH = Region[ 1000189 ];
    ht223_TH = Region[ 1000191 ];
    ht221_TH = Region[ 1000193 ];
    ht219_TH = Region[ 1000195 ];
    ht217_TH = Region[ 1000197 ];
    ht215_TH = Region[ 1000199 ];
    ht213_TH = Region[ 1000201 ];
    ht211_TH = Region[ 1000203 ];
    ht209_TH = Region[ 1000205 ];
    ht207_TH = Region[ 1000207 ];
    ht206_TH = Region[ 1000208 ];
    ht204_TH = Region[ 1000210 ];
    ht202_TH = Region[ 1000212 ];
    ht200_TH = Region[ 1000214 ];
    ht198_TH = Region[ 1000216 ];
    ht196_TH = Region[ 1000218 ];
    ht194_TH = Region[ 1000220 ];
    ht192_TH = Region[ 1000222 ];
    ht24_TH = Region[ 1000089 ];
    ht26_TH = Region[ 1000091 ];
    ht28_TH = Region[ 1000093 ];
    ht30_TH = Region[ 1000095 ];
    ht32_TH = Region[ 1000097 ];
    ht34_TH = Region[ 1000099 ];
    ht36_TH = Region[ 1000101 ];
    ht38_TH = Region[ 1000103 ];
    ht39_TH = Region[ 1000104 ];
    ht41_TH = Region[ 1000106 ];
    ht43_TH = Region[ 1000108 ];
    ht45_TH = Region[ 1000110 ];
    ht47_TH = Region[ 1000112 ];
    ht49_TH = Region[ 1000114 ];
    ht51_TH = Region[ 1000116 ];
    ht53_TH = Region[ 1000118 ];
    ht55_TH = Region[ 1000120 ];
    ht168_TH = Region[ 1000122 ];
    ht166_TH = Region[ 1000124 ];
    ht164_TH = Region[ 1000126 ];
    ht162_TH = Region[ 1000128 ];
    ht160_TH = Region[ 1000130 ];
    ht158_TH = Region[ 1000132 ];
    ht156_TH = Region[ 1000134 ];
    ht154_TH = Region[ 1000136 ];
    ht152_TH = Region[ 1000138 ];
    ht149_TH = Region[ 1000141 ];
    ht147_TH = Region[ 1000143 ];
    ht145_TH = Region[ 1000145 ];
    ht143_TH = Region[ 1000147 ];
    ht141_TH = Region[ 1000149 ];
    ht139_TH = Region[ 1000151 ];
    ht137_TH = Region[ 1000153 ];
    ht135_TH = Region[ 1000155 ];
    ht80_TH = Region[ 1000157 ];
    ht82_TH = Region[ 1000159 ];
    ht84_TH = Region[ 1000161 ];
    ht86_TH = Region[ 1000163 ];
    ht88_TH = Region[ 1000165 ];
    ht90_TH = Region[ 1000167 ];
    ht92_TH = Region[ 1000169 ];
    ht94_TH = Region[ 1000171 ];
    ht95_TH = Region[ 1000172 ];
    ht97_TH = Region[ 1000174 ];
    ht99_TH = Region[ 1000176 ];
    ht101_TH = Region[ 1000178 ];
    ht103_TH = Region[ 1000180 ];
    ht105_TH = Region[ 1000182 ];
    ht107_TH = Region[ 1000184 ];
    ht109_TH = Region[ 1000186 ];
    ht111_TH = Region[ 1000188 ];
    ht224_TH = Region[ 1000190 ];
    ht222_TH = Region[ 1000192 ];
    ht220_TH = Region[ 1000194 ];
    ht218_TH = Region[ 1000196 ];
    ht216_TH = Region[ 1000198 ];
    ht214_TH = Region[ 1000200 ];
    ht212_TH = Region[ 1000202 ];
    ht210_TH = Region[ 1000204 ];
    ht208_TH = Region[ 1000206 ];
    ht205_TH = Region[ 1000209 ];
    ht203_TH = Region[ 1000211 ];
    ht201_TH = Region[ 1000213 ];
    ht199_TH = Region[ 1000215 ];
    ht197_TH = Region[ 1000217 ];
    ht195_TH = Region[ 1000219 ];
    ht193_TH = Region[ 1000221 ];
    ht191_TH = Region[ 1000223 ];

      w2_TH = Region[ 1000229 ];
      w3_TH = Region[ 1000230 ];
      w5_TH = Region[ 1000232 ];
      w10_TH = Region[ 1000235 ];
      w11_TH = Region[ 1000236 ];
      w13_TH = Region[ 1000238 ];
      w1_TH = Region[ 1000228 ];
      w4_TH = Region[ 1000231 ];
      w6_TH = Region[ 1000233 ];
      w9_TH = Region[ 1000234 ];
      w12_TH = Region[ 1000237 ];
      w14_TH = Region[ 1000239 ];
      w7_TH = Region[ 1000240 ];
      w15_TH = Region[ 1000242 ];
      w8_TH = Region[ 1000241 ];
      w16_TH = Region[ 1000243 ];


      ins2 = Region[ 1000224 ];
      ins1 = Region[ 1000226 ];
      insext2 = Region[ 1000225 ];
      insext1 = Region[ 1000227 ];

    Omega_p_FNAL40_NC_TH = Region[ {
ht1_TH, ht3_TH, ht5_TH, ht7_TH, ht9_TH, ht10_TH, ht12_TH, ht14_TH, ht16_TH, ht19_TH, ht22_TH, ht133_TH, ht132_TH, ht130_TH, ht129_TH, ht127_TH, ht125_TH, ht123_TH, ht120_TH, ht118_TH, ht116_TH, ht114_TH, ht57_TH, ht59_TH, ht61_TH, ht63_TH, ht65_TH, ht66_TH, ht68_TH, ht70_TH, ht72_TH, ht75_TH, ht78_TH, ht189_TH, ht188_TH, ht186_TH, ht185_TH, ht183_TH, ht181_TH, ht179_TH, ht176_TH, ht174_TH, ht172_TH, ht170_TH, ht23_TH, ht25_TH, ht27_TH, ht29_TH, ht31_TH, ht33_TH, ht35_TH, ht37_TH, ht40_TH, ht42_TH, ht44_TH, ht46_TH, ht48_TH, ht50_TH, ht52_TH, ht54_TH, ht56_TH, ht167_TH, ht165_TH, ht163_TH, ht161_TH, ht159_TH, ht157_TH, ht155_TH, ht153_TH, ht151_TH, ht150_TH, ht148_TH, ht146_TH, ht144_TH, ht142_TH, ht140_TH, ht138_TH, ht136_TH, ht79_TH, ht81_TH, ht83_TH, ht85_TH, ht87_TH, ht89_TH, ht91_TH, ht93_TH, ht96_TH, ht98_TH, ht100_TH, ht102_TH, ht104_TH, ht106_TH, ht108_TH, ht110_TH, ht112_TH, ht223_TH, ht221_TH, ht219_TH, ht217_TH, ht215_TH, ht213_TH, ht211_TH, ht209_TH, ht207_TH, ht206_TH, ht204_TH, ht202_TH, ht200_TH, ht198_TH, ht196_TH, ht194_TH, ht192_TH, ht2_TH, ht4_TH, ht6_TH, ht8_TH, ht11_TH, ht13_TH, ht15_TH, ht17_TH, ht18_TH, ht20_TH, ht21_TH, ht134_TH, ht131_TH, ht128_TH, ht126_TH, ht124_TH, ht122_TH, ht121_TH, ht119_TH, ht117_TH, ht115_TH, ht113_TH, ht58_TH, ht60_TH, ht62_TH, ht64_TH, ht67_TH, ht69_TH, ht71_TH, ht73_TH, ht74_TH, ht76_TH, ht77_TH, ht190_TH, ht187_TH, ht184_TH, ht182_TH, ht180_TH, ht178_TH, ht177_TH, ht175_TH, ht173_TH, ht171_TH, ht169_TH, ht24_TH, ht26_TH, ht28_TH, ht30_TH, ht32_TH, ht34_TH, ht36_TH, ht38_TH, ht39_TH, ht41_TH, ht43_TH, ht45_TH, ht47_TH, ht49_TH, ht51_TH, ht53_TH, ht55_TH, ht168_TH, ht166_TH, ht164_TH, ht162_TH, ht160_TH, ht158_TH, ht156_TH, ht154_TH, ht152_TH, ht149_TH, ht147_TH, ht145_TH, ht143_TH, ht141_TH, ht139_TH, ht137_TH, ht135_TH, ht80_TH, ht82_TH, ht84_TH, ht86_TH, ht88_TH, ht90_TH, ht92_TH, ht94_TH, ht95_TH, ht97_TH, ht99_TH, ht101_TH, ht103_TH, ht105_TH, ht107_TH, ht109_TH, ht111_TH, ht224_TH, ht222_TH, ht220_TH, ht218_TH, ht216_TH, ht214_TH, ht212_TH, ht210_TH, ht208_TH, ht205_TH, ht203_TH, ht201_TH, ht199_TH, ht197_TH, ht195_TH, ht193_TH, ht191_TH} ];
    Omega_p_TH = Region[ {Omega_p_FNAL40_NC_TH} ];
    Omega_i_TH = Region[ {
w2_TH, w3_TH, w5_TH, w10_TH, w11_TH, w13_TH, w7_TH, w15_TH, w1_TH, w4_TH, w6_TH, w9_TH, w12_TH, w14_TH, w8_TH, w16_TH} ];
  Omega_c_TH = Region[ {Omega_p_TH, Omega_i_TH} ];
    Omega_ins_TH = Region[ {ins2, ins1} ];
    Omega_TH = Region[ {Omega_p_TH, Omega_i_TH, Omega_ins_TH} ];

    jcZero_ht27 = Region[{ht27_TH}];
    jcZero_ht28 = Region[{ht28_TH}];
    jcZero_ht29 = Region[{ht29_TH}];
    jcZero_ht30 = Region[{ht30_TH}];
    jcZero_ht31 = Region[{ht31_TH}];
    jcZero_ht32 = Region[{ht32_TH}];
    jcZero_ht33 = Region[{ht33_TH}];
    jcZero_ht34 = Region[{ht34_TH}];
    jcZero_ht35 = Region[{ht35_TH}];
    jcZero_ht36 = Region[{ht36_TH}];
    jcZero_ht37 = Region[{ht37_TH}];
    jcZero_ht38 = Region[{ht38_TH}];
    jcZero_ht42 = Region[{ht42_TH}];
    jcZero_ht43 = Region[{ht43_TH}];
    jcZero_ht44 = Region[{ht44_TH}];
    jcZero_ht45 = Region[{ht45_TH}];
    jcZero_ht46 = Region[{ht46_TH}];
    jcZero_ht47 = Region[{ht47_TH}];
    jcZero_ht48 = Region[{ht48_TH}];
    jcZero_ht49 = Region[{ht49_TH}];
    jcZero_ht50 = Region[{ht50_TH}];
    jcZero_ht51 = Region[{ht51_TH}];
    jcZero_ht52 = Region[{ht52_TH}];
    jcZero_ht53 = Region[{ht53_TH}];
    jcZero_ht83 = Region[{ht83_TH}];
    jcZero_ht84 = Region[{ht84_TH}];
    jcZero_ht85 = Region[{ht85_TH}];
    jcZero_ht86 = Region[{ht86_TH}];
    jcZero_ht87 = Region[{ht87_TH}];
    jcZero_ht88 = Region[{ht88_TH}];
    jcZero_ht89 = Region[{ht89_TH}];
    jcZero_ht90 = Region[{ht90_TH}];
    jcZero_ht91 = Region[{ht91_TH}];
    jcZero_ht92 = Region[{ht92_TH}];
    jcZero_ht93 = Region[{ht93_TH}];
    jcZero_ht94 = Region[{ht94_TH}];
    jcZero_ht98 = Region[{ht98_TH}];
    jcZero_ht99 = Region[{ht99_TH}];
    jcZero_ht100 = Region[{ht100_TH}];
    jcZero_ht101 = Region[{ht101_TH}];
    jcZero_ht102 = Region[{ht102_TH}];
    jcZero_ht103 = Region[{ht103_TH}];
    jcZero_ht104 = Region[{ht104_TH}];
    jcZero_ht105 = Region[{ht105_TH}];
    jcZero_ht106 = Region[{ht106_TH}];
    jcZero_ht107 = Region[{ht107_TH}];
    jcZero_ht108 = Region[{ht108_TH}];
    jcZero_ht109 = Region[{ht109_TH}];
    jcZero_ht139 = Region[{ht139_TH}];
    jcZero_ht140 = Region[{ht140_TH}];
    jcZero_ht141 = Region[{ht141_TH}];
    jcZero_ht142 = Region[{ht142_TH}];
    jcZero_ht143 = Region[{ht143_TH}];
    jcZero_ht144 = Region[{ht144_TH}];
    jcZero_ht145 = Region[{ht145_TH}];
    jcZero_ht146 = Region[{ht146_TH}];
    jcZero_ht147 = Region[{ht147_TH}];
    jcZero_ht148 = Region[{ht148_TH}];
    jcZero_ht149 = Region[{ht149_TH}];
    jcZero_ht150 = Region[{ht150_TH}];
    jcZero_ht154 = Region[{ht154_TH}];
    jcZero_ht155 = Region[{ht155_TH}];
    jcZero_ht156 = Region[{ht156_TH}];
    jcZero_ht157 = Region[{ht157_TH}];
    jcZero_ht158 = Region[{ht158_TH}];
    jcZero_ht159 = Region[{ht159_TH}];
    jcZero_ht160 = Region[{ht160_TH}];
    jcZero_ht161 = Region[{ht161_TH}];
    jcZero_ht162 = Region[{ht162_TH}];
    jcZero_ht163 = Region[{ht163_TH}];
    jcZero_ht164 = Region[{ht164_TH}];
    jcZero_ht165 = Region[{ht165_TH}];
    jcZero_ht195 = Region[{ht195_TH}];
    jcZero_ht196 = Region[{ht196_TH}];
    jcZero_ht197 = Region[{ht197_TH}];
    jcZero_ht198 = Region[{ht198_TH}];
    jcZero_ht199 = Region[{ht199_TH}];
    jcZero_ht200 = Region[{ht200_TH}];
    jcZero_ht201 = Region[{ht201_TH}];
    jcZero_ht202 = Region[{ht202_TH}];
    jcZero_ht203 = Region[{ht203_TH}];
    jcZero_ht204 = Region[{ht204_TH}];
    jcZero_ht205 = Region[{ht205_TH}];
    jcZero_ht206 = Region[{ht206_TH}];
    jcZero_ht210 = Region[{ht210_TH}];
    jcZero_ht211 = Region[{ht211_TH}];
    jcZero_ht212 = Region[{ht212_TH}];
    jcZero_ht213 = Region[{ht213_TH}];
    jcZero_ht214 = Region[{ht214_TH}];
    jcZero_ht215 = Region[{ht215_TH}];
    jcZero_ht216 = Region[{ht216_TH}];
    jcZero_ht217 = Region[{ht217_TH}];
    jcZero_ht218 = Region[{ht218_TH}];
    jcZero_ht219 = Region[{ht219_TH}];
    jcZero_ht220 = Region[{ht220_TH}];
    jcZero_ht221 = Region[{ht221_TH}];
    jcZero_FNAL40_NC = Region[{ht27_TH, ht28_TH, ht29_TH, ht30_TH, ht31_TH, ht32_TH, ht33_TH, ht34_TH, ht35_TH, ht36_TH, ht37_TH, ht38_TH, ht42_TH, ht43_TH, ht44_TH, ht45_TH, ht46_TH, ht47_TH, ht48_TH, ht49_TH, ht50_TH, ht51_TH, ht52_TH, ht53_TH, ht83_TH, ht84_TH, ht85_TH, ht86_TH, ht87_TH, ht88_TH, ht89_TH, ht90_TH, ht91_TH, ht92_TH, ht93_TH, ht94_TH, ht98_TH, ht99_TH, ht100_TH, ht101_TH, ht102_TH, ht103_TH, ht104_TH, ht105_TH, ht106_TH, ht107_TH, ht108_TH, ht109_TH, ht139_TH, ht140_TH, ht141_TH, ht142_TH, ht143_TH, ht144_TH, ht145_TH, ht146_TH, ht147_TH, ht148_TH, ht149_TH, ht150_TH, ht154_TH, ht155_TH, ht156_TH, ht157_TH, ht158_TH, ht159_TH, ht160_TH, ht161_TH, ht162_TH, ht163_TH, ht164_TH, ht165_TH, ht195_TH, ht196_TH, ht197_TH, ht198_TH, ht199_TH, ht200_TH, ht201_TH, ht202_TH, ht203_TH, ht204_TH, ht205_TH, ht206_TH, ht210_TH, ht211_TH, ht212_TH, ht213_TH, ht214_TH, ht215_TH, ht216_TH, ht217_TH, ht218_TH, ht219_TH, ht220_TH, ht221_TH}];
    jcNonZero_FNAL40_NC = Region[Omega_p_FNAL40_NC_TH];
    jcNonZero_FNAL40_NC -= Region[jcZero_FNAL40_NC];

 

    general_adiabatic = Region[ {insext2, insext1} ];


    Bnds_dirichlet = Region[ {} ];
    Bnds_neumann = Region[ {} ];
    Bnds_neumann += Region[ general_adiabatic ];

    Bnds_robin = Region[ {} ];

    Bnds_support = Region[ {Bnds_neumann, Bnds_robin} ];


    projection_points = Region[ 4000000 ];

}

Function {

      mu0 = 4.e-7 * Pi;
      nu [ Region[{Air, Omega_p_EM, AirInf, Omega_i_EM}] ]  = 1. / mu0;

        nu [ BHiron2 ]  = nuBHiron2[$1];
        dnuIronYoke [ BHiron2 ]  = dnuBHiron2[$1];

      js_fct[ ht1_EM ] = 11850.0/SurfaceArea[]{ 6 };
      js_fct[ ht3_EM ] = 11850.0/SurfaceArea[]{ 8 };
      js_fct[ ht5_EM ] = 11850.0/SurfaceArea[]{ 10 };
      js_fct[ ht7_EM ] = 11850.0/SurfaceArea[]{ 12 };
      js_fct[ ht9_EM ] = 11850.0/SurfaceArea[]{ 14 };
      js_fct[ ht10_EM ] = 11850.0/SurfaceArea[]{ 15 };
      js_fct[ ht12_EM ] = 11850.0/SurfaceArea[]{ 17 };
      js_fct[ ht14_EM ] = 11850.0/SurfaceArea[]{ 19 };
      js_fct[ ht16_EM ] = 11850.0/SurfaceArea[]{ 21 };
      js_fct[ ht19_EM ] = 11850.0/SurfaceArea[]{ 24 };
      js_fct[ ht22_EM ] = 11850.0/SurfaceArea[]{ 27 };
      js_fct[ ht133_EM ] = -11850.0/SurfaceArea[]{ 29 };
      js_fct[ ht132_EM ] = -11850.0/SurfaceArea[]{ 30 };
      js_fct[ ht130_EM ] = -11850.0/SurfaceArea[]{ 32 };
      js_fct[ ht129_EM ] = -11850.0/SurfaceArea[]{ 33 };
      js_fct[ ht127_EM ] = -11850.0/SurfaceArea[]{ 35 };
      js_fct[ ht125_EM ] = -11850.0/SurfaceArea[]{ 37 };
      js_fct[ ht123_EM ] = -11850.0/SurfaceArea[]{ 39 };
      js_fct[ ht120_EM ] = -11850.0/SurfaceArea[]{ 42 };
      js_fct[ ht118_EM ] = -11850.0/SurfaceArea[]{ 44 };
      js_fct[ ht116_EM ] = -11850.0/SurfaceArea[]{ 46 };
      js_fct[ ht114_EM ] = -11850.0/SurfaceArea[]{ 48 };
      js_fct[ ht57_EM ] = -11850.0/SurfaceArea[]{ 50 };
      js_fct[ ht59_EM ] = -11850.0/SurfaceArea[]{ 52 };
      js_fct[ ht61_EM ] = -11850.0/SurfaceArea[]{ 54 };
      js_fct[ ht63_EM ] = -11850.0/SurfaceArea[]{ 56 };
      js_fct[ ht65_EM ] = -11850.0/SurfaceArea[]{ 58 };
      js_fct[ ht66_EM ] = -11850.0/SurfaceArea[]{ 59 };
      js_fct[ ht68_EM ] = -11850.0/SurfaceArea[]{ 61 };
      js_fct[ ht70_EM ] = -11850.0/SurfaceArea[]{ 63 };
      js_fct[ ht72_EM ] = -11850.0/SurfaceArea[]{ 65 };
      js_fct[ ht75_EM ] = -11850.0/SurfaceArea[]{ 68 };
      js_fct[ ht78_EM ] = -11850.0/SurfaceArea[]{ 71 };
      js_fct[ ht189_EM ] = 11850.0/SurfaceArea[]{ 73 };
      js_fct[ ht188_EM ] = 11850.0/SurfaceArea[]{ 74 };
      js_fct[ ht186_EM ] = 11850.0/SurfaceArea[]{ 76 };
      js_fct[ ht185_EM ] = 11850.0/SurfaceArea[]{ 77 };
      js_fct[ ht183_EM ] = 11850.0/SurfaceArea[]{ 79 };
      js_fct[ ht181_EM ] = 11850.0/SurfaceArea[]{ 81 };
      js_fct[ ht179_EM ] = 11850.0/SurfaceArea[]{ 83 };
      js_fct[ ht176_EM ] = 11850.0/SurfaceArea[]{ 86 };
      js_fct[ ht174_EM ] = 11850.0/SurfaceArea[]{ 88 };
      js_fct[ ht172_EM ] = 11850.0/SurfaceArea[]{ 90 };
      js_fct[ ht170_EM ] = 11850.0/SurfaceArea[]{ 92 };
      js_fct[ ht23_EM ] = 11850.0/SurfaceArea[]{ 94 };
      js_fct[ ht25_EM ] = 11850.0/SurfaceArea[]{ 96 };
      js_fct[ ht27_EM ] = 11850.0/SurfaceArea[]{ 98 };
      js_fct[ ht29_EM ] = 11850.0/SurfaceArea[]{ 100 };
      js_fct[ ht31_EM ] = 11850.0/SurfaceArea[]{ 102 };
      js_fct[ ht33_EM ] = 11850.0/SurfaceArea[]{ 104 };
      js_fct[ ht35_EM ] = 11850.0/SurfaceArea[]{ 106 };
      js_fct[ ht37_EM ] = 11850.0/SurfaceArea[]{ 108 };
      js_fct[ ht40_EM ] = 11850.0/SurfaceArea[]{ 111 };
      js_fct[ ht42_EM ] = 11850.0/SurfaceArea[]{ 113 };
      js_fct[ ht44_EM ] = 11850.0/SurfaceArea[]{ 115 };
      js_fct[ ht46_EM ] = 11850.0/SurfaceArea[]{ 117 };
      js_fct[ ht48_EM ] = 11850.0/SurfaceArea[]{ 119 };
      js_fct[ ht50_EM ] = 11850.0/SurfaceArea[]{ 121 };
      js_fct[ ht52_EM ] = 11850.0/SurfaceArea[]{ 123 };
      js_fct[ ht54_EM ] = 11850.0/SurfaceArea[]{ 125 };
      js_fct[ ht56_EM ] = 11850.0/SurfaceArea[]{ 127 };
      js_fct[ ht167_EM ] = -11850.0/SurfaceArea[]{ 129 };
      js_fct[ ht165_EM ] = -11850.0/SurfaceArea[]{ 131 };
      js_fct[ ht163_EM ] = -11850.0/SurfaceArea[]{ 133 };
      js_fct[ ht161_EM ] = -11850.0/SurfaceArea[]{ 135 };
      js_fct[ ht159_EM ] = -11850.0/SurfaceArea[]{ 137 };
      js_fct[ ht157_EM ] = -11850.0/SurfaceArea[]{ 139 };
      js_fct[ ht155_EM ] = -11850.0/SurfaceArea[]{ 141 };
      js_fct[ ht153_EM ] = -11850.0/SurfaceArea[]{ 143 };
      js_fct[ ht151_EM ] = -11850.0/SurfaceArea[]{ 145 };
      js_fct[ ht150_EM ] = -11850.0/SurfaceArea[]{ 146 };
      js_fct[ ht148_EM ] = -11850.0/SurfaceArea[]{ 148 };
      js_fct[ ht146_EM ] = -11850.0/SurfaceArea[]{ 150 };
      js_fct[ ht144_EM ] = -11850.0/SurfaceArea[]{ 152 };
      js_fct[ ht142_EM ] = -11850.0/SurfaceArea[]{ 154 };
      js_fct[ ht140_EM ] = -11850.0/SurfaceArea[]{ 156 };
      js_fct[ ht138_EM ] = -11850.0/SurfaceArea[]{ 158 };
      js_fct[ ht136_EM ] = -11850.0/SurfaceArea[]{ 160 };
      js_fct[ ht79_EM ] = -11850.0/SurfaceArea[]{ 162 };
      js_fct[ ht81_EM ] = -11850.0/SurfaceArea[]{ 164 };
      js_fct[ ht83_EM ] = -11850.0/SurfaceArea[]{ 166 };
      js_fct[ ht85_EM ] = -11850.0/SurfaceArea[]{ 168 };
      js_fct[ ht87_EM ] = -11850.0/SurfaceArea[]{ 170 };
      js_fct[ ht89_EM ] = -11850.0/SurfaceArea[]{ 172 };
      js_fct[ ht91_EM ] = -11850.0/SurfaceArea[]{ 174 };
      js_fct[ ht93_EM ] = -11850.0/SurfaceArea[]{ 176 };
      js_fct[ ht96_EM ] = -11850.0/SurfaceArea[]{ 179 };
      js_fct[ ht98_EM ] = -11850.0/SurfaceArea[]{ 181 };
      js_fct[ ht100_EM ] = -11850.0/SurfaceArea[]{ 183 };
      js_fct[ ht102_EM ] = -11850.0/SurfaceArea[]{ 185 };
      js_fct[ ht104_EM ] = -11850.0/SurfaceArea[]{ 187 };
      js_fct[ ht106_EM ] = -11850.0/SurfaceArea[]{ 189 };
      js_fct[ ht108_EM ] = -11850.0/SurfaceArea[]{ 191 };
      js_fct[ ht110_EM ] = -11850.0/SurfaceArea[]{ 193 };
      js_fct[ ht112_EM ] = -11850.0/SurfaceArea[]{ 195 };
      js_fct[ ht223_EM ] = 11850.0/SurfaceArea[]{ 197 };
      js_fct[ ht221_EM ] = 11850.0/SurfaceArea[]{ 199 };
      js_fct[ ht219_EM ] = 11850.0/SurfaceArea[]{ 201 };
      js_fct[ ht217_EM ] = 11850.0/SurfaceArea[]{ 203 };
      js_fct[ ht215_EM ] = 11850.0/SurfaceArea[]{ 205 };
      js_fct[ ht213_EM ] = 11850.0/SurfaceArea[]{ 207 };
      js_fct[ ht211_EM ] = 11850.0/SurfaceArea[]{ 209 };
      js_fct[ ht209_EM ] = 11850.0/SurfaceArea[]{ 211 };
      js_fct[ ht207_EM ] = 11850.0/SurfaceArea[]{ 213 };
      js_fct[ ht206_EM ] = 11850.0/SurfaceArea[]{ 214 };
      js_fct[ ht204_EM ] = 11850.0/SurfaceArea[]{ 216 };
      js_fct[ ht202_EM ] = 11850.0/SurfaceArea[]{ 218 };
      js_fct[ ht200_EM ] = 11850.0/SurfaceArea[]{ 220 };
      js_fct[ ht198_EM ] = 11850.0/SurfaceArea[]{ 222 };
      js_fct[ ht196_EM ] = 11850.0/SurfaceArea[]{ 224 };
      js_fct[ ht194_EM ] = 11850.0/SurfaceArea[]{ 226 };
      js_fct[ ht192_EM ] = 11850.0/SurfaceArea[]{ 228 };
      js_fct[ ht2_EM ] = 11850.0/SurfaceArea[]{ 7 };
      js_fct[ ht4_EM ] = 11850.0/SurfaceArea[]{ 9 };
      js_fct[ ht6_EM ] = 11850.0/SurfaceArea[]{ 11 };
      js_fct[ ht8_EM ] = 11850.0/SurfaceArea[]{ 13 };
      js_fct[ ht11_EM ] = 11850.0/SurfaceArea[]{ 16 };
      js_fct[ ht13_EM ] = 11850.0/SurfaceArea[]{ 18 };
      js_fct[ ht15_EM ] = 11850.0/SurfaceArea[]{ 20 };
      js_fct[ ht17_EM ] = 11850.0/SurfaceArea[]{ 22 };
      js_fct[ ht18_EM ] = 11850.0/SurfaceArea[]{ 23 };
      js_fct[ ht20_EM ] = 11850.0/SurfaceArea[]{ 25 };
      js_fct[ ht21_EM ] = 11850.0/SurfaceArea[]{ 26 };
      js_fct[ ht134_EM ] = -11850.0/SurfaceArea[]{ 28 };
      js_fct[ ht131_EM ] = -11850.0/SurfaceArea[]{ 31 };
      js_fct[ ht128_EM ] = -11850.0/SurfaceArea[]{ 34 };
      js_fct[ ht126_EM ] = -11850.0/SurfaceArea[]{ 36 };
      js_fct[ ht124_EM ] = -11850.0/SurfaceArea[]{ 38 };
      js_fct[ ht122_EM ] = -11850.0/SurfaceArea[]{ 40 };
      js_fct[ ht121_EM ] = -11850.0/SurfaceArea[]{ 41 };
      js_fct[ ht119_EM ] = -11850.0/SurfaceArea[]{ 43 };
      js_fct[ ht117_EM ] = -11850.0/SurfaceArea[]{ 45 };
      js_fct[ ht115_EM ] = -11850.0/SurfaceArea[]{ 47 };
      js_fct[ ht113_EM ] = -11850.0/SurfaceArea[]{ 49 };
      js_fct[ ht58_EM ] = -11850.0/SurfaceArea[]{ 51 };
      js_fct[ ht60_EM ] = -11850.0/SurfaceArea[]{ 53 };
      js_fct[ ht62_EM ] = -11850.0/SurfaceArea[]{ 55 };
      js_fct[ ht64_EM ] = -11850.0/SurfaceArea[]{ 57 };
      js_fct[ ht67_EM ] = -11850.0/SurfaceArea[]{ 60 };
      js_fct[ ht69_EM ] = -11850.0/SurfaceArea[]{ 62 };
      js_fct[ ht71_EM ] = -11850.0/SurfaceArea[]{ 64 };
      js_fct[ ht73_EM ] = -11850.0/SurfaceArea[]{ 66 };
      js_fct[ ht74_EM ] = -11850.0/SurfaceArea[]{ 67 };
      js_fct[ ht76_EM ] = -11850.0/SurfaceArea[]{ 69 };
      js_fct[ ht77_EM ] = -11850.0/SurfaceArea[]{ 70 };
      js_fct[ ht190_EM ] = 11850.0/SurfaceArea[]{ 72 };
      js_fct[ ht187_EM ] = 11850.0/SurfaceArea[]{ 75 };
      js_fct[ ht184_EM ] = 11850.0/SurfaceArea[]{ 78 };
      js_fct[ ht182_EM ] = 11850.0/SurfaceArea[]{ 80 };
      js_fct[ ht180_EM ] = 11850.0/SurfaceArea[]{ 82 };
      js_fct[ ht178_EM ] = 11850.0/SurfaceArea[]{ 84 };
      js_fct[ ht177_EM ] = 11850.0/SurfaceArea[]{ 85 };
      js_fct[ ht175_EM ] = 11850.0/SurfaceArea[]{ 87 };
      js_fct[ ht173_EM ] = 11850.0/SurfaceArea[]{ 89 };
      js_fct[ ht171_EM ] = 11850.0/SurfaceArea[]{ 91 };
      js_fct[ ht169_EM ] = 11850.0/SurfaceArea[]{ 93 };
      js_fct[ ht24_EM ] = 11850.0/SurfaceArea[]{ 95 };
      js_fct[ ht26_EM ] = 11850.0/SurfaceArea[]{ 97 };
      js_fct[ ht28_EM ] = 11850.0/SurfaceArea[]{ 99 };
      js_fct[ ht30_EM ] = 11850.0/SurfaceArea[]{ 101 };
      js_fct[ ht32_EM ] = 11850.0/SurfaceArea[]{ 103 };
      js_fct[ ht34_EM ] = 11850.0/SurfaceArea[]{ 105 };
      js_fct[ ht36_EM ] = 11850.0/SurfaceArea[]{ 107 };
      js_fct[ ht38_EM ] = 11850.0/SurfaceArea[]{ 109 };
      js_fct[ ht39_EM ] = 11850.0/SurfaceArea[]{ 110 };
      js_fct[ ht41_EM ] = 11850.0/SurfaceArea[]{ 112 };
      js_fct[ ht43_EM ] = 11850.0/SurfaceArea[]{ 114 };
      js_fct[ ht45_EM ] = 11850.0/SurfaceArea[]{ 116 };
      js_fct[ ht47_EM ] = 11850.0/SurfaceArea[]{ 118 };
      js_fct[ ht49_EM ] = 11850.0/SurfaceArea[]{ 120 };
      js_fct[ ht51_EM ] = 11850.0/SurfaceArea[]{ 122 };
      js_fct[ ht53_EM ] = 11850.0/SurfaceArea[]{ 124 };
      js_fct[ ht55_EM ] = 11850.0/SurfaceArea[]{ 126 };
      js_fct[ ht168_EM ] = -11850.0/SurfaceArea[]{ 128 };
      js_fct[ ht166_EM ] = -11850.0/SurfaceArea[]{ 130 };
      js_fct[ ht164_EM ] = -11850.0/SurfaceArea[]{ 132 };
      js_fct[ ht162_EM ] = -11850.0/SurfaceArea[]{ 134 };
      js_fct[ ht160_EM ] = -11850.0/SurfaceArea[]{ 136 };
      js_fct[ ht158_EM ] = -11850.0/SurfaceArea[]{ 138 };
      js_fct[ ht156_EM ] = -11850.0/SurfaceArea[]{ 140 };
      js_fct[ ht154_EM ] = -11850.0/SurfaceArea[]{ 142 };
      js_fct[ ht152_EM ] = -11850.0/SurfaceArea[]{ 144 };
      js_fct[ ht149_EM ] = -11850.0/SurfaceArea[]{ 147 };
      js_fct[ ht147_EM ] = -11850.0/SurfaceArea[]{ 149 };
      js_fct[ ht145_EM ] = -11850.0/SurfaceArea[]{ 151 };
      js_fct[ ht143_EM ] = -11850.0/SurfaceArea[]{ 153 };
      js_fct[ ht141_EM ] = -11850.0/SurfaceArea[]{ 155 };
      js_fct[ ht139_EM ] = -11850.0/SurfaceArea[]{ 157 };
      js_fct[ ht137_EM ] = -11850.0/SurfaceArea[]{ 159 };
      js_fct[ ht135_EM ] = -11850.0/SurfaceArea[]{ 161 };
      js_fct[ ht80_EM ] = -11850.0/SurfaceArea[]{ 163 };
      js_fct[ ht82_EM ] = -11850.0/SurfaceArea[]{ 165 };
      js_fct[ ht84_EM ] = -11850.0/SurfaceArea[]{ 167 };
      js_fct[ ht86_EM ] = -11850.0/SurfaceArea[]{ 169 };
      js_fct[ ht88_EM ] = -11850.0/SurfaceArea[]{ 171 };
      js_fct[ ht90_EM ] = -11850.0/SurfaceArea[]{ 173 };
      js_fct[ ht92_EM ] = -11850.0/SurfaceArea[]{ 175 };
      js_fct[ ht94_EM ] = -11850.0/SurfaceArea[]{ 177 };
      js_fct[ ht95_EM ] = -11850.0/SurfaceArea[]{ 178 };
      js_fct[ ht97_EM ] = -11850.0/SurfaceArea[]{ 180 };
      js_fct[ ht99_EM ] = -11850.0/SurfaceArea[]{ 182 };
      js_fct[ ht101_EM ] = -11850.0/SurfaceArea[]{ 184 };
      js_fct[ ht103_EM ] = -11850.0/SurfaceArea[]{ 186 };
      js_fct[ ht105_EM ] = -11850.0/SurfaceArea[]{ 188 };
      js_fct[ ht107_EM ] = -11850.0/SurfaceArea[]{ 190 };
      js_fct[ ht109_EM ] = -11850.0/SurfaceArea[]{ 192 };
      js_fct[ ht111_EM ] = -11850.0/SurfaceArea[]{ 194 };
      js_fct[ ht224_EM ] = 11850.0/SurfaceArea[]{ 196 };
      js_fct[ ht222_EM ] = 11850.0/SurfaceArea[]{ 198 };
      js_fct[ ht220_EM ] = 11850.0/SurfaceArea[]{ 200 };
      js_fct[ ht218_EM ] = 11850.0/SurfaceArea[]{ 202 };
      js_fct[ ht216_EM ] = 11850.0/SurfaceArea[]{ 204 };
      js_fct[ ht214_EM ] = 11850.0/SurfaceArea[]{ 206 };
      js_fct[ ht212_EM ] = 11850.0/SurfaceArea[]{ 208 };
      js_fct[ ht210_EM ] = 11850.0/SurfaceArea[]{ 210 };
      js_fct[ ht208_EM ] = 11850.0/SurfaceArea[]{ 212 };
      js_fct[ ht205_EM ] = 11850.0/SurfaceArea[]{ 215 };
      js_fct[ ht203_EM ] = 11850.0/SurfaceArea[]{ 217 };
      js_fct[ ht201_EM ] = 11850.0/SurfaceArea[]{ 219 };
      js_fct[ ht199_EM ] = 11850.0/SurfaceArea[]{ 221 };
      js_fct[ ht197_EM ] = 11850.0/SurfaceArea[]{ 223 };
      js_fct[ ht195_EM ] = 11850.0/SurfaceArea[]{ 225 };
      js_fct[ ht193_EM ] = 11850.0/SurfaceArea[]{ 227 };
      js_fct[ ht191_EM ] = 11850.0/SurfaceArea[]{ 229 };



  area_fct[ ht1_TH ] = SurfaceArea[]{ 1000000 };
  area_fct[ ht3_TH ] = SurfaceArea[]{ 1000002 };
  area_fct[ ht5_TH ] = SurfaceArea[]{ 1000004 };
  area_fct[ ht7_TH ] = SurfaceArea[]{ 1000006 };
  area_fct[ ht9_TH ] = SurfaceArea[]{ 1000008 };
  area_fct[ ht10_TH ] = SurfaceArea[]{ 1000009 };
  area_fct[ ht12_TH ] = SurfaceArea[]{ 1000011 };
  area_fct[ ht14_TH ] = SurfaceArea[]{ 1000013 };
  area_fct[ ht16_TH ] = SurfaceArea[]{ 1000015 };
  area_fct[ ht19_TH ] = SurfaceArea[]{ 1000018 };
  area_fct[ ht22_TH ] = SurfaceArea[]{ 1000021 };
  area_fct[ ht133_TH ] = SurfaceArea[]{ 1000023 };
  area_fct[ ht132_TH ] = SurfaceArea[]{ 1000024 };
  area_fct[ ht130_TH ] = SurfaceArea[]{ 1000026 };
  area_fct[ ht129_TH ] = SurfaceArea[]{ 1000027 };
  area_fct[ ht127_TH ] = SurfaceArea[]{ 1000029 };
  area_fct[ ht125_TH ] = SurfaceArea[]{ 1000031 };
  area_fct[ ht123_TH ] = SurfaceArea[]{ 1000033 };
  area_fct[ ht120_TH ] = SurfaceArea[]{ 1000036 };
  area_fct[ ht118_TH ] = SurfaceArea[]{ 1000038 };
  area_fct[ ht116_TH ] = SurfaceArea[]{ 1000040 };
  area_fct[ ht114_TH ] = SurfaceArea[]{ 1000042 };
  area_fct[ ht57_TH ] = SurfaceArea[]{ 1000044 };
  area_fct[ ht59_TH ] = SurfaceArea[]{ 1000046 };
  area_fct[ ht61_TH ] = SurfaceArea[]{ 1000048 };
  area_fct[ ht63_TH ] = SurfaceArea[]{ 1000050 };
  area_fct[ ht65_TH ] = SurfaceArea[]{ 1000052 };
  area_fct[ ht66_TH ] = SurfaceArea[]{ 1000053 };
  area_fct[ ht68_TH ] = SurfaceArea[]{ 1000055 };
  area_fct[ ht70_TH ] = SurfaceArea[]{ 1000057 };
  area_fct[ ht72_TH ] = SurfaceArea[]{ 1000059 };
  area_fct[ ht75_TH ] = SurfaceArea[]{ 1000062 };
  area_fct[ ht78_TH ] = SurfaceArea[]{ 1000065 };
  area_fct[ ht189_TH ] = SurfaceArea[]{ 1000067 };
  area_fct[ ht188_TH ] = SurfaceArea[]{ 1000068 };
  area_fct[ ht186_TH ] = SurfaceArea[]{ 1000070 };
  area_fct[ ht185_TH ] = SurfaceArea[]{ 1000071 };
  area_fct[ ht183_TH ] = SurfaceArea[]{ 1000073 };
  area_fct[ ht181_TH ] = SurfaceArea[]{ 1000075 };
  area_fct[ ht179_TH ] = SurfaceArea[]{ 1000077 };
  area_fct[ ht176_TH ] = SurfaceArea[]{ 1000080 };
  area_fct[ ht174_TH ] = SurfaceArea[]{ 1000082 };
  area_fct[ ht172_TH ] = SurfaceArea[]{ 1000084 };
  area_fct[ ht170_TH ] = SurfaceArea[]{ 1000086 };
  area_fct[ ht23_TH ] = SurfaceArea[]{ 1000088 };
  area_fct[ ht25_TH ] = SurfaceArea[]{ 1000090 };
  area_fct[ ht27_TH ] = SurfaceArea[]{ 1000092 };
  area_fct[ ht29_TH ] = SurfaceArea[]{ 1000094 };
  area_fct[ ht31_TH ] = SurfaceArea[]{ 1000096 };
  area_fct[ ht33_TH ] = SurfaceArea[]{ 1000098 };
  area_fct[ ht35_TH ] = SurfaceArea[]{ 1000100 };
  area_fct[ ht37_TH ] = SurfaceArea[]{ 1000102 };
  area_fct[ ht40_TH ] = SurfaceArea[]{ 1000105 };
  area_fct[ ht42_TH ] = SurfaceArea[]{ 1000107 };
  area_fct[ ht44_TH ] = SurfaceArea[]{ 1000109 };
  area_fct[ ht46_TH ] = SurfaceArea[]{ 1000111 };
  area_fct[ ht48_TH ] = SurfaceArea[]{ 1000113 };
  area_fct[ ht50_TH ] = SurfaceArea[]{ 1000115 };
  area_fct[ ht52_TH ] = SurfaceArea[]{ 1000117 };
  area_fct[ ht54_TH ] = SurfaceArea[]{ 1000119 };
  area_fct[ ht56_TH ] = SurfaceArea[]{ 1000121 };
  area_fct[ ht167_TH ] = SurfaceArea[]{ 1000123 };
  area_fct[ ht165_TH ] = SurfaceArea[]{ 1000125 };
  area_fct[ ht163_TH ] = SurfaceArea[]{ 1000127 };
  area_fct[ ht161_TH ] = SurfaceArea[]{ 1000129 };
  area_fct[ ht159_TH ] = SurfaceArea[]{ 1000131 };
  area_fct[ ht157_TH ] = SurfaceArea[]{ 1000133 };
  area_fct[ ht155_TH ] = SurfaceArea[]{ 1000135 };
  area_fct[ ht153_TH ] = SurfaceArea[]{ 1000137 };
  area_fct[ ht151_TH ] = SurfaceArea[]{ 1000139 };
  area_fct[ ht150_TH ] = SurfaceArea[]{ 1000140 };
  area_fct[ ht148_TH ] = SurfaceArea[]{ 1000142 };
  area_fct[ ht146_TH ] = SurfaceArea[]{ 1000144 };
  area_fct[ ht144_TH ] = SurfaceArea[]{ 1000146 };
  area_fct[ ht142_TH ] = SurfaceArea[]{ 1000148 };
  area_fct[ ht140_TH ] = SurfaceArea[]{ 1000150 };
  area_fct[ ht138_TH ] = SurfaceArea[]{ 1000152 };
  area_fct[ ht136_TH ] = SurfaceArea[]{ 1000154 };
  area_fct[ ht79_TH ] = SurfaceArea[]{ 1000156 };
  area_fct[ ht81_TH ] = SurfaceArea[]{ 1000158 };
  area_fct[ ht83_TH ] = SurfaceArea[]{ 1000160 };
  area_fct[ ht85_TH ] = SurfaceArea[]{ 1000162 };
  area_fct[ ht87_TH ] = SurfaceArea[]{ 1000164 };
  area_fct[ ht89_TH ] = SurfaceArea[]{ 1000166 };
  area_fct[ ht91_TH ] = SurfaceArea[]{ 1000168 };
  area_fct[ ht93_TH ] = SurfaceArea[]{ 1000170 };
  area_fct[ ht96_TH ] = SurfaceArea[]{ 1000173 };
  area_fct[ ht98_TH ] = SurfaceArea[]{ 1000175 };
  area_fct[ ht100_TH ] = SurfaceArea[]{ 1000177 };
  area_fct[ ht102_TH ] = SurfaceArea[]{ 1000179 };
  area_fct[ ht104_TH ] = SurfaceArea[]{ 1000181 };
  area_fct[ ht106_TH ] = SurfaceArea[]{ 1000183 };
  area_fct[ ht108_TH ] = SurfaceArea[]{ 1000185 };
  area_fct[ ht110_TH ] = SurfaceArea[]{ 1000187 };
  area_fct[ ht112_TH ] = SurfaceArea[]{ 1000189 };
  area_fct[ ht223_TH ] = SurfaceArea[]{ 1000191 };
  area_fct[ ht221_TH ] = SurfaceArea[]{ 1000193 };
  area_fct[ ht219_TH ] = SurfaceArea[]{ 1000195 };
  area_fct[ ht217_TH ] = SurfaceArea[]{ 1000197 };
  area_fct[ ht215_TH ] = SurfaceArea[]{ 1000199 };
  area_fct[ ht213_TH ] = SurfaceArea[]{ 1000201 };
  area_fct[ ht211_TH ] = SurfaceArea[]{ 1000203 };
  area_fct[ ht209_TH ] = SurfaceArea[]{ 1000205 };
  area_fct[ ht207_TH ] = SurfaceArea[]{ 1000207 };
  area_fct[ ht206_TH ] = SurfaceArea[]{ 1000208 };
  area_fct[ ht204_TH ] = SurfaceArea[]{ 1000210 };
  area_fct[ ht202_TH ] = SurfaceArea[]{ 1000212 };
  area_fct[ ht200_TH ] = SurfaceArea[]{ 1000214 };
  area_fct[ ht198_TH ] = SurfaceArea[]{ 1000216 };
  area_fct[ ht196_TH ] = SurfaceArea[]{ 1000218 };
  area_fct[ ht194_TH ] = SurfaceArea[]{ 1000220 };
  area_fct[ ht192_TH ] = SurfaceArea[]{ 1000222 };
  area_fct[ ht2_TH ] = SurfaceArea[]{ 1000001 };
  area_fct[ ht4_TH ] = SurfaceArea[]{ 1000003 };
  area_fct[ ht6_TH ] = SurfaceArea[]{ 1000005 };
  area_fct[ ht8_TH ] = SurfaceArea[]{ 1000007 };
  area_fct[ ht11_TH ] = SurfaceArea[]{ 1000010 };
  area_fct[ ht13_TH ] = SurfaceArea[]{ 1000012 };
  area_fct[ ht15_TH ] = SurfaceArea[]{ 1000014 };
  area_fct[ ht17_TH ] = SurfaceArea[]{ 1000016 };
  area_fct[ ht18_TH ] = SurfaceArea[]{ 1000017 };
  area_fct[ ht20_TH ] = SurfaceArea[]{ 1000019 };
  area_fct[ ht21_TH ] = SurfaceArea[]{ 1000020 };
  area_fct[ ht134_TH ] = SurfaceArea[]{ 1000022 };
  area_fct[ ht131_TH ] = SurfaceArea[]{ 1000025 };
  area_fct[ ht128_TH ] = SurfaceArea[]{ 1000028 };
  area_fct[ ht126_TH ] = SurfaceArea[]{ 1000030 };
  area_fct[ ht124_TH ] = SurfaceArea[]{ 1000032 };
  area_fct[ ht122_TH ] = SurfaceArea[]{ 1000034 };
  area_fct[ ht121_TH ] = SurfaceArea[]{ 1000035 };
  area_fct[ ht119_TH ] = SurfaceArea[]{ 1000037 };
  area_fct[ ht117_TH ] = SurfaceArea[]{ 1000039 };
  area_fct[ ht115_TH ] = SurfaceArea[]{ 1000041 };
  area_fct[ ht113_TH ] = SurfaceArea[]{ 1000043 };
  area_fct[ ht58_TH ] = SurfaceArea[]{ 1000045 };
  area_fct[ ht60_TH ] = SurfaceArea[]{ 1000047 };
  area_fct[ ht62_TH ] = SurfaceArea[]{ 1000049 };
  area_fct[ ht64_TH ] = SurfaceArea[]{ 1000051 };
  area_fct[ ht67_TH ] = SurfaceArea[]{ 1000054 };
  area_fct[ ht69_TH ] = SurfaceArea[]{ 1000056 };
  area_fct[ ht71_TH ] = SurfaceArea[]{ 1000058 };
  area_fct[ ht73_TH ] = SurfaceArea[]{ 1000060 };
  area_fct[ ht74_TH ] = SurfaceArea[]{ 1000061 };
  area_fct[ ht76_TH ] = SurfaceArea[]{ 1000063 };
  area_fct[ ht77_TH ] = SurfaceArea[]{ 1000064 };
  area_fct[ ht190_TH ] = SurfaceArea[]{ 1000066 };
  area_fct[ ht187_TH ] = SurfaceArea[]{ 1000069 };
  area_fct[ ht184_TH ] = SurfaceArea[]{ 1000072 };
  area_fct[ ht182_TH ] = SurfaceArea[]{ 1000074 };
  area_fct[ ht180_TH ] = SurfaceArea[]{ 1000076 };
  area_fct[ ht178_TH ] = SurfaceArea[]{ 1000078 };
  area_fct[ ht177_TH ] = SurfaceArea[]{ 1000079 };
  area_fct[ ht175_TH ] = SurfaceArea[]{ 1000081 };
  area_fct[ ht173_TH ] = SurfaceArea[]{ 1000083 };
  area_fct[ ht171_TH ] = SurfaceArea[]{ 1000085 };
  area_fct[ ht169_TH ] = SurfaceArea[]{ 1000087 };
  area_fct[ ht24_TH ] = SurfaceArea[]{ 1000089 };
  area_fct[ ht26_TH ] = SurfaceArea[]{ 1000091 };
  area_fct[ ht28_TH ] = SurfaceArea[]{ 1000093 };
  area_fct[ ht30_TH ] = SurfaceArea[]{ 1000095 };
  area_fct[ ht32_TH ] = SurfaceArea[]{ 1000097 };
  area_fct[ ht34_TH ] = SurfaceArea[]{ 1000099 };
  area_fct[ ht36_TH ] = SurfaceArea[]{ 1000101 };
  area_fct[ ht38_TH ] = SurfaceArea[]{ 1000103 };
  area_fct[ ht39_TH ] = SurfaceArea[]{ 1000104 };
  area_fct[ ht41_TH ] = SurfaceArea[]{ 1000106 };
  area_fct[ ht43_TH ] = SurfaceArea[]{ 1000108 };
  area_fct[ ht45_TH ] = SurfaceArea[]{ 1000110 };
  area_fct[ ht47_TH ] = SurfaceArea[]{ 1000112 };
  area_fct[ ht49_TH ] = SurfaceArea[]{ 1000114 };
  area_fct[ ht51_TH ] = SurfaceArea[]{ 1000116 };
  area_fct[ ht53_TH ] = SurfaceArea[]{ 1000118 };
  area_fct[ ht55_TH ] = SurfaceArea[]{ 1000120 };
  area_fct[ ht168_TH ] = SurfaceArea[]{ 1000122 };
  area_fct[ ht166_TH ] = SurfaceArea[]{ 1000124 };
  area_fct[ ht164_TH ] = SurfaceArea[]{ 1000126 };
  area_fct[ ht162_TH ] = SurfaceArea[]{ 1000128 };
  area_fct[ ht160_TH ] = SurfaceArea[]{ 1000130 };
  area_fct[ ht158_TH ] = SurfaceArea[]{ 1000132 };
  area_fct[ ht156_TH ] = SurfaceArea[]{ 1000134 };
  area_fct[ ht154_TH ] = SurfaceArea[]{ 1000136 };
  area_fct[ ht152_TH ] = SurfaceArea[]{ 1000138 };
  area_fct[ ht149_TH ] = SurfaceArea[]{ 1000141 };
  area_fct[ ht147_TH ] = SurfaceArea[]{ 1000143 };
  area_fct[ ht145_TH ] = SurfaceArea[]{ 1000145 };
  area_fct[ ht143_TH ] = SurfaceArea[]{ 1000147 };
  area_fct[ ht141_TH ] = SurfaceArea[]{ 1000149 };
  area_fct[ ht139_TH ] = SurfaceArea[]{ 1000151 };
  area_fct[ ht137_TH ] = SurfaceArea[]{ 1000153 };
  area_fct[ ht135_TH ] = SurfaceArea[]{ 1000155 };
  area_fct[ ht80_TH ] = SurfaceArea[]{ 1000157 };
  area_fct[ ht82_TH ] = SurfaceArea[]{ 1000159 };
  area_fct[ ht84_TH ] = SurfaceArea[]{ 1000161 };
  area_fct[ ht86_TH ] = SurfaceArea[]{ 1000163 };
  area_fct[ ht88_TH ] = SurfaceArea[]{ 1000165 };
  area_fct[ ht90_TH ] = SurfaceArea[]{ 1000167 };
  area_fct[ ht92_TH ] = SurfaceArea[]{ 1000169 };
  area_fct[ ht94_TH ] = SurfaceArea[]{ 1000171 };
  area_fct[ ht95_TH ] = SurfaceArea[]{ 1000172 };
  area_fct[ ht97_TH ] = SurfaceArea[]{ 1000174 };
  area_fct[ ht99_TH ] = SurfaceArea[]{ 1000176 };
  area_fct[ ht101_TH ] = SurfaceArea[]{ 1000178 };
  area_fct[ ht103_TH ] = SurfaceArea[]{ 1000180 };
  area_fct[ ht105_TH ] = SurfaceArea[]{ 1000182 };
  area_fct[ ht107_TH ] = SurfaceArea[]{ 1000184 };
  area_fct[ ht109_TH ] = SurfaceArea[]{ 1000186 };
  area_fct[ ht111_TH ] = SurfaceArea[]{ 1000188 };
  area_fct[ ht224_TH ] = SurfaceArea[]{ 1000190 };
  area_fct[ ht222_TH ] = SurfaceArea[]{ 1000192 };
  area_fct[ ht220_TH ] = SurfaceArea[]{ 1000194 };
  area_fct[ ht218_TH ] = SurfaceArea[]{ 1000196 };
  area_fct[ ht216_TH ] = SurfaceArea[]{ 1000198 };
  area_fct[ ht214_TH ] = SurfaceArea[]{ 1000200 };
  area_fct[ ht212_TH ] = SurfaceArea[]{ 1000202 };
  area_fct[ ht210_TH ] = SurfaceArea[]{ 1000204 };
  area_fct[ ht208_TH ] = SurfaceArea[]{ 1000206 };
  area_fct[ ht205_TH ] = SurfaceArea[]{ 1000209 };
  area_fct[ ht203_TH ] = SurfaceArea[]{ 1000211 };
  area_fct[ ht201_TH ] = SurfaceArea[]{ 1000213 };
  area_fct[ ht199_TH ] = SurfaceArea[]{ 1000215 };
  area_fct[ ht197_TH ] = SurfaceArea[]{ 1000217 };
  area_fct[ ht195_TH ] = SurfaceArea[]{ 1000219 };
  area_fct[ ht193_TH ] = SurfaceArea[]{ 1000221 };
  area_fct[ ht191_TH ] = SurfaceArea[]{ 1000223 };


      // time steps adaptive time stepping must hit
      Breakpoints = {};


      // --------------- MATERIAL FUNCTIONS ----------------------------------------



                                               


        f_inner_voids_FNAL40_NC = 0.0980433235055614;
        f_outer_voids_FNAL40_NC = 0.10836367334825207;
        f_strand_FNAL40_NC = 1.0 - 0.20640699685381347;

      f_stabilizer_FNAL40_NC = f_strand_FNAL40_NC * 1.19 / (1. + 1.19);
      f_sc_FNAL40_NC = f_strand_FNAL40_NC * (1.0 - 1.19 / (1. + 1.19));

      source_current = 11850.0;

        criticalCurrentDensity[jcNonZero_FNAL40_NC] = $Time > 1000000.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht27] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht28] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht29] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht30] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht31] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht32] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht33] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht34] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht35] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht36] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht37] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht38] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht42] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht43] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht44] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht45] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht46] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht47] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht48] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht49] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht50] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht51] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht52] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht53] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht83] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht84] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht85] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht86] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht87] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht88] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht89] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht90] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht91] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht92] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht93] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht94] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht98] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht99] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht100] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht101] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht102] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht103] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht104] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht105] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht106] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht107] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht108] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht109] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht139] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht140] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht141] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht142] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht143] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht144] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht145] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht146] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht147] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht148] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht149] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht150] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht154] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht155] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht156] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht157] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht158] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht159] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht160] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht161] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht162] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht163] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht164] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht165] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht195] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht196] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht197] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht198] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht199] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht200] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht201] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht202] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht203] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht204] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht205] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht206] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht210] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht211] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht212] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht213] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht214] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht215] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht216] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht217] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht218] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht219] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht220] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


        criticalCurrentDensity[jcZero_ht221] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{42240000000.0, 16.33, 26.45} * f_sc_FNAL40_NC;


      rho[Omega_p_FNAL40_NC_TH] = EffectiveResistivity[CFUN_rhoCu_T_B[$1, Norm[$2]]{159.0}]{f_stabilizer_FNAL40_NC};

      // effective thermal conductivity of the bare part
      kappa[Omega_p_FNAL40_NC_TH] = RuleOfMixtures[
        CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 159.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{159.0}]{159.0}
      , CFUN_kG10_T[$1]
      , CFUN_kG10_T[$1]
    ]
    {f_stabilizer_FNAL40_NC
      , f_inner_voids_FNAL40_NC
      ,  f_outer_voids_FNAL40_NC
    };

      // heat capacity of bare part
      heatCap[Omega_p_FNAL40_NC_TH] = RuleOfMixtures[
        CFUN_CvCu_T[$1],
        CFUN_CvNb3Sn_T_B[$1, Norm[$2]],
        CFUN_CvG10_T[$1],
        CFUN_CvG10_T[$1]
      ]
      {
        f_stabilizer_FNAL40_NC, 
        f_sc_FNAL40_NC,
        f_inner_voids_FNAL40_NC, 
        f_outer_voids_FNAL40_NC
      };

      // joule losses of bare part
      jouleLosses[] = CFUN_quenchState_Ic[criticalCurrentDensity[$1, $2] * area_fct[]]{source_current} * rho[$1, $2] * SquNorm[source_current/area_fct[]];

      // thermal conductivity of the wedges
      kappa[Omega_i_TH] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 159.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{159.0}]{159.0};

      // heat capacity of wedges
      heatCap[Omega_i_TH] = CFUN_CvCu_T[$1];


      // thermal conductivity of the insulation
      kappa[Omega_ins_TH] = CFUN_kG10_T[$1];

      // heat capacity of insulation
      heatCap[ Omega_ins_TH ] = CFUN_CvG10_T[$1];


}




Constraint {
  { Name Dirichlet_a_Mag;
    Case {
      { Region Bd_Omega ; Value 0.; }
    }
  }
  { Name SourceCurrentDensityZ;
    Case {
      { Region Omega_p_EM ; Value js_fct[]; }
    }
  }

  { Name initTemp ;
    Case {
        { Region Omega_TH ; Value 1.9 ; Type Init; } // init. condition
    }
  }
  { Name Dirichlet_a_projection;
    Case {
      { Region projection_points ; Value 0; Type Assign; }
    }
  }
}

FunctionSpace {
  { Name Hcurl_a_Mag_2D; Type Form1P; // Magnetic vector potential a
    BasisFunction {
      { Name se; NameOfCoef ae; Function BF_PerpendicularEdge;
        Support Omega_EM ; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef ae; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_Mag; }
    }
  }

  { Name Hregion_j_Mag_2D; Type Vector; // Electric current density js
    BasisFunction {
      { Name sr; NameOfCoef jsr; Function BF_RegionZ;
        Support Omega_p_EM; Entity Omega_p_EM; }
    }
    Constraint {
      { NameOfCoef jsr; EntityType Region;
        NameOfConstraint SourceCurrentDensityZ; }
    }
  }

  { Name H_curl_a_artificial_dof; Type Form1P;  
    BasisFunction {
      { Name se_after_projection; NameOfCoef ae_after_projection; Function BF_PerpendicularEdge;
        Support Omega_TH ; Entity NodesOf[ All ]; }
    }
    // not needed since boundary is not part of Omega_TH
    Constraint {
      { NameOfCoef ae_after_projection; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_projection; }     
    }
  }                                               

  { Name Hgrad_T; Type Form0;
    BasisFunction {
      { Name un;  NameOfCoef ui;  Function BF_Node;
          Support Region[{Omega_TH, Bnds_support}] ; Entity NodesOf[All];
      }

    }


    Constraint {
      { NameOfCoef ui; EntityType NodesOf; NameOfConstraint initTemp; }
      // do not constraint second order basis function as it's already covered by ui
    }
  }

}

Jacobian {
  { Name Jac_Vol_EM ;
    Case {
      { Region Omega_aff_EM ;
        Jacobian VolSphShell {0.636225, 0.814368} ; }
      { Region All ; Jacobian Vol ; }
    }
  }

  { Name Jac_Vol_TH ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Jac_Sur_TH ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int_EM ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point ; NumberOfPoints 1 ; }
          { GeoElement Line ; NumberOfPoints 2 ; }
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_line_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Line ; NumberOfPoints 2 ; }
        }
      }
    }
  }

  { Name Int_conducting_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_insulating_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }
}

Formulation {
  { Name Magnetostatics_a_2D; Type FemEquation;
    Quantity {
      { Name a ; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      { Name js; Type Local; NameOfSpace Hregion_j_Mag_2D; }
    }
    Equation {
      Integral { [ nu[{d a}] * Dof{d a} , {d a} ];
        In Omega_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }

      Integral { JacNL[ dnuIronYoke[{d a}] * Dof{d a} , {d a} ];
        In Omega_bh_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }

      Integral { [ -Dof{js} , {a} ];
        In Omega_p_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }
    }
  }

  // Dummy formulation just to save the values of the norm of B from the EM mesh on the Gaussian points of
  // the thermal mesh. Alternatively, a Galerkin projection could be used.
  { Name Projection_EM_to_TH; Type FemEquation;
    Quantity {
      {Name a_before_projection; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      {Name a_artificial_dof; Type Local; NameOfSpace H_curl_a_artificial_dof; }
    }
    Equation {
      Integral { [ - SetVariable[Norm[{d a_before_projection}], ElementNum[], QuadraturePointIndex[]]{$Bnorm}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }

        Integral { [ Dof{d a_artificial_dof}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }
    }
  }                                              

  { Name Thermal_T;   Type FemEquation;
    Quantity {
      // cont temperature
      { Name T; Type Local; NameOfSpace Hgrad_T; }
    }

    Equation {
      Integral { [ kappa[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{d T} , {d T} ] ;
        In Region[ {Omega_p_TH, Omega_i_TH } ]; Integration Int_conducting_TH ; Jacobian Jac_Vol_TH ; }

      Integral { DtDof[ heatCap[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{T}, {T} ];
        In Region[ {Omega_p_TH, Omega_i_TH } ]; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }

      Integral { [ kappa[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{d T} , {d T} ] ;
        In Omega_ins_TH; Integration Int_insulating_TH ; Jacobian Jac_Vol_TH ; }

      Integral { DtDof[ heatCap[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{T}, {T} ];
        In Omega_ins_TH; Integration Int_insulating_TH; Jacobian Jac_Vol_TH;  }

    // TODO: implement derivatives, missing copper for example
    /*   Integral { JacNL[ dkappadT[{T}, {d a}] * {d T} * Dof{T} , {d T} ] ;
         In Omega_TH; Integration Int<> ; Jacobian Jac_Vol_TH ; } */

      Integral { [ - jouleLosses[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}], {T}];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }

 
        // Neumann
          Integral { [- 0.0 , {T} ] ;
            In  general_adiabatic ; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

        // Robin
        // n * kappa grad (T) = h (T - Tinf) becomes two terms since GetDP can only
        // handle linear and not affine terms
        // NOTE: signs might be switched
    }
  }
}

Resolution {
  { Name resolution;
    System {
      { Name Sys_Mag; NameOfFormulation Magnetostatics_a_2D; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_MBH_1in1_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_MBH_1in1_REF_EM.msh"; }
      { Name Sys_The; NameOfFormulation Thermal_T; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_MBH_1in1_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_MBH_1in1_REF_TH.msh"; }
      { Name sys_Mag_projection; NameOfFormulation Projection_EM_to_TH; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_MBH_1in1_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_MBH_1in1_REF_TH.msh";}
    }
    Operation {
      InitSolution[Sys_Mag];
      IterativeLoopN[20, 0.9,
        System { { Sys_Mag, 1e-06, 0.0001, Solution LinfNorm } }
      ] { GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
      PostOperation[Map_a];

      Generate[sys_Mag_projection]; Solve[sys_Mag_projection];
      SaveSolution[sys_Mag_projection]; //PostOperation[b_after_projection_pos];

      SetExtrapolationOrder[0];
      InitSolution Sys_The; // init. the solution using init. constraints

      //PostOperation[b_thermal];

      CreateDirectory["T_avg"];
      PostOperation[T_avg];

      PostOperation[Map_T];

      // initialized cumulate times to zero to avoid warning
      Evaluate[$tg_cumul_cpu = 0, $ts_cumul_cpu = 0, $tg_cumul_wall = 0, $ts_cumul_wall = 0];
      Print["timestep,gen_wall,gen_cpu,sol_wall,sol_cpu,pos_wall,pos_cpu,gen_wall_cumul,gen_cpu_cumul,sol_wall_cumul,sol_cpu_cumul,pos_wall_cumul,pos_cpu_cumul", File "computation_times.csv"];
      //PostOperation[b_after_projection_pos];
      

      Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

      TimeLoopAdaptive
      [ 0.0, 0.0001, 1e-06, 1e-12, 0.1, "Euler", List[Breakpoints],
      System { { Sys_The, 0.001, 0.01, LinfNorm } } ]
      {
        IterativeLoopN[20, 0.7,
          System { { Sys_The, 0.001, 0.01, Solution LinfNorm } }]
        {
          Evaluate[ $tg1_wall = GetWallClockTime[], $tg1_cpu = GetCpuTime[] ];
          GenerateJac Sys_The ;
          Evaluate[ $tg2_wall = GetWallClockTime[], $tg2_cpu = GetCpuTime[] ];

          // add to generation times of previous rejected time steps
          Evaluate[ $tg_wall = $tg_wall + $tg2_wall - $tg1_wall, $tg_cpu = $tg_cpu + $tg2_cpu - $tg1_cpu ];

          Evaluate[ $ts1_wall = GetWallClockTime[], $ts1_cpu = GetCpuTime[] ];
          SolveJac Sys_The;
          Evaluate[ $ts2_wall = GetWallClockTime[], $ts2_cpu = GetCpuTime[] ];

          // add to solution times of previous rejected time steps
          Evaluate[ $ts_wall = $ts_wall + $ts2_wall - $ts1_wall, $ts_cpu = $ts_cpu + $ts2_cpu - $ts1_cpu ];

        }
      }
      {
        // save solution to .res file
        SaveSolution[Sys_The];

        Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];
        // print average temperature

        // print temperature map

        PostOperation[PrintMaxTemp]; // save maximum temperature in register 1
        Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

        Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

        // cumulated times
        Evaluate[ $tg_cumul_wall = $tg_cumul_wall + $tg_wall, $tg_cumul_cpu = $tg_cumul_cpu + $tg_cpu, $ts_cumul_wall = $ts_cumul_wall + $ts_wall, $ts_cumul_cpu = $ts_cumul_cpu + $ts_cpu, $tp_cumul_wall = $tp_cumul_wall + $tp2_wall - $tp1_wall, $tp_cumul_cpu = $tp_cumul_cpu + $tp2_cpu - $tp1_cpu];

        // print to file
        Print[{$TimeStep, $tg_wall, $tg_cpu, $ts_wall, $ts_cpu, $tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];

        // reset after accepted time step
        Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

        // check if maximum temperature is reached


        Print[{#1}, Format "Maximum temperature: %g "];
        Test[#1 > stop_temperature] {
          Break[];
        }
      }
    

      Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];
      PostOperation[T_avg];

      PostOperation[Map_T];
      Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

      Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

      Print[{$tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "-1,0,0,0,0,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];
    }
  }
}

PostProcessing {
  { Name MagSta_a_2D; NameOfFormulation Magnetostatics_a_2D; NameOfSystem Sys_Mag;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name b;
        Value {
          Term { [ {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[{d a}] * {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
    }
  }

  { Name Thermal_T ; NameOfFormulation Thermal_T ; NameOfSystem Sys_The ;
    PostQuantity {
      // Temperature
      { Name T ;
        Value {
          Local { [ {T} ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      { Name jOverJc ;
        Value {
          Term { [ source_current/area_fct[] * 1/(criticalCurrentDensity[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] + 1) ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      // Temperature average as integral quantity
      { Name T_avg ;
        Value {
          Integral {  [ {T} / area_fct[] ] ;
            In Region[ {Omega_p_TH, Omega_i_TH } ] ; Jacobian Jac_Vol_TH ; Integration Int_conducting_TH; }

          Integral {  [ {T} / area_fct[] ] ;
            In Omega_ins_TH ; Jacobian Jac_Vol_TH ; Integration Int_insulating_TH; }
        }
      }

      { Name b_thermal ;
        Value {
          Local {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

    { Name rho ;
      Value {
          Term { [ rho[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ] ;
          In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
      }
    }
    }
  }

  { Name post_projection; NameOfFormulation Projection_EM_to_TH; NameOfSystem sys_Mag_projection;
    PostQuantity {
      { Name b_before_projection ;
        Value {
          Term {  [Norm[{d a_before_projection}]] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
      { Name b_after_projection ;
        Value {
          Term {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
    }
  }
}

PostOperation PrintMaxTemp UsingPost Thermal_T {
  // Get maximum in bare region and store in register 1
  Print[ T, OnElementsOf Omega_TH, StoreMaxInRegister 1, Format Table,
    LastTimeStepOnly 1, SendToServer "No"] ;
}

PostOperation {
  { Name Dummy; NameOfPostProcessing  Thermal_T ;
    Operation { }
  }

  { Name Map_a; NameOfPostProcessing MagSta_a_2D;
    Operation {
      Print[ b, OnElementsOf Omega_EM, File "b_Omega.pos"] ;
	  //Print [ b, OnLine {{List[{0,0,0}]}{List[{0.814368,0,0}]}} {1000}, Format SimpleTable, File "Center_line.csv"];
    }
  }

  { Name b_thermal; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ b_thermal, OnElementsOf Omega_p_TH, File "b_thermal.pos"] ;
    }
  }
  { Name b_after_projection_pos; NameOfPostProcessing post_projection;
    Operation {
      Print[ b_before_projection, OnElementsOf Omega_p_TH, File "b_before_projection_gmsh.pos"] ;
      Print[ b_after_projection, OnElementsOf Omega_p_TH, File "b_after_projection.pos"] ;
    }
  }

  { Name Map_T; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ T, OnElementsOf Omega_c_TH, File "T_Omega_c.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0 ] ;
      //Print[ JoverJc, OnElementsOf Omega_p_TH, File "JoverJc_Omega_p.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0, AtGaussPoints 4, Depth 0 ] ;
      //Print[ rho, OnElementsOf Omega_p_TH, File "rho_Omega_p.pos", SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0 ] ;
    }
  }

  { Name T_avg; NameOfPostProcessing Thermal_T;
    Operation {
    // writes pairs of time step and average temperature to file, one line for each time step
      Print[ T_avg[Region[1000000]], OnGlobal, File "T_avg/T_avg_0.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000002]], OnGlobal, File "T_avg/T_avg_1.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000004]], OnGlobal, File "T_avg/T_avg_2.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000006]], OnGlobal, File "T_avg/T_avg_3.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000008]], OnGlobal, File "T_avg/T_avg_4.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000009]], OnGlobal, File "T_avg/T_avg_5.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000011]], OnGlobal, File "T_avg/T_avg_6.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000013]], OnGlobal, File "T_avg/T_avg_7.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000015]], OnGlobal, File "T_avg/T_avg_8.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000018]], OnGlobal, File "T_avg/T_avg_9.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000021]], OnGlobal, File "T_avg/T_avg_10.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000023]], OnGlobal, File "T_avg/T_avg_11.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000024]], OnGlobal, File "T_avg/T_avg_12.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000026]], OnGlobal, File "T_avg/T_avg_13.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000027]], OnGlobal, File "T_avg/T_avg_14.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000029]], OnGlobal, File "T_avg/T_avg_15.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000031]], OnGlobal, File "T_avg/T_avg_16.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000033]], OnGlobal, File "T_avg/T_avg_17.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000036]], OnGlobal, File "T_avg/T_avg_18.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000038]], OnGlobal, File "T_avg/T_avg_19.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000040]], OnGlobal, File "T_avg/T_avg_20.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000042]], OnGlobal, File "T_avg/T_avg_21.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000044]], OnGlobal, File "T_avg/T_avg_22.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000046]], OnGlobal, File "T_avg/T_avg_23.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000048]], OnGlobal, File "T_avg/T_avg_24.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000050]], OnGlobal, File "T_avg/T_avg_25.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000052]], OnGlobal, File "T_avg/T_avg_26.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000053]], OnGlobal, File "T_avg/T_avg_27.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000055]], OnGlobal, File "T_avg/T_avg_28.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000057]], OnGlobal, File "T_avg/T_avg_29.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000059]], OnGlobal, File "T_avg/T_avg_30.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000062]], OnGlobal, File "T_avg/T_avg_31.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000065]], OnGlobal, File "T_avg/T_avg_32.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000067]], OnGlobal, File "T_avg/T_avg_33.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000068]], OnGlobal, File "T_avg/T_avg_34.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000070]], OnGlobal, File "T_avg/T_avg_35.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000071]], OnGlobal, File "T_avg/T_avg_36.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000073]], OnGlobal, File "T_avg/T_avg_37.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000075]], OnGlobal, File "T_avg/T_avg_38.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000077]], OnGlobal, File "T_avg/T_avg_39.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000080]], OnGlobal, File "T_avg/T_avg_40.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000082]], OnGlobal, File "T_avg/T_avg_41.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000084]], OnGlobal, File "T_avg/T_avg_42.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000086]], OnGlobal, File "T_avg/T_avg_43.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000001]], OnGlobal, File "T_avg/T_avg_44.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000003]], OnGlobal, File "T_avg/T_avg_45.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000005]], OnGlobal, File "T_avg/T_avg_46.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000007]], OnGlobal, File "T_avg/T_avg_47.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000010]], OnGlobal, File "T_avg/T_avg_48.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000012]], OnGlobal, File "T_avg/T_avg_49.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000014]], OnGlobal, File "T_avg/T_avg_50.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000016]], OnGlobal, File "T_avg/T_avg_51.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000017]], OnGlobal, File "T_avg/T_avg_52.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000019]], OnGlobal, File "T_avg/T_avg_53.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000020]], OnGlobal, File "T_avg/T_avg_54.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000022]], OnGlobal, File "T_avg/T_avg_55.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000025]], OnGlobal, File "T_avg/T_avg_56.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000028]], OnGlobal, File "T_avg/T_avg_57.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000030]], OnGlobal, File "T_avg/T_avg_58.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000032]], OnGlobal, File "T_avg/T_avg_59.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000034]], OnGlobal, File "T_avg/T_avg_60.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000035]], OnGlobal, File "T_avg/T_avg_61.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000037]], OnGlobal, File "T_avg/T_avg_62.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000039]], OnGlobal, File "T_avg/T_avg_63.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000041]], OnGlobal, File "T_avg/T_avg_64.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000043]], OnGlobal, File "T_avg/T_avg_65.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000045]], OnGlobal, File "T_avg/T_avg_66.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000047]], OnGlobal, File "T_avg/T_avg_67.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000049]], OnGlobal, File "T_avg/T_avg_68.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000051]], OnGlobal, File "T_avg/T_avg_69.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000054]], OnGlobal, File "T_avg/T_avg_70.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000056]], OnGlobal, File "T_avg/T_avg_71.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000058]], OnGlobal, File "T_avg/T_avg_72.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000060]], OnGlobal, File "T_avg/T_avg_73.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000061]], OnGlobal, File "T_avg/T_avg_74.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000063]], OnGlobal, File "T_avg/T_avg_75.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000064]], OnGlobal, File "T_avg/T_avg_76.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000066]], OnGlobal, File "T_avg/T_avg_77.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000069]], OnGlobal, File "T_avg/T_avg_78.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000072]], OnGlobal, File "T_avg/T_avg_79.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000074]], OnGlobal, File "T_avg/T_avg_80.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000076]], OnGlobal, File "T_avg/T_avg_81.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000078]], OnGlobal, File "T_avg/T_avg_82.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000079]], OnGlobal, File "T_avg/T_avg_83.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000081]], OnGlobal, File "T_avg/T_avg_84.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000083]], OnGlobal, File "T_avg/T_avg_85.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000085]], OnGlobal, File "T_avg/T_avg_86.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000087]], OnGlobal, File "T_avg/T_avg_87.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000088]], OnGlobal, File "T_avg/T_avg_88.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000090]], OnGlobal, File "T_avg/T_avg_89.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000092]], OnGlobal, File "T_avg/T_avg_90.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000094]], OnGlobal, File "T_avg/T_avg_91.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000096]], OnGlobal, File "T_avg/T_avg_92.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000098]], OnGlobal, File "T_avg/T_avg_93.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000100]], OnGlobal, File "T_avg/T_avg_94.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000102]], OnGlobal, File "T_avg/T_avg_95.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000105]], OnGlobal, File "T_avg/T_avg_96.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000107]], OnGlobal, File "T_avg/T_avg_97.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000109]], OnGlobal, File "T_avg/T_avg_98.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000111]], OnGlobal, File "T_avg/T_avg_99.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000113]], OnGlobal, File "T_avg/T_avg_100.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000115]], OnGlobal, File "T_avg/T_avg_101.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000117]], OnGlobal, File "T_avg/T_avg_102.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000119]], OnGlobal, File "T_avg/T_avg_103.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000121]], OnGlobal, File "T_avg/T_avg_104.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000123]], OnGlobal, File "T_avg/T_avg_105.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000125]], OnGlobal, File "T_avg/T_avg_106.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000127]], OnGlobal, File "T_avg/T_avg_107.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000129]], OnGlobal, File "T_avg/T_avg_108.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000131]], OnGlobal, File "T_avg/T_avg_109.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000133]], OnGlobal, File "T_avg/T_avg_110.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000135]], OnGlobal, File "T_avg/T_avg_111.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000137]], OnGlobal, File "T_avg/T_avg_112.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000139]], OnGlobal, File "T_avg/T_avg_113.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000140]], OnGlobal, File "T_avg/T_avg_114.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000142]], OnGlobal, File "T_avg/T_avg_115.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000144]], OnGlobal, File "T_avg/T_avg_116.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000146]], OnGlobal, File "T_avg/T_avg_117.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000148]], OnGlobal, File "T_avg/T_avg_118.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000150]], OnGlobal, File "T_avg/T_avg_119.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000152]], OnGlobal, File "T_avg/T_avg_120.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000154]], OnGlobal, File "T_avg/T_avg_121.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000156]], OnGlobal, File "T_avg/T_avg_122.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000158]], OnGlobal, File "T_avg/T_avg_123.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000160]], OnGlobal, File "T_avg/T_avg_124.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000162]], OnGlobal, File "T_avg/T_avg_125.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000164]], OnGlobal, File "T_avg/T_avg_126.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000166]], OnGlobal, File "T_avg/T_avg_127.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000168]], OnGlobal, File "T_avg/T_avg_128.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000170]], OnGlobal, File "T_avg/T_avg_129.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000173]], OnGlobal, File "T_avg/T_avg_130.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000175]], OnGlobal, File "T_avg/T_avg_131.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000177]], OnGlobal, File "T_avg/T_avg_132.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000179]], OnGlobal, File "T_avg/T_avg_133.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000181]], OnGlobal, File "T_avg/T_avg_134.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000183]], OnGlobal, File "T_avg/T_avg_135.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000185]], OnGlobal, File "T_avg/T_avg_136.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000187]], OnGlobal, File "T_avg/T_avg_137.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000189]], OnGlobal, File "T_avg/T_avg_138.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000191]], OnGlobal, File "T_avg/T_avg_139.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000193]], OnGlobal, File "T_avg/T_avg_140.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000195]], OnGlobal, File "T_avg/T_avg_141.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000197]], OnGlobal, File "T_avg/T_avg_142.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000199]], OnGlobal, File "T_avg/T_avg_143.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000201]], OnGlobal, File "T_avg/T_avg_144.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000203]], OnGlobal, File "T_avg/T_avg_145.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000205]], OnGlobal, File "T_avg/T_avg_146.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000207]], OnGlobal, File "T_avg/T_avg_147.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000208]], OnGlobal, File "T_avg/T_avg_148.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000210]], OnGlobal, File "T_avg/T_avg_149.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000212]], OnGlobal, File "T_avg/T_avg_150.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000214]], OnGlobal, File "T_avg/T_avg_151.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000216]], OnGlobal, File "T_avg/T_avg_152.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000218]], OnGlobal, File "T_avg/T_avg_153.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000220]], OnGlobal, File "T_avg/T_avg_154.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000222]], OnGlobal, File "T_avg/T_avg_155.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000089]], OnGlobal, File "T_avg/T_avg_156.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000091]], OnGlobal, File "T_avg/T_avg_157.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000093]], OnGlobal, File "T_avg/T_avg_158.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000095]], OnGlobal, File "T_avg/T_avg_159.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000097]], OnGlobal, File "T_avg/T_avg_160.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000099]], OnGlobal, File "T_avg/T_avg_161.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000101]], OnGlobal, File "T_avg/T_avg_162.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000103]], OnGlobal, File "T_avg/T_avg_163.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000104]], OnGlobal, File "T_avg/T_avg_164.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000106]], OnGlobal, File "T_avg/T_avg_165.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000108]], OnGlobal, File "T_avg/T_avg_166.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000110]], OnGlobal, File "T_avg/T_avg_167.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000112]], OnGlobal, File "T_avg/T_avg_168.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000114]], OnGlobal, File "T_avg/T_avg_169.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000116]], OnGlobal, File "T_avg/T_avg_170.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000118]], OnGlobal, File "T_avg/T_avg_171.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000120]], OnGlobal, File "T_avg/T_avg_172.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000122]], OnGlobal, File "T_avg/T_avg_173.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000124]], OnGlobal, File "T_avg/T_avg_174.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000126]], OnGlobal, File "T_avg/T_avg_175.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000128]], OnGlobal, File "T_avg/T_avg_176.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000130]], OnGlobal, File "T_avg/T_avg_177.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000132]], OnGlobal, File "T_avg/T_avg_178.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000134]], OnGlobal, File "T_avg/T_avg_179.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000136]], OnGlobal, File "T_avg/T_avg_180.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000138]], OnGlobal, File "T_avg/T_avg_181.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000141]], OnGlobal, File "T_avg/T_avg_182.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000143]], OnGlobal, File "T_avg/T_avg_183.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000145]], OnGlobal, File "T_avg/T_avg_184.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000147]], OnGlobal, File "T_avg/T_avg_185.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000149]], OnGlobal, File "T_avg/T_avg_186.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000151]], OnGlobal, File "T_avg/T_avg_187.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000153]], OnGlobal, File "T_avg/T_avg_188.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000155]], OnGlobal, File "T_avg/T_avg_189.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000157]], OnGlobal, File "T_avg/T_avg_190.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000159]], OnGlobal, File "T_avg/T_avg_191.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000161]], OnGlobal, File "T_avg/T_avg_192.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000163]], OnGlobal, File "T_avg/T_avg_193.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000165]], OnGlobal, File "T_avg/T_avg_194.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000167]], OnGlobal, File "T_avg/T_avg_195.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000169]], OnGlobal, File "T_avg/T_avg_196.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000171]], OnGlobal, File "T_avg/T_avg_197.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000172]], OnGlobal, File "T_avg/T_avg_198.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000174]], OnGlobal, File "T_avg/T_avg_199.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000176]], OnGlobal, File "T_avg/T_avg_200.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000178]], OnGlobal, File "T_avg/T_avg_201.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000180]], OnGlobal, File "T_avg/T_avg_202.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000182]], OnGlobal, File "T_avg/T_avg_203.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000184]], OnGlobal, File "T_avg/T_avg_204.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000186]], OnGlobal, File "T_avg/T_avg_205.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000188]], OnGlobal, File "T_avg/T_avg_206.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000190]], OnGlobal, File "T_avg/T_avg_207.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000192]], OnGlobal, File "T_avg/T_avg_208.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000194]], OnGlobal, File "T_avg/T_avg_209.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000196]], OnGlobal, File "T_avg/T_avg_210.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000198]], OnGlobal, File "T_avg/T_avg_211.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000200]], OnGlobal, File "T_avg/T_avg_212.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000202]], OnGlobal, File "T_avg/T_avg_213.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000204]], OnGlobal, File "T_avg/T_avg_214.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000206]], OnGlobal, File "T_avg/T_avg_215.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000209]], OnGlobal, File "T_avg/T_avg_216.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000211]], OnGlobal, File "T_avg/T_avg_217.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000213]], OnGlobal, File "T_avg/T_avg_218.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000215]], OnGlobal, File "T_avg/T_avg_219.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000217]], OnGlobal, File "T_avg/T_avg_220.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000219]], OnGlobal, File "T_avg/T_avg_221.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000221]], OnGlobal, File "T_avg/T_avg_222.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
      Print[ T_avg[Region[1000223]], OnGlobal, File "T_avg/T_avg_223.txt", Format Table, SendToServer "No", LastTimeStepOnly 0, AppendToExistingFile 0] ;
    }
  }
}