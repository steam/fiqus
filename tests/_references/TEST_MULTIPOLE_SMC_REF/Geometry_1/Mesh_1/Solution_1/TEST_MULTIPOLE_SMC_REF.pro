Include "d:\cernbox\Repositories\steam-fiqus-dev\fiqus\pro_material_functions\ironBHcurves.pro";



    stop_temperature = 300.0;
    /* -------------------------------------------------------------------------- */


Group {

      Air = Region[ 5 ];  // Air
      AirInf = Region[ 4 ];  // AirInf
      ht35_EM = Region[ 6 ];
      ht33_EM = Region[ 8 ];
      ht31_EM = Region[ 10 ];
      ht29_EM = Region[ 12 ];
      ht27_EM = Region[ 14 ];
      ht25_EM = Region[ 16 ];
      ht23_EM = Region[ 18 ];
      ht21_EM = Region[ 20 ];
      ht19_EM = Region[ 22 ];
      ht17_EM = Region[ 24 ];
      ht15_EM = Region[ 26 ];
      ht13_EM = Region[ 28 ];
      ht11_EM = Region[ 30 ];
      ht9_EM = Region[ 32 ];
      ht7_EM = Region[ 34 ];
      ht5_EM = Region[ 36 ];
      ht3_EM = Region[ 38 ];
      ht1_EM = Region[ 40 ];
      ht72_EM = Region[ 42 ];
      ht74_EM = Region[ 44 ];
      ht76_EM = Region[ 46 ];
      ht78_EM = Region[ 48 ];
      ht80_EM = Region[ 50 ];
      ht82_EM = Region[ 52 ];
      ht84_EM = Region[ 54 ];
      ht86_EM = Region[ 56 ];
      ht88_EM = Region[ 58 ];
      ht90_EM = Region[ 60 ];
      ht92_EM = Region[ 62 ];
      ht94_EM = Region[ 64 ];
      ht96_EM = Region[ 66 ];
      ht98_EM = Region[ 68 ];
      ht100_EM = Region[ 70 ];
      ht102_EM = Region[ 72 ];
      ht104_EM = Region[ 74 ];
      ht70_EM = Region[ 76 ];
      ht68_EM = Region[ 78 ];
      ht66_EM = Region[ 80 ];
      ht64_EM = Region[ 82 ];
      ht62_EM = Region[ 84 ];
      ht60_EM = Region[ 86 ];
      ht58_EM = Region[ 88 ];
      ht56_EM = Region[ 90 ];
      ht54_EM = Region[ 92 ];
      ht52_EM = Region[ 94 ];
      ht50_EM = Region[ 96 ];
      ht48_EM = Region[ 98 ];
      ht46_EM = Region[ 100 ];
      ht44_EM = Region[ 102 ];
      ht42_EM = Region[ 104 ];
      ht40_EM = Region[ 106 ];
      ht38_EM = Region[ 108 ];
      ht36_EM = Region[ 110 ];
      ht107_EM = Region[ 112 ];
      ht109_EM = Region[ 114 ];
      ht111_EM = Region[ 116 ];
      ht113_EM = Region[ 118 ];
      ht115_EM = Region[ 120 ];
      ht117_EM = Region[ 122 ];
      ht119_EM = Region[ 124 ];
      ht121_EM = Region[ 126 ];
      ht123_EM = Region[ 128 ];
      ht125_EM = Region[ 130 ];
      ht127_EM = Region[ 132 ];
      ht129_EM = Region[ 134 ];
      ht131_EM = Region[ 136 ];
      ht133_EM = Region[ 138 ];
      ht135_EM = Region[ 140 ];
      ht137_EM = Region[ 142 ];
      ht139_EM = Region[ 144 ];
      ht34_EM = Region[ 7 ];
      ht32_EM = Region[ 9 ];
      ht30_EM = Region[ 11 ];
      ht28_EM = Region[ 13 ];
      ht26_EM = Region[ 15 ];
      ht24_EM = Region[ 17 ];
      ht22_EM = Region[ 19 ];
      ht20_EM = Region[ 21 ];
      ht18_EM = Region[ 23 ];
      ht16_EM = Region[ 25 ];
      ht14_EM = Region[ 27 ];
      ht12_EM = Region[ 29 ];
      ht10_EM = Region[ 31 ];
      ht8_EM = Region[ 33 ];
      ht6_EM = Region[ 35 ];
      ht4_EM = Region[ 37 ];
      ht2_EM = Region[ 39 ];
      ht71_EM = Region[ 41 ];
      ht73_EM = Region[ 43 ];
      ht75_EM = Region[ 45 ];
      ht77_EM = Region[ 47 ];
      ht79_EM = Region[ 49 ];
      ht81_EM = Region[ 51 ];
      ht83_EM = Region[ 53 ];
      ht85_EM = Region[ 55 ];
      ht87_EM = Region[ 57 ];
      ht89_EM = Region[ 59 ];
      ht91_EM = Region[ 61 ];
      ht93_EM = Region[ 63 ];
      ht95_EM = Region[ 65 ];
      ht97_EM = Region[ 67 ];
      ht99_EM = Region[ 69 ];
      ht101_EM = Region[ 71 ];
      ht103_EM = Region[ 73 ];
      ht105_EM = Region[ 75 ];
      ht69_EM = Region[ 77 ];
      ht67_EM = Region[ 79 ];
      ht65_EM = Region[ 81 ];
      ht63_EM = Region[ 83 ];
      ht61_EM = Region[ 85 ];
      ht59_EM = Region[ 87 ];
      ht57_EM = Region[ 89 ];
      ht55_EM = Region[ 91 ];
      ht53_EM = Region[ 93 ];
      ht51_EM = Region[ 95 ];
      ht49_EM = Region[ 97 ];
      ht47_EM = Region[ 99 ];
      ht45_EM = Region[ 101 ];
      ht43_EM = Region[ 103 ];
      ht41_EM = Region[ 105 ];
      ht39_EM = Region[ 107 ];
      ht37_EM = Region[ 109 ];
      ht106_EM = Region[ 111 ];
      ht108_EM = Region[ 113 ];
      ht110_EM = Region[ 115 ];
      ht112_EM = Region[ 117 ];
      ht114_EM = Region[ 119 ];
      ht116_EM = Region[ 121 ];
      ht118_EM = Region[ 123 ];
      ht120_EM = Region[ 125 ];
      ht122_EM = Region[ 127 ];
      ht124_EM = Region[ 129 ];
      ht126_EM = Region[ 131 ];
      ht128_EM = Region[ 133 ];
      ht130_EM = Region[ 135 ];
      ht132_EM = Region[ 137 ];
      ht134_EM = Region[ 139 ];
      ht136_EM = Region[ 141 ];
      ht138_EM = Region[ 143 ];
      ht140_EM = Region[ 145 ];


        BHiron1 = Region[ 1 ];

      Surface_Inf = Region[ 3 ];

      Omega_p_EM = Region[ {
ht35_EM, ht33_EM, ht31_EM, ht29_EM, ht27_EM, ht25_EM, ht23_EM, ht21_EM, ht19_EM, ht17_EM, ht15_EM, ht13_EM, ht11_EM, ht9_EM, ht7_EM, ht5_EM, ht3_EM, ht1_EM, ht72_EM, ht74_EM, ht76_EM, ht78_EM, ht80_EM, ht82_EM, ht84_EM, ht86_EM, ht88_EM, ht90_EM, ht92_EM, ht94_EM, ht96_EM, ht98_EM, ht100_EM, ht102_EM, ht104_EM, ht70_EM, ht68_EM, ht66_EM, ht64_EM, ht62_EM, ht60_EM, ht58_EM, ht56_EM, ht54_EM, ht52_EM, ht50_EM, ht48_EM, ht46_EM, ht44_EM, ht42_EM, ht40_EM, ht38_EM, ht36_EM, ht107_EM, ht109_EM, ht111_EM, ht113_EM, ht115_EM, ht117_EM, ht119_EM, ht121_EM, ht123_EM, ht125_EM, ht127_EM, ht129_EM, ht131_EM, ht133_EM, ht135_EM, ht137_EM, ht139_EM, ht34_EM, ht32_EM, ht30_EM, ht28_EM, ht26_EM, ht24_EM, ht22_EM, ht20_EM, ht18_EM, ht16_EM, ht14_EM, ht12_EM, ht10_EM, ht8_EM, ht6_EM, ht4_EM, ht2_EM, ht71_EM, ht73_EM, ht75_EM, ht77_EM, ht79_EM, ht81_EM, ht83_EM, ht85_EM, ht87_EM, ht89_EM, ht91_EM, ht93_EM, ht95_EM, ht97_EM, ht99_EM, ht101_EM, ht103_EM, ht105_EM, ht69_EM, ht67_EM, ht65_EM, ht63_EM, ht61_EM, ht59_EM, ht57_EM, ht55_EM, ht53_EM, ht51_EM, ht49_EM, ht47_EM, ht45_EM, ht43_EM, ht41_EM, ht39_EM, ht37_EM, ht106_EM, ht108_EM, ht110_EM, ht112_EM, ht114_EM, ht116_EM, ht118_EM, ht120_EM, ht122_EM, ht124_EM, ht126_EM, ht128_EM, ht130_EM, ht132_EM, ht134_EM, ht136_EM, ht138_EM, ht140_EM} ];

      Omega_bh_EM = Region[ {BHiron1} ];

      Omega_i_EM = Region[ {
} ];

      Omega_aff_EM = Region[ AirInf ];
      Omega_a_EM = Region[ Air ];
      Omega_c_EM = Region[ {Omega_p_EM, Omega_bh_EM, Omega_i_EM} ];
      Omega_EM = Region[ {Air, AirInf, Omega_p_EM, Omega_bh_EM, Omega_i_EM} ];
      Bd_Omega = Region[ {Surface_Inf}];




    ht35_TH = Region[ 1000000 ];
    ht33_TH = Region[ 1000002 ];
    ht31_TH = Region[ 1000004 ];
    ht29_TH = Region[ 1000006 ];
    ht27_TH = Region[ 1000008 ];
    ht25_TH = Region[ 1000010 ];
    ht23_TH = Region[ 1000012 ];
    ht21_TH = Region[ 1000014 ];
    ht19_TH = Region[ 1000016 ];
    ht17_TH = Region[ 1000018 ];
    ht15_TH = Region[ 1000020 ];
    ht13_TH = Region[ 1000022 ];
    ht11_TH = Region[ 1000024 ];
    ht9_TH = Region[ 1000026 ];
    ht7_TH = Region[ 1000028 ];
    ht5_TH = Region[ 1000030 ];
    ht3_TH = Region[ 1000032 ];
    ht1_TH = Region[ 1000034 ];
    ht70_TH = Region[ 1000070 ];
    ht68_TH = Region[ 1000072 ];
    ht66_TH = Region[ 1000074 ];
    ht64_TH = Region[ 1000076 ];
    ht62_TH = Region[ 1000078 ];
    ht60_TH = Region[ 1000080 ];
    ht58_TH = Region[ 1000082 ];
    ht56_TH = Region[ 1000084 ];
    ht54_TH = Region[ 1000086 ];
    ht52_TH = Region[ 1000088 ];
    ht50_TH = Region[ 1000090 ];
    ht48_TH = Region[ 1000092 ];
    ht46_TH = Region[ 1000094 ];
    ht44_TH = Region[ 1000096 ];
    ht42_TH = Region[ 1000098 ];
    ht40_TH = Region[ 1000100 ];
    ht38_TH = Region[ 1000102 ];
    ht36_TH = Region[ 1000104 ];
    ht34_TH = Region[ 1000001 ];
    ht32_TH = Region[ 1000003 ];
    ht30_TH = Region[ 1000005 ];
    ht28_TH = Region[ 1000007 ];
    ht26_TH = Region[ 1000009 ];
    ht24_TH = Region[ 1000011 ];
    ht22_TH = Region[ 1000013 ];
    ht20_TH = Region[ 1000015 ];
    ht18_TH = Region[ 1000017 ];
    ht16_TH = Region[ 1000019 ];
    ht14_TH = Region[ 1000021 ];
    ht12_TH = Region[ 1000023 ];
    ht10_TH = Region[ 1000025 ];
    ht8_TH = Region[ 1000027 ];
    ht6_TH = Region[ 1000029 ];
    ht4_TH = Region[ 1000031 ];
    ht2_TH = Region[ 1000033 ];
    ht69_TH = Region[ 1000071 ];
    ht67_TH = Region[ 1000073 ];
    ht65_TH = Region[ 1000075 ];
    ht63_TH = Region[ 1000077 ];
    ht61_TH = Region[ 1000079 ];
    ht59_TH = Region[ 1000081 ];
    ht57_TH = Region[ 1000083 ];
    ht55_TH = Region[ 1000085 ];
    ht53_TH = Region[ 1000087 ];
    ht51_TH = Region[ 1000089 ];
    ht49_TH = Region[ 1000091 ];
    ht47_TH = Region[ 1000093 ];
    ht45_TH = Region[ 1000095 ];
    ht43_TH = Region[ 1000097 ];
    ht41_TH = Region[ 1000099 ];
    ht39_TH = Region[ 1000101 ];
    ht37_TH = Region[ 1000103 ];
    ht72_TH = Region[ 1000036 ];
    ht74_TH = Region[ 1000038 ];
    ht76_TH = Region[ 1000040 ];
    ht78_TH = Region[ 1000042 ];
    ht80_TH = Region[ 1000044 ];
    ht82_TH = Region[ 1000046 ];
    ht84_TH = Region[ 1000048 ];
    ht86_TH = Region[ 1000050 ];
    ht88_TH = Region[ 1000052 ];
    ht90_TH = Region[ 1000054 ];
    ht92_TH = Region[ 1000056 ];
    ht94_TH = Region[ 1000058 ];
    ht96_TH = Region[ 1000060 ];
    ht98_TH = Region[ 1000062 ];
    ht100_TH = Region[ 1000064 ];
    ht102_TH = Region[ 1000066 ];
    ht104_TH = Region[ 1000068 ];
    ht107_TH = Region[ 1000106 ];
    ht109_TH = Region[ 1000108 ];
    ht111_TH = Region[ 1000110 ];
    ht113_TH = Region[ 1000112 ];
    ht115_TH = Region[ 1000114 ];
    ht117_TH = Region[ 1000116 ];
    ht119_TH = Region[ 1000118 ];
    ht121_TH = Region[ 1000120 ];
    ht123_TH = Region[ 1000122 ];
    ht125_TH = Region[ 1000124 ];
    ht127_TH = Region[ 1000126 ];
    ht129_TH = Region[ 1000128 ];
    ht131_TH = Region[ 1000130 ];
    ht133_TH = Region[ 1000132 ];
    ht135_TH = Region[ 1000134 ];
    ht137_TH = Region[ 1000136 ];
    ht139_TH = Region[ 1000138 ];
    ht71_TH = Region[ 1000035 ];
    ht73_TH = Region[ 1000037 ];
    ht75_TH = Region[ 1000039 ];
    ht77_TH = Region[ 1000041 ];
    ht79_TH = Region[ 1000043 ];
    ht81_TH = Region[ 1000045 ];
    ht83_TH = Region[ 1000047 ];
    ht85_TH = Region[ 1000049 ];
    ht87_TH = Region[ 1000051 ];
    ht89_TH = Region[ 1000053 ];
    ht91_TH = Region[ 1000055 ];
    ht93_TH = Region[ 1000057 ];
    ht95_TH = Region[ 1000059 ];
    ht97_TH = Region[ 1000061 ];
    ht99_TH = Region[ 1000063 ];
    ht101_TH = Region[ 1000065 ];
    ht103_TH = Region[ 1000067 ];
    ht105_TH = Region[ 1000069 ];
    ht106_TH = Region[ 1000105 ];
    ht108_TH = Region[ 1000107 ];
    ht110_TH = Region[ 1000109 ];
    ht112_TH = Region[ 1000111 ];
    ht114_TH = Region[ 1000113 ];
    ht116_TH = Region[ 1000115 ];
    ht118_TH = Region[ 1000117 ];
    ht120_TH = Region[ 1000119 ];
    ht122_TH = Region[ 1000121 ];
    ht124_TH = Region[ 1000123 ];
    ht126_TH = Region[ 1000125 ];
    ht128_TH = Region[ 1000127 ];
    ht130_TH = Region[ 1000129 ];
    ht132_TH = Region[ 1000131 ];
    ht134_TH = Region[ 1000133 ];
    ht136_TH = Region[ 1000135 ];
    ht138_TH = Region[ 1000137 ];
    ht140_TH = Region[ 1000139 ];



      ins2 = Region[ 1000140 ];
      ins1 = Region[ 1000142 ];
      insext2 = Region[ 1000141 ];
      insext1 = Region[ 1000143 ];

    Omega_p_SMC11T150_TH = Region[ {
ht35_TH, ht33_TH, ht31_TH, ht29_TH, ht27_TH, ht25_TH, ht23_TH, ht21_TH, ht19_TH, ht17_TH, ht15_TH, ht13_TH, ht11_TH, ht9_TH, ht7_TH, ht5_TH, ht3_TH, ht1_TH, ht70_TH, ht68_TH, ht66_TH, ht64_TH, ht62_TH, ht60_TH, ht58_TH, ht56_TH, ht54_TH, ht52_TH, ht50_TH, ht48_TH, ht46_TH, ht44_TH, ht42_TH, ht40_TH, ht38_TH, ht36_TH, ht72_TH, ht74_TH, ht76_TH, ht78_TH, ht80_TH, ht82_TH, ht84_TH, ht86_TH, ht88_TH, ht90_TH, ht92_TH, ht94_TH, ht96_TH, ht98_TH, ht100_TH, ht102_TH, ht104_TH, ht107_TH, ht109_TH, ht111_TH, ht113_TH, ht115_TH, ht117_TH, ht119_TH, ht121_TH, ht123_TH, ht125_TH, ht127_TH, ht129_TH, ht131_TH, ht133_TH, ht135_TH, ht137_TH, ht139_TH, ht34_TH, ht32_TH, ht30_TH, ht28_TH, ht26_TH, ht24_TH, ht22_TH, ht20_TH, ht18_TH, ht16_TH, ht14_TH, ht12_TH, ht10_TH, ht8_TH, ht6_TH, ht4_TH, ht2_TH, ht69_TH, ht67_TH, ht65_TH, ht63_TH, ht61_TH, ht59_TH, ht57_TH, ht55_TH, ht53_TH, ht51_TH, ht49_TH, ht47_TH, ht45_TH, ht43_TH, ht41_TH, ht39_TH, ht37_TH, ht71_TH, ht73_TH, ht75_TH, ht77_TH, ht79_TH, ht81_TH, ht83_TH, ht85_TH, ht87_TH, ht89_TH, ht91_TH, ht93_TH, ht95_TH, ht97_TH, ht99_TH, ht101_TH, ht103_TH, ht105_TH, ht106_TH, ht108_TH, ht110_TH, ht112_TH, ht114_TH, ht116_TH, ht118_TH, ht120_TH, ht122_TH, ht124_TH, ht126_TH, ht128_TH, ht130_TH, ht132_TH, ht134_TH, ht136_TH, ht138_TH, ht140_TH} ];
    Omega_p_TH = Region[ {Omega_p_SMC11T150_TH} ];
    Omega_i_TH = Region[ {
} ];
  Omega_c_TH = Region[ {Omega_p_TH, Omega_i_TH} ];
    Omega_ins_TH = Region[ {ins2, ins1} ];
    Omega_TH = Region[ {Omega_p_TH, Omega_i_TH, Omega_ins_TH} ];

    jcZero_ht2 = Region[{ht2_TH}];
    jcZero_ht3 = Region[{ht3_TH}];
    jcZero_ht4 = Region[{ht4_TH}];
    jcZero_ht5 = Region[{ht5_TH}];
    jcZero_ht6 = Region[{ht6_TH}];
    jcZero_ht7 = Region[{ht7_TH}];
    jcZero_ht8 = Region[{ht8_TH}];
    jcZero_ht9 = Region[{ht9_TH}];
    jcZero_ht10 = Region[{ht10_TH}];
    jcZero_ht11 = Region[{ht11_TH}];
    jcZero_ht12 = Region[{ht12_TH}];
    jcZero_ht13 = Region[{ht13_TH}];
    jcZero_ht14 = Region[{ht14_TH}];
    jcZero_ht15 = Region[{ht15_TH}];
    jcZero_ht16 = Region[{ht16_TH}];
    jcZero_ht37 = Region[{ht37_TH}];
    jcZero_ht38 = Region[{ht38_TH}];
    jcZero_ht39 = Region[{ht39_TH}];
    jcZero_ht40 = Region[{ht40_TH}];
    jcZero_ht41 = Region[{ht41_TH}];
    jcZero_ht42 = Region[{ht42_TH}];
    jcZero_ht43 = Region[{ht43_TH}];
    jcZero_ht44 = Region[{ht44_TH}];
    jcZero_ht45 = Region[{ht45_TH}];
    jcZero_ht46 = Region[{ht46_TH}];
    jcZero_ht47 = Region[{ht47_TH}];
    jcZero_ht48 = Region[{ht48_TH}];
    jcZero_ht49 = Region[{ht49_TH}];
    jcZero_ht50 = Region[{ht50_TH}];
    jcZero_ht51 = Region[{ht51_TH}];
    jcZero_ht72 = Region[{ht72_TH}];
    jcZero_ht73 = Region[{ht73_TH}];
    jcZero_ht74 = Region[{ht74_TH}];
    jcZero_ht75 = Region[{ht75_TH}];
    jcZero_ht76 = Region[{ht76_TH}];
    jcZero_ht77 = Region[{ht77_TH}];
    jcZero_ht78 = Region[{ht78_TH}];
    jcZero_ht79 = Region[{ht79_TH}];
    jcZero_ht80 = Region[{ht80_TH}];
    jcZero_ht81 = Region[{ht81_TH}];
    jcZero_ht82 = Region[{ht82_TH}];
    jcZero_ht83 = Region[{ht83_TH}];
    jcZero_ht84 = Region[{ht84_TH}];
    jcZero_ht85 = Region[{ht85_TH}];
    jcZero_ht86 = Region[{ht86_TH}];
    jcZero_ht107 = Region[{ht107_TH}];
    jcZero_ht108 = Region[{ht108_TH}];
    jcZero_ht109 = Region[{ht109_TH}];
    jcZero_ht110 = Region[{ht110_TH}];
    jcZero_ht111 = Region[{ht111_TH}];
    jcZero_ht112 = Region[{ht112_TH}];
    jcZero_ht113 = Region[{ht113_TH}];
    jcZero_ht114 = Region[{ht114_TH}];
    jcZero_ht115 = Region[{ht115_TH}];
    jcZero_ht116 = Region[{ht116_TH}];
    jcZero_ht117 = Region[{ht117_TH}];
    jcZero_ht118 = Region[{ht118_TH}];
    jcZero_ht119 = Region[{ht119_TH}];
    jcZero_ht120 = Region[{ht120_TH}];
    jcZero_ht121 = Region[{ht121_TH}];
    jcZero_SMC11T150 = Region[{ht2_TH, ht3_TH, ht4_TH, ht5_TH, ht6_TH, ht7_TH, ht8_TH, ht9_TH, ht10_TH, ht11_TH, ht12_TH, ht13_TH, ht14_TH, ht15_TH, ht16_TH, ht37_TH, ht38_TH, ht39_TH, ht40_TH, ht41_TH, ht42_TH, ht43_TH, ht44_TH, ht45_TH, ht46_TH, ht47_TH, ht48_TH, ht49_TH, ht50_TH, ht51_TH, ht72_TH, ht73_TH, ht74_TH, ht75_TH, ht76_TH, ht77_TH, ht78_TH, ht79_TH, ht80_TH, ht81_TH, ht82_TH, ht83_TH, ht84_TH, ht85_TH, ht86_TH, ht107_TH, ht108_TH, ht109_TH, ht110_TH, ht111_TH, ht112_TH, ht113_TH, ht114_TH, ht115_TH, ht116_TH, ht117_TH, ht118_TH, ht119_TH, ht120_TH, ht121_TH}];
    jcNonZero_SMC11T150 = Region[Omega_p_SMC11T150_TH];
    jcNonZero_SMC11T150 -= Region[jcZero_SMC11T150];

 

    general_adiabatic = Region[ {insext2, insext1} ];


    Bnds_dirichlet = Region[ {} ];
    Bnds_neumann = Region[ {} ];
    Bnds_neumann += Region[ general_adiabatic ];

    Bnds_robin = Region[ {} ];

    Bnds_support = Region[ {Bnds_neumann, Bnds_robin} ];


    projection_points = Region[ 4000000 ];

}

Function {

      mu0 = 4.e-7 * Pi;
      nu [ Region[{Air, Omega_p_EM, AirInf, Omega_i_EM}] ]  = 1. / mu0;

        nu [ BHiron1 ]  = nuBHiron1[$1];
        dnuIronYoke [ BHiron1 ]  = dnuBHiron1[$1];

      js_fct[ ht35_EM ] = 14500.0/SurfaceArea[]{ 6 };
      js_fct[ ht33_EM ] = 14500.0/SurfaceArea[]{ 8 };
      js_fct[ ht31_EM ] = 14500.0/SurfaceArea[]{ 10 };
      js_fct[ ht29_EM ] = 14500.0/SurfaceArea[]{ 12 };
      js_fct[ ht27_EM ] = 14500.0/SurfaceArea[]{ 14 };
      js_fct[ ht25_EM ] = 14500.0/SurfaceArea[]{ 16 };
      js_fct[ ht23_EM ] = 14500.0/SurfaceArea[]{ 18 };
      js_fct[ ht21_EM ] = 14500.0/SurfaceArea[]{ 20 };
      js_fct[ ht19_EM ] = 14500.0/SurfaceArea[]{ 22 };
      js_fct[ ht17_EM ] = 14500.0/SurfaceArea[]{ 24 };
      js_fct[ ht15_EM ] = 14500.0/SurfaceArea[]{ 26 };
      js_fct[ ht13_EM ] = 14500.0/SurfaceArea[]{ 28 };
      js_fct[ ht11_EM ] = 14500.0/SurfaceArea[]{ 30 };
      js_fct[ ht9_EM ] = 14500.0/SurfaceArea[]{ 32 };
      js_fct[ ht7_EM ] = 14500.0/SurfaceArea[]{ 34 };
      js_fct[ ht5_EM ] = 14500.0/SurfaceArea[]{ 36 };
      js_fct[ ht3_EM ] = 14500.0/SurfaceArea[]{ 38 };
      js_fct[ ht1_EM ] = 14500.0/SurfaceArea[]{ 40 };
      js_fct[ ht72_EM ] = -14500.0/SurfaceArea[]{ 42 };
      js_fct[ ht74_EM ] = -14500.0/SurfaceArea[]{ 44 };
      js_fct[ ht76_EM ] = -14500.0/SurfaceArea[]{ 46 };
      js_fct[ ht78_EM ] = -14500.0/SurfaceArea[]{ 48 };
      js_fct[ ht80_EM ] = -14500.0/SurfaceArea[]{ 50 };
      js_fct[ ht82_EM ] = -14500.0/SurfaceArea[]{ 52 };
      js_fct[ ht84_EM ] = -14500.0/SurfaceArea[]{ 54 };
      js_fct[ ht86_EM ] = -14500.0/SurfaceArea[]{ 56 };
      js_fct[ ht88_EM ] = -14500.0/SurfaceArea[]{ 58 };
      js_fct[ ht90_EM ] = -14500.0/SurfaceArea[]{ 60 };
      js_fct[ ht92_EM ] = -14500.0/SurfaceArea[]{ 62 };
      js_fct[ ht94_EM ] = -14500.0/SurfaceArea[]{ 64 };
      js_fct[ ht96_EM ] = -14500.0/SurfaceArea[]{ 66 };
      js_fct[ ht98_EM ] = -14500.0/SurfaceArea[]{ 68 };
      js_fct[ ht100_EM ] = -14500.0/SurfaceArea[]{ 70 };
      js_fct[ ht102_EM ] = -14500.0/SurfaceArea[]{ 72 };
      js_fct[ ht104_EM ] = -14500.0/SurfaceArea[]{ 74 };
      js_fct[ ht70_EM ] = -14500.0/SurfaceArea[]{ 76 };
      js_fct[ ht68_EM ] = -14500.0/SurfaceArea[]{ 78 };
      js_fct[ ht66_EM ] = -14500.0/SurfaceArea[]{ 80 };
      js_fct[ ht64_EM ] = -14500.0/SurfaceArea[]{ 82 };
      js_fct[ ht62_EM ] = -14500.0/SurfaceArea[]{ 84 };
      js_fct[ ht60_EM ] = -14500.0/SurfaceArea[]{ 86 };
      js_fct[ ht58_EM ] = -14500.0/SurfaceArea[]{ 88 };
      js_fct[ ht56_EM ] = -14500.0/SurfaceArea[]{ 90 };
      js_fct[ ht54_EM ] = -14500.0/SurfaceArea[]{ 92 };
      js_fct[ ht52_EM ] = -14500.0/SurfaceArea[]{ 94 };
      js_fct[ ht50_EM ] = -14500.0/SurfaceArea[]{ 96 };
      js_fct[ ht48_EM ] = -14500.0/SurfaceArea[]{ 98 };
      js_fct[ ht46_EM ] = -14500.0/SurfaceArea[]{ 100 };
      js_fct[ ht44_EM ] = -14500.0/SurfaceArea[]{ 102 };
      js_fct[ ht42_EM ] = -14500.0/SurfaceArea[]{ 104 };
      js_fct[ ht40_EM ] = -14500.0/SurfaceArea[]{ 106 };
      js_fct[ ht38_EM ] = -14500.0/SurfaceArea[]{ 108 };
      js_fct[ ht36_EM ] = -14500.0/SurfaceArea[]{ 110 };
      js_fct[ ht107_EM ] = 14500.0/SurfaceArea[]{ 112 };
      js_fct[ ht109_EM ] = 14500.0/SurfaceArea[]{ 114 };
      js_fct[ ht111_EM ] = 14500.0/SurfaceArea[]{ 116 };
      js_fct[ ht113_EM ] = 14500.0/SurfaceArea[]{ 118 };
      js_fct[ ht115_EM ] = 14500.0/SurfaceArea[]{ 120 };
      js_fct[ ht117_EM ] = 14500.0/SurfaceArea[]{ 122 };
      js_fct[ ht119_EM ] = 14500.0/SurfaceArea[]{ 124 };
      js_fct[ ht121_EM ] = 14500.0/SurfaceArea[]{ 126 };
      js_fct[ ht123_EM ] = 14500.0/SurfaceArea[]{ 128 };
      js_fct[ ht125_EM ] = 14500.0/SurfaceArea[]{ 130 };
      js_fct[ ht127_EM ] = 14500.0/SurfaceArea[]{ 132 };
      js_fct[ ht129_EM ] = 14500.0/SurfaceArea[]{ 134 };
      js_fct[ ht131_EM ] = 14500.0/SurfaceArea[]{ 136 };
      js_fct[ ht133_EM ] = 14500.0/SurfaceArea[]{ 138 };
      js_fct[ ht135_EM ] = 14500.0/SurfaceArea[]{ 140 };
      js_fct[ ht137_EM ] = 14500.0/SurfaceArea[]{ 142 };
      js_fct[ ht139_EM ] = 14500.0/SurfaceArea[]{ 144 };
      js_fct[ ht34_EM ] = 14500.0/SurfaceArea[]{ 7 };
      js_fct[ ht32_EM ] = 14500.0/SurfaceArea[]{ 9 };
      js_fct[ ht30_EM ] = 14500.0/SurfaceArea[]{ 11 };
      js_fct[ ht28_EM ] = 14500.0/SurfaceArea[]{ 13 };
      js_fct[ ht26_EM ] = 14500.0/SurfaceArea[]{ 15 };
      js_fct[ ht24_EM ] = 14500.0/SurfaceArea[]{ 17 };
      js_fct[ ht22_EM ] = 14500.0/SurfaceArea[]{ 19 };
      js_fct[ ht20_EM ] = 14500.0/SurfaceArea[]{ 21 };
      js_fct[ ht18_EM ] = 14500.0/SurfaceArea[]{ 23 };
      js_fct[ ht16_EM ] = 14500.0/SurfaceArea[]{ 25 };
      js_fct[ ht14_EM ] = 14500.0/SurfaceArea[]{ 27 };
      js_fct[ ht12_EM ] = 14500.0/SurfaceArea[]{ 29 };
      js_fct[ ht10_EM ] = 14500.0/SurfaceArea[]{ 31 };
      js_fct[ ht8_EM ] = 14500.0/SurfaceArea[]{ 33 };
      js_fct[ ht6_EM ] = 14500.0/SurfaceArea[]{ 35 };
      js_fct[ ht4_EM ] = 14500.0/SurfaceArea[]{ 37 };
      js_fct[ ht2_EM ] = 14500.0/SurfaceArea[]{ 39 };
      js_fct[ ht71_EM ] = -14500.0/SurfaceArea[]{ 41 };
      js_fct[ ht73_EM ] = -14500.0/SurfaceArea[]{ 43 };
      js_fct[ ht75_EM ] = -14500.0/SurfaceArea[]{ 45 };
      js_fct[ ht77_EM ] = -14500.0/SurfaceArea[]{ 47 };
      js_fct[ ht79_EM ] = -14500.0/SurfaceArea[]{ 49 };
      js_fct[ ht81_EM ] = -14500.0/SurfaceArea[]{ 51 };
      js_fct[ ht83_EM ] = -14500.0/SurfaceArea[]{ 53 };
      js_fct[ ht85_EM ] = -14500.0/SurfaceArea[]{ 55 };
      js_fct[ ht87_EM ] = -14500.0/SurfaceArea[]{ 57 };
      js_fct[ ht89_EM ] = -14500.0/SurfaceArea[]{ 59 };
      js_fct[ ht91_EM ] = -14500.0/SurfaceArea[]{ 61 };
      js_fct[ ht93_EM ] = -14500.0/SurfaceArea[]{ 63 };
      js_fct[ ht95_EM ] = -14500.0/SurfaceArea[]{ 65 };
      js_fct[ ht97_EM ] = -14500.0/SurfaceArea[]{ 67 };
      js_fct[ ht99_EM ] = -14500.0/SurfaceArea[]{ 69 };
      js_fct[ ht101_EM ] = -14500.0/SurfaceArea[]{ 71 };
      js_fct[ ht103_EM ] = -14500.0/SurfaceArea[]{ 73 };
      js_fct[ ht105_EM ] = -14500.0/SurfaceArea[]{ 75 };
      js_fct[ ht69_EM ] = -14500.0/SurfaceArea[]{ 77 };
      js_fct[ ht67_EM ] = -14500.0/SurfaceArea[]{ 79 };
      js_fct[ ht65_EM ] = -14500.0/SurfaceArea[]{ 81 };
      js_fct[ ht63_EM ] = -14500.0/SurfaceArea[]{ 83 };
      js_fct[ ht61_EM ] = -14500.0/SurfaceArea[]{ 85 };
      js_fct[ ht59_EM ] = -14500.0/SurfaceArea[]{ 87 };
      js_fct[ ht57_EM ] = -14500.0/SurfaceArea[]{ 89 };
      js_fct[ ht55_EM ] = -14500.0/SurfaceArea[]{ 91 };
      js_fct[ ht53_EM ] = -14500.0/SurfaceArea[]{ 93 };
      js_fct[ ht51_EM ] = -14500.0/SurfaceArea[]{ 95 };
      js_fct[ ht49_EM ] = -14500.0/SurfaceArea[]{ 97 };
      js_fct[ ht47_EM ] = -14500.0/SurfaceArea[]{ 99 };
      js_fct[ ht45_EM ] = -14500.0/SurfaceArea[]{ 101 };
      js_fct[ ht43_EM ] = -14500.0/SurfaceArea[]{ 103 };
      js_fct[ ht41_EM ] = -14500.0/SurfaceArea[]{ 105 };
      js_fct[ ht39_EM ] = -14500.0/SurfaceArea[]{ 107 };
      js_fct[ ht37_EM ] = -14500.0/SurfaceArea[]{ 109 };
      js_fct[ ht106_EM ] = 14500.0/SurfaceArea[]{ 111 };
      js_fct[ ht108_EM ] = 14500.0/SurfaceArea[]{ 113 };
      js_fct[ ht110_EM ] = 14500.0/SurfaceArea[]{ 115 };
      js_fct[ ht112_EM ] = 14500.0/SurfaceArea[]{ 117 };
      js_fct[ ht114_EM ] = 14500.0/SurfaceArea[]{ 119 };
      js_fct[ ht116_EM ] = 14500.0/SurfaceArea[]{ 121 };
      js_fct[ ht118_EM ] = 14500.0/SurfaceArea[]{ 123 };
      js_fct[ ht120_EM ] = 14500.0/SurfaceArea[]{ 125 };
      js_fct[ ht122_EM ] = 14500.0/SurfaceArea[]{ 127 };
      js_fct[ ht124_EM ] = 14500.0/SurfaceArea[]{ 129 };
      js_fct[ ht126_EM ] = 14500.0/SurfaceArea[]{ 131 };
      js_fct[ ht128_EM ] = 14500.0/SurfaceArea[]{ 133 };
      js_fct[ ht130_EM ] = 14500.0/SurfaceArea[]{ 135 };
      js_fct[ ht132_EM ] = 14500.0/SurfaceArea[]{ 137 };
      js_fct[ ht134_EM ] = 14500.0/SurfaceArea[]{ 139 };
      js_fct[ ht136_EM ] = 14500.0/SurfaceArea[]{ 141 };
      js_fct[ ht138_EM ] = 14500.0/SurfaceArea[]{ 143 };
      js_fct[ ht140_EM ] = 14500.0/SurfaceArea[]{ 145 };



  area_fct[ ht35_TH ] = SurfaceArea[]{ 1000000 };
  area_fct[ ht33_TH ] = SurfaceArea[]{ 1000002 };
  area_fct[ ht31_TH ] = SurfaceArea[]{ 1000004 };
  area_fct[ ht29_TH ] = SurfaceArea[]{ 1000006 };
  area_fct[ ht27_TH ] = SurfaceArea[]{ 1000008 };
  area_fct[ ht25_TH ] = SurfaceArea[]{ 1000010 };
  area_fct[ ht23_TH ] = SurfaceArea[]{ 1000012 };
  area_fct[ ht21_TH ] = SurfaceArea[]{ 1000014 };
  area_fct[ ht19_TH ] = SurfaceArea[]{ 1000016 };
  area_fct[ ht17_TH ] = SurfaceArea[]{ 1000018 };
  area_fct[ ht15_TH ] = SurfaceArea[]{ 1000020 };
  area_fct[ ht13_TH ] = SurfaceArea[]{ 1000022 };
  area_fct[ ht11_TH ] = SurfaceArea[]{ 1000024 };
  area_fct[ ht9_TH ] = SurfaceArea[]{ 1000026 };
  area_fct[ ht7_TH ] = SurfaceArea[]{ 1000028 };
  area_fct[ ht5_TH ] = SurfaceArea[]{ 1000030 };
  area_fct[ ht3_TH ] = SurfaceArea[]{ 1000032 };
  area_fct[ ht1_TH ] = SurfaceArea[]{ 1000034 };
  area_fct[ ht70_TH ] = SurfaceArea[]{ 1000070 };
  area_fct[ ht68_TH ] = SurfaceArea[]{ 1000072 };
  area_fct[ ht66_TH ] = SurfaceArea[]{ 1000074 };
  area_fct[ ht64_TH ] = SurfaceArea[]{ 1000076 };
  area_fct[ ht62_TH ] = SurfaceArea[]{ 1000078 };
  area_fct[ ht60_TH ] = SurfaceArea[]{ 1000080 };
  area_fct[ ht58_TH ] = SurfaceArea[]{ 1000082 };
  area_fct[ ht56_TH ] = SurfaceArea[]{ 1000084 };
  area_fct[ ht54_TH ] = SurfaceArea[]{ 1000086 };
  area_fct[ ht52_TH ] = SurfaceArea[]{ 1000088 };
  area_fct[ ht50_TH ] = SurfaceArea[]{ 1000090 };
  area_fct[ ht48_TH ] = SurfaceArea[]{ 1000092 };
  area_fct[ ht46_TH ] = SurfaceArea[]{ 1000094 };
  area_fct[ ht44_TH ] = SurfaceArea[]{ 1000096 };
  area_fct[ ht42_TH ] = SurfaceArea[]{ 1000098 };
  area_fct[ ht40_TH ] = SurfaceArea[]{ 1000100 };
  area_fct[ ht38_TH ] = SurfaceArea[]{ 1000102 };
  area_fct[ ht36_TH ] = SurfaceArea[]{ 1000104 };
  area_fct[ ht72_TH ] = SurfaceArea[]{ 1000036 };
  area_fct[ ht74_TH ] = SurfaceArea[]{ 1000038 };
  area_fct[ ht76_TH ] = SurfaceArea[]{ 1000040 };
  area_fct[ ht78_TH ] = SurfaceArea[]{ 1000042 };
  area_fct[ ht80_TH ] = SurfaceArea[]{ 1000044 };
  area_fct[ ht82_TH ] = SurfaceArea[]{ 1000046 };
  area_fct[ ht84_TH ] = SurfaceArea[]{ 1000048 };
  area_fct[ ht86_TH ] = SurfaceArea[]{ 1000050 };
  area_fct[ ht88_TH ] = SurfaceArea[]{ 1000052 };
  area_fct[ ht90_TH ] = SurfaceArea[]{ 1000054 };
  area_fct[ ht92_TH ] = SurfaceArea[]{ 1000056 };
  area_fct[ ht94_TH ] = SurfaceArea[]{ 1000058 };
  area_fct[ ht96_TH ] = SurfaceArea[]{ 1000060 };
  area_fct[ ht98_TH ] = SurfaceArea[]{ 1000062 };
  area_fct[ ht100_TH ] = SurfaceArea[]{ 1000064 };
  area_fct[ ht102_TH ] = SurfaceArea[]{ 1000066 };
  area_fct[ ht104_TH ] = SurfaceArea[]{ 1000068 };
  area_fct[ ht107_TH ] = SurfaceArea[]{ 1000106 };
  area_fct[ ht109_TH ] = SurfaceArea[]{ 1000108 };
  area_fct[ ht111_TH ] = SurfaceArea[]{ 1000110 };
  area_fct[ ht113_TH ] = SurfaceArea[]{ 1000112 };
  area_fct[ ht115_TH ] = SurfaceArea[]{ 1000114 };
  area_fct[ ht117_TH ] = SurfaceArea[]{ 1000116 };
  area_fct[ ht119_TH ] = SurfaceArea[]{ 1000118 };
  area_fct[ ht121_TH ] = SurfaceArea[]{ 1000120 };
  area_fct[ ht123_TH ] = SurfaceArea[]{ 1000122 };
  area_fct[ ht125_TH ] = SurfaceArea[]{ 1000124 };
  area_fct[ ht127_TH ] = SurfaceArea[]{ 1000126 };
  area_fct[ ht129_TH ] = SurfaceArea[]{ 1000128 };
  area_fct[ ht131_TH ] = SurfaceArea[]{ 1000130 };
  area_fct[ ht133_TH ] = SurfaceArea[]{ 1000132 };
  area_fct[ ht135_TH ] = SurfaceArea[]{ 1000134 };
  area_fct[ ht137_TH ] = SurfaceArea[]{ 1000136 };
  area_fct[ ht139_TH ] = SurfaceArea[]{ 1000138 };
  area_fct[ ht34_TH ] = SurfaceArea[]{ 1000001 };
  area_fct[ ht32_TH ] = SurfaceArea[]{ 1000003 };
  area_fct[ ht30_TH ] = SurfaceArea[]{ 1000005 };
  area_fct[ ht28_TH ] = SurfaceArea[]{ 1000007 };
  area_fct[ ht26_TH ] = SurfaceArea[]{ 1000009 };
  area_fct[ ht24_TH ] = SurfaceArea[]{ 1000011 };
  area_fct[ ht22_TH ] = SurfaceArea[]{ 1000013 };
  area_fct[ ht20_TH ] = SurfaceArea[]{ 1000015 };
  area_fct[ ht18_TH ] = SurfaceArea[]{ 1000017 };
  area_fct[ ht16_TH ] = SurfaceArea[]{ 1000019 };
  area_fct[ ht14_TH ] = SurfaceArea[]{ 1000021 };
  area_fct[ ht12_TH ] = SurfaceArea[]{ 1000023 };
  area_fct[ ht10_TH ] = SurfaceArea[]{ 1000025 };
  area_fct[ ht8_TH ] = SurfaceArea[]{ 1000027 };
  area_fct[ ht6_TH ] = SurfaceArea[]{ 1000029 };
  area_fct[ ht4_TH ] = SurfaceArea[]{ 1000031 };
  area_fct[ ht2_TH ] = SurfaceArea[]{ 1000033 };
  area_fct[ ht69_TH ] = SurfaceArea[]{ 1000071 };
  area_fct[ ht67_TH ] = SurfaceArea[]{ 1000073 };
  area_fct[ ht65_TH ] = SurfaceArea[]{ 1000075 };
  area_fct[ ht63_TH ] = SurfaceArea[]{ 1000077 };
  area_fct[ ht61_TH ] = SurfaceArea[]{ 1000079 };
  area_fct[ ht59_TH ] = SurfaceArea[]{ 1000081 };
  area_fct[ ht57_TH ] = SurfaceArea[]{ 1000083 };
  area_fct[ ht55_TH ] = SurfaceArea[]{ 1000085 };
  area_fct[ ht53_TH ] = SurfaceArea[]{ 1000087 };
  area_fct[ ht51_TH ] = SurfaceArea[]{ 1000089 };
  area_fct[ ht49_TH ] = SurfaceArea[]{ 1000091 };
  area_fct[ ht47_TH ] = SurfaceArea[]{ 1000093 };
  area_fct[ ht45_TH ] = SurfaceArea[]{ 1000095 };
  area_fct[ ht43_TH ] = SurfaceArea[]{ 1000097 };
  area_fct[ ht41_TH ] = SurfaceArea[]{ 1000099 };
  area_fct[ ht39_TH ] = SurfaceArea[]{ 1000101 };
  area_fct[ ht37_TH ] = SurfaceArea[]{ 1000103 };
  area_fct[ ht71_TH ] = SurfaceArea[]{ 1000035 };
  area_fct[ ht73_TH ] = SurfaceArea[]{ 1000037 };
  area_fct[ ht75_TH ] = SurfaceArea[]{ 1000039 };
  area_fct[ ht77_TH ] = SurfaceArea[]{ 1000041 };
  area_fct[ ht79_TH ] = SurfaceArea[]{ 1000043 };
  area_fct[ ht81_TH ] = SurfaceArea[]{ 1000045 };
  area_fct[ ht83_TH ] = SurfaceArea[]{ 1000047 };
  area_fct[ ht85_TH ] = SurfaceArea[]{ 1000049 };
  area_fct[ ht87_TH ] = SurfaceArea[]{ 1000051 };
  area_fct[ ht89_TH ] = SurfaceArea[]{ 1000053 };
  area_fct[ ht91_TH ] = SurfaceArea[]{ 1000055 };
  area_fct[ ht93_TH ] = SurfaceArea[]{ 1000057 };
  area_fct[ ht95_TH ] = SurfaceArea[]{ 1000059 };
  area_fct[ ht97_TH ] = SurfaceArea[]{ 1000061 };
  area_fct[ ht99_TH ] = SurfaceArea[]{ 1000063 };
  area_fct[ ht101_TH ] = SurfaceArea[]{ 1000065 };
  area_fct[ ht103_TH ] = SurfaceArea[]{ 1000067 };
  area_fct[ ht105_TH ] = SurfaceArea[]{ 1000069 };
  area_fct[ ht106_TH ] = SurfaceArea[]{ 1000105 };
  area_fct[ ht108_TH ] = SurfaceArea[]{ 1000107 };
  area_fct[ ht110_TH ] = SurfaceArea[]{ 1000109 };
  area_fct[ ht112_TH ] = SurfaceArea[]{ 1000111 };
  area_fct[ ht114_TH ] = SurfaceArea[]{ 1000113 };
  area_fct[ ht116_TH ] = SurfaceArea[]{ 1000115 };
  area_fct[ ht118_TH ] = SurfaceArea[]{ 1000117 };
  area_fct[ ht120_TH ] = SurfaceArea[]{ 1000119 };
  area_fct[ ht122_TH ] = SurfaceArea[]{ 1000121 };
  area_fct[ ht124_TH ] = SurfaceArea[]{ 1000123 };
  area_fct[ ht126_TH ] = SurfaceArea[]{ 1000125 };
  area_fct[ ht128_TH ] = SurfaceArea[]{ 1000127 };
  area_fct[ ht130_TH ] = SurfaceArea[]{ 1000129 };
  area_fct[ ht132_TH ] = SurfaceArea[]{ 1000131 };
  area_fct[ ht134_TH ] = SurfaceArea[]{ 1000133 };
  area_fct[ ht136_TH ] = SurfaceArea[]{ 1000135 };
  area_fct[ ht138_TH ] = SurfaceArea[]{ 1000137 };
  area_fct[ ht140_TH ] = SurfaceArea[]{ 1000139 };


      // time steps adaptive time stepping must hit
      Breakpoints = {};


      // --------------- MATERIAL FUNCTIONS ----------------------------------------



                                               


        f_inner_voids_SMC11T150 = 0.09761003996936089;
        f_outer_voids_SMC11T150 = 0.10788478101876732;
        f_strand_SMC11T150 = 1.0 - 0.2054948209881282;

      f_stabilizer_SMC11T150 = f_strand_SMC11T150 * 1.106 / (1. + 1.106);
      f_sc_SMC11T150 = f_strand_SMC11T150 * (1.0 - 1.106 / (1. + 1.106));

      source_current = 14500.0;

        criticalCurrentDensity[jcNonZero_SMC11T150] = $Time > 1000000.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht2] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht3] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht4] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht5] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht6] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht7] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht8] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht9] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht10] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht11] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht12] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht13] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht14] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht15] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht16] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht37] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht38] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht39] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht40] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht41] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht42] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht43] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht44] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht45] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht46] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht47] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht48] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht49] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht50] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht51] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht72] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht73] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht74] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht75] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht76] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht77] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht78] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht79] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht80] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht81] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht82] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht83] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht84] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht85] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht86] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht107] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht108] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht109] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht110] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht111] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht112] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht113] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht114] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht115] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht116] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht117] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht118] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht119] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht120] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


        criticalCurrentDensity[jcZero_ht121] = $Time > 0.0? 0: CFUN_Jc_Nb3Sn_Summers_T_B[$1, Norm[$2]]{31500000000.0, 18.0, 29.0} * f_sc_SMC11T150;


      rho[Omega_p_SMC11T150_TH] = EffectiveResistivity[CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{f_stabilizer_SMC11T150};

      // effective thermal conductivity of the bare part
      kappa[Omega_p_SMC11T150_TH] = RuleOfMixtures[
        CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0}
      , CFUN_kG10_T[$1]
      , CFUN_kG10_T[$1]
    ]
    {f_stabilizer_SMC11T150
      , f_inner_voids_SMC11T150
      ,  f_outer_voids_SMC11T150
    };

      // heat capacity of bare part
      heatCap[Omega_p_SMC11T150_TH] = RuleOfMixtures[
        CFUN_CvCu_T[$1],
        CFUN_CvNb3Sn_T_B[$1, Norm[$2]],
        CFUN_CvG10_T[$1],
        CFUN_CvG10_T[$1]
      ]
      {
        f_stabilizer_SMC11T150, 
        f_sc_SMC11T150,
        f_inner_voids_SMC11T150, 
        f_outer_voids_SMC11T150
      };

      // joule losses of bare part
      jouleLosses[] = CFUN_quenchState_Ic[criticalCurrentDensity[$1, $2] * area_fct[]]{source_current} * rho[$1, $2] * SquNorm[source_current/area_fct[]];

      // thermal conductivity of the wedges
      kappa[Omega_i_TH] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0};

      // heat capacity of wedges
      heatCap[Omega_i_TH] = CFUN_CvCu_T[$1];


      // thermal conductivity of the insulation
      kappa[Omega_ins_TH] = CFUN_kG10_T[$1];

      // heat capacity of insulation
      heatCap[ Omega_ins_TH ] = CFUN_CvG10_T[$1];


}




Constraint {
  { Name Dirichlet_a_Mag;
    Case {
      { Region Bd_Omega ; Value 0.; }
    }
  }
  { Name SourceCurrentDensityZ;
    Case {
      { Region Omega_p_EM ; Value js_fct[]; }
    }
  }

  { Name initTemp ;
    Case {
        { Region Omega_TH ; Value 1.9 ; Type Init; } // init. condition
    }
  }
  { Name Dirichlet_a_projection;
    Case {
      { Region projection_points ; Value 0; Type Assign; }
    }
  }
}

FunctionSpace {
  { Name Hcurl_a_Mag_2D; Type Form1P; // Magnetic vector potential a
    BasisFunction {
      { Name se; NameOfCoef ae; Function BF_PerpendicularEdge;
        Support Omega_EM ; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef ae; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_Mag; }
    }
  }

  { Name Hregion_j_Mag_2D; Type Vector; // Electric current density js
    BasisFunction {
      { Name sr; NameOfCoef jsr; Function BF_RegionZ;
        Support Omega_p_EM; Entity Omega_p_EM; }
    }
    Constraint {
      { NameOfCoef jsr; EntityType Region;
        NameOfConstraint SourceCurrentDensityZ; }
    }
  }

  { Name H_curl_a_artificial_dof; Type Form1P;  
    BasisFunction {
      { Name se_after_projection; NameOfCoef ae_after_projection; Function BF_PerpendicularEdge;
        Support Omega_TH ; Entity NodesOf[ All ]; }
    }
    // not needed since boundary is not part of Omega_TH
    Constraint {
      { NameOfCoef ae_after_projection; EntityType NodesOf;
        NameOfConstraint Dirichlet_a_projection; }     
    }
  }                                               

  { Name Hgrad_T; Type Form0;
    BasisFunction {
      { Name un;  NameOfCoef ui;  Function BF_Node;
          Support Region[{Omega_TH, Bnds_support}] ; Entity NodesOf[All];
      }

    }


    Constraint {
      { NameOfCoef ui; EntityType NodesOf; NameOfConstraint initTemp; }
      // do not constraint second order basis function as it's already covered by ui
    }
  }

}

Jacobian {
  { Name Jac_Vol_EM ;
    Case {
      { Region Omega_aff_EM ;
        Jacobian VolSphShell {0.625, 0.8} ; }
      { Region All ; Jacobian Vol ; }
    }
  }

  { Name Jac_Vol_TH ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Jac_Sur_TH ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int_EM ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point ; NumberOfPoints 1 ; }
          { GeoElement Line ; NumberOfPoints 2 ; }
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_line_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Line ; NumberOfPoints 2 ; }
        }
      }
    }
  }

  { Name Int_conducting_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }

  { Name Int_insulating_TH ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
        }
      }
    }
  }
}

Formulation {
  { Name Magnetostatics_a_2D; Type FemEquation;
    Quantity {
      { Name a ; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      { Name js; Type Local; NameOfSpace Hregion_j_Mag_2D; }
    }
    Equation {
      Integral { [ nu[{d a}] * Dof{d a} , {d a} ];
        In Omega_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }

      Integral { JacNL[ dnuIronYoke[{d a}] * Dof{d a} , {d a} ];
        In Omega_bh_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }

      Integral { [ -Dof{js} , {a} ];
        In Omega_p_EM; Jacobian Jac_Vol_EM; Integration Int_EM; }
    }
  }

  // Dummy formulation just to save the values of the norm of B from the EM mesh on the Gaussian points of
  // the thermal mesh. Alternatively, a Galerkin projection could be used.
  { Name Projection_EM_to_TH; Type FemEquation;
    Quantity {
      {Name a_before_projection; Type Local; NameOfSpace Hcurl_a_Mag_2D; }
      {Name a_artificial_dof; Type Local; NameOfSpace H_curl_a_artificial_dof; }
    }
    Equation {
      Integral { [ - SetVariable[Norm[{d a_before_projection}], ElementNum[], QuadraturePointIndex[]]{$Bnorm}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }

        Integral { [ Dof{d a_artificial_dof}, {d a_artificial_dof} ];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH; }
    }
  }                                              

  { Name Thermal_T;   Type FemEquation;
    Quantity {
      // cont temperature
      { Name T; Type Local; NameOfSpace Hgrad_T; }
    }

    Equation {
      Integral { [ kappa[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{d T} , {d T} ] ;
        In Region[ {Omega_p_TH, Omega_i_TH } ]; Integration Int_conducting_TH ; Jacobian Jac_Vol_TH ; }

      Integral { DtDof[ heatCap[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{T}, {T} ];
        In Region[ {Omega_p_TH, Omega_i_TH } ]; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }

      Integral { [ kappa[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{d T} , {d T} ] ;
        In Omega_ins_TH; Integration Int_insulating_TH ; Jacobian Jac_Vol_TH ; }

      Integral { DtDof[ heatCap[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] * Dof{T}, {T} ];
        In Omega_ins_TH; Integration Int_insulating_TH; Jacobian Jac_Vol_TH;  }

    // TODO: implement derivatives, missing copper for example
    /*   Integral { JacNL[ dkappadT[{T}, {d a}] * {d T} * Dof{T} , {d T} ] ;
         In Omega_TH; Integration Int<> ; Jacobian Jac_Vol_TH ; } */

      Integral { [ - jouleLosses[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}], {T}];
        In Omega_p_TH; Integration Int_conducting_TH; Jacobian Jac_Vol_TH;  }

 
        // Neumann
          Integral { [- 0.0 , {T} ] ;
            In  general_adiabatic ; Integration Int_line_TH ; Jacobian Jac_Sur_TH ; }

        // Robin
        // n * kappa grad (T) = h (T - Tinf) becomes two terms since GetDP can only
        // handle linear and not affine terms
        // NOTE: signs might be switched
    }
  }
}

Resolution {
  { Name resolution;
    System {
      { Name Sys_Mag; NameOfFormulation Magnetostatics_a_2D; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_SMC_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_SMC_REF_EM.msh"; }
      { Name Sys_The; NameOfFormulation Thermal_T; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_SMC_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_SMC_REF_TH.msh"; }
      { Name sys_Mag_projection; NameOfFormulation Projection_EM_to_TH; NameOfMesh "d:\cernbox\Repositories\steam-fiqus-dev\tests\_outputs\TEST_MULTIPOLE_SMC_REF_solve_only\Geometry_1\Mesh_1\TEST_MULTIPOLE_SMC_REF_TH.msh";}
    }
    Operation {
      InitSolution[Sys_Mag];
      IterativeLoopN[20, 0.9,
        System { { Sys_Mag, 1e-06, 0.0001, Solution LinfNorm } }
      ] { GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
      PostOperation[Map_a];

      Generate[sys_Mag_projection]; Solve[sys_Mag_projection];
      SaveSolution[sys_Mag_projection]; //PostOperation[b_after_projection_pos];

      SetExtrapolationOrder[0];
      InitSolution Sys_The; // init. the solution using init. constraints

      //PostOperation[b_thermal];

      CreateDirectory["T_avg"];
      PostOperation[T_avg];

      PostOperation[Map_T];

      // initialized cumulate times to zero to avoid warning
      Evaluate[$tg_cumul_cpu = 0, $ts_cumul_cpu = 0, $tg_cumul_wall = 0, $ts_cumul_wall = 0];
      Print["timestep,gen_wall,gen_cpu,sol_wall,sol_cpu,pos_wall,pos_cpu,gen_wall_cumul,gen_cpu_cumul,sol_wall_cumul,sol_cpu_cumul,pos_wall_cumul,pos_cpu_cumul", File "computation_times.csv"];
      //PostOperation[b_after_projection_pos];
      

      Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

      TimeLoopAdaptive
      [ 0.0, 2e-05, 1e-10, 1e-12, 0.1, "Euler", List[Breakpoints],
      System { { Sys_The, 0.001, 0.01, LinfNorm } } ]
      {
        IterativeLoopN[20, 0.7,
          System { { Sys_The, 0.001, 0.01, Solution LinfNorm } }]
        {
          Evaluate[ $tg1_wall = GetWallClockTime[], $tg1_cpu = GetCpuTime[] ];
          GenerateJac Sys_The ;
          Evaluate[ $tg2_wall = GetWallClockTime[], $tg2_cpu = GetCpuTime[] ];

          // add to generation times of previous rejected time steps
          Evaluate[ $tg_wall = $tg_wall + $tg2_wall - $tg1_wall, $tg_cpu = $tg_cpu + $tg2_cpu - $tg1_cpu ];

          Evaluate[ $ts1_wall = GetWallClockTime[], $ts1_cpu = GetCpuTime[] ];
          SolveJac Sys_The;
          Evaluate[ $ts2_wall = GetWallClockTime[], $ts2_cpu = GetCpuTime[] ];

          // add to solution times of previous rejected time steps
          Evaluate[ $ts_wall = $ts_wall + $ts2_wall - $ts1_wall, $ts_cpu = $ts_cpu + $ts2_cpu - $ts1_cpu ];

        }
      }
      {
        // save solution to .res file
        SaveSolution[Sys_The];

        Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];
        // print average temperature
        PostOperation[T_avg];

        // print temperature map
        PostOperation[Map_T];

        PostOperation[PrintMaxTemp]; // save maximum temperature in register 1
        Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

        Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

        // cumulated times
        Evaluate[ $tg_cumul_wall = $tg_cumul_wall + $tg_wall, $tg_cumul_cpu = $tg_cumul_cpu + $tg_cpu, $ts_cumul_wall = $ts_cumul_wall + $ts_wall, $ts_cumul_cpu = $ts_cumul_cpu + $ts_cpu, $tp_cumul_wall = $tp_cumul_wall + $tp2_wall - $tp1_wall, $tp_cumul_cpu = $tp_cumul_cpu + $tp2_cpu - $tp1_cpu];

        // print to file
        Print[{$TimeStep, $tg_wall, $tg_cpu, $ts_wall, $ts_cpu, $tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];

        // reset after accepted time step
        Evaluate[$tg_wall = 0, $tg_cpu = 0, $ts_wall = 0, $ts_cpu = 0];

        // check if maximum temperature is reached


        Print[{#1}, Format "Maximum temperature: %g "];
        Test[#1 > stop_temperature] {
          Break[];
        }
      }
    

      Evaluate[ $tp1_wall = GetWallClockTime[], $tp1_cpu = GetCpuTime[] ];

      Evaluate[ $tp2_wall = GetWallClockTime[], $tp2_cpu = GetCpuTime[] ];

      Evaluate[ $tp_wall = $tp2_wall - $tp1_wall, $tp_cpu = $tp2_cpu - $tp1_cpu ];

      Print[{$tp_wall, $tp_cpu, $tg_cumul_wall, $tg_cumul_cpu, $ts_cumul_wall, $ts_cumul_cpu, $tp_cumul_wall, $tp_cumul_cpu}, Format "-1,0,0,0,0,%g,%g,%g,%g,%g,%g,%g,%g", File "computation_times.csv"];
    }
  }
}

PostProcessing {
  { Name MagSta_a_2D; NameOfFormulation Magnetostatics_a_2D; NameOfSystem Sys_Mag;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name b;
        Value {
          Term { [ {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[{d a}] * {d a} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Omega_EM; Jacobian Jac_Vol_EM; }
        }
      }
    }
  }

  { Name Thermal_T ; NameOfFormulation Thermal_T ; NameOfSystem Sys_The ;
    PostQuantity {
      // Temperature
      { Name T ;
        Value {
          Local { [ {T} ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      { Name jOverJc ;
        Value {
          Term { [ source_current/area_fct[] * 1/(criticalCurrentDensity[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] + 1) ] ;
            In Omega_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

      // Temperature average as integral quantity
      { Name T_avg ;
        Value {
          Integral {  [ {T} / area_fct[] ] ;
            In Region[ {Omega_p_TH, Omega_i_TH } ] ; Jacobian Jac_Vol_TH ; Integration Int_conducting_TH; }

          Integral {  [ {T} / area_fct[] ] ;
            In Omega_ins_TH ; Jacobian Jac_Vol_TH ; Integration Int_insulating_TH; }
        }
      }

      { Name b_thermal ;
        Value {
          Local {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }

    { Name rho ;
      Value {
          Term { [ rho[{T}, GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ] ;
          In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
      }
    }
    }
  }

  { Name post_projection; NameOfFormulation Projection_EM_to_TH; NameOfSystem sys_Mag_projection;
    PostQuantity {
      { Name b_before_projection ;
        Value {
          Term {  [Norm[{d a_before_projection}]] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
      { Name b_after_projection ;
        Value {
          Term {  [GetVariable[ElementNum[], QuadraturePointIndex[]]{$Bnorm}] ;
            In Omega_p_TH ; Jacobian Jac_Vol_TH ; }
        }
      }
    }
  }
}

PostOperation PrintMaxTemp UsingPost Thermal_T {
  // Get maximum in bare region and store in register 1
  Print[ T, OnElementsOf Omega_TH, StoreMaxInRegister 1, Format Table,
    LastTimeStepOnly 1, SendToServer "No"] ;
}

PostOperation {
  { Name Dummy; NameOfPostProcessing  Thermal_T ;
    Operation { }
  }

  { Name Map_a; NameOfPostProcessing MagSta_a_2D;
    Operation {
      Print[ b, OnElementsOf Omega_EM, File "b_Omega.pos"] ;
	  //Print [ b, OnLine {{List[{0,0,0}]}{List[{0.8,0,0}]}} {1000}, Format SimpleTable, File "Center_line.csv"];
    }
  }

  { Name b_thermal; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ b_thermal, OnElementsOf Omega_p_TH, File "b_thermal.pos"] ;
    }
  }
  { Name b_after_projection_pos; NameOfPostProcessing post_projection;
    Operation {
      Print[ b_before_projection, OnElementsOf Omega_p_TH, File "b_before_projection_gmsh.pos"] ;
      Print[ b_after_projection, OnElementsOf Omega_p_TH, File "b_after_projection.pos"] ;
    }
  }

  { Name Map_T; NameOfPostProcessing Thermal_T;
    Operation {
      Print[ T, OnElementsOf Omega_c_TH, File "T_Omega_c.pos", SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1 ] ;
      //Print[ JoverJc, OnElementsOf Omega_p_TH, File "JoverJc_Omega_p.pos", SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1, AtGaussPoints 4, Depth 0 ] ;
      //Print[ rho, OnElementsOf Omega_p_TH, File "rho_Omega_p.pos", SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1 ] ;
    }
  }

  { Name T_avg; NameOfPostProcessing Thermal_T;
    Operation {
    // writes pairs of time step and average temperature to file, one line for each time step
      Print[ T_avg[Region[1000000]], OnGlobal, File "T_avg/T_avg_0.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000002]], OnGlobal, File "T_avg/T_avg_1.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000004]], OnGlobal, File "T_avg/T_avg_2.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000006]], OnGlobal, File "T_avg/T_avg_3.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000008]], OnGlobal, File "T_avg/T_avg_4.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000010]], OnGlobal, File "T_avg/T_avg_5.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000012]], OnGlobal, File "T_avg/T_avg_6.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000014]], OnGlobal, File "T_avg/T_avg_7.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000016]], OnGlobal, File "T_avg/T_avg_8.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000018]], OnGlobal, File "T_avg/T_avg_9.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000020]], OnGlobal, File "T_avg/T_avg_10.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000022]], OnGlobal, File "T_avg/T_avg_11.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000024]], OnGlobal, File "T_avg/T_avg_12.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000026]], OnGlobal, File "T_avg/T_avg_13.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000028]], OnGlobal, File "T_avg/T_avg_14.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000030]], OnGlobal, File "T_avg/T_avg_15.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000032]], OnGlobal, File "T_avg/T_avg_16.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000034]], OnGlobal, File "T_avg/T_avg_17.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000070]], OnGlobal, File "T_avg/T_avg_18.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000072]], OnGlobal, File "T_avg/T_avg_19.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000074]], OnGlobal, File "T_avg/T_avg_20.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000076]], OnGlobal, File "T_avg/T_avg_21.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000078]], OnGlobal, File "T_avg/T_avg_22.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000080]], OnGlobal, File "T_avg/T_avg_23.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000082]], OnGlobal, File "T_avg/T_avg_24.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000084]], OnGlobal, File "T_avg/T_avg_25.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000086]], OnGlobal, File "T_avg/T_avg_26.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000088]], OnGlobal, File "T_avg/T_avg_27.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000090]], OnGlobal, File "T_avg/T_avg_28.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000092]], OnGlobal, File "T_avg/T_avg_29.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000094]], OnGlobal, File "T_avg/T_avg_30.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000096]], OnGlobal, File "T_avg/T_avg_31.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000098]], OnGlobal, File "T_avg/T_avg_32.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000100]], OnGlobal, File "T_avg/T_avg_33.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000102]], OnGlobal, File "T_avg/T_avg_34.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000104]], OnGlobal, File "T_avg/T_avg_35.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000001]], OnGlobal, File "T_avg/T_avg_36.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000003]], OnGlobal, File "T_avg/T_avg_37.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000005]], OnGlobal, File "T_avg/T_avg_38.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000007]], OnGlobal, File "T_avg/T_avg_39.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000009]], OnGlobal, File "T_avg/T_avg_40.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000011]], OnGlobal, File "T_avg/T_avg_41.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000013]], OnGlobal, File "T_avg/T_avg_42.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000015]], OnGlobal, File "T_avg/T_avg_43.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000017]], OnGlobal, File "T_avg/T_avg_44.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000019]], OnGlobal, File "T_avg/T_avg_45.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000021]], OnGlobal, File "T_avg/T_avg_46.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000023]], OnGlobal, File "T_avg/T_avg_47.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000025]], OnGlobal, File "T_avg/T_avg_48.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000027]], OnGlobal, File "T_avg/T_avg_49.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000029]], OnGlobal, File "T_avg/T_avg_50.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000031]], OnGlobal, File "T_avg/T_avg_51.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000033]], OnGlobal, File "T_avg/T_avg_52.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000071]], OnGlobal, File "T_avg/T_avg_53.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000073]], OnGlobal, File "T_avg/T_avg_54.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000075]], OnGlobal, File "T_avg/T_avg_55.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000077]], OnGlobal, File "T_avg/T_avg_56.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000079]], OnGlobal, File "T_avg/T_avg_57.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000081]], OnGlobal, File "T_avg/T_avg_58.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000083]], OnGlobal, File "T_avg/T_avg_59.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000085]], OnGlobal, File "T_avg/T_avg_60.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000087]], OnGlobal, File "T_avg/T_avg_61.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000089]], OnGlobal, File "T_avg/T_avg_62.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000091]], OnGlobal, File "T_avg/T_avg_63.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000093]], OnGlobal, File "T_avg/T_avg_64.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000095]], OnGlobal, File "T_avg/T_avg_65.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000097]], OnGlobal, File "T_avg/T_avg_66.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000099]], OnGlobal, File "T_avg/T_avg_67.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000101]], OnGlobal, File "T_avg/T_avg_68.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000103]], OnGlobal, File "T_avg/T_avg_69.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000036]], OnGlobal, File "T_avg/T_avg_70.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000038]], OnGlobal, File "T_avg/T_avg_71.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000040]], OnGlobal, File "T_avg/T_avg_72.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000042]], OnGlobal, File "T_avg/T_avg_73.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000044]], OnGlobal, File "T_avg/T_avg_74.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000046]], OnGlobal, File "T_avg/T_avg_75.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000048]], OnGlobal, File "T_avg/T_avg_76.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000050]], OnGlobal, File "T_avg/T_avg_77.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000052]], OnGlobal, File "T_avg/T_avg_78.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000054]], OnGlobal, File "T_avg/T_avg_79.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000056]], OnGlobal, File "T_avg/T_avg_80.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000058]], OnGlobal, File "T_avg/T_avg_81.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000060]], OnGlobal, File "T_avg/T_avg_82.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000062]], OnGlobal, File "T_avg/T_avg_83.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000064]], OnGlobal, File "T_avg/T_avg_84.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000066]], OnGlobal, File "T_avg/T_avg_85.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000068]], OnGlobal, File "T_avg/T_avg_86.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000106]], OnGlobal, File "T_avg/T_avg_87.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000108]], OnGlobal, File "T_avg/T_avg_88.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000110]], OnGlobal, File "T_avg/T_avg_89.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000112]], OnGlobal, File "T_avg/T_avg_90.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000114]], OnGlobal, File "T_avg/T_avg_91.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000116]], OnGlobal, File "T_avg/T_avg_92.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000118]], OnGlobal, File "T_avg/T_avg_93.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000120]], OnGlobal, File "T_avg/T_avg_94.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000122]], OnGlobal, File "T_avg/T_avg_95.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000124]], OnGlobal, File "T_avg/T_avg_96.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000126]], OnGlobal, File "T_avg/T_avg_97.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000128]], OnGlobal, File "T_avg/T_avg_98.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000130]], OnGlobal, File "T_avg/T_avg_99.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000132]], OnGlobal, File "T_avg/T_avg_100.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000134]], OnGlobal, File "T_avg/T_avg_101.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000136]], OnGlobal, File "T_avg/T_avg_102.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000138]], OnGlobal, File "T_avg/T_avg_103.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000035]], OnGlobal, File "T_avg/T_avg_104.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000037]], OnGlobal, File "T_avg/T_avg_105.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000039]], OnGlobal, File "T_avg/T_avg_106.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000041]], OnGlobal, File "T_avg/T_avg_107.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000043]], OnGlobal, File "T_avg/T_avg_108.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000045]], OnGlobal, File "T_avg/T_avg_109.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000047]], OnGlobal, File "T_avg/T_avg_110.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000049]], OnGlobal, File "T_avg/T_avg_111.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000051]], OnGlobal, File "T_avg/T_avg_112.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000053]], OnGlobal, File "T_avg/T_avg_113.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000055]], OnGlobal, File "T_avg/T_avg_114.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000057]], OnGlobal, File "T_avg/T_avg_115.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000059]], OnGlobal, File "T_avg/T_avg_116.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000061]], OnGlobal, File "T_avg/T_avg_117.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000063]], OnGlobal, File "T_avg/T_avg_118.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000065]], OnGlobal, File "T_avg/T_avg_119.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000067]], OnGlobal, File "T_avg/T_avg_120.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000069]], OnGlobal, File "T_avg/T_avg_121.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000105]], OnGlobal, File "T_avg/T_avg_122.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000107]], OnGlobal, File "T_avg/T_avg_123.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000109]], OnGlobal, File "T_avg/T_avg_124.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000111]], OnGlobal, File "T_avg/T_avg_125.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000113]], OnGlobal, File "T_avg/T_avg_126.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000115]], OnGlobal, File "T_avg/T_avg_127.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000117]], OnGlobal, File "T_avg/T_avg_128.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000119]], OnGlobal, File "T_avg/T_avg_129.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000121]], OnGlobal, File "T_avg/T_avg_130.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000123]], OnGlobal, File "T_avg/T_avg_131.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000125]], OnGlobal, File "T_avg/T_avg_132.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000127]], OnGlobal, File "T_avg/T_avg_133.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000129]], OnGlobal, File "T_avg/T_avg_134.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000131]], OnGlobal, File "T_avg/T_avg_135.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000133]], OnGlobal, File "T_avg/T_avg_136.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000135]], OnGlobal, File "T_avg/T_avg_137.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000137]], OnGlobal, File "T_avg/T_avg_138.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
      Print[ T_avg[Region[1000139]], OnGlobal, File "T_avg/T_avg_139.txt", Format Table, SendToServer "No", LastTimeStepOnly 1, AppendToExistingFile 1] ;
    }
  }
}