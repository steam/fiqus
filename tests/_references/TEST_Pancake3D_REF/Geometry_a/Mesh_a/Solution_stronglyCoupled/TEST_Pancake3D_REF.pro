//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{
    DOM_insulation = Region[ 1000001 ];
    DOM_terminalContactLayer = Region[ 1000002 ];
    DOM_allInsulations = Region[{ 1000001, 1000002 }];

    // create windings region:
    DOM_allWindings = Region[{ 1000000 }];

    // create terminals region:
    DOM_terminals = Region[{ 1000005, 1000006}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{1000003, 1000004}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
    DOM_thermal = Region[{ DOM_powered, DOM_allInsulations}];
    DOM_allConducting = Region[{ DOM_powered, DOM_allInsulations }];
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
    DOM_resistiveHeating += Region[{ DOM_allInsulations }];
    DOM_air = Region[{ 1000007 }];
    DOM_airPoints = Region[{ 4000000, 4000001 }];

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];




    // boundary surface between the all conducting and non-conducting domains:
    DOM_pancakeBoundary = Region[{ 2000002 }];

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
    DOM_terminalCut = Region[{ 3000000 }];
    DOM_airCuts = Region[{ DOM_terminalCut }];

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ 2000001 }];
    DOM_topTerminalSurface = Region[{ 2000000 }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {0.0, 5.0, 10.0};
    listOfCurrentValues = {0.0, 1.0, 1.0};
    current[] = InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].



    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = 77.0; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = 0.0; // start time, [s]
    INPUT_tEnd = 10.0; // end time, [s]
    INPUT_extrapolationOrder = 1; // order of extrapolation for the time stepping scheme
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = 0.01; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = 30.0; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = 1.0; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ 0.0, 5.0, 10.0 }; // force solution at these time points, [s]

    // Nonlinear solver parameters:
    INPUT_NLSMaxNumOfIter = 50; // maximum number of iterations for the nonlinear solver
    INPUT_NLSRelaxFactor = 0.7; // relaxation factor for the nonlinear solver

    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================
    // Air permeability starts =========================================================
    // Linear:
    INPUT_airMagneticPermeability = 1.2566e-06;
    mu[] = INPUT_airMagneticPermeability;
    // Air permeability ends ===========================================================

    // Winding material combination parameters start ===================================
    INPUT_relativeThicknessCopper = 0.2143;
    INPUT_relativeThicknessStainlessSteel = 0.01;
    INPUT_relativeThicknessHastelloy = 0.5615;
    INPUT_relativeThicknessSilver = 0.1428;

    INPUT_relativeThicknessOfSuperConductor = 0.0714;
    INPUT_relativeThicknessOfNormalConductor = 0.9286000000000001;
    INPUT_relativeWidthOfPlating = 0.0;
    // Factor 1.0/1 is used equate the scaling applied to Jcritical in the parallel direction 
    INPUT_jCriticalScalingNormalToWinding = 1.0/1 * 1;
    // Winding material combination parameters end =====================================



    // Winding resistivity, Jc, and lambda starts ======================================================
    
    rhoWindingAndDerivative[] = WindingWithSuperConductorRhoAndDerivativeV1[
        $Time,
        XYZ[],
        $3,
        $2,
        $1
    ]{  77.0,
        0.0,
        90.0,
        -1,
 
        0,
        1, // N of Ic Values
        1,
        230.0,
        4, // N of materials,
        0,
        4,
        1,
        2,
        0.2143,
        0.01,
        0.5615,
        0.1428,
        100.0,
        100,
        100,
        100.0,
        295.0,
        295,
        295,
        295.0,
        0,
        0.0,
        100,
        295,
        5, // material integer: HTS
        0.0714, // relative thickness of HTS
        0.0001, // electric field criterion of HTS
        30.0, // n value of HTS
        0.0, // winding minimum possible resistivity (or superconductor minimum? bug)
        0.01, // winding maximum possible resistivity (or superconductor maximum? bug)
        0, // local defect start turn
        0, // local defect end turn
        1, // local defect which pancake coil
        0, // local defect value
        99999999, // local defect start time
        0.005,
        0.00024,
        0.0001,
        0.0,
        32,
        3,
        0.004,
        0.01,
        1,
        1 // arbitrary jCritical scaling normal to winding
    };
    rho[DOM_allWindings] =     GetFirstTensor[
        SetVariable[
            rhoWindingAndDerivative[$1, $2, $3],
            ElementNum[],
            QuadraturePointIndex[],
            $NLIteration
        ]{
            $rhoWindingAndDerivative
        }
    ];

    
    d_of_rho_wrt_j_TIMES_j[DOM_allWindings] = GetSecondTensor[
        GetVariable[ElementNum[], QuadraturePointIndex[], $NLIteration]{$rhoWindingAndDerivative}
    ];

    Jcritical[] = Pancake3DCriticalCurrentDensity[
        $Time,
        XYZ[],
        $2,
        $1
        ]{
            77.0,
            0.0,
            90.0,
            -1,
            1, // N of Ic Values
            1,
            230.0,
            4, // N of materials,
            0,
            4,
            1,
            2,
            0.2143,
            0.01,
            0.5615,
            0.1428,
            100.0,
            100,
            100,
            100.0,
            295.0,
            295,
            295,
            295.0,
            5, // material integer: HTS
            0.0714, // relative thickness of HTS
            0, // local defect start turn
            0, // local defect end turn
            1, // local defect which pancake coil
            0, // local defect value
            99999999, // local defect start time
            0.00024,
            0.004,
            1,
            0,
            0.0,
            100,
            295
        };

        Icritical[] = Pancake3DCriticalCurrent[
            $Time,
            XYZ[],
            $2,
            $1
            ]{
                77.0,
                0.0,
                90.0,
                -1,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.01,
                0.5615,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00024,
                0.004,
                1,
                0,
                0.0,
                100,
                295
            };

            lambda[] = Pancake3DHTSCurrentSharingIndex[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                -1,
 
                0,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.01,
                0.5615,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00024,
                0.004,
                1,
                0,
                0.0,
                100,
                295
            };

            jHTS[] = Pancake3DHTSCurrentDensity[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                -1,
 
                0,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.01,
                0.5615,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00024,
                0.004,
                1,
                0,
                0.0,
                100,
                295
            };
    // Winding resistivity, Jc, and lambda ends ========================================================

    // Terminals resistivity starts ====================================================
    rho[DOM_transitionNotchVolumes] = CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0};

    // Nonlinear:
    rho[DOM_terminals] = CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0};
    
    // Terminals resistivity ends ======================================================

    // Insulation resistivity starts ===================================================
    // Linear:
    INPUT_insulationResistivity =0.000112; // resistivity of the insulation, [Ohm*m]

    // Volume insulation:
    rho[DOM_insulation] = INPUT_insulationResistivity;
    // Insulation resistivity ends =====================================================
    

    // Transition layer resistivity starts =============================================
    // Linear:
    INPUT_terminalContactLayerResistivity = 0.000112; // resistivity of the insulation, [Ohm*m]

    rho[DOM_terminalContactLayer] = INPUT_terminalContactLayerResistivity;
    // Transition layer resistivity ends ===============================================

    // Winding thermal conductivity starts =============================================
    // Nonlinear:
    kappa[DOM_allWindings] = WindingThermalConductivityV1[
        XYZ[],
        $2,
        $1
    ]{   0 ,
        4, // N of materials,
        0,
        4,
        1,
        2,
        0.2143,
        0.01,
        0.5615,
        0.1428,
        100.0,
        100,
        100,
        100.0,
        295.0,
        295,
        295,
        295.0,
        0,
        0.0,
        100,
        295,
        0.005,
        0.00024,
        0.0001,
        0.0,
        32,
        3,
        0.004,
        0.01,
        1,
        1 // arbitrary jCritical scaling normal to winding
    };
    // Winding thermal conductivity ends ===============================================

    // Terminals thermal conductivity starts ===========================================
    kappa[DOM_transitionNotchVolumes] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0};

    // Nonlinear:
    kappa[DOM_terminals] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0};
    // Terminals thermal conductivity ends =============================================

    // Insulation thermal conductivity starts ==========================================
    // Nonlinear:
    // Volume insulation:
    kappa[DOM_insulation] = CFUN_kSteel_T[$1];
    // Insulation thermal conductivity ends ============================================

    // Transition layer thermal conductivity starts ====================================
    // Nonlinear:
    // Volume insulation:
    kappa[DOM_terminalContactLayer] = CFUN_kSteel_T[$1];
    // Transition layer thermal conductivity ends ======================================
    

    // Winding specific heat capacity starts ===========================================
    // Nonlinear:
    CvCopper[] =  CFUN_CvCu_T[$1];
    CvStainlessSteel[] =  CFUN_CvSteel_T[$1];
    CvHastelloy[] =  CFUN_CvHast_T[$1];
    CvSilver[] =  CFUN_CvAg_T[$1];

    Cv[DOM_allWindings] = 1 * RuleOfMixtures[
        CvCopper[$1],
        CvStainlessSteel[$1],
        CvHastelloy[$1],
        CvSilver[$1]
    ]{
        INPUT_relativeThicknessCopper,
        INPUT_relativeThicknessStainlessSteel,
        INPUT_relativeThicknessHastelloy,
        INPUT_relativeThicknessSilver
    };
    // Winding specific heat capacity ends =============================================

    // Terminals specific heat capacity starts =========================================
    Cv[DOM_transitionNotchVolumes] = CFUN_CvCu_T[$1];

    // Nonlinear:
    Cv[DOM_terminals] =  CFUN_CvCu_T[$1];
    // Terminals specific heat capacity ends ===========================================
    
    // Insulation specific heat capacity starts ========================================
    // Nonlinear:
    // Volume insulation:
    Cv[DOM_insulation] = CFUN_CvSteel_T[$1];
    // Insulation specific heat capacity ends ==========================================

    // Transition layer specific heat capacity starts ==================================
    // Nonlinear:
    // Volume insulation:
    Cv[DOM_terminalContactLayer] = CFUN_CvSteel_T[$1];
    // Transition layer specific heat capacity ends ====================================
    

}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
                Region DOM_terminalCut;
                Type Assign;
                Value 1;


                TimeFunction current[$Time];
            }
        }
    }
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
            {
                Region DOM_bottomTerminalSurface;
                Type Assign;
                Value 77.0;
            }
            {
                Region DOM_topTerminalSurface;
                Type Assign;
                Value 77.0;
            }
            {
                Region Region[{DOM_powered, DOM_allInsulations}];
                Type Init;
                Value 77.0;
            }
        }
    }

}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
                Support DOM_total;
                Entity NodesOf[DOM_Phi];
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
                Entity EdgesOf[All, Not DOM_pancakeBoundary];
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
                Support DOM_total;
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
        }
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {
                NameOfCoef GLOBALQUANT_I;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_current;
            }
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
                Support Region[{DOM_thermal 
                }];
                Entity NodesOf[DOM_thermal];
            }
        }
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
        }
    }


}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
    {
        Name FORMULATION_stronglyCoupled;
        Type FemEquation;
        Quantity{
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                // Different test function is needed for non-symmetric tensors, otherwise, getdp
                // assumes the tensors are symmetric and the result is wrong.
                Name LOCALQUANT_h_Derivative;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name GLOBALQUANT_I;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
            }
            {
                Name GLOBALQUANT_V;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_T;
                Type Local;
                NameOfSpace SPACE_temperature;
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }

        Equation{
            Integral{
                // note that it is only defined in DOM_allConducting, not all domain
                [ rho[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h}, {d LOCALQUANT_h} ];
                In DOM_allConducting;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral{
                DtDof[mu[] * Dof{LOCALQUANT_h}, {LOCALQUANT_h}];
                In DOM_total;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral
            {
                JacNL[d_of_rho_wrt_j_TIMES_j[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h} , {d LOCALQUANT_h_Derivative} ];
                In DOM_allWindings; 
                Jacobian JAC_vol;
                Integration Int; 
            }
            // the global term allows to link current and voltage in the cuts
            GlobalTerm{
                [ Dof{GLOBALQUANT_V}, {GLOBALQUANT_I} ];
                In DOM_airCuts;
            }

            Integral {
                [ kappa[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}] * Dof{d LOCALQUANT_T}, {d LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral {
                DtDof[ Cv[{LOCALQUANT_T}, Norm[mu[] * {LOCALQUANT_h}]] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }



 
            Integral {
                [ -(rho[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}, {LOCALQUANT_T} ];
                In DOM_resistiveHeating;
                Jacobian JAC_vol;
                Integration Int;
            }

        }
    }



}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_stronglyCoupled;
        System{
            {
                Name SYSTEM_stronglyCoupled;
                NameOfFormulation FORMULATION_stronglyCoupled;
            }
        }

        Operation{
            InitSolution[SYSTEM_stronglyCoupled];
            SaveSolution[SYSTEM_stronglyCoupled];
            SetExtrapolationOrder[INPUT_extrapolationOrder];


            TimeLoopAdaptive[
                INPUT_tStart,
                INPUT_tEnd,
                INPUT_tAdaptiveInitStep,
                INPUT_tAdaptiveMinStep,
                INPUT_tAdaptiveMaxStep,
                "Euler",
                List[INPUT_tAdaptiveBreakPoints],
                PostOperation{
                    {
                        POSTOP_CONV_voltageBetweenTerminals,
                        0.1,
                        0.05,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfCurrentDensity,
                        0.1,
                        16000000.0,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfMagneticField,
                        0.1,
                        0.002,
                        LinfNorm
                    }
                }

            ]{
                // Nonlinear solver starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                IterativeLoopN[
                    INPUT_NLSMaxNumOfIter,
                    INPUT_NLSRelaxFactor,
                    PostOperation{
                        {
                            POSTOP_CONV_voltageBetweenTerminals,
                            0.1,
                            0.05,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfCurrentDensity,
                            0.1,
                            16000000.0,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfMagneticField,
                            0.1,
                            0.002,
                            LinfNorm
                        }
                    }

                ]{
                    GenerateJac SYSTEM_stronglyCoupled;
                    SolveJac SYSTEM_stronglyCoupled;

                }
                // Check if the solution is NaN and remove it
                Test[$KSPResidual != $KSPResidual]{
                    Print["Critical: Removing NaN solution from the solution vector."];
                    RemoveLastSolution[SYSTEM_stronglyCoupled];
                }
                // Nonlinear solver ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            }{
                SaveSolution[SYSTEM_stronglyCoupled];

            }

            PostOperation[POSTOP_magneticField];
            PostOperation[POSTOP_currentDensity];
            PostOperation[POSTOP_temperature];



        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_stronglyCoupled;
        NameOfFormulation FORMULATION_stronglyCoupled;
        NameOfSystem SYSTEM_stronglyCoupled;
        Quantity{
            {
                Name RESULT_magneticField; // magnetic flux density (magnetic field)
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfMagneticField; // magnetic flux density magnitude
                Value{
                    Local{
                        [Norm[mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentDensity; // current density
                Value{
                    Local{
                        [{d LOCALQUANT_h}];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfCurrentDensity; // current density magnitude
                Value{
                    Local{
                        [Norm[{d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_resistivity; // current density magnitude
                Value{
                    Local{
                        [rho[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_arcLength;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength[XYZ[]]{
                            0.005,
                            0.00024,
                            0.0001,
                            0.0,
                            32,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_turnNumber;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber[XYZ[]]{
                            0.005,
                            0.00024,
                            0.0001,
                            0.0,
                            32,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrentDensity; // critical current density of the winding
                Value{
                    Local{
                        [Jcritical[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrent;
                Value{
                    Local{
                        [Icritical[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentSharingIndex;
                Value{
                    Local{
                        [lambda[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTS;
                Value{
                    Local{
                        [jHTS[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTSOverjCritical;
                Value{
                    Local{
                        // add small epsilon to avoid division by zero
                        [jHTS[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]/(Jcritical[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] + 1e-10)];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magneticEnergy;
                Value{
                    Integral{
                        Type Global;
                        [1/2 * mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_inductance;
                Value{
                    Integral{
                        Type Global;
                        [mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_resistiveHeating; // resistive heating
                Value{
                    Local{
                        [(rho[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In Region[{ DOM_resistiveHeating }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_voltageBetweenTerminals; // voltages in cuts
                Value{
                    Local{
                        [ - {GLOBALQUANT_V} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_currentThroughCoil; // currents in cuts
                Value{
                    Local{
                        [ {GLOBALQUANT_I} ];
                        In DOM_terminalCut;
                    }
                }
            }


            {
                Name RESULT_axialComponentOfTheMagneticField; // axial magnetic flux density
                Value{
                    Local{
                        [ CompZ[mu[] * {LOCALQUANT_h}] ];
                        In DOM_total;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_totalResistiveHeating; // total resistive heating for convergence
                Value{
                    Integral{
                        Type Global;
                        [(rho[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In DOM_resistiveHeating;
                        Jacobian JAC_vol;
                        Integration Int;
                    }

 
                }
            }
            {
                Name RESULT_temperature;
                Value {
                    Local{
                        [{LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_maximumTemperature; // maximum temperature
                Value{
                    Term {
                        Type Global;
                        [ #999 ];
                        In DOM_powered;
                    }
                }
            }

            {
                Name RESULT_heatFlux;
                Value {
                    Local{
                        [-kappa[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}] * {d LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfHeatFlux;
                Value {
                    Local{
                        [Norm[-kappa[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}] * {d LOCALQUANT_T}]];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_thermalConductivity; // current density magnitude
                Value{
                    Local{
                        [kappa[{LOCALQUANT_T}, mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_specificHeatCapacity; // current density magnitude
                Value{
                    Local{
                        [Cv[{LOCALQUANT_T}, Norm[mu[] * {LOCALQUANT_h}]]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

        }
    }

}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation {
        }
    }
    {
        Name POSTOP_magneticField;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_magneticField,
                OnElementsOf DOM_total,
                File "MagneticField-DefaultFormat.pos",
                Name "Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // 3D magnetic field magnitude scalar field:
            
            Print[
                RESULT_magnitudeOfMagneticField,
                OnElementsOf DOM_total,
                File "MagneticFieldMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_currentDensity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_currentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensity-DefaultFormat.pos",
                Name "Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensityMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_arcLength;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            
            Print[
                RESULT_arcLength,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "arcLength-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumber;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            
            Print[
                RESULT_turnNumber,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "turnNumber-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number [m]"
            ];

        }
    }
    {
        Name POSTOP_resistiveHeating;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "ResistiveHeating_Windings-DefaultFormat.pos",
                Name "Resistive Heating Windings [W/m^3]"
            ];


            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "ResistiveHeating-DefaultFormat.pos",
                Name "Resistive Heating Without Windings [W/m^3]"
            ];

        }
    }
    {
        Name POSTOP_resistivity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "Resistivity_Windings-DefaultFormat.pos",
                Name "Resistivity Windings [Ohm*m]"
            ];


            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "Resistivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Resistivity Without Windings [Ohm*m]"
            ];

        }
    }
    {
        Name POSTOP_inductance;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_inductance,
                OnGlobal,
                File "Inductance-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                Name "Inductance [H]"
            ];

        }
    }
    {
        Name POSTOP_timeConstant;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_axialComponentOfTheMagneticField,
                OnPoint {0, 0, 0},
                File "axialComponentOfTheMagneticFieldForTimeConstant-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrentDensity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Critical current density:
            
            Print[
                RESULT_criticalCurrentDensity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "CriticalCurrentDensity-DefaultFormat.pos",
                Name "Critical Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrent;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Critical current:
            
            Print[
                RESULT_criticalCurrent,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "CriticalCurrent-DefaultFormat.pos",
                Name "Critical Current [A]"
            ];

        }
    }
    {
        Name POSTOP_currentSharingIndex;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Critical current:
            
            Print[
                RESULT_currentSharingIndex,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "currentSharingIndex-DefaultFormat.pos",
                Name "Current Sharing Index [-]"
            ];

        }
    }
    {
        Name POSTOP_jHTS;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Current density in HTS layer:
            
            Print[
                RESULT_jHTS,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "jHTS-DefaultFormat.pos",
                Name "Current Density in HTS Layer [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_jHTSOverjCritical;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Normalized HTS current density:
            
            Print[
                RESULT_jHTSOverjCritical,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "HTSCurrentDensityOverCriticalCurrentDensity-DefaultFormat.pos",
                Name "(HTS Current Density)/(Critical Current Density)"
            ];

        }
    }
    // {
    //     Name POSTOP_Ic;
    //     NameOfPostProcessing POSTPRO_stronglyCoupled;
    //     LastTimeStepOnly 1;
    //     Operation {
    //         Print[
    //             RESULT_criticalCurrent,
    //             OnElementsOf DOM_allWindings,
    //             StoreMinInRegister 1
    //         ];
    //     }
    // }
    {
        Name POSTOP_I;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                Format Table,
                StoreInVariable $I
            ];
        }
    }
    {
        Name POSTOP_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Voltage at the cut:
            
            Print[
                RESULT_voltageBetweenTerminals,
                OnRegion DOM_terminalCut,
                File "VoltageBetweenTerminals-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Voltage [V]"
            ];

        }
    }
    {
        Name POSTOP_currentThroughCoil;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                File "CurrentThroughCoil-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Current [A]"
            ];

        }
    }
    {
        Name POSTOP_maximumTemperature;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation {
            Print[ RESULT_temperature, 
                    OnElementsOf DOM_powered, 
                    StoreMaxInRegister 999, 
                    Format Table,
                    LastTimeStepOnly 1, 
                    SendToServer "No",
                    File "maximumTemperature_dump.txt"
                ] ;
            // We can print the maximum temperature at any region that is part
            // of the thermal domain since the `StoreMaxInRegister` command
            // already searches all of the thermal region for the maximum and
            //populates the same value for all physical regions of the thermal 
            // domain.
            // Printing in just one domain makes the parsing of the output easier.
            
            Print[
                RESULT_maximumTemperature,
                OnRegion Region[1000000],
                File "maximumTemperature(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Maximum temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_temperature;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_temperature,
                OnElementsOf DOM_thermal,
                File "Temperature-DefaultFormat.pos",
                Name "Temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_heatFlux;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_heatFlux,
                OnElementsOf DOM_thermal,
                File "HeatFlux-DefaultFormat.pos",
                Name "Heat Flux [W/m^2]"
            ];

        }
    }
    {
        Name POSTOP_thermalConductivity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Thermal conductivity:
            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "thermalConductivity_Windings-DefaultFormat.pos",
                Name "Thermal Conductivity Windings [W/(m*K)]"
            ];


            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "thermalConductivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Thermal Conductivity Without Windings [W/(m*K)]"
            ];

        }
    }
    {
        Name POSTOP_specificHeatCapacity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        Operation{
            // Specific heat:
            
            Print[
                RESULT_specificHeatCapacity,
                OnElementsOf DOM_thermal,
                File "specificHeatCapacity-DefaultFormat.pos",
                Name "Specific Heat Capacity [J/(kg*K)]"
            ];

        }
    }
    // convergence criteria as postoperations:
    {
        Name POSTOP_CONV_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_voltageBetweenTerminals, OnRegion DOM_terminalCut];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnPoint {-0.0019605939094794397, -0.006038758089569408, -0.014},
                StoreInVariable $test
            ];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_stronglyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfMagneticField,
                OnPoint {0.0, 0.0, 0.0},
                StoreInVariable $test
            ];
        }
    }

}