//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{
    DOM_insulation = Region[ 1000001 ];
    DOM_terminalContactLayer = Region[ 1000002 ];
    DOM_allInsulations = Region[{ 1000001, 1000002 }];

    // create windings region:
    DOM_allWindings = Region[{ 1000000 }];

    // create terminals region:
    DOM_terminals = Region[{ 1000005, 1000006}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{1000003, 1000004}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
    DOM_thermal = Region[{ DOM_powered, DOM_allInsulations}];
    DOM_allConducting = Region[{ DOM_powered, DOM_allInsulations }];
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
    DOM_resistiveHeating += Region[{ DOM_allInsulations }];
    DOM_air = Region[{ 1000007 }];
    DOM_airPoints = Region[{ 4000000, 4000001 }];

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];




    // boundary surface between the all conducting and non-conducting domains:
    DOM_pancakeBoundary = Region[{ 2000002 }];

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
    DOM_terminalCut = Region[{ 3000000 }];
    DOM_airCuts = Region[{ DOM_terminalCut }];

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ 2000001 }];
    DOM_topTerminalSurface = Region[{ 2000000 }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {0.0, 5.0, 10.0};
    listOfCurrentValues = {0.0, 1.0, 1.0};
    current[] = InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].



    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = 77.0; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = 0.0; // start time, [s]
    INPUT_tEnd = 10.0; // end time, [s]
    INPUT_extrapolationOrder = 1; // order of extrapolation for the time stepping scheme
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = 0.01; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = 30.0; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = 1.0; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ 0.0, 5.0, 10.0 }; // force solution at these time points, [s]


    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================
    // Air permeability starts =========================================================
    // Linear:
    INPUT_airMagneticPermeability = 1.2566e-06;
    mu[] = INPUT_airMagneticPermeability;
    // Air permeability ends ===========================================================




    // Winding resistivity, Jc, and lambda starts ======================================================
    // Linear:
    INPUT_coilResistivity = 1e-12; // resistivity of the coil, [Ohm*m]
    rho[DOM_allWindings] = INPUT_coilResistivity;
    // Winding resistivity, Jc, and lambda ends ========================================================

    // Terminals resistivity starts ====================================================
    rho[DOM_transitionNotchVolumes] = 0.01;

    // Linear:
    INPUT_terminalResistivity = 1e-12; // resistivity of the terminals, [Ohm*m]
    rho[DOM_terminals] = INPUT_terminalResistivity;
    // Terminals resistivity ends ======================================================

    // Insulation resistivity starts ===================================================
    // Linear:
    INPUT_insulationResistivity =0.001; // resistivity of the insulation, [Ohm*m]

    // Volume insulation:
    rho[DOM_insulation] = INPUT_insulationResistivity;
    // Insulation resistivity ends =====================================================
    

    // Transition layer resistivity starts =============================================
    // Linear:
    INPUT_terminalContactLayerResistivity = 0.001; // resistivity of the insulation, [Ohm*m]

    rho[DOM_terminalContactLayer] = INPUT_terminalContactLayerResistivity;
    // Transition layer resistivity ends ===============================================


}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
                Region DOM_terminalCut;
                Type Assign;
                Value 1;


                TimeFunction current[$Time];
            }
        }
    }
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
            {
                Region Region[{DOM_powered, DOM_allInsulations}];
                Type Init;
                Value 77.0;
            }
        }
    }

}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
                Support DOM_total;
                Entity NodesOf[DOM_Phi];
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
                Entity EdgesOf[All, Not DOM_pancakeBoundary];
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
                Support DOM_total;
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
        }
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {
                NameOfCoef GLOBALQUANT_I;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_current;
            }
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
                Support Region[{DOM_thermal 
                }];
                Entity NodesOf[DOM_thermal];
            }
        }
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
        }
    }


}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
    {
        Name FORMULATION_electromagnetic;
        Type FemEquation;
        Quantity{
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                // Different test function is needed for non-symmetric tensors, otherwise, getdp
                // assumes the tensors are symmetric and the result is wrong.
                Name LOCALQUANT_h_Derivative;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name GLOBALQUANT_I;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
            }
            {
                Name GLOBALQUANT_V;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }

        Equation{
            Integral{
                // note that it is only defined in DOM_allConducting, not all domain
                [ rho[] * Dof{d LOCALQUANT_h}, {d LOCALQUANT_h} ];
                In DOM_allConducting;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral{
                DtDof[mu[] * Dof{LOCALQUANT_h}, {LOCALQUANT_h}];
                In DOM_total;
                Jacobian JAC_vol;
                Integration Int;
            }
            // the global term allows to link current and voltage in the cuts
            GlobalTerm{
                [ Dof{GLOBALQUANT_V}, {GLOBALQUANT_I} ];
                In DOM_airCuts;
            }

        }
    }



}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_electromagnetic;
        System{
            {
                Name SYSTEM_electromagnetic;
                NameOfFormulation FORMULATION_electromagnetic;
            }
        }

        Operation{
            InitSolution[SYSTEM_electromagnetic];
            SaveSolution[SYSTEM_electromagnetic];
            SetExtrapolationOrder[INPUT_extrapolationOrder];


            TimeLoopAdaptive[
                INPUT_tStart,
                INPUT_tEnd,
                INPUT_tAdaptiveInitStep,
                INPUT_tAdaptiveMinStep,
                INPUT_tAdaptiveMaxStep,
                "Euler",
                List[INPUT_tAdaptiveBreakPoints],
                PostOperation{
                    {
                        POSTOP_CONV_voltageBetweenTerminals,
                        0.1,
                        0.05,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfCurrentDensity,
                        0.1,
                        16000000.0,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfMagneticField,
                        0.1,
                        0.002,
                        LinfNorm
                    }
                }

            ]{
                // Linear solver starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                Generate SYSTEM_electromagnetic;
                Solve SYSTEM_electromagnetic;
                // Linear solver ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            }{
                SaveSolution[SYSTEM_electromagnetic];

            }

            PostOperation[POSTOP_magneticField];
            PostOperation[POSTOP_currentDensity];



        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_electromagnetic;
        NameOfFormulation FORMULATION_electromagnetic;
        NameOfSystem SYSTEM_electromagnetic;
        Quantity{
            {
                Name RESULT_magneticField; // magnetic flux density (magnetic field)
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfMagneticField; // magnetic flux density magnitude
                Value{
                    Local{
                        [Norm[mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentDensity; // current density
                Value{
                    Local{
                        [{d LOCALQUANT_h}];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfCurrentDensity; // current density magnitude
                Value{
                    Local{
                        [Norm[{d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_resistivity; // current density magnitude
                Value{
                    Local{
                        [rho[]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_arcLength;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength[XYZ[]]{
                            0.005,
                            0.00024,
                            0.0001,
                            0.0,
                            32,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_turnNumber;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber[XYZ[]]{
                            0.005,
                            0.00024,
                            0.0001,
                            0.0,
                            32,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }


            {
                Name RESULT_magneticEnergy;
                Value{
                    Integral{
                        Type Global;
                        [1/2 * mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_inductance;
                Value{
                    Integral{
                        Type Global;
                        [mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_resistiveHeating; // resistive heating
                Value{
                    Local{
                        [(rho[] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In Region[{ DOM_resistiveHeating }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_voltageBetweenTerminals; // voltages in cuts
                Value{
                    Local{
                        [ - {GLOBALQUANT_V} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_currentThroughCoil; // currents in cuts
                Value{
                    Local{
                        [ {GLOBALQUANT_I} ];
                        In DOM_terminalCut;
                    }
                }
            }


            {
                Name RESULT_axialComponentOfTheMagneticField; // axial magnetic flux density
                Value{
                    Local{
                        [ CompZ[mu[] * {LOCALQUANT_h}] ];
                        In DOM_total;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_totalResistiveHeating; // total resistive heating for convergence
                Value{
                    Integral{
                        Type Global;
                        [(rho[] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In DOM_resistiveHeating;
                        Jacobian JAC_vol;
                        Integration Int;
                    }

 
                }
            }
        }
    }

}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation {
        }
    }
    {
        Name POSTOP_magneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_magneticField,
                OnElementsOf DOM_total,
                File "MagneticField-DefaultFormat.pos",
                Name "Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D magnetic field magnitude scalar field:
            
            Print[
                RESULT_magnitudeOfMagneticField,
                OnElementsOf DOM_total,
                File "MagneticFieldMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_currentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_currentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensity-DefaultFormat.pos",
                Name "Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensityMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_arcLength;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_arcLength,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 4,
                File "arcLength-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumber;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_turnNumber,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 4,
                File "turnNumber-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number [m]"
            ];

        }
    }
    {
        Name POSTOP_resistiveHeating;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 4,
                File "ResistiveHeating_Windings-DefaultFormat.pos",
                Name "Resistive Heating Windings [W/m^3]"
            ];


            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "ResistiveHeating-DefaultFormat.pos",
                Name "Resistive Heating Without Windings [W/m^3]"
            ];

        }
    }
    {
        Name POSTOP_resistivity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 4,
                File "Resistivity_Windings-DefaultFormat.pos",
                Name "Resistivity Windings [Ohm*m]"
            ];


            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "Resistivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Resistivity Without Windings [Ohm*m]"
            ];

        }
    }
    {
        Name POSTOP_inductance;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_inductance,
                OnGlobal,
                File "Inductance-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                Name "Inductance [H]"
            ];

        }
    }
    {
        Name POSTOP_timeConstant;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_axialComponentOfTheMagneticField,
                OnPoint {0, 0, 0},
                File "axialComponentOfTheMagneticFieldForTimeConstant-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Voltage at the cut:
            
            Print[
                RESULT_voltageBetweenTerminals,
                OnRegion DOM_terminalCut,
                File "VoltageBetweenTerminals-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Voltage [V]"
            ];

        }
    }
    {
        Name POSTOP_currentThroughCoil;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                File "CurrentThroughCoil-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Current [A]"
            ];

        }
    }
    // convergence criteria as postoperations:
    {
        Name POSTOP_CONV_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_voltageBetweenTerminals, OnRegion DOM_terminalCut];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnPoint {-0.0019605939094794397, -0.006038758089569408, -0.014},
                StoreInVariable $test
            ];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfMagneticField,
                OnPoint {0.0, 0.0, 0.0},
                StoreInVariable $test
            ];
        }
    }

}