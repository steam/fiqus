//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{

    DOM_allInsulationSurface_0 = Region[{ 2000009, 2000010, 2000011 }];

    DOM_terminalContactLayerSurface_WithoutNotch_0 = Region[{ 2000010 }];
    DOM_terminalContactLayerSurface_Notch_0 = Region[{ 2000011 }];

    DOM_terminalContactLayerSurface_0 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_0, DOM_terminalContactLayerSurface_Notch_0 }];

    DOM_windingMinus_1 = Region[{ 1000000 }];
    DOM_windingPlus_1 = Region[{ 1000002 }];

    DOM_allInsulationSurface_1 = Region[{ 2000000 }];
    DOM_allInsulationSurface_1 += Region[{ 2000001 }];
    DOM_allInsulationSurface_1 += Region[{ 2000002 }];


    DOM_windingMinus_2 = Region[{ 1000001 }];
    DOM_windingPlus_2 = Region[{ 1000003 }];

    DOM_allInsulationSurface_2 = Region[{ 2000003 }];
    DOM_allInsulationSurface_2 += Region[{ 2000004 }];
    DOM_allInsulationSurface_2 += Region[{ 2000005 }];


    DOM_windingMinus_3 = Region[{ 1000002 }];
    DOM_windingPlus_3 = Region[{ 1000000 }];

    DOM_allInsulationSurface_3 = Region[{ 2000006 }];
    DOM_allInsulationSurface_3 += Region[{ 2000007 }];
    DOM_allInsulationSurface_3 += Region[{ 2000008 }];


    DOM_windingMinus_4 = Region[{ 1000003 }];
    DOM_windingPlus_4 = Region[{ 1000001 }];

    DOM_allInsulationSurface_4 = Region[{ 2000009 }];
    DOM_allInsulationSurface_4 += Region[{ 2000010 }];
    DOM_allInsulationSurface_4 += Region[{ 2000011 }];


    DOM_windingMinus_5 = Region[{ 1000000 }];
    DOM_windingPlus_5 = Region[{ 1000002 }];

    DOM_allInsulationSurface_5 = Region[{ 2000000 }];
    DOM_allInsulationSurface_5 += Region[{ 2000001 }];
    DOM_allInsulationSurface_5 += Region[{ 2000002 }];




    // Add terminals to winding region logic:
    // 1000006: inner terminal
    // 1000007: outer terminal
    // 1000004: inner layer transition angle
    // 1000005: outer layer transition angle
    DOM_windingMinus_1 += Region[{ 1000006 }];
    DOM_windingMinus_1 += Region[{ 1000004 }];
    DOM_windingMinus_2 += Region[{ 1000006 }];
    DOM_windingMinus_2 += Region[{ 1000004 }];
    DOM_windingMinus_3 += Region[{ 1000006 }];
    DOM_windingMinus_3 += Region[{ 1000004 }];
    DOM_windingMinus_4 += Region[{ 1000006 }];
    DOM_windingMinus_4 += Region[{ 1000004 }];
    DOM_windingMinus_5 += Region[{ 1000006 }];
    DOM_windingMinus_5 += Region[{ 1000004 }];

    DOM_windingPlus_1 += Region[{ 1000007 }];
    DOM_windingPlus_1 += Region[{ 1000005 }];
    DOM_windingPlus_2 += Region[{ 1000007 }];
    DOM_windingPlus_2 += Region[{ 1000005 }];
    DOM_windingPlus_3 += Region[{ 1000007 }];
    DOM_windingPlus_3 += Region[{ 1000005 }];
    DOM_windingPlus_4 += Region[{ 1000007 }];
    DOM_windingPlus_4 += Region[{ 1000005 }];

    DOM_allInsulationSurface = Region[{ 2000000, 2000001, 2000002, 2000003, 2000004, 2000005, 2000006, 2000007, 2000008, 2000009, 2000010, 2000011 }];
    DOM_insulationSurface = Region[{ 2000000, 2000003, 2000006, 2000009 }];
    DOM_terminalContactLayerSurface_WithoutNotch = Region[{ 2000001, 2000004, 2000007, 2000010 }];
    DOM_terminalContactLayerSurface_Notch = Region[{ 2000002, 2000005, 2000008, 2000011 }];
    DOM_terminalContactLayerSurface = Region[{ DOM_terminalContactLayerSurface_WithoutNotch, DOM_terminalContactLayerSurface_Notch }];
    DOM_allInsulationSurface_WithoutNotch = Region[ {DOM_insulationSurface, DOM_terminalContactLayerSurface_WithoutNotch} ];


    DOM_insulationBoundaryCurvesAir = Region[{ 3000000 }];
    DOM_insulationBoundaryCurvesTerminal = Region[{ 3000001 }];

    // create windings region:
    DOM_allWindings = Region[{ 1000000, 1000001, 1000002, 1000003 }];

    // create terminals region:
    DOM_terminals = Region[{ 1000006, 1000007}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{1000004, 1000005}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
    DOM_thermal = Region[{ DOM_powered}];
    DOM_allConducting = Region[{ DOM_powered }];
    DOM_resistiveHeating = Region[{ DOM_allWindings, DOM_terminals }];
    DOM_air = Region[{ 1000008 }];
    DOM_airPoints = Region[{ 4000000, 4000001 }];

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];




    // boundary surface between the all conducting and non-conducting domains:
    DOM_pancakeBoundary = Region[{ 2000014 }];

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
    DOM_terminalCut = Region[{ 3000002 }];
    DOM_airCuts = Region[{ DOM_terminalCut }];

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ 2000013 }];
    DOM_topTerminalSurface = Region[{ 2000012 }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {0.0, 5.0, 10.0};
    listOfCurrentValues = {0.0, 1.0, 1.0};
    current[] = InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].



    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = 77.0; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = 0.0; // start time, [s]
    INPUT_tEnd = 10.0; // end time, [s]
    INPUT_extrapolationOrder = 1; // order of extrapolation for the time stepping scheme
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = 0.001; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = 2.0; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = 1.0; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ 0.0, 5.0, 10.0 }; // force solution at these time points, [s]

    // Nonlinear solver parameters:
    INPUT_NLSMaxNumOfIter = 50; // maximum number of iterations for the nonlinear solver
    INPUT_NLSRelaxFactor = 0.7; // relaxation factor for the nonlinear solver

    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================
    // Air permeability starts =========================================================
    // Linear:
    INPUT_airMagneticPermeability = 1.2566e-06;
    mu[] = INPUT_airMagneticPermeability;
    // Air permeability ends ===========================================================

    // Winding material combination parameters start ===================================
    INPUT_relativeThicknessCopper = 0.2143;
    INPUT_relativeThicknessStainlessSteel = 0.1;
    INPUT_relativeThicknessHastelloy = 0.4715;
    INPUT_relativeThicknessSilver = 0.1428;

    INPUT_relativeThicknessOfSuperConductor = 0.0714;
    INPUT_relativeThicknessOfNormalConductor = 0.9286000000000001;
    INPUT_relativeWidthOfPlating = 0.0;
    // Factor 1.0/0.8808864265927977 is used equate the scaling applied to Jcritical in the parallel direction 
    INPUT_jCriticalScalingNormalToWinding = 1.0/0.8808864265927977 * 1;
    // Winding material combination parameters end =====================================

    // TSA parameters start ============================================================
    INPUT_insulationThickness = 4e-05; // thickness of the insulation, [m]
    INPUT_NumOfTSAElements = 2;
    th_terminal_k = INPUT_insulationThickness / (INPUT_NumOfTSAElements == 0 ? 1 : INPUT_NumOfTSAElements);
    th_insulation_k = th_terminal_k;
    // TSA parameters end ==============================================================
    


    // Winding resistivity, Jc, and lambda starts ======================================================
    
    rhoWindingAndDerivative[] = WindingWithSuperConductorRhoAndDerivativeV1[
        $Time,
        XYZ[],
        $3,
        $2,
        $1
    ]{  77.0,
        0.0,
        90.0,
        -1,
 
        0,
        1, // N of Ic Values
        1,
        230.0,
        4, // N of materials,
        0,
        4,
        1,
        2,
        0.2143,
        0.1,
        0.4715,
        0.1428,
        100.0,
        100,
        100,
        100.0,
        295.0,
        295,
        295,
        295.0,
        0,
        0.0,
        100,
        295,
        5, // material integer: HTS
        0.0714, // relative thickness of HTS
        0.0001, // electric field criterion of HTS
        30.0, // n value of HTS
        0.0, // winding minimum possible resistivity (or superconductor minimum? bug)
        0.01, // winding maximum possible resistivity (or superconductor maximum? bug)
        0, // local defect start turn
        0, // local defect end turn
        1, // local defect which pancake coil
        0, // local defect value
        99999999, // local defect start time
        0.005,
        0.00027245283018867926,
        0,
        0.0,
        26,
        3,
        0.004,
        0.01,
        0.8808864265927977,
        1 // arbitrary jCritical scaling normal to winding
    };
    rho[DOM_allWindings] =     GetFirstTensor[
        SetVariable[
            rhoWindingAndDerivative[$1, $2, $3],
            ElementNum[],
            QuadraturePointIndex[],
            $NLIteration
        ]{
            $rhoWindingAndDerivative
        }
    ];

    
    d_of_rho_wrt_j_TIMES_j[DOM_allWindings] = GetSecondTensor[
        GetVariable[ElementNum[], QuadraturePointIndex[], $NLIteration]{$rhoWindingAndDerivative}
    ];

    Jcritical[] = Pancake3DCriticalCurrentDensity[
        $Time,
        XYZ[],
        $2,
        $1
        ]{
            77.0,
            0.0,
            90.0,
            -1,
            1, // N of Ic Values
            1,
            230.0,
            4, // N of materials,
            0,
            4,
            1,
            2,
            0.2143,
            0.1,
            0.4715,
            0.1428,
            100.0,
            100,
            100,
            100.0,
            295.0,
            295,
            295,
            295.0,
            5, // material integer: HTS
            0.0714, // relative thickness of HTS
            0, // local defect start turn
            0, // local defect end turn
            1, // local defect which pancake coil
            0, // local defect value
            99999999, // local defect start time
            0.00027245283018867926,
            0.004,
            0.8808864265927977,
            0,
            0.0,
            100,
            295
        };

        Icritical[] = Pancake3DCriticalCurrent[
            $Time,
            XYZ[],
            $2,
            $1
            ]{
                77.0,
                0.0,
                90.0,
                -1,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.1,
                0.4715,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00027245283018867926,
                0.004,
                0.8808864265927977,
                0,
                0.0,
                100,
                295
            };

            lambda[] = Pancake3DHTSCurrentSharingIndex[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                -1,
 
                0,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.1,
                0.4715,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00027245283018867926,
                0.004,
                0.8808864265927977,
                0,
                0.0,
                100,
                295
            };

            jHTS[] = Pancake3DHTSCurrentDensity[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                -1,
 
                0,
                1, // N of Ic Values
                1,
                230.0,
                4, // N of materials,
                0,
                4,
                1,
                2,
                0.2143,
                0.1,
                0.4715,
                0.1428,
                100.0,
                100,
                100,
                100.0,
                295.0,
                295,
                295,
                295.0,
                5, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.00027245283018867926,
                0.004,
                0.8808864265927977,
                0,
                0.0,
                100,
                295
            };
    // Winding resistivity, Jc, and lambda ends ========================================================

    // Terminals resistivity starts ====================================================
    rho[DOM_transitionNotchVolumes] = CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0};

    // Nonlinear:
    rho[DOM_terminals] = CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0};
    
    // Terminals resistivity ends ======================================================

    // Insulation resistivity starts ===================================================
    // Linear:
    INPUT_insulationResistivity =0.000112; // resistivity of the insulation, [Ohm*m]

        electromagneticOnlyFunction[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_fct_only[]{
            th_terminal_k, INPUT_insulationResistivity
        };
    // Thin-shell insulation:
    electromagneticMassFunctionNoDta1b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 1
        };
    
    electromagneticStiffnessFunctiona1b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 1
        };

    electromagneticMassFunctionDta1b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 1
        };

    electromagneticMassFunctionNoDta1b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 2
        };
    
    electromagneticStiffnessFunctiona1b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 2
        };

    electromagneticMassFunctionDta1b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 2
        };

    electromagneticMassFunctionNoDta2b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 1
        };
    
    electromagneticStiffnessFunctiona2b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 1
        };

    electromagneticMassFunctionDta2b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 1
        };

    electromagneticMassFunctionNoDta2b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 2
        };
    
    electromagneticStiffnessFunctiona2b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 2
        };

    electromagneticMassFunctionDta2b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 2
        };

        
    electromagneticRHSFunctionk1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_insulationResistivity
        };

    electromagneticTripleFunctionk1a1b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 1, 1
        };

    electromagneticTripleFunctionk1a1b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 1, 2
        };

    electromagneticTripleFunctionk1a2b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 2, 1
        };

    electromagneticTripleFunctionk1a2b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 1, 2, 2
        };

    electromagneticRHSFunctionk2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_insulationResistivity
        };

    electromagneticTripleFunctionk2a1b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 1, 1
        };

    electromagneticTripleFunctionk2a1b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 1, 2
        };

    electromagneticTripleFunctionk2a2b1[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 2, 1
        };

    electromagneticTripleFunctionk2a2b2[DOM_insulationSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_insulationResistivity, 2, 2, 2
        };


    // Insulation resistivity ends =====================================================
    

    // Transition layer resistivity starts =============================================
    // Linear:
    INPUT_terminalContactLayerResistivity = 0.000112; // resistivity of the insulation, [Ohm*m]

    electromagneticOnlyFunction[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_fct_only[]{
        th_terminal_k, INPUT_terminalContactLayerResistivity
    };
    // Thin-shell insulation:
    electromagneticMassFunctionNoDta1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1
        };

    electromagneticStiffnessFunctiona1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1
        };

    electromagneticMassFunctionDta1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 1
        };

    electromagneticMassFunctionNoDta1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2
        };

    electromagneticStiffnessFunctiona1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2
        };

    electromagneticMassFunctionDta1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 2
        };

    electromagneticMassFunctionNoDta2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1
        };

    electromagneticStiffnessFunctiona2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1
        };

    electromagneticMassFunctionDta2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 1
        };

    electromagneticMassFunctionNoDta2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2
        };

    electromagneticStiffnessFunctiona2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2
        };

    electromagneticMassFunctionDta2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 2
        };

        
    electromagneticRHSFunctionk1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity
        };

    electromagneticTripleFunctionk1a1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1, 1
        };

    electromagneticTripleFunctionk1a1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1, 2
        };

    electromagneticTripleFunctionk1a2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2, 1
        };

    electromagneticTripleFunctionk1a2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2, 2
        };

    electromagneticRHSFunctionk2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity
        };

    electromagneticTripleFunctionk2a1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1, 1
        };

    electromagneticTripleFunctionk2a1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1, 2
        };

    electromagneticTripleFunctionk2a2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2, 1
        };

    electromagneticTripleFunctionk2a2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2, 2
        };


    // Transition layer resistivity ends ===============================================


}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
                Region DOM_terminalCut;
                Type Assign;
                Value 1;


                TimeFunction current[$Time];
            }
        }
    }
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
            {
                Region DOM_bottomTerminalSurface;
                Type Assign;
                Value 77.0;
            }
            {
                Region DOM_topTerminalSurface;
                Type Assign;
                Value 77.0;
            }
            {
                Region Region[
                            {
                                DOM_powered, 
                                DOM_allInsulationSurface
                            }
                        ];
                Type Init;
                Value 77.0;
            }
        }
    }

}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
                Support DOM_total;
                Entity NodesOf[{DOM_Phi}, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal}];
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
                Entity EdgesOf[All, Not {DOM_pancakeBoundary, DOM_allInsulationSurface}];
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
                Support Region[{DOM_total, DOM_allInsulationSurface}];
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_0 }
                    ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_0 }
                    ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_1 }
                    ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_1 }
                    ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_2 }
                    ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_2 }
                    ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_3 }
                    ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_allInsulationSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_allInsulationSurface_3 }
                    ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contribution on boundary of the thin layer
            {
                Name BASISFUN_gradpsinBnd;
                NameOfCoef phin_bnd;
                Function BF_GradNode;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_Phi,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity NodesOf[DOM_insulationBoundaryCurvesAir];
            }
            {
                Name BASISFUN_psieBnd;
                NameOfCoef psie_bnd;
                Function BF_Edge;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity EdgesOf[DOM_insulationBoundaryCurvesTerminal];
            }
            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef he~{i};
                    Function BF_Edge;
                    Support DOM_allInsulationSurface;
                    Entity EdgesOf[ All, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal} ];
                }
            EndFor
        }

        SubSpace{
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd
                };
            }



            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_sc,
                        BASISFUN_gradpsinBnd,
                        BASISFUN_psieBnd
                    };
                }

            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {
                NameOfCoef GLOBALQUANT_I;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_current;
            }
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
            {
                NameOfCoef phin_bnd;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
                Support Region[{DOM_thermal 
                }];
                Entity NodesOf[DOM_thermal, 
                    Not {DOM_allInsulationSurface}];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




            {
                Name BASISFUN_snBndTerminal;
                NameOfCoef Tn_Bnd;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_allInsulationSurface
                }];
                Entity NodesOf[DOM_insulationBoundaryCurvesTerminal];
            }

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    Support DOM_allInsulationSurface;
                    Entity NodesOf[ All, Not DOM_insulationBoundaryCurvesTerminal ];
                }

            EndFor

            
        }
        SubSpace {
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_snBndTerminal
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_snBndTerminal
                };
            }



            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_snBndTerminal
                    };
                }

            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

            {
                NameOfCoef BASISFUN_snMinus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

            {
                NameOfCoef BASISFUN_snMinus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

            {
                NameOfCoef BASISFUN_snMinus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }


    For i In {1:INPUT_NumOfTSAElements - 1}
        {
            NameOfCoef sn~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }

    EndFor
    
    {
        NameOfCoef Tn_Bnd;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
        }
    }


}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
    {
        Name FORMULATION_electromagnetic;
        Type FemEquation;
        Quantity{
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                // Different test function is needed for non-symmetric tensors, otherwise, getdp
                // assumes the tensors are symmetric and the result is wrong.
                Name LOCALQUANT_h_Derivative;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name GLOBALQUANT_I;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
            }
            {
                Name GLOBALQUANT_V;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_hThinShell~{0};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_down];
            }

            For i In {1:INPUT_NumOfTSAElements-1}
                {
                    Name LOCALQUANT_hThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_hPhi[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_hThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_up];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }

        Equation{
            Integral{
                // note that it is only defined in DOM_allConducting, not all domain
                [ rho[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h}, {d LOCALQUANT_h} ];
                In DOM_allConducting;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral{
                DtDof[mu[] * Dof{LOCALQUANT_h}, {LOCALQUANT_h}];
                In DOM_total;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral
            {
                JacNL[d_of_rho_wrt_j_TIMES_j[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h} , {d LOCALQUANT_h_Derivative} ];
                In DOM_allWindings; 
                Jacobian JAC_vol;
                Integration Int; 
            }
            // the global term allows to link current and voltage in the cuts
            GlobalTerm{
                [ Dof{GLOBALQUANT_V}, {GLOBALQUANT_I} ];
                In DOM_airCuts;
            }

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   electromagneticMassFunctionNoDta1b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona1b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta1b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta1b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona1b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta1b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta2b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona2b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta2b1[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta2b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona2b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta2b2[
                                INPUT_initialTemperature,
                                INPUT_initialTemperature
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_allInsulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

        }
    }



}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_electromagnetic;
        System{
            {
                Name SYSTEM_electromagnetic;
                NameOfFormulation FORMULATION_electromagnetic;
            }
        }

        Operation{
            InitSolution[SYSTEM_electromagnetic];
            SaveSolution[SYSTEM_electromagnetic];
            SetExtrapolationOrder[INPUT_extrapolationOrder];


            TimeLoopAdaptive[
                INPUT_tStart,
                INPUT_tEnd,
                INPUT_tAdaptiveInitStep,
                INPUT_tAdaptiveMinStep,
                INPUT_tAdaptiveMaxStep,
                "Euler",
                List[INPUT_tAdaptiveBreakPoints],
                PostOperation{
                    {
                        POSTOP_CONV_voltageBetweenTerminals,
                        0.1,
                        0.05,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfCurrentDensity,
                        0.1,
                        16000000.0,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfMagneticField,
                        0.1,
                        0.002,
                        LinfNorm
                    }
                }

            ]{
                // Nonlinear solver starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                IterativeLoopN[
                    INPUT_NLSMaxNumOfIter,
                    INPUT_NLSRelaxFactor,
                    PostOperation{
                        {
                            POSTOP_CONV_voltageBetweenTerminals,
                            0.1,
                            0.05,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfCurrentDensity,
                            0.1,
                            16000000.0,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfMagneticField,
                            0.1,
                            0.002,
                            LinfNorm
                        }
                    }

                ]{
                    GenerateJac SYSTEM_electromagnetic;
                    SolveJac SYSTEM_electromagnetic;

                }
                // Check if the solution is NaN and remove it
                Test[$KSPResidual != $KSPResidual]{
                    Print["Critical: Removing NaN solution from the solution vector."];
                    RemoveLastSolution[SYSTEM_electromagnetic];
                }
                // Nonlinear solver ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            }{
                SaveSolution[SYSTEM_electromagnetic];

            }

            PostOperation[POSTOP_magneticField];
            PostOperation[POSTOP_currentDensity];



        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_electromagnetic;
        NameOfFormulation FORMULATION_electromagnetic;
        NameOfSystem SYSTEM_electromagnetic;
        Quantity{
            {
                Name RESULT_magneticField; // magnetic flux density (magnetic field)
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfMagneticField; // magnetic flux density magnitude
                Value{
                    Local{
                        [Norm[mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentDensity; // current density
                Value{
                    Local{
                        [{d LOCALQUANT_h}];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfCurrentDensity; // current density magnitude
                Value{
                    Local{
                        [Norm[{d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_resistivity; // current density magnitude
                Value{
                    Local{
                        [rho[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_arcLength;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength[XYZ[]]{
                            0.005,
                            0.00027245283018867926,
                            0,
                            0.0,
                            26,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_arcLengthContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength_contactLayer[XYZ[]]{
                            0.005,
                            0.00027245283018867926,
                            0,
                            0.0,
                            26,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {
                Name RESULT_turnNumberContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber_contactLayer[XYZ[]]{
                            0.005,
                            0.00027245283018867926,
                            0,
                            0.0,
                            26,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {
                Name RESULT_turnNumber;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber[XYZ[]]{
                            0.005,
                            0.00027245283018867926,
                            0,
                            0.0,
                            26,
                            3,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrentDensity; // critical current density of the winding
                Value{
                    Local{
                        [Jcritical[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrent;
                Value{
                    Local{
                        [Icritical[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentSharingIndex;
                Value{
                    Local{
                        [lambda[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTS;
                Value{
                    Local{
                        [jHTS[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTSOverjCritical;
                Value{
                    Local{
                        // add small epsilon to avoid division by zero
                        [jHTS[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}]/(Jcritical[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] + 1e-10)];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magneticEnergy;
                Value{
                    Integral{
                        Type Global;
                        [1/2 * mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_inductance;
                Value{
                    Integral{
                        Type Global;
                        [mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_resistiveHeating; // resistive heating
                Value{
                    Local{
                        [(rho[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In Region[{ DOM_resistiveHeating }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_voltageBetweenTerminals; // voltages in cuts
                Value{
                    Local{
                        [ - {GLOBALQUANT_V} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_currentThroughCoil; // currents in cuts
                Value{
                    Local{
                        [ {GLOBALQUANT_I} ];
                        In DOM_terminalCut;
                    }
                }
            }


            {
                Name RESULT_axialComponentOfTheMagneticField; // axial magnetic flux density
                Value{
                    Local{
                        [ CompZ[mu[] * {LOCALQUANT_h}] ];
                        In DOM_total;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_totalResistiveHeating; // total resistive heating for convergence
                Value{
                    Integral{
                        Type Global;
                        [(rho[INPUT_initialTemperature, mu[] * {LOCALQUANT_h}, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In DOM_resistiveHeating;
                        Jacobian JAC_vol;
                        Integration Int;
                    }

                        For i In {0:INPUT_NumOfTSAElements-1}
                            Integral {
                                Type Global;
                                [
                                    electromagneticOnlyFunction[
                                        INPUT_initialTemperature,
                                        INPUT_initialTemperature
                                    ] * SquNorm[
                                        ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
                                    ]
                                ];
                                In DOM_allInsulationSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            } 
                        
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta1b1[
                                        INPUT_initialTemperature,
                                        INPUT_initialTemperature
                                    ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                                ];
                                In DOM_allInsulationSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta1b2[
                                        INPUT_initialTemperature,
                                        INPUT_initialTemperature
                                    ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                                ];
                                In DOM_allInsulationSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta2b1[
                                        INPUT_initialTemperature,
                                        INPUT_initialTemperature
                                    ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                                ];
                                In DOM_allInsulationSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta2b2[
                                        INPUT_initialTemperature,
                                        INPUT_initialTemperature
                                    ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                                ];
                                In DOM_allInsulationSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                        EndFor
 
                }
            }
 {
                Name RESULT_debug;
                Value{
                    Local{
                        [ 5 ];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
        }
    }

}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation {
        }
    }
 {
        Name POSTOP_debug;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_debug,
                OnElementsOf DOM_allInsulationSurface,
                File "debug-DefaultFormat.pos",
                Name "Debug Variable"
            ];

        }
    }
    {
        Name POSTOP_magneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_magneticField,
                OnElementsOf DOM_total,
                File "MagneticField-DefaultFormat.pos",
                Name "Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D magnetic field magnitude scalar field:
            
            Print[
                RESULT_magnitudeOfMagneticField,
                OnElementsOf DOM_total,
                File "MagneticFieldMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_currentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_currentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensity-DefaultFormat.pos",
                Name "Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensityMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_arcLength;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_arcLength,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "arcLength-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length [m]"
            ];

        }
    }
    {
        Name POSTOP_arcLengthContactLayer;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_arcLengthContactLayer,
                OnElementsOf DOM_allInsulationSurface,
                File "arcLengthContactLayer-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length Contact Layer [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumberContactLayer;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_turnNumberContactLayer,
                OnElementsOf DOM_allInsulationSurface,
                File "turnNumberContactLayer-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number contact layer [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumber;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            
            Print[
                RESULT_turnNumber,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "turnNumber-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number [m]"
            ];

        }
    }
    {
        Name POSTOP_resistiveHeating;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "ResistiveHeating_Windings-DefaultFormat.pos",
                Name "Resistive Heating Windings [W/m^3]"
            ];


            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "ResistiveHeating-DefaultFormat.pos",
                Name "Resistive Heating Without Windings [W/m^3]"
            ];

        }
    }
    {
        Name POSTOP_resistivity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "Resistivity_Windings-DefaultFormat.pos",
                Name "Resistivity Windings [Ohm*m]"
            ];


            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "Resistivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Resistivity Without Windings [Ohm*m]"
            ];

        }
    }
    {
        Name POSTOP_inductance;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_inductance,
                OnGlobal,
                File "Inductance-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                Name "Inductance [H]"
            ];

        }
    }
    {
        Name POSTOP_timeConstant;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_axialComponentOfTheMagneticField,
                OnPoint {0, 0, 0},
                File "axialComponentOfTheMagneticFieldForTimeConstant-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Critical current density:
            
            Print[
                RESULT_criticalCurrentDensity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "CriticalCurrentDensity-DefaultFormat.pos",
                Name "Critical Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrent;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Critical current:
            
            Print[
                RESULT_criticalCurrent,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "CriticalCurrent-DefaultFormat.pos",
                Name "Critical Current [A]"
            ];

        }
    }
    {
        Name POSTOP_currentSharingIndex;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Critical current:
            
            Print[
                RESULT_currentSharingIndex,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "currentSharingIndex-DefaultFormat.pos",
                Name "Current Sharing Index [-]"
            ];

        }
    }
    {
        Name POSTOP_jHTS;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current density in HTS layer:
            
            Print[
                RESULT_jHTS,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "jHTS-DefaultFormat.pos",
                Name "Current Density in HTS Layer [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_jHTSOverjCritical;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Normalized HTS current density:
            
            Print[
                RESULT_jHTSOverjCritical,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 6,
                File "HTSCurrentDensityOverCriticalCurrentDensity-DefaultFormat.pos",
                Name "(HTS Current Density)/(Critical Current Density)"
            ];

        }
    }
    // {
    //     Name POSTOP_Ic;
    //     NameOfPostProcessing POSTPRO_electromagnetic;
    //     LastTimeStepOnly 1;
    //     Operation {
    //         Print[
    //             RESULT_criticalCurrent,
    //             OnElementsOf DOM_allWindings,
    //             StoreMinInRegister 1
    //         ];
    //     }
    // }
    {
        Name POSTOP_I;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                Format Table,
                StoreInVariable $I
            ];
        }
    }
    {
        Name POSTOP_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Voltage at the cut:
            
            Print[
                RESULT_voltageBetweenTerminals,
                OnRegion DOM_terminalCut,
                File "VoltageBetweenTerminals-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Voltage [V]"
            ];

        }
    }
    {
        Name POSTOP_currentThroughCoil;
        NameOfPostProcessing POSTPRO_electromagnetic;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                File "CurrentThroughCoil-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Current [A]"
            ];

        }
    }
    // convergence criteria as postoperations:
    {
        Name POSTOP_CONV_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_voltageBetweenTerminals, OnRegion DOM_terminalCut];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnPoint {-0.0018823903108267201, -0.005819099716693622, -0.014},
                StoreInVariable $test
            ];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_electromagnetic;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfMagneticField,
                OnPoint {0.0, 0.0, 0.0},
                StoreInVariable $test
            ];
        }
    }

}