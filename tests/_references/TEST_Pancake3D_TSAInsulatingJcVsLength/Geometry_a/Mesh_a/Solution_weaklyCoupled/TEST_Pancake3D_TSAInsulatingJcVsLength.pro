//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{
    DOM_innerTerminalAndTransitionNotch_surface = Region[{ 2000015 }];
    DOM_outerTerminalAndTransitionNotch_surface = Region[{ 2000016 }];

    DOM_allInsulationSurface_0 = Region[{ 2000009, 2000010, 2000011 }];

    DOM_terminalContactLayerSurface_WithoutNotch_0 = Region[{ 2000010 }];
    DOM_terminalContactLayerSurface_Notch_0 = Region[{ 2000011 }];

    DOM_terminalContactLayerSurface_0 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_0, DOM_terminalContactLayerSurface_Notch_0 }];
    DOM_insulationSurface_0 = Region[{ 2000009 }];

    DOM_windingMinus_1 = Region[{ 1000000 }];
    DOM_windingPlus_1 = Region[{ 1000002 }];
    DOM_windingMinus_electricScalarPotential_1 = Region[{ 1000000 }];
    DOM_windingPlus_electricScalarPotential_1 = Region[{ 1000002 }];

    DOM_allInsulationSurface_1 = Region[{ 2000000 }];
    DOM_allInsulationSurface_1 += Region[{ 2000001 }];
    DOM_allInsulationSurface_1 += Region[{ 2000002 }];

    DOM_terminalContactLayerSurface_WithoutNotch_1 = Region[{ 2000001 }];
    DOM_terminalContactLayerSurface_Notch_1 = Region[{ 2000002 }];
    DOM_terminalContactLayerSurface_1 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_1, DOM_terminalContactLayerSurface_Notch_1 }];
    DOM_insulationSurface_1 = Region[{ 2000000 }];
    DOM_windingSurfaceMinus_1 = Region[{ 2000017 }];
    DOM_windingSurfacePlus_1 = Region[{ 2000019 }];

    DOM_windingMinus_2 = Region[{ 1000001 }];
    DOM_windingPlus_2 = Region[{ 1000003 }];
    DOM_windingMinus_electricScalarPotential_2 = Region[{ 1000001 }];
    DOM_windingPlus_electricScalarPotential_2 = Region[{ 1000003 }];

    DOM_allInsulationSurface_2 = Region[{ 2000003 }];
    DOM_allInsulationSurface_2 += Region[{ 2000004 }];
    DOM_allInsulationSurface_2 += Region[{ 2000005 }];

    DOM_terminalContactLayerSurface_WithoutNotch_2 = Region[{ 2000004 }];
    DOM_terminalContactLayerSurface_Notch_2 = Region[{ 2000005 }];
    DOM_terminalContactLayerSurface_2 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_2, DOM_terminalContactLayerSurface_Notch_2 }];
    DOM_insulationSurface_2 = Region[{ 2000003 }];
    DOM_windingSurfaceMinus_2 = Region[{ 2000018 }];
    DOM_windingSurfacePlus_2 = Region[{ 2000020 }];

    DOM_windingMinus_3 = Region[{ 1000002 }];
    DOM_windingPlus_3 = Region[{ 1000000 }];
    DOM_windingMinus_electricScalarPotential_3 = Region[{ 1000002 }];
    DOM_windingPlus_electricScalarPotential_3 = Region[{ 1000000 }];

    DOM_allInsulationSurface_3 = Region[{ 2000006 }];
    DOM_allInsulationSurface_3 += Region[{ 2000007 }];
    DOM_allInsulationSurface_3 += Region[{ 2000008 }];

    DOM_terminalContactLayerSurface_WithoutNotch_3 = Region[{ 2000007 }];
    DOM_terminalContactLayerSurface_Notch_3 = Region[{ 2000008 }];
    DOM_terminalContactLayerSurface_3 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_3, DOM_terminalContactLayerSurface_Notch_3 }];
    DOM_insulationSurface_3 = Region[{ 2000006 }];
    DOM_windingSurfaceMinus_3 = Region[{ 2000019 }];
    DOM_windingSurfacePlus_3 = Region[{ 2000017 }];

    DOM_windingMinus_4 = Region[{ 1000003 }];
    DOM_windingPlus_4 = Region[{ 1000001 }];
    DOM_windingMinus_electricScalarPotential_4 = Region[{ 1000003 }];
    DOM_windingPlus_electricScalarPotential_4 = Region[{ 1000001 }];

    DOM_allInsulationSurface_4 = Region[{ 2000009 }];
    DOM_allInsulationSurface_4 += Region[{ 2000010 }];
    DOM_allInsulationSurface_4 += Region[{ 2000011 }];

    DOM_terminalContactLayerSurface_WithoutNotch_4 = Region[{ 2000010 }];
    DOM_terminalContactLayerSurface_Notch_4 = Region[{ 2000011 }];
    DOM_terminalContactLayerSurface_4 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_4, DOM_terminalContactLayerSurface_Notch_4 }];
    DOM_insulationSurface_4 = Region[{ 2000009 }];
    DOM_windingSurfaceMinus_4 = Region[{ 2000020 }];
    DOM_windingSurfacePlus_4 = Region[{ 2000018 }];

    DOM_windingMinus_5 = Region[{ 1000000 }];
    DOM_windingPlus_5 = Region[{ 1000002 }];
    DOM_windingMinus_electricScalarPotential_5 = Region[{ 1000000 }];
    DOM_windingPlus_electricScalarPotential_5 = Region[{ 1000002 }];

    DOM_allInsulationSurface_5 = Region[{ 2000000 }];
    DOM_allInsulationSurface_5 += Region[{ 2000001 }];
    DOM_allInsulationSurface_5 += Region[{ 2000002 }];

    DOM_terminalContactLayerSurface_WithoutNotch_5 = Region[{ 2000001 }];
    DOM_terminalContactLayerSurface_Notch_5 = Region[{ 2000002 }];
    DOM_terminalContactLayerSurface_5 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_5, DOM_terminalContactLayerSurface_Notch_5 }];
    DOM_insulationSurface_5 = Region[{ 2000000 }];
    DOM_windingSurfaceMinus_5 = Region[{ 2000017 }];
    DOM_windingSurfacePlus_5 = Region[{ 2000019 }];



    // Add terminals to winding region logic:
    // 1000006: inner terminal
    // 1000007: outer terminal
    // 1000004: inner layer transition angle
    // 1000005: outer layer transition angle
    DOM_windingMinus_1 += Region[{ 1000006 }];
    DOM_windingMinus_electricScalarPotential_1 += Region[{ 1000006 }];
    DOM_windingMinus_1 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_1 += Region[{ 2000015 }];
    DOM_windingMinus_2 += Region[{ 1000006 }];
    DOM_windingMinus_electricScalarPotential_2 += Region[{ 1000006 }];
    DOM_windingMinus_2 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_2 += Region[{ 2000015 }];
    DOM_windingMinus_3 += Region[{ 1000006 }];
    DOM_windingMinus_electricScalarPotential_3 += Region[{ 1000006 }];
    DOM_windingMinus_3 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_3 += Region[{ 2000015 }];
    DOM_windingMinus_4 += Region[{ 1000006 }];
    DOM_windingMinus_electricScalarPotential_4 += Region[{ 1000006 }];
    DOM_windingMinus_4 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_4 += Region[{ 2000015 }];
    DOM_windingMinus_5 += Region[{ 1000006 }];
    DOM_windingMinus_electricScalarPotential_5 += Region[{ 1000006 }];
    DOM_windingMinus_5 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_5 += Region[{ 2000015 }];

    DOM_windingPlus_1 += Region[{ 1000007 }];
    DOM_windingPlus_electricScalarPotential_1 += Region[{ 1000007 }];
    DOM_windingPlus_1 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_1 += Region[{ 2000016 }];
    DOM_windingPlus_2 += Region[{ 1000007 }];
    DOM_windingPlus_electricScalarPotential_2 += Region[{ 1000007 }];
    DOM_windingPlus_2 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_2 += Region[{ 2000016 }];
    DOM_windingPlus_3 += Region[{ 1000007 }];
    DOM_windingPlus_electricScalarPotential_3 += Region[{ 1000007 }];
    DOM_windingPlus_3 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_3 += Region[{ 2000016 }];
    DOM_windingPlus_4 += Region[{ 1000007 }];
    DOM_windingPlus_electricScalarPotential_4 += Region[{ 1000007 }];
    DOM_windingPlus_4 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_4 += Region[{ 2000016 }];

    DOM_allInsulationSurface = Region[{ 2000000, 2000001, 2000002, 2000003, 2000004, 2000005, 2000006, 2000007, 2000008, 2000009, 2000010, 2000011 }];
    DOM_insulationSurface = Region[{ 2000000, 2000003, 2000006, 2000009 }];
    DOM_terminalContactLayerSurface_WithoutNotch = Region[{ 2000001, 2000004, 2000007, 2000010 }];
    DOM_terminalContactLayerSurface_Notch = Region[{ 2000002, 2000005, 2000008, 2000011 }];
    DOM_terminalContactLayerSurface = Region[{ DOM_terminalContactLayerSurface_WithoutNotch, DOM_terminalContactLayerSurface_Notch }];
    DOM_allInsulationSurface_WithoutNotch = Region[ {DOM_insulationSurface, DOM_terminalContactLayerSurface_WithoutNotch} ];

    DOM_allWindingSurface = Region[{ 2000017, 2000018, 2000019, 2000020 }];
    DOM_allConvectiveSurface = Region[{ 2000015, 2000016, 2000017, 2000018, 2000019, 2000020 }];

    DOM_insulationBoundaryCurvesAir = Region[{ 3000000 }];
    DOM_insulationBoundaryCurvesTerminal = Region[{ 3000001 }];

    // create windings region:
    DOM_allWindings = Region[{ 1000000, 1000001, 1000002, 1000003 }];

    // create terminals region:
    DOM_terminals = Region[{ 1000006, 1000007}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{1000004, 1000005}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
    DOM_allConducting = Region[{ DOM_powered}];
    DOM_resistiveHeating = Region[{ DOM_allWindings }];
    DOM_thermal = Region[{ DOM_powered}];
    DOM_air = Region[{ 1000008 }];
    DOM_airPoints = Region[{ 4000000 }];

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];


    // add shell volume for shell transformation:
    DOM_air += Region[{ 1000009 }];
    DOM_airInf = Region[{ 1000009 }];


    // boundary surface between the all conducting and non-conducting domains:
    DOM_pancakeBoundary = Region[{ 2000014 }];

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
    DOM_terminalCut = Region[{ 3000003 }];
    DOM_airHoleCut = Region[{  }];
    DOM_airCuts = Region[{ DOM_terminalCut, DOM_airHoleCut }];

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ 2000013 }];
    DOM_topTerminalSurface = Region[{ 2000012 }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    DOM_dummyPrintResistiveVoltages = Region[ 9999999 ] ;
    
    DOM_currentSource = Region[ 9000000 ];
    DOM_resistanceCrowbar = Region[ 9000001 ];
    DOM_switchCrowbar = Region[ 9000002 ];
    DOM_resistanceEE = Region[ 9000003 ];
    DOM_switchEE = Region[ 9000004 ];
    DOM_inductanceEE = Region[ 9000005 ];

    DOM_circuitResistance = Region[{ DOM_resistanceCrowbar, DOM_resistanceEE, DOM_switchCrowbar, DOM_switchEE }];
    DOM_circuitInductance = Region[{ DOM_inductanceEE }];

    DOM_circuit = Region[{ DOM_currentSource, DOM_circuitResistance, DOM_circuitInductance }];
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {0.0, 5.0, 10.0};
    listOfCurrentValues = {0.0, 1.0, 1.0};
    current[] = $quenchDetected == 1? (
        $Time - $quenchDetectionTime > 1.5 ? 0 :
        InterpolationLinear[$quenchDetectionTime]{ListAlt[listOfTimeValues, listOfCurrentValues]} * (1 - ($Time - $quenchDetectionTime)/1.5) ) : 
        InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].


    imposedPowerDensity[] = Pancake3DPowerDensity[$1, $2]
        {1.0,
        7.0, 
        0.15,
        0.18,
        100.0,
        0.005,
        0.000275,
        0,
        0.0,
        26,
        1,
        0.004,
        0.01
        };

    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = 77.0; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = 0.0; // start time, [s]
    INPUT_tEnd = 15.0; // end time, [s]
    INPUT_extrapolationOrder = 1; // order of extrapolation for the time stepping scheme
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = 0.001; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = 1.0; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = 0.1; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ 0.0, 5.0, 10.0 }; // force solution at these time points, [s]

    // Nonlinear solver parameters:
    INPUT_NLSMaxNumOfIter = 20; // maximum number of iterations for the nonlinear solver
    INPUT_NLSRelaxFactor = 0.7; // relaxation factor for the nonlinear solver

    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================
    // Air permeability starts =========================================================
    // Linear:
    INPUT_airMagneticPermeability = 1.2566e-06;
    mu[] = INPUT_airMagneticPermeability;
    // Air permeability ends ===========================================================

    // Winding material combination parameters start ===================================
    INPUT_relativeThicknessCopper = 0.2143;
    INPUT_relativeThicknessHastelloy = 0.5715;
    INPUT_relativeThicknessSilver = 0.1428;

    INPUT_relativeThicknessOfSuperConductor = 0.0714;
    INPUT_relativeThicknessOfNormalConductor = 0.9286000000000001;
    INPUT_relativeWidthOfPlating = 0.02;
    // Factor 1.0/0.8727272727272727 is used equate the scaling applied to Jcritical in the parallel direction 
    INPUT_jCriticalScalingNormalToWinding = 1.0/0.8727272727272727 * 1;
    // Winding material combination parameters end =====================================

    // TSA parameters start ============================================================
    INPUT_insulationThickness = 4e-05; // thickness of the insulation, [m]
    INPUT_NumOfTSAElements = 2;
    th_terminal_k = INPUT_insulationThickness / (INPUT_NumOfTSAElements == 0 ? 1 : INPUT_NumOfTSAElements);
    th_insulation_k = 0.5 * th_terminal_k;
    // TSA parameters end ==============================================================
    
    // Cryocooler TSA parameters start ==================================================
    INPUT_NumOfCryocoolerTSAElements = 2;
    cryocooler_thickness[] = 0.0001/ GetVariable[]{$areaCryocooler} ;
    th_cryocooler_k[] = cryocooler_thickness[]/INPUT_NumOfCryocoolerTSAElements;
    // Cryocooler TSA parameters end ====================================================


    // Winding resistivity, Jc, and lambda starts ======================================================
    
    rhoWindingAndDerivative[] = WindingWithSuperConductorRhoAndDerivativeV1[
        $Time,
        XYZ[],
        $3,
        $2,
        $1
    ]{  77.0,
        0.0,
        90.0,
        0,
 
        1,
        4, // N of Ic Values
        0,
        2,
        6,
        8,
        300,
        100,
        500,
        300,
        3, // N of materials,
        0,
        1,
        2,
        0.2143,
        0.5715,
        0.1428,
        100.0,
        100,
        100.0,
        295.0,
        295,
        295.0,
        0,
        0.02,
        100.0,
        295.0,
        7, // material integer: HTS
        0.0714, // relative thickness of HTS
        0.0001, // electric field criterion of HTS
        30.0, // n value of HTS
        1e-20, // winding minimum possible resistivity (or superconductor minimum? bug)
        0.01, // winding maximum possible resistivity (or superconductor maximum? bug)
        0, // local defect start turn
        0, // local defect end turn
        1, // local defect which pancake coil
        0, // local defect value
        99999999, // local defect start time
        0.005,
        0.000275,
        0,
        0.0,
        26,
        1,
        0.004,
        0.01,
        0.8727272727272727,
        1 // arbitrary jCritical scaling normal to winding
    };
    rho[DOM_allWindings] =     	CompXX[
            GetFirstTensor[
                SetVariable[
                    rhoWindingAndDerivative[$1, $2, $3],
                    ElementNum[],
                    QuadraturePointIndex[],
                    $NLIteration
                ]{
                    $rhoWindingAndDerivative
                }
            ]
        ];

    
    d_of_rho_wrt_j_TIMES_j[DOM_allWindings] = GetSecondTensor[
        GetVariable[ElementNum[], QuadraturePointIndex[], $NLIteration]{$rhoWindingAndDerivative}
    ];

    Jcritical[] = Pancake3DCriticalCurrentDensity[
        $Time,
        XYZ[],
        $2,
        $1
        ]{
            77.0,
            0.0,
            90.0,
            0,
            4, // N of Ic Values
            0,
            2,
            6,
            8,
            300,
            100,
            500,
            300,
            3, // N of materials,
            0,
            1,
            2,
            0.2143,
            0.5715,
            0.1428,
            100.0,
            100,
            100.0,
            295.0,
            295,
            295.0,
            7, // material integer: HTS
            0.0714, // relative thickness of HTS
            0, // local defect start turn
            0, // local defect end turn
            1, // local defect which pancake coil
            0, // local defect value
            99999999, // local defect start time
            0.000275,
            0.004,
            0.8727272727272727,
            0,
            0.02,
            100.0,
            295.0
        };

        Icritical[] = Pancake3DCriticalCurrent[
            $Time,
            XYZ[],
            $2,
            $1
            ]{
                77.0,
                0.0,
                90.0,
                0,
                4, // N of Ic Values
                0,
                2,
                6,
                8,
                300,
                100,
                500,
                300,
                3, // N of materials,
                0,
                1,
                2,
                0.2143,
                0.5715,
                0.1428,
                100.0,
                100,
                100.0,
                295.0,
                295,
                295.0,
                7, // material integer: HTS
                0.0714, // relative thickness of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.000275,
                0.004,
                0.8727272727272727,
                0,
                0.02,
                100.0,
                295.0
            };

            lambda[] = Pancake3DHTSCurrentSharingIndex[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                0,
 
                1,
                4, // N of Ic Values
                0,
                2,
                6,
                8,
                300,
                100,
                500,
                300,
                3, // N of materials,
                0,
                1,
                2,
                0.2143,
                0.5715,
                0.1428,
                100.0,
                100,
                100.0,
                295.0,
                295,
                295.0,
                7, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.000275,
                0.004,
                0.8727272727272727,
                0,
                0.02,
                100.0,
                295.0
            };

            jHTS[] = Pancake3DHTSCurrentDensity[
                $Time,
                XYZ[],
                $3,
                $2,
                $1
            ]{  77.0,
                0.0,
                90.0,
                0,
 
                1,
                4, // N of Ic Values
                0,
                2,
                6,
                8,
                300,
                100,
                500,
                300,
                3, // N of materials,
                0,
                1,
                2,
                0.2143,
                0.5715,
                0.1428,
                100.0,
                100,
                100.0,
                295.0,
                295,
                295.0,
                7, // material integer: HTS
                0.0714, // relative thickness of HTS
                0.0001, // electric field criterion of HTS
                30.0, // n value of HTS
                0, // local defect start turn
                0, // local defect end turn
                1, // local defect which pancake coil
                0, // local defect value
                99999999, // local defect start time
                0.000275,
                0.004,
                0.8727272727272727,
                0,
                0.02,
                100.0,
                295.0
            };
    // Winding resistivity, Jc, and lambda ends ========================================================

    // Terminals resistivity starts ====================================================
    rho[DOM_transitionNotchVolumes] = 0.01;

    // Nonlinear:
    rho[DOM_terminals] = CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0};
    
    // Terminals resistivity ends ======================================================

    // Insulation resistivity starts ===================================================
    // Insulation resistivity ends =====================================================
    
    // Insulation conductivity starts ===================================================
    // Insulation conductivity ends =====================================================

    // Transition layer resistivity starts =============================================
    // Linear:
    INPUT_terminalContactLayerResistivity = 0.000112; // resistivity of the insulation, [Ohm*m]

    electromagneticOnlyFunction[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_fct_only[]{
        th_terminal_k, INPUT_terminalContactLayerResistivity
    };
    // Thin-shell insulation:
    electromagneticMassFunctionNoDta1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1
        };

    electromagneticStiffnessFunctiona1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1
        };

    electromagneticMassFunctionDta1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 1
        };

    electromagneticMassFunctionNoDta1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2
        };

    electromagneticStiffnessFunctiona1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2
        };

    electromagneticMassFunctionDta1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 1, 2
        };

    electromagneticMassFunctionNoDta2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1
        };

    electromagneticStiffnessFunctiona2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1
        };

    electromagneticMassFunctionDta2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 1
        };

    electromagneticMassFunctionNoDta2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2
        };

    electromagneticStiffnessFunctiona2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2
        };

    electromagneticMassFunctionDta2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_airMagneticPermeability, 2, 2
        };

        
    electromagneticRHSFunctionk1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity
        };

    electromagneticTripleFunctionk1a1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1, 1
        };

    electromagneticTripleFunctionk1a1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 1, 2
        };

    electromagneticTripleFunctionk1a2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2, 1
        };

    electromagneticTripleFunctionk1a2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 1, 2, 2
        };

    electromagneticRHSFunctionk2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_rhs[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity
        };

    electromagneticTripleFunctionk2a1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1, 1
        };

    electromagneticTripleFunctionk2a1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 1, 2
        };

    electromagneticTripleFunctionk2a2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2, 1
        };

    electromagneticTripleFunctionk2a2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_triple[]{
            th_terminal_k, INPUT_terminalContactLayerResistivity, 2, 2, 2
        };


    // Transition layer resistivity ends ===============================================
    // the electric scalar potential formulation has to deal with very large conductivity values
    // to yield an equivalent system with better conditioning, it is scaled with this factor
    electricScalarPotential_scalingFactor = 1E-10; 
    // Transition layer conductivity starts =============================================
    
    // Linear:
    INPUT_terminalContactLayerConductivity = 1./0.000112; // resistivity of the insulation, [S/m]

    // Thin-shell insulation:
    electricScalarPotentialMassFunctionNoDta1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 1, 1
        };

    electricScalarPotentialStiffnessFunctiona1b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 1, 1
        };

    electricScalarPotentialMassFunctionNoDta1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 1, 2
        };

    electricScalarPotentialStiffnessFunctiona1b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 1, 2
        };

    electricScalarPotentialMassFunctionNoDta2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 2, 1
        };

    electricScalarPotentialStiffnessFunctiona2b1[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 2, 1
        };

    electricScalarPotentialMassFunctionNoDta2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_mass[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 2, 2
        };

    electricScalarPotentialStiffnessFunctiona2b2[DOM_terminalContactLayerSurface] = TSA_constantMaterial_constantThickness_stiffness[]{
            th_terminal_k, INPUT_terminalContactLayerConductivity, 2, 2
        };


    // Transition layer conductivity ends ===============================================


    // Winding thermal conductivity starts =============================================
    // Nonlinear:
    kappa[DOM_allWindings] = WindingThermalConductivityV1[
        XYZ[],
        $2,
        $1
    ]{   1 ,
        3, // N of materials,
        0,
        1,
        2,
        0.2143,
        0.5715,
        0.1428,
        100.0,
        100,
        100.0,
        295.0,
        295,
        295.0,
        0,
        0.02,
        100.0,
        295.0,
        0.005,
        0.000275,
        0,
        0.0,
        26,
        1,
        0.004,
        0.01,
        0.8727272727272727,
        1 // arbitrary jCritical scaling normal to winding
    };
    // Winding thermal conductivity ends ===============================================

    // Terminals thermal conductivity starts ===========================================
    kappa[DOM_transitionNotchVolumes] = 100.0;

    // Nonlinear:
    kappa[DOM_terminals] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0};
    // Terminals thermal conductivity ends =============================================

    // Insulation thermal conductivity starts ==========================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionNoDta1b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona1b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta1b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona1b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta2b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona2b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta2b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona2b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};
    // Insulation thermal conductivity ends ============================================

    // Transition layer thermal conductivity starts ====================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionNoDta1b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona1b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta1b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona1b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta2b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona2b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta2b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona2b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};
    
    // Transition layer thermal conductivity ends ======================================
    
    // Cryocooler thermal conductivity starts ====================================
    // Linear:
    INPUT_cryocoolerThermalConductivity = 100.0; // thermal conductivity of the insulation, [W/*(m*K)]

    thermalMassFunctionNoDta1b1[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 1
        };

    thermalStiffnessFunctiona1b1[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 1
        };

    thermalMassFunctionNoDta1b2[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 2
        };

    thermalStiffnessFunctiona1b2[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 2
        };

    thermalMassFunctionNoDta2b1[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 1
        };

    thermalStiffnessFunctiona2b1[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 1
        };

    thermalMassFunctionNoDta2b2[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 2
        };

    thermalStiffnessFunctiona2b2[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 2
        };

    // Cryocooler thermal conductivity ends ======================================

    // Winding specific heat capacity starts ===========================================
    // Nonlinear:
    CvCopper[] =  CFUN_CvCu_T[$1];
    CvHastelloy[] =  CFUN_CvHast_T[$1];
    CvSilver[] =  CFUN_CvAg_T[$1];

    Cv[DOM_allWindings] = 0.8727272727272727 * RuleOfMixtures[
        CvCopper[$1],
        CvHastelloy[$1],
        CvSilver[$1]
    ]{
        INPUT_relativeThicknessCopper,
        INPUT_relativeThicknessHastelloy,
        INPUT_relativeThicknessSilver
    };
    // Winding specific heat capacity ends =============================================

    // Terminals specific heat capacity starts =========================================
    Cv[DOM_transitionNotchVolumes] = 150.0;

    // Nonlinear:
    Cv[DOM_terminals] =  CFUN_CvCu_T[$1];
    // Terminals specific heat capacity ends ===========================================
    
    // Insulation specific heat capacity starts ========================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionDta1b1[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta1b2[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta2b1[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta2b2[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};

    // Insulation specific heat capacity ends ==========================================

    // Transition layer specific heat capacity starts ==================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionDta1b1[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta1b2[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta2b1[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta2b2[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};
    // Transition layer specific heat capacity ends ====================================
    
    // Cryocooler specific heat capacity starts ==================================
    // Nonlinear:
    thermalMassFunctionDta1b1[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{1, 1, oneDGaussianOrder};
    thermalMassFunctionDta1b2[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{1, 2, oneDGaussianOrder};
    thermalMassFunctionDta2b1[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{2, 1, oneDGaussianOrder};
    thermalMassFunctionDta2b2[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{2, 2, oneDGaussianOrder};

    // Cryocooler specific heat capacity ends ====================================

    // Shell transformation parameters:
    INPUT_shellInnerRadius = 0.015; // inner radius of the shell, [m]
    INPUT_shellOuterRadius = 0.020999999999999998; // outer radius of the shell, [m]

    // EE circuit quantities
    Resistance[DOM_resistanceCrowbar] = 0.1;
    Resistance[DOM_resistanceEE] = 0.1;

    Resistance[DOM_switchCrowbar] = $quenchDetected == 0? 1000000.0: 1e-06;

    Resistance[DOM_switchEE] = $quenchDetected == 0? 1e-06: 1000000.0;

    Inductance[DOM_inductanceEE] = 0.001;
    // Cryocooler quantities

    // Division by area to compute Watts per meter squared
    // SurfaceArea function does not allow DOM_*** as argument, so we need to use
    // the actual ids
    cryocoolerCoolingPower[] = 
        (0.8 * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_T[$1] - 28.0)/ GetVariable[]{$areaCryocooler} ; 

    cryocoolerCoolingPowerDerivativeT[] =      
        0.8 * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_dT_T[$1]/ GetVariable[]{$areaCryocooler}; 
}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
            {
                Region DOM_airInf;
                Jacobian VolCylShell {INPUT_shellInnerRadius, INPUT_shellOuterRadius};
            }
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
                Region DOM_currentSource;
                Type Assign;
                Value 1;


                TimeFunction current[$Time];
            }
        }
    }
    {
        // Impose current:
        Name CONSTRAINT_voltage;
        Case{
            {
                Region DOM_airHoleCut;
                Type Assign;
                Value 0;
            }
        }
    }
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
            {
                Region Region[
                            {
                                DOM_powered, 
                                DOM_allInsulationSurface
                            }
                        ];
                Type Init;
                Value 77.0;
            }
        }
    }
    {
        Name CONSTRAINT_electricScalarPotential;
        Case{
            {
                Region DOM_topTerminalSurface;
                Type Assign;
                Value 1;
            }
            {
                Region DOM_bottomTerminalSurface;
                Type Assign;
                Value 0;
            }
        }
    }

    { Name EECircuit_Netlist; Type Network;
        Case Circuit1 { // Describes node connection of branches
        // power source
        { Region DOM_currentSource;  Branch {1, 2} ; }

        // crowbar resistance and switch in series
        { Region DOM_resistanceCrowbar; Branch {2, 3} ; }
        { Region DOM_switchCrowbar; Branch {3, 1} ; }

        // energy extraction and switch in parallel
        { Region DOM_resistanceEE;  Branch {2, 4} ; }
        { Region DOM_switchEE;  Branch {2, 4} ; }

        // magnet current via terminal cut
        { Region DOM_terminalCut; Branch {4, 5} ; }

        // inductance in series with pancake coil
        { Region DOM_inductanceEE; Branch {5, 1} ; }
        }
    }
}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
                Support Region[{DOM_total, DOM_allInsulationSurface}];
                Entity NodesOf[{DOM_Phi, DOM_insulationSurface}, Not {DOM_insulationBoundaryCurvesAir}];
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
                Entity EdgesOf[All, Not {DOM_pancakeBoundary, DOM_allInsulationSurface}];
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
                Support Region[{DOM_total, DOM_terminalContactLayerSurface}];
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_1,
                                DOM_terminalContactLayerSurface_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_0, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_1,
                                DOM_terminalContactLayerSurface_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_0, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_2,
                                DOM_terminalContactLayerSurface_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_1, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_2,
                                DOM_terminalContactLayerSurface_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_1, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_3,
                                DOM_terminalContactLayerSurface_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_2, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_3,
                                DOM_terminalContactLayerSurface_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_2, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_4,
                                DOM_terminalContactLayerSurface_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_3, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_4,
                                DOM_terminalContactLayerSurface_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_3, DOM_insulationSurface }
                    ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contribution on boundary of the thin layer
            {
                Name BASISFUN_gradpsinBnd;
                NameOfCoef phin_bnd;
                Function BF_GradNode;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_Phi,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity NodesOf[DOM_insulationBoundaryCurvesAir];
            }
            {
                Name BASISFUN_psieBnd;
                NameOfCoef psie_bnd;
                Function BF_Edge;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity EdgesOf[DOM_insulationBoundaryCurvesTerminal];
            }
            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef he~{i};
                    Function BF_Edge;
                    Support DOM_terminalContactLayerSurface;
                    Entity EdgesOf[ All, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal, DOM_insulationSurface } ];
                }
            EndFor
        }

        SubSpace{
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd,
                    BASISFUN_gradpsin
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd,
                    BASISFUN_gradpsin
                };
            }



            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_sc,
                        BASISFUN_gradpsinBnd,
                        BASISFUN_psieBnd,
                        BASISFUN_gradpsin
                    };
                }

            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {
                NameOfCoef GLOBALQUANT_V;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_voltage;
            }
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_terminalSurfaces 
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface
                }];
                Entity NodesOf[DOM_thermal, 
                    Not {DOM_allInsulationSurface}];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingSurfaceMinus_1,
                                DOM_windingSurfaceMinus_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingSurfacePlus_1,
                                DOM_windingSurfacePlus_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingSurfaceMinus_2,
                                DOM_windingSurfaceMinus_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingSurfacePlus_2,
                                DOM_windingSurfacePlus_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingSurfaceMinus_3,
                                DOM_windingSurfaceMinus_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingSurfacePlus_3,
                                DOM_windingSurfacePlus_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingSurfaceMinus_4,
                                DOM_windingSurfaceMinus_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingSurfacePlus_4,
                                DOM_windingSurfacePlus_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
                {
                    Name BASISFUN_snPlus_not_connected_1;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_1;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_1,
                                    DOM_insulationSurface_2
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_1,
                        Not DOM_insulationSurface_0
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_2;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_2;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_2,
                                    DOM_insulationSurface_3
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_2,
                        Not DOM_insulationSurface_1
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_3;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_3;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_3,
                                    DOM_insulationSurface_4
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_3,
                        Not DOM_insulationSurface_2
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_4;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_4;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_4,
                                    DOM_insulationSurface_5
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_4,
                        Not DOM_insulationSurface_3
                        ];
                }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
                
                {
                    Name BASISFUN_snMinus_not_connected_1;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_1;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_1,
                                    DOM_insulationSurface_2
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_1,
                                Not DOM_insulationSurface_0
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_2;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_2;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_2,
                                    DOM_insulationSurface_3
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_2,
                                Not DOM_insulationSurface_1
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_3;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_3;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_3,
                                    DOM_insulationSurface_4
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_3,
                                Not DOM_insulationSurface_2
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_4;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_4;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_4,
                                    DOM_insulationSurface_5
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_4,
                                Not DOM_insulationSurface_3
                                ];
                }
                

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            // Cryocooler TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name BASISFUN_snOutside;
                NameOfCoef BASISFUN_snOutside_coeff;
                Function BF_Node;
                Support DOM_terminalSurfaces;
                Entity NodesOf[ DOM_terminalSurfaces ];
            }

            // Cryocooler TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            {
                Name BASISFUN_snBndTerminal;
                NameOfCoef Tn_Bnd;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_allInsulationSurface
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface, DOM_allWindingSurface
                }];
                Entity NodesOf[DOM_insulationBoundaryCurvesTerminal];
            }

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    Support DOM_terminalContactLayerSurface;
                    Entity NodesOf[ All, Not DOM_insulationBoundaryCurvesTerminal ];
                }

                    {
                        Name BASISFUN_snMinus_adiabatic~{i};
                        NameOfCoef snMinus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
                    {
                        Name BASISFUN_snPlus_adiabatic~{i};
                        NameOfCoef snPlus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
            EndFor

            For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
                {
                    Name BASISFUN_cryocooler_sn~{i};
                    NameOfCoef sn_cryocooler~{i};
                    Function BF_Node;
                    Support DOM_terminalSurfaces;
                    Entity NodesOf[ All ];
                }
            EndFor
            
        }
        SubSpace {
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_snBndTerminal
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_snBndTerminal
                };
            }

            {
                Name SUBSPACE_adiabatic_minus_up;
                NameOfBasisFunction{
                BASISFUN_snMinus_1,
                BASISFUN_snMinus_2,
                BASISFUN_snMinus_3,
                BASISFUN_snMinus_4
                };
            }
            {
                Name SUBSPACE_adiabatic_minus_down;
                NameOfBasisFunction{
                BASISFUN_snMinus_not_connected_1,
                BASISFUN_snMinus_not_connected_2,
                BASISFUN_snMinus_not_connected_3,
                BASISFUN_snMinus_not_connected_4
                };
            }

            {
                Name SUBSPACE_adiabatic_plus_up;
                NameOfBasisFunction{
                BASISFUN_snPlus_1,
                BASISFUN_snPlus_2,
                BASISFUN_snPlus_3,
                BASISFUN_snPlus_4
                };
            }
            {
                Name SUBSPACE_adiabatic_plus_down;
                NameOfBasisFunction{
                BASISFUN_snPlus_not_connected_1,
                BASISFUN_snPlus_not_connected_2,
                BASISFUN_snPlus_not_connected_3,
                BASISFUN_snPlus_not_connected_4
                };
            }


            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_snBndTerminal
                    };
                }

            {
                Name SUBSPACE_adiabatic_plus_tsa~{i}; 
                NameOfBasisFunction {
                    BASISFUN_snPlus_adiabatic~{i}
                };
            }
            {
                Name SUBSPACE_adiabatic_minus_tsa~{i}; 
                NameOfBasisFunction {
                    BASISFUN_snMinus_adiabatic~{i}
                };
            }
            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Cryocooler TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the cryocooler lumped mass:
            {
                Name SUBSPACE_cryocooler_inside;
                NameOfBasisFunction{
                    BASISFUN_sn
                };
            }
            {
                Name SUBSPACE_cryocooler_outside;
                NameOfBasisFunction{
                    BASISFUN_snOutside
                };
            }

            For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
                {
                    Name SUBSPACE_cryocooler~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_cryocooler_sn~{i}
                    };
                }
            EndFor

            // Cryocooler TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    For i In {1:INPUT_NumOfTSAElements - 1}
        {
            NameOfCoef sn~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }

            {
                NameOfCoef snMinus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef snPlus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
    EndFor
    
    {
        NameOfCoef Tn_Bnd;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    {
        NameOfCoef BASISFUN_snOutside_coeff;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
        {
            NameOfCoef sn_cryocooler~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }
    EndFor
        }
    }

    {
        Name SPACE_electricScalarPotential;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef sn;
                Function BF_Node;
                Support Region[{DOM_terminals, DOM_allWindings
 
                }];
                Entity NodesOf[All
                    , Not DOM_allInsulationSurface
                ];
            }
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef snMinus_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingMinus_electricScalarPotential_1,
                                DOM_windingMinus_electricScalarPotential_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0 }
                ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef snPlus_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingPlus_electricScalarPotential_1,
                                DOM_windingPlus_electricScalarPotential_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0 }
                ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef snMinus_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingMinus_electricScalarPotential_2,
                                DOM_windingMinus_electricScalarPotential_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1 }
                ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef snPlus_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingPlus_electricScalarPotential_2,
                                DOM_windingPlus_electricScalarPotential_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1 }
                ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef snMinus_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingMinus_electricScalarPotential_3,
                                DOM_windingMinus_electricScalarPotential_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2 }
                ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef snPlus_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingPlus_electricScalarPotential_3,
                                DOM_windingPlus_electricScalarPotential_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2 }
                ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef snMinus_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingMinus_electricScalarPotential_4,
                                DOM_windingMinus_electricScalarPotential_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3 }
                ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef snPlus_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingPlus_electricScalarPotential_4,
                                DOM_windingPlus_electricScalarPotential_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3 }
                ];
            }

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    Support DOM_terminalContactLayerSurface;
                    Entity NodesOf[ All ];
                }
            EndFor

    
   
            
        }

        Constraint {
            { NameOfCoef sn ;
                EntityType NodesOf ; NameOfConstraint CONSTRAINT_electricScalarPotential ; }
        }

        SubSpace {
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4
                };
            }



            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i}
                    };
                }

            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
    }
    { Name SPACE_circuit; Type Scalar;
        BasisFunction {
            {
                Name BASISFUN_ICircuit ; NameOfCoef ICircuit_coeff ; Function BF_Region ;
                    Support DOM_circuit ; Entity DOM_circuit ;
          }
        }

        GlobalQuantity {
            { Name ICircuit; Type AliasOf       ; NameOfCoef ICircuit_coeff; }
            { Name UCircuit; Type AssociatedWith; NameOfCoef ICircuit_coeff; }
        }

        Constraint {
             { NameOfCoef ICircuit; EntityType Region; NameOfConstraint CONSTRAINT_current; }
        }
    }

}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
    {
        Name FORMULATION_electromagnetic;
        Type FemEquation;
        Quantity{
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_T;
                Type Local;
                NameOfSpace SPACE_temperature;
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                // Different test function is needed for non-symmetric tensors, otherwise, getdp
                // assumes the tensors are symmetric and the result is wrong.
                Name LOCALQUANT_h_Derivative;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name GLOBALQUANT_I;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
            }
            {
                Name GLOBALQUANT_V;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_minus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_minus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_minus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_plus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_plus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_plus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_hThinShell~{0};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_down];
            }

            For i In {1:INPUT_NumOfTSAElements-1}
                {
                    Name LOCALQUANT_hThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_hPhi[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_hThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_up];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // EECircuit quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {   
                Name CIRCUITQUANT_ICircuit; 
                Type Global; 
                NameOfSpace SPACE_circuit[ICircuit]; 
            }
            {   
                Name CIRCUITQUANT_UCircuit; 
                Type Global; 
                NameOfSpace SPACE_circuit[UCircuit]; 
            }		   

            // EECircuit quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // Cryocooler TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_cryocooler_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_inside];
            }

            For i In {1:INPUT_NumOfCryocoolerTSAElements-1}
                {
                    Name LOCALQUANT_cryocooler_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_cryocooler~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_outside];
            }

            // Cryocooler TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }

        Equation{
            Integral{
                // note that it is only defined in DOM_allConducting, not all domain
                [ rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h}, {d LOCALQUANT_h} ];
                In DOM_allConducting;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral{
                DtDof[mu[] * Dof{LOCALQUANT_h}, {LOCALQUANT_h}];
                In DOM_total;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral
            {
                JacNL[d_of_rho_wrt_j_TIMES_j[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_h} , {d LOCALQUANT_h_Derivative} ];
                In DOM_allWindings; 
                Jacobian JAC_vol;
                Integration Int; 
            }
            // the global term allows to link current and voltage in the cuts
            GlobalTerm{
                [ Dof{GLOBALQUANT_V}, {GLOBALQUANT_I} ];
                In DOM_airCuts;
            }

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   electromagneticMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 1 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   electromagneticMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   electromagneticStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            electromagneticMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_hThinShell~{i + 2 - 1}},
                            {LOCALQUANT_hThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

                // U = R * I for resistor as global terms
                GlobalTerm {  [Dof{CIRCUITQUANT_UCircuit}, {CIRCUITQUANT_ICircuit}] ; In DOM_circuitResistance; }
                GlobalTerm { [Resistance[] * Dof{CIRCUITQUANT_ICircuit}, {CIRCUITQUANT_ICircuit} ]; In DOM_circuitResistance; }

                // U = L * dI/dt for inductor as global terms
                GlobalTerm{ [ Dof{CIRCUITQUANT_UCircuit}, {CIRCUITQUANT_ICircuit} ];  In DOM_circuitInductance; }
                GlobalTerm{ DtDof[ Inductance[] * Dof{CIRCUITQUANT_ICircuit}, {CIRCUITQUANT_ICircuit} ];  In DOM_circuitInductance; }

                // Circuital equations related to circuit netlist
                GlobalEquation{
                    Type Network; NameOfConstraint EECircuit_Netlist;
                    { Node {GLOBALQUANT_I}; Loop {GLOBALQUANT_V}; Equation {GLOBALQUANT_V}; In DOM_terminalCut; }
                    { Node {CIRCUITQUANT_ICircuit}; Loop {CIRCUITQUANT_UCircuit}; Equation {CIRCUITQUANT_UCircuit}; In DOM_circuit; }
                }
              

        }
    }

    {
        Name FORMULATION_thermal;
        Type FemEquation;
        Quantity {
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_T;
                Type Local;
                NameOfSpace SPACE_temperature;
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                // Different test function is needed for non-symmetric tensors, otherwise, getdp
                // assumes the tensors are symmetric and the result is wrong.
                Name LOCALQUANT_h_Derivative;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name GLOBALQUANT_I;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_I];
            }
            {
                Name GLOBALQUANT_V;
                Type Global;
                NameOfSpace SPACE_hPhi[GLOBALQUANT_V];
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_minus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_minus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_minus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_plus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_plus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_plus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_hThinShell~{0};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_down];
            }

            For i In {1:INPUT_NumOfTSAElements-1}
                {
                    Name LOCALQUANT_hThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_hPhi[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_hThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_hPhi[SUBSPACE_insulationSurface_up];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Cryocooler TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_cryocooler_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_inside];
            }

            For i In {1:INPUT_NumOfCryocoolerTSAElements-1}
                {
                    Name LOCALQUANT_cryocooler_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_cryocooler~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_outside];
            }

            // Cryocooler TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        
        Equation{
            Integral {
                [ kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001] * Dof{d LOCALQUANT_T}, {d LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral {
                DtDof[ Cv[{LOCALQUANT_T}, Norm[UnitVectorZ[] * 0.001]] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral {
                [ -imposedPowerDensity[XYZ[], $Time], {LOCALQUANT_T} ];
                In DOM_allWindings;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral {
                [ cryocoolerCoolingPower[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}], {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}} ];
                In DOM_terminalSurfaces;
                Jacobian JAC_sur;
                Integration Int;
            }

            // This Jacobian entry is extremely important for the convergence of the nonlinear solver
            Integral {
                JacNL [ cryocoolerCoolingPowerDerivativeT[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}] * Dof{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}, {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}} ];
                In DOM_terminalSurfaces;
                Jacobian JAC_sur;
                Integration Int;
            }

            Integral {
                [ 100000.0 * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
                In DOM_allConvectiveSurface;
                Jacobian JAC_sur;
                Integration Int;
            }
            Integral {
                [ - 100000.0 * 70.0, {LOCALQUANT_T} ];
                In DOM_allConvectiveSurface;
                Jacobian JAC_sur;
                Integration Int;
            }


            Integral {
                [ -(rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}, {LOCALQUANT_T} ];
                In DOM_resistiveHeating;
                Jacobian JAC_vol;
                Integration Int;
            }

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfTSAElements-1}
             // row of the 1D FE matrix
                Integral {
                    [
                        - electromagneticRHSFunctionk1[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * SquNorm[
                            ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
                        ],
                        {LOCALQUANT_TThinShell~{i + 1 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                } 

                Integral {
                    [
                        -electromagneticTripleFunctionk1a1b1[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}},
                        {LOCALQUANT_TThinShell~{i + 1 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk1a1b2[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}},
                        {LOCALQUANT_TThinShell~{i + 1 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk1a2b1[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}},
                        {LOCALQUANT_TThinShell~{i + 1 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk1a2b2[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}},
                        {LOCALQUANT_TThinShell~{i + 1 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
             // row of the 1D FE matrix
                Integral {
                    [
                        - electromagneticRHSFunctionk2[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * SquNorm[
                            ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
                        ],
                        {LOCALQUANT_TThinShell~{i + 2 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                } 

                Integral {
                    [
                        -electromagneticTripleFunctionk2a1b1[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}},
                        {LOCALQUANT_TThinShell~{i + 2 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk2a1b2[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}},
                        {LOCALQUANT_TThinShell~{i + 2 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk2a2b1[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}},
                        {LOCALQUANT_TThinShell~{i + 2 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
                Integral {
                    [
                        -electromagneticTripleFunctionk2a2b2[
                            {LOCALQUANT_TThinShell~{i}},
                            {LOCALQUANT_TThinShell~{i+1}}
                        ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}},
                        {LOCALQUANT_TThinShell~{i + 2 - 1}}
                    ];
                    In DOM_terminalContactLayerSurface;
                    Integration Int;
                    Jacobian JAC_sur;
                }
            EndFor

            

            For i In {0:INPUT_NumOfCryocoolerTSAElements-1}
                    Integral {
                        [   
                            thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

        }
    }


      { Name FORMULATION_electricScalarPotential; Type FemEquation;
        Quantity {
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_electricScalarPotential;
                Type Local;
                NameOfSpace SPACE_electricScalarPotential;
            }
            {
                Name LOCALQUANT_h;
                Type Local;
                NameOfSpace SPACE_hPhi;
            }
            {
                Name LOCALQUANT_T;
                Type Local;
                NameOfSpace SPACE_temperature;
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_electricScalarPotentialThinShell~{0};
                Type Local;
                NameOfSpace SPACE_electricScalarPotential[SUBSPACE_insulationSurface_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_electricScalarPotentialThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_electricScalarPotential[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_electricScalarPotentialThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_electricScalarPotential[SUBSPACE_insulationSurface_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
  
        Equation {
            
            Integral { [electricScalarPotential_scalingFactor/rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * Dof{d LOCALQUANT_electricScalarPotential} , {d LOCALQUANT_electricScalarPotential} ];
            In DOM_allConducting; Jacobian JAC_vol; Integration Int; }


            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}},
                            {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}},
                            {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}},
                            {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [    electricScalarPotential_scalingFactor *                 electricScalarPotentialStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}},
                            {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface_WithoutNotch;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

            EndFor

        }
      }


}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_weaklyCoupled;
        System{
            {
                Name SYSTEM_thermal;
                NameOfFormulation FORMULATION_thermal;
            }
            {
                Name SYSTEM_electromagnetic;
                NameOfFormulation FORMULATION_electromagnetic;
            }
            {
                Name SYSTEM_electricScalarPotential;
                NameOfFormulation FORMULATION_electricScalarPotential;
            }
        }

        Operation{
            Evaluate[SetVariable[SurfaceArea[]{2000013, 2000012 }]{$areaCryocooler}];
            InitSolution[SYSTEM_electromagnetic];
            SaveSolution[SYSTEM_electromagnetic];
            InitSolution[SYSTEM_thermal];
            SaveSolution[SYSTEM_thermal];
            SetExtrapolationOrder[INPUT_extrapolationOrder];


            TimeLoopAdaptive[
                INPUT_tStart,
                INPUT_tEnd,
                INPUT_tAdaptiveInitStep,
                INPUT_tAdaptiveMinStep,
                INPUT_tAdaptiveMaxStep,
                "Euler",
                List[INPUT_tAdaptiveBreakPoints],
                PostOperation{
                    {
                        POSTOP_CONV_voltageBetweenTerminals,
                        0.1,
                        0.05,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfCurrentDensity,
                        0.1,
                        16000000.0,
                        LinfNorm
                    }
                    {
                        POSTOP_CONV_magnitudeOfMagneticField,
                        0.1,
                        0.002,
                        LinfNorm
                    }
                }

            ]{
                // Nonlinear solver starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                IterativeLoopN[
                    INPUT_NLSMaxNumOfIter,
                    INPUT_NLSRelaxFactor,
                    PostOperation{
                        {
                            POSTOP_CONV_voltageBetweenTerminals,
                            0.0001,
                            0.001,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfCurrentDensity,
                            0.0001,
                            16000.0,
                            LinfNorm
                        }
                        {
                            POSTOP_CONV_magnitudeOfMagneticField,
                            0.0001,
                            0.001,
                            LinfNorm
                        }
                    }

                ]{
                    GenerateJac SYSTEM_electromagnetic;
                    SolveJac SYSTEM_electromagnetic;

                    GenerateJac SYSTEM_thermal;
                    SolveJac SYSTEM_thermal;

                }
                // Check if the solution is NaN and remove it
                Test[$KSPResidual != $KSPResidual]{
                    Print["Critical: Removing NaN solution from the solution vector."];
                    RemoveLastSolution[SYSTEM_electromagnetic];
                    RemoveLastSolution[SYSTEM_thermal];
                }
                // Nonlinear solver ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            }{
                    Generate SYSTEM_electricScalarPotential;
                    Solve SYSTEM_electricScalarPotential;
                    SaveSolution SYSTEM_electricScalarPotential;

                        PostOperation[POSTOP_electricScalarPotential_weaklyCoupled];
                        PostOperation[POSTOP_electricScalarPotential];

                        Test[$quenchDetected == 0] {

                                Test[$surpassedVoltageThreshold_0 == 0 && Abs[$voltage_0] > 1.5e-05]{
                                    Evaluate[$surpassedVoltageThreshold_0 = 1];
                                    Evaluate[$surpassedVoltageThresholdTime_0 = $Time];
                                    Print[{$surpassedVoltageThresholdTime_0}, Format "Critical: surpassed dection voltage in voltage tap pair no. 0 at t = %.12g s"];
                                }
                    
                                Test[Abs[$voltage_0] <= 1.5e-05]{
                                    Evaluate[$surpassedVoltageThreshold_0 = 0];
                                }
                    
                                Test[$surpassedVoltageThreshold_0 == 1 && $Time - $surpassedVoltageThresholdTime_0 > 0.1]{
                                    Evaluate[$quenchDetected = 1];
                                    Evaluate[$quenchDetectionTime = $Time];

                                    Print[{$quenchDetectionTime}, Format "Critical: detected quench with voltage tap pair no. 0 at t = %.12g s"];
                                }

                                Test[$surpassedVoltageThreshold_1 == 0 && Abs[$voltage_1] > 0.1]{
                                    Evaluate[$surpassedVoltageThreshold_1 = 1];
                                    Evaluate[$surpassedVoltageThresholdTime_1 = $Time];
                                    Print[{$surpassedVoltageThresholdTime_1}, Format "Critical: surpassed dection voltage in voltage tap pair no. 1 at t = %.12g s"];
                                }
                    
                                Test[Abs[$voltage_1] <= 0.1]{
                                    Evaluate[$surpassedVoltageThreshold_1 = 0];
                                }
                    
                                Test[$surpassedVoltageThreshold_1 == 1 && $Time - $surpassedVoltageThresholdTime_1 > 1.5]{
                                    Evaluate[$quenchDetected = 1];
                                    Evaluate[$quenchDetectionTime = $Time];

                                    Print[{$quenchDetectionTime}, Format "Critical: detected quench with voltage tap pair no. 1 at t = %.12g s"];
                                }
                        }

                        Test[$quenchDetected == 1] {
                            PostOperation[POSTOP_I];
                            Test[$currentThresholdReached == 0 && $I < 0.2] {
                                Evaluate[$currentThresholdReached = 1];
                                Evaluate[$currentThresholdReachedTime = $Time];
                                Print[{$currentThresholdReachedTime}, Format "Critical: below stop simulation current at t = %.12g s"];
                            }

                            Test[$currentThresholdReached == 1 && $Time - $currentThresholdReachedTime > 1.5] {
                                Print["Critical: stop simulation due to low current"];
                                Break[];
                            }
                        }

                PostOperation[POSTOP_maximumTemperature];
                Test[#999 > 200.0] {
                    Print["Critical: stop simulation since maximum temperature surpassed threshold"];
                    Break[];
                }
                SaveSolution[SYSTEM_electromagnetic];
                SaveSolution[SYSTEM_thermal];

            }

            PostOperation[POSTOP_magneticField];
            PostOperation[POSTOP_currentDensity];
            PostOperation[POSTOP_temperature];
            PostOperation[POSTOP_timeSeriesPlot_voltageBetweenTerminals];
            PostOperation[POSTOP_timeSeriesPlot_currentThroughCoil];
            PostOperation[POSTOP_timeSeriesPlot_maximumTemperature];
            PostOperation[POSTOP_timeSeriesPlot_cryocoolerAveragePower];
            PostOperation[POSTOP_timeSeriesPlot_cryocoolerAverageTemperature];
            PostOperation[POSTOP_timeSeriesPlot_axialComponentOfTheMagneticField];



        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_weaklyCoupled;
        NameOfFormulation FORMULATION_electromagnetic;
        NameOfSystem SYSTEM_electromagnetic;
        Quantity{
            {
                Name RESULT_magneticField; // magnetic flux density (magnetic field)
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h} + UnitVectorZ[] * 0.001 ];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_imposedAxialField; // axial field
                Value{
                    Local{
                        [UnitVectorZ[] * 0.001];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_reactionField; // axial field
                Value{
                    Local{
                        [mu[] * {LOCALQUANT_h}];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfMagneticField; // magnetic flux density magnitude
                Value{
                    Local{
                        [Norm[mu[] * {LOCALQUANT_h}]];
                        In Region[{ DOM_total }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentDensity; // current density
                Value{
                    Local{
                        [{d LOCALQUANT_h}];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfCurrentDensity; // current density magnitude
                Value{
                    Local{
                        [Norm[{d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_resistivity; // current density magnitude
                Value{
                    Local{
                        [rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allConducting }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_arcLength;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength[XYZ[]]{
                            0.005,
                            0.000275,
                            0,
                            0.0,
                            26,
                            1,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_arcLengthContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousArcLength_contactLayer[XYZ[]]{
                            0.005,
                            0.000275,
                            0,
                            0.0,
                            26,
                            1,
                            0.004,
                            0.01
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {
                Name RESULT_turnNumberContactLayer;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber_contactLayer[XYZ[]]{
                            0.005,
                            0.000275,
                            0,
                            0.0,
                            26,
                            1,
                            0.004,
                            0.01
                        }];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
            {
                Name RESULT_turnNumber;
                Value{
                    Local{
                        [Pancake3DGetContinuousTurnNumber[XYZ[]]{
                            0.005,
                            0.000275,
                            0,
                            0.0,
                            26,
                            1,
                            0.004,
                            0.01
                        }];
                        In DOM_allWindings;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrentDensity; // critical current density of the winding
                Value{
                    Local{
                        [Jcritical[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_criticalCurrent;
                Value{
                    Local{
                        [Icritical[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_currentSharingIndex;
                Value{
                    Local{
                        [lambda[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTS;
                Value{
                    Local{
                        [jHTS[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_jHTSOverjCritical;
                Value{
                    Local{
                        // add small epsilon to avoid division by zero
                        [jHTS[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}]/(Jcritical[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] + 1e-10)];
                        In Region[{ DOM_allWindings }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magneticEnergy;
                Value{
                    Integral{
                        Type Global;
                        [1/2 * mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_inductance;
                Value{
                    Integral{
                        Type Global;
                        [mu[] * {LOCALQUANT_h} * {LOCALQUANT_h}];
                        In DOM_total;
                        Jacobian JAC_vol;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_resistiveHeating; // resistive heating
                Value{
                    Local{
                        [(rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In Region[{ DOM_resistiveHeating }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_voltageBetweenTerminals; // voltages in cuts
                Value{
                    Local{
                        [ - {GLOBALQUANT_V} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_currentThroughCoil; // currents in cuts
                Value{
                    Local{
                        [ {GLOBALQUANT_I} ];
                        In DOM_terminalCut;
                    }
                }
            }

            {
                Name RESULT_current_DOM_currentSource; // currents in cuts
                Value{
                    Local{
                        [ {CIRCUITQUANT_ICircuit} ];
                        In DOM_currentSource;
                    }
                }
            }

            {
                Name RESULT_resistance_DOM_switches; // currents in cuts
                Value{
                    Term {
                        Type Global;
                        [ Resistance[] ];
                        In DOM_switchCrowbar;
                    }
                    Term {
                        Type Global;
                        [ Resistance[] ];
                        In DOM_switchEE;
                    }
                }
            }

            {
                Name RESULT_axialComponentOfTheMagneticField; // axial magnetic flux density
                Value{
                    Local{
                        [ CompZ[mu[] * {LOCALQUANT_h}] ];
                        In DOM_total;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_totalResistiveHeating; // total resistive heating for convergence
                Value{
                    Integral{
                        Type Global;
                        [(rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * {d LOCALQUANT_h}) * {d LOCALQUANT_h}];
                        In DOM_resistiveHeating;
                        Jacobian JAC_vol;
                        Integration Int;
                    }

                        For i In {0:INPUT_NumOfTSAElements-1}
                            Integral {
                                Type Global;
                                [
                                    electromagneticOnlyFunction[
                                        {LOCALQUANT_TThinShell~{i}},
                                        {LOCALQUANT_TThinShell~{i+1}}
                                    ] * SquNorm[
                                        ({LOCALQUANT_hThinShell~{i + 1}} - {LOCALQUANT_hThinShell~{i}})/th_terminal_k
                                    ]
                                ];
                                In DOM_terminalContactLayerSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            } 
                        
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta1b1[
                                        {LOCALQUANT_TThinShell~{i}},
                                        {LOCALQUANT_TThinShell~{i+1}}
                                    ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                                ];
                                In DOM_terminalContactLayerSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta1b2[
                                        {LOCALQUANT_TThinShell~{i}},
                                        {LOCALQUANT_TThinShell~{i+1}}
                                    ] * {d LOCALQUANT_hThinShell~{i + 1 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                                ];
                                In DOM_terminalContactLayerSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta2b1[
                                        {LOCALQUANT_TThinShell~{i}},
                                        {LOCALQUANT_TThinShell~{i+1}}
                                    ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 1 - 1}}
                                ];
                                In DOM_terminalContactLayerSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                            Integral {
                                Type Global;
                                [
                                    electromagneticMassFunctionNoDta2b2[
                                        {LOCALQUANT_TThinShell~{i}},
                                        {LOCALQUANT_TThinShell~{i+1}}
                                    ] * {d LOCALQUANT_hThinShell~{i + 2 - 1}} * {d LOCALQUANT_hThinShell~{i + 2 - 1}}
                                ];
                                In DOM_terminalContactLayerSurface;
                                Integration Int;
                                Jacobian JAC_sur;
                            }
                        EndFor
 
                }
            }
            {
                Name RESULT_temperature;
                Value {
                    Local{
                        [{LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_maximumTemperature; // maximum temperature
                Value{
                    Term {
                        Type Global;
                        [ #999 ];
                        In DOM_powered;
                    }
                }
            }

            {
                Name RESULT_heatFlux;
                Value {
                    Local{
                        [-kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001] * {d LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfHeatFlux;
                Value {
                    Local{
                        [Norm[-kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001] * {d LOCALQUANT_T}]];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_thermalConductivity; // current density magnitude
                Value{
                    Local{
                        [kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_specificHeatCapacity; // current density magnitude
                Value{
                    Local{
                        [Cv[{LOCALQUANT_T}, Norm[UnitVectorZ[] * 0.001]]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_cryocoolerAveragePower; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;

                            [ cryocoolerCoolingPower[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}] ];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_cryocoolerAverageTemperature; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;
                            // Division by area to compute Watts per meter squared
                            // SurfaceArea function does not allow DOM_*** as argument, so we need to use
                            // the actual ids

                            [  {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}/ GetVariable[]{$areaCryocooler}];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }
 {
                Name RESULT_debug;
                Value{
                    Local{
                        [ 5 ];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
        }
    }

    {
        Name POSTPRO_electricScalarPotential;
        NameOfFormulation FORMULATION_electricScalarPotential;
        NameOfSystem SYSTEM_electricScalarPotential;
        Quantity{
            {
                Name RESULT_conductance;
                Value{
                    Integral{
                        Type Global;
                        [1./rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * {d LOCALQUANT_electricScalarPotential} * {d LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings}];
                        Jacobian JAC_vol;
                        Integration Int;
                    }

                    For i In {0:INPUT_NumOfTSAElements-1}


                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialMassFunctionNoDta1b1[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}} * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialStiffnessFunctiona1b1[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}} * {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }


                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialMassFunctionNoDta1b2[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}} * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialStiffnessFunctiona1b2[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}} * {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }


                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialMassFunctionNoDta2b1[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}} * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialStiffnessFunctiona2b1[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}} * {LOCALQUANT_electricScalarPotentialThinShell~{i + 1 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }


                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialMassFunctionNoDta2b2[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}} * {d LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                        Integral {
                            Type Global;
                            [
                                electricScalarPotentialStiffnessFunctiona2b2[
                                    {LOCALQUANT_TThinShell~{i}},
                                    {LOCALQUANT_TThinShell~{i+1}}
                                ] * {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}} * {LOCALQUANT_electricScalarPotentialThinShell~{i + 2 - 1}}
                            ];
                            In DOM_terminalContactLayerSurface_WithoutNotch;
                            Integration Int;
                            Jacobian JAC_sur;
                        }

                    EndFor

                }
            }
        
            {
                Name RESULT_electricScalarPotential;
                Value{
                    Local{
                        [{LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings  }];
                        Jacobian JAC_vol;
                    }
                }
            }
            {
                Name RESULT_currentFromElectricScalarPotential;
                Value{
                    Local{
                        [1./rho[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, {d LOCALQUANT_h}] * {d LOCALQUANT_electricScalarPotential}];
                        In Region[{DOM_terminals, DOM_allWindings  }];
                        Jacobian JAC_vol;
                    }
                }
            }


            {
                Name RESULT_resistiveVoltage_0; // voltages in cuts
                Value{
                    Term {
                    Type Global;
                        [ $currentThroughCoil/$conductance * ($potential_0 - $potential_1) ];
                        In DOM_dummyPrintResistiveVoltages;
                    }
                }
            }


            {
                Name RESULT_resistiveVoltage_1; // voltages in cuts
                Value{
                    Term {
                    Type Global;
                        [ $currentThroughCoil/$conductance * ($potential_2 - $potential_3) ];
                        In DOM_dummyPrintResistiveVoltages;
                    }
                }
            }

        }
    }
}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation {
        }
    }
 {
        Name POSTOP_debug;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_debug,
                OnElementsOf DOM_allInsulationSurface,
                File "debug-DefaultFormat.pos",
                Name "Debug Variable"
            ];

        }
    }
    {
        Name POSTOP_magneticField;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_magneticField,
                OnElementsOf DOM_total,
                File "MagneticField-DefaultFormat.pos",
                Name "Magnetic Field [T]"
            ];

        
            Print[
                RESULT_imposedAxialField,
                OnElementsOf DOM_total,
                File "ImposedMagneticField-DefaultFormat.pos",
                Name "Imposed Magnetic Field [T]"
            ];


        
            Print[
                RESULT_reactionField,
                OnElementsOf DOM_total,
                File "MagneticReactionField-DefaultFormat.pos",
                Name "Magnetic Reaction Field [T]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // 3D magnetic field magnitude scalar field:
            
            Print[
                RESULT_magnitudeOfMagneticField,
                OnElementsOf DOM_total,
                File "MagneticFieldMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_currentDensity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_currentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensity-DefaultFormat.pos",
                Name "Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // 3D current density vector field:
            
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnElementsOf DOM_allConducting,
                File "CurrentDensityMagnitude-DefaultFormat.pos",
                Name "The Magnitude of the Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_arcLength;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_arcLength,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "arcLength-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length [m]"
            ];

        }
    }
    {
        Name POSTOP_arcLengthContactLayer;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_arcLengthContactLayer,
                OnElementsOf DOM_allInsulationSurface,
                File "arcLengthContactLayer-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Arc Length Contact Layer [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumberContactLayer;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_turnNumberContactLayer,
                OnElementsOf DOM_allInsulationSurface,
                File "turnNumberContactLayer-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number contact layer [m]"
            ];

        }
    }
    {
        Name POSTOP_turnNumber;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_turnNumber,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "turnNumber-DefaultFormat.pos",
                LastTimeStepOnly 1,
                Name "Turn number [m]"
            ];

        }
    }
    {
        Name POSTOP_resistiveHeating;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "ResistiveHeating_Windings-DefaultFormat.pos",
                Name "Resistive Heating Windings [W/m^3]"
            ];


            
            Print[
                RESULT_resistiveHeating,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "ResistiveHeating-DefaultFormat.pos",
                Name "Resistive Heating Without Windings [W/m^3]"
            ];

        }
    }
    {
        Name POSTOP_resistivity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Resistive heating:
            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "Resistivity_Windings-DefaultFormat.pos",
                Name "Resistivity Windings [Ohm*m]"
            ];


            
            Print[
                RESULT_resistivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "Resistivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Resistivity Without Windings [Ohm*m]"
            ];

        }
    }
    {
        Name POSTOP_inductance;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_inductance,
                OnGlobal,
                File "Inductance-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                Name "Inductance [H]"
            ];

        }
    }
    {
        Name POSTOP_timeConstant;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_axialComponentOfTheMagneticField,
                OnPoint {0, 0, 0},
                File "axialComponentOfTheMagneticFieldForTimeConstant-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "The Magnitude of the Magnetic Field [T]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrentDensity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Critical current density:
            
            Print[
                RESULT_criticalCurrentDensity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "CriticalCurrentDensity-DefaultFormat.pos",
                Name "Critical Current Density [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_criticalCurrent;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Critical current:
            
            Print[
                RESULT_criticalCurrent,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "CriticalCurrent-DefaultFormat.pos",
                Name "Critical Current [A]"
            ];

        }
    }
    {
        Name POSTOP_currentSharingIndex;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Critical current:
            
            Print[
                RESULT_currentSharingIndex,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "currentSharingIndex-DefaultFormat.pos",
                Name "Current Sharing Index [-]"
            ];

        }
    }
    {
        Name POSTOP_jHTS;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Current density in HTS layer:
            
            Print[
                RESULT_jHTS,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "jHTS-DefaultFormat.pos",
                Name "Current Density in HTS Layer [A/m^2]"
            ];

        }
    }
    {
        Name POSTOP_jHTSOverjCritical;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Normalized HTS current density:
            
            Print[
                RESULT_jHTSOverjCritical,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "HTSCurrentDensityOverCriticalCurrentDensity-DefaultFormat.pos",
                Name "(HTS Current Density)/(Critical Current Density)"
            ];

        }
    }
    // {
    //     Name POSTOP_Ic;
    //     NameOfPostProcessing POSTPRO_weaklyCoupled;
    //     LastTimeStepOnly 1;
    //     Operation {
    //         Print[
    //             RESULT_criticalCurrent,
    //             OnElementsOf DOM_allWindings,
    //             StoreMinInRegister 1
    //         ];
    //     }
    // }
    {
        Name POSTOP_I;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                Format Table,
                StoreInVariable $I
            ];
        }
    }
    {
        Name POSTOP_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Voltage at the cut:
            
            Print[
                RESULT_voltageBetweenTerminals,
                OnRegion DOM_terminalCut,
                File "VoltageBetweenTerminals-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Voltage [V]"
            ];

        }
    }
    {
        Name POSTOP_currentThroughCoil;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Current at the cut:
            
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                File "CurrentThroughCoil-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "Current [A]"
            ];

        }
    }
    {
        Name POSTOP_maximumTemperature;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation {
            Print[ RESULT_temperature, 
                    OnElementsOf DOM_powered, 
                    StoreMaxInRegister 999, 
                    Format Table,
                    LastTimeStepOnly 1, 
                    SendToServer "No",
                    File "maximumTemperature_dump.txt"
                ] ;
            // We can print the maximum temperature at any region that is part
            // of the thermal domain since the `StoreMaxInRegister` command
            // already searches all of the thermal region for the maximum and
            //populates the same value for all physical regions of the thermal 
            // domain.
            // Printing in just one domain makes the parsing of the output easier.
            
            Print[
                RESULT_maximumTemperature,
                OnRegion Region[1000000],
                File "maximumTemperature(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Maximum temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_temperature;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_temperature,
                OnElementsOf DOM_thermal,
                File "Temperature-DefaultFormat.pos",
                Name "Temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_heatFlux;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_heatFlux,
                OnElementsOf DOM_thermal,
                File "HeatFlux-DefaultFormat.pos",
                Name "Heat Flux [W/m^2]"
            ];

        }
    }
    {
        Name POSTOP_thermalConductivity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Thermal conductivity:
            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "thermalConductivity_Windings-DefaultFormat.pos",
                Name "Thermal Conductivity Windings [W/(m*K)]"
            ];


            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "thermalConductivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Thermal Conductivity Without Windings [W/(m*K)]"
            ];

        }
    }
    {
        Name POSTOP_specificHeatCapacity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            // Specific heat:
            
            Print[
                RESULT_specificHeatCapacity,
                OnElementsOf DOM_thermal,
                File "specificHeatCapacity-DefaultFormat.pos",
                Name "Specific Heat Capacity [J/(kg*K)]"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_voltageBetweenTerminals,
                OnRegion DOM_terminalCut,
                File "voltageBetweenTerminals(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "voltageBetweenTerminals(TimeSeriesPlot)"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_currentThroughCoil;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                File "currentThroughCoil(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "currentThroughCoil(TimeSeriesPlot)"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_maximumTemperature;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
        }
    }
    {
        Name POSTOP_timeSeriesPlot_cryocoolerAveragePower;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_cryocoolerAveragePower,
                OnGlobal,
                File "cryocoolerAveragePower(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "cryocoolerAveragePower(TimeSeriesPlot)"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_cryocoolerAverageTemperature;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_cryocoolerAverageTemperature,
                OnGlobal,
                File "cryocoolerAverageTemperature(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "cryocoolerAverageTemperature(TimeSeriesPlot)"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_axialComponentOfTheMagneticField;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        Operation{
            
            Print[
                RESULT_axialComponentOfTheMagneticField,
                OnPoint {0.0, 0.0, 0.0},
                File "axialComponentOfTheMagneticField(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "axialComponentOfTheMagneticField(TimeSeriesPlot)"
            ];

        }
    }
    // convergence criteria as postoperations:
    {
        Name POSTOP_CONV_voltageBetweenTerminals;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[RESULT_voltageBetweenTerminals, OnRegion DOM_terminalCut];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfCurrentDensity;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfCurrentDensity,
                OnPoint {-0.001885608513462981, -0.005829253922922831, 0.0},
                StoreInVariable $test
            ];
        }
    }
    {
        Name POSTOP_CONV_magnitudeOfMagneticField;
        NameOfPostProcessing POSTPRO_weaklyCoupled;
        LastTimeStepOnly 1;
        Operation {
            Print[
                RESULT_magnitudeOfMagneticField,
                OnPoint {0.0, 0.0, 0.0},
                StoreInVariable $test
            ];
        }
    }

{
    Name POSTOP_electricScalarPotential_weaklyCoupled;
    NameOfPostProcessing POSTPRO_weaklyCoupled;
    Operation {
        
            Print[
                RESULT_currentThroughCoil,
                OnRegion DOM_terminalCut,
                StoreInVariable $currentThroughCoil,
                File "CurrentThroughCoil_quenchDetection-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Current [A]"
            ];


        
            Print[
                RESULT_current_DOM_currentSource,
                OnRegion DOM_currentSource,
                File "Current_DOM_currentSource-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Current [A]"
            ];


        
            Print[
                RESULT_resistance_DOM_switches,
                OnRegion Region[{DOM_switchCrowbar, DOM_switchEE}],
                File "Resistance_DOM_switches-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Current [A]"
            ];

    }
}

{
    Name POSTOP_electricScalarPotential;
    NameOfPostProcessing POSTPRO_electricScalarPotential;
    Operation {

        
            Print[
                RESULT_conductance,
                OnGlobal,
                StoreInVariable $conductance,
                File "conductance-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Conductance [S]"
            ];



            
            Print[
                RESULT_electricScalarPotential,
                OnPoint {0.00513663651706879, 1.538545427702476e-05, 0.0},
                StoreInVariable $potential_0,
                File "electricScalarPotential_0-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Potential at point [0.00513663651706879, 1.538545427702476e-05, 0.0] [V]"
            ];

            
            Print[
                RESULT_electricScalarPotential,
                OnPoint {0.007336595159631046, 1.5748408343973932e-05, 0.0},
                StoreInVariable $potential_1,
                File "electricScalarPotential_1-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Potential at point [0.007336595159631046, 1.5748408343973932e-05, 0.0] [V]"
            ];

            
            Print[
                RESULT_electricScalarPotential,
                OnPoint {-0.0017659023252844684, 0.005465127193774739, 0.0},
                StoreInVariable $potential_2,
                File "electricScalarPotential_2-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Potential at point [-0.0017659023252844684, 0.005465127193774739, 0.0] [V]"
            ];

            
            Print[
                RESULT_electricScalarPotential,
                OnPoint {-0.0020548130007696157, -0.006349879530424585, 0.0},
                StoreInVariable $potential_3,
                File "electricScalarPotential_3-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Potential at point [-0.0020548130007696157, -0.006349879530424585, 0.0] [V]"
            ];


        // loop over voltage_tap_pairs to write voltage
            
            Print[
                RESULT_resistiveVoltage_0,
                OnRegion DOM_dummyPrintResistiveVoltages,
                StoreInVariable $voltage_0,
                File "resistiveVoltage_0-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Resistive voltage between voltage tap 0 and 1 [V]"
            ];

            
            Print[
                RESULT_resistiveVoltage_1,
                OnRegion DOM_dummyPrintResistiveVoltages,
                StoreInVariable $voltage_1,
                File "resistiveVoltage_1-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Resistive voltage between voltage tap 2 and 3 [V]"
            ];


        Print[RESULT_electricScalarPotential, OnElementsOf Region[{DOM_terminals, DOM_allWindings  }], File "electricScalarPotential.pos"];
        //Print[RESULT_currentFromElectricScalarPotential, OnElementsOf DOM_allWindings, File "currentFromElectricScalarPotential.pos", AtGaussPoints 6];
    }
}
}