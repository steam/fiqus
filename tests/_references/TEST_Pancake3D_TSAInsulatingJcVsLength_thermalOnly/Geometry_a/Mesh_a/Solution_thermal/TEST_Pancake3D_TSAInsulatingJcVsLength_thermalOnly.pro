//======================================================================================
// Physical regions: ===================================================================
//======================================================================================
Group{
    DOM_innerTerminalAndTransitionNotch_surface = Region[{ 2000015 }];
    DOM_outerTerminalAndTransitionNotch_surface = Region[{ 2000016 }];

    DOM_allInsulationSurface_0 = Region[{ 2000009, 2000010, 2000011 }];

    DOM_terminalContactLayerSurface_WithoutNotch_0 = Region[{ 2000010 }];
    DOM_terminalContactLayerSurface_Notch_0 = Region[{ 2000011 }];

    DOM_terminalContactLayerSurface_0 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_0, DOM_terminalContactLayerSurface_Notch_0 }];
    DOM_insulationSurface_0 = Region[{ 2000009 }];

    DOM_windingMinus_1 = Region[{ 1000000 }];
    DOM_windingPlus_1 = Region[{ 1000002 }];

    DOM_allInsulationSurface_1 = Region[{ 2000000 }];
    DOM_allInsulationSurface_1 += Region[{ 2000001 }];
    DOM_allInsulationSurface_1 += Region[{ 2000002 }];

    DOM_terminalContactLayerSurface_WithoutNotch_1 = Region[{ 2000001 }];
    DOM_terminalContactLayerSurface_Notch_1 = Region[{ 2000002 }];
    DOM_terminalContactLayerSurface_1 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_1, DOM_terminalContactLayerSurface_Notch_1 }];
    DOM_insulationSurface_1 = Region[{ 2000000 }];
    DOM_windingSurfaceMinus_1 = Region[{ 2000017 }];
    DOM_windingSurfacePlus_1 = Region[{ 2000019 }];

    DOM_windingMinus_2 = Region[{ 1000001 }];
    DOM_windingPlus_2 = Region[{ 1000003 }];

    DOM_allInsulationSurface_2 = Region[{ 2000003 }];
    DOM_allInsulationSurface_2 += Region[{ 2000004 }];
    DOM_allInsulationSurface_2 += Region[{ 2000005 }];

    DOM_terminalContactLayerSurface_WithoutNotch_2 = Region[{ 2000004 }];
    DOM_terminalContactLayerSurface_Notch_2 = Region[{ 2000005 }];
    DOM_terminalContactLayerSurface_2 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_2, DOM_terminalContactLayerSurface_Notch_2 }];
    DOM_insulationSurface_2 = Region[{ 2000003 }];
    DOM_windingSurfaceMinus_2 = Region[{ 2000018 }];
    DOM_windingSurfacePlus_2 = Region[{ 2000020 }];

    DOM_windingMinus_3 = Region[{ 1000002 }];
    DOM_windingPlus_3 = Region[{ 1000000 }];

    DOM_allInsulationSurface_3 = Region[{ 2000006 }];
    DOM_allInsulationSurface_3 += Region[{ 2000007 }];
    DOM_allInsulationSurface_3 += Region[{ 2000008 }];

    DOM_terminalContactLayerSurface_WithoutNotch_3 = Region[{ 2000007 }];
    DOM_terminalContactLayerSurface_Notch_3 = Region[{ 2000008 }];
    DOM_terminalContactLayerSurface_3 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_3, DOM_terminalContactLayerSurface_Notch_3 }];
    DOM_insulationSurface_3 = Region[{ 2000006 }];
    DOM_windingSurfaceMinus_3 = Region[{ 2000019 }];
    DOM_windingSurfacePlus_3 = Region[{ 2000017 }];

    DOM_windingMinus_4 = Region[{ 1000003 }];
    DOM_windingPlus_4 = Region[{ 1000001 }];

    DOM_allInsulationSurface_4 = Region[{ 2000009 }];
    DOM_allInsulationSurface_4 += Region[{ 2000010 }];
    DOM_allInsulationSurface_4 += Region[{ 2000011 }];

    DOM_terminalContactLayerSurface_WithoutNotch_4 = Region[{ 2000010 }];
    DOM_terminalContactLayerSurface_Notch_4 = Region[{ 2000011 }];
    DOM_terminalContactLayerSurface_4 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_4, DOM_terminalContactLayerSurface_Notch_4 }];
    DOM_insulationSurface_4 = Region[{ 2000009 }];
    DOM_windingSurfaceMinus_4 = Region[{ 2000020 }];
    DOM_windingSurfacePlus_4 = Region[{ 2000018 }];

    DOM_windingMinus_5 = Region[{ 1000000 }];
    DOM_windingPlus_5 = Region[{ 1000002 }];

    DOM_allInsulationSurface_5 = Region[{ 2000000 }];
    DOM_allInsulationSurface_5 += Region[{ 2000001 }];
    DOM_allInsulationSurface_5 += Region[{ 2000002 }];

    DOM_terminalContactLayerSurface_WithoutNotch_5 = Region[{ 2000001 }];
    DOM_terminalContactLayerSurface_Notch_5 = Region[{ 2000002 }];
    DOM_terminalContactLayerSurface_5 = Region[{ DOM_terminalContactLayerSurface_WithoutNotch_5, DOM_terminalContactLayerSurface_Notch_5 }];
    DOM_insulationSurface_5 = Region[{ 2000000 }];
    DOM_windingSurfaceMinus_5 = Region[{ 2000017 }];
    DOM_windingSurfacePlus_5 = Region[{ 2000019 }];



    // Add terminals to winding region logic:
    // 1000006: inner terminal
    // 1000007: outer terminal
    // 1000004: inner layer transition angle
    // 1000005: outer layer transition angle
    DOM_windingMinus_1 += Region[{ 1000006 }];
    DOM_windingMinus_1 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_1 += Region[{ 2000015 }];
    DOM_windingMinus_2 += Region[{ 1000006 }];
    DOM_windingMinus_2 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_2 += Region[{ 2000015 }];
    DOM_windingMinus_3 += Region[{ 1000006 }];
    DOM_windingMinus_3 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_3 += Region[{ 2000015 }];
    DOM_windingMinus_4 += Region[{ 1000006 }];
    DOM_windingMinus_4 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_4 += Region[{ 2000015 }];
    DOM_windingMinus_5 += Region[{ 1000006 }];
    DOM_windingMinus_5 += Region[{ 1000004 }];
    DOM_windingSurfaceMinus_5 += Region[{ 2000015 }];

    DOM_windingPlus_1 += Region[{ 1000007 }];
    DOM_windingPlus_1 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_1 += Region[{ 2000016 }];
    DOM_windingPlus_2 += Region[{ 1000007 }];
    DOM_windingPlus_2 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_2 += Region[{ 2000016 }];
    DOM_windingPlus_3 += Region[{ 1000007 }];
    DOM_windingPlus_3 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_3 += Region[{ 2000016 }];
    DOM_windingPlus_4 += Region[{ 1000007 }];
    DOM_windingPlus_4 += Region[{ 1000005 }];
    DOM_windingSurfacePlus_4 += Region[{ 2000016 }];

    DOM_allInsulationSurface = Region[{ 2000000, 2000001, 2000002, 2000003, 2000004, 2000005, 2000006, 2000007, 2000008, 2000009, 2000010, 2000011 }];
    DOM_insulationSurface = Region[{ 2000000, 2000003, 2000006, 2000009 }];
    DOM_terminalContactLayerSurface_WithoutNotch = Region[{ 2000001, 2000004, 2000007, 2000010 }];
    DOM_terminalContactLayerSurface_Notch = Region[{ 2000002, 2000005, 2000008, 2000011 }];
    DOM_terminalContactLayerSurface = Region[{ DOM_terminalContactLayerSurface_WithoutNotch, DOM_terminalContactLayerSurface_Notch }];
    DOM_allInsulationSurface_WithoutNotch = Region[ {DOM_insulationSurface, DOM_terminalContactLayerSurface_WithoutNotch} ];

    DOM_allWindingSurface = Region[{ 2000017, 2000018, 2000019, 2000020 }];
    DOM_allConvectiveSurface = Region[{ 2000015, 2000016, 2000017, 2000018, 2000019, 2000020 }];

    DOM_insulationBoundaryCurvesAir = Region[{ 3000000 }];
    DOM_insulationBoundaryCurvesTerminal = Region[{ 3000001 }];

    // create windings region:
    DOM_allWindings = Region[{ 1000000, 1000001, 1000002, 1000003 }];

    // create terminals region:
    DOM_terminals = Region[{ 1000006, 1000007}];

    // create layer transition angle region:
    DOM_transitionNotchVolumes = Region[{1000004, 1000005}];

    // create powered region:
    DOM_powered = Region[{ DOM_allWindings, DOM_terminals, DOM_transitionNotchVolumes }];

    // support of edge-based magnetic field strength, i.e., all conducting doms:
    DOM_allConducting = Region[{ DOM_powered}];
    DOM_resistiveHeating = Region[{ DOM_allWindings }];
    DOM_thermal = Region[{ DOM_powered}];
    DOM_air = Region[{ 1000008 }];
    DOM_airPoints = Region[{ 4000000 }];

DOM_allConductingWithoutWindings  = Region[DOM_allConducting];
DOM_allConductingWithoutWindings -= Region[DOM_allWindings];


    // add shell volume for shell transformation:
    DOM_air += Region[{ 1000009 }];
    DOM_airInf = Region[{ 1000009 }];


    // boundary surface between the all conducting and non-conducting domains:
    DOM_pancakeBoundary = Region[{ 2000014 }];

    // support of magnetic scalar potential, i.e., all non-conducting doms:
    DOM_Phi = Region[{ DOM_air }];

    // cut inbetween current leads, used to impose current or voltage:
    DOM_terminalCut = Region[{ 3000003 }];
    DOM_airHoleCut = Region[{  }];
    DOM_airCuts = Region[{ DOM_terminalCut, DOM_airHoleCut }];

    // total computational domain (Omega):
    DOM_total = Region[{ DOM_allConducting, DOM_Phi }];

    // top and bottom surfaces of the terminals for constant temperature BC:
    DOM_bottomTerminalSurface = Region[{ 2000013 }];
    DOM_topTerminalSurface = Region[{ 2000012 }];
    DOM_terminalSurfaces = Region[{ DOM_bottomTerminalSurface, DOM_topTerminalSurface }];

    
    DOM_currentSource = Region[ 9000000 ];
    DOM_resistanceCrowbar = Region[ 9000001 ];
    DOM_switchCrowbar = Region[ 9000002 ];
    DOM_resistanceEE = Region[ 9000003 ];
    DOM_switchEE = Region[ 9000004 ];
    DOM_inductanceEE = Region[ 9000005 ];

    DOM_circuitResistance = Region[{ DOM_resistanceCrowbar, DOM_resistanceEE, DOM_switchCrowbar, DOM_switchEE }];
    DOM_circuitInductance = Region[{ DOM_inductanceEE }];

    DOM_circuit = Region[{ DOM_currentSource, DOM_circuitResistance, DOM_circuitInductance }];
}

//======================================================================================
// Physical properties: ================================================================
//======================================================================================
Function{
    // Power supply / current source:
    listOfTimeValues = {0.0, 5.0, 10.0};
    listOfCurrentValues = {0.0, 1.0, 1.0};
    current[] = $quenchDetected == 1? (
        $Time - $quenchDetectionTime > 1.5 ? 0 :
        InterpolationLinear[$quenchDetectionTime]{ListAlt[listOfTimeValues, listOfCurrentValues]} * (1 - ($Time - $quenchDetectionTime)/1.5) ) : 
        InterpolationLinear[$1]{ListAlt[listOfTimeValues, listOfCurrentValues]};

    // Pancake3D geometry related functions:

    // We use SetVariable and GetVariable to ensure some variables are only evaluated once
    // and saved in the memory. Input of the functions below is the position vector XYZ[].


    imposedPowerDensity[] = Pancake3DPowerDensity[$1, $2]
        {1.0,
        7.0, 
        0.15,
        0.18,
        100.0,
        0.005,
        0.000275,
        0,
        0.0,
        26,
        1,
        0.004,
        0.01
        };

    // to be templated below
    oneDGaussianOrder = 2;
    // to be templated above

    // Initial conditions:
    INPUT_initialTemperature = 77.0; // initial temperature, [K]

    // Time stepping parameters:
    INPUT_tStart = 0.0; // start time, [s]
    INPUT_tEnd = 5.0; // end time, [s]
    INPUT_extrapolationOrder = 1; // order of extrapolation for the time stepping scheme
    // Adaptive time stepping parameters:
    INPUT_tAdaptiveMinStep = 0.001; // minimum time step, [s]
    INPUT_tAdaptiveMaxStep = 1.0; // maximum time step, [s]
    INPUT_tAdaptiveInitStep = 0.1; // initial time step, [s]
    INPUT_tAdaptiveBreakPoints ={ 0.0, 5.0, 10.0 }; // force solution at these time points, [s]

    // Nonlinear solver parameters:
    INPUT_NLSMaxNumOfIter = 20; // maximum number of iterations for the nonlinear solver
    INPUT_NLSRelaxFactor = 0.7; // relaxation factor for the nonlinear solver

    //==================================================================================
    // Material parameters: ============================================================
    //==================================================================================

    // Winding material combination parameters start ===================================
    INPUT_relativeThicknessCopper = 0.2143;
    INPUT_relativeThicknessHastelloy = 0.5715;
    INPUT_relativeThicknessSilver = 0.1428;

    INPUT_relativeThicknessOfSuperConductor = 0.0714;
    INPUT_relativeThicknessOfNormalConductor = 0.9286000000000001;
    INPUT_relativeWidthOfPlating = 0.02;
    // Factor 1.0/0.8727272727272727 is used equate the scaling applied to Jcritical in the parallel direction 
    INPUT_jCriticalScalingNormalToWinding = 1.0/0.8727272727272727 * 1;
    // Winding material combination parameters end =====================================

    // TSA parameters start ============================================================
    INPUT_insulationThickness = 4e-05; // thickness of the insulation, [m]
    INPUT_NumOfTSAElements = 2;
    th_terminal_k = INPUT_insulationThickness / (INPUT_NumOfTSAElements == 0 ? 1 : INPUT_NumOfTSAElements);
    th_insulation_k = 0.5 * th_terminal_k;
    // TSA parameters end ==============================================================
    
    // Cryocooler TSA parameters start ==================================================
    INPUT_NumOfCryocoolerTSAElements = 2;
    cryocooler_thickness[] = 0.0001/ GetVariable[]{$areaCryocooler} ;
    th_cryocooler_k[] = cryocooler_thickness[]/INPUT_NumOfCryocoolerTSAElements;
    // Cryocooler TSA parameters end ====================================================



    // Winding thermal conductivity starts =============================================
    // Nonlinear:
    kappa[DOM_allWindings] = WindingThermalConductivityV1[
        XYZ[],
        $2,
        $1
    ]{   1 ,
        3, // N of materials,
        0,
        1,
        2,
        0.2143,
        0.5715,
        0.1428,
        100.0,
        100,
        100.0,
        295.0,
        295,
        295.0,
        0,
        0.02,
        100.0,
        295.0,
        0.005,
        0.000275,
        0,
        0.0,
        26,
        1,
        0.004,
        0.01,
        0.8727272727272727,
        1 // arbitrary jCritical scaling normal to winding
    };
    // Winding thermal conductivity ends ===============================================

    // Terminals thermal conductivity starts ===========================================
    kappa[DOM_transitionNotchVolumes] = 100.0;

    // Nonlinear:
    kappa[DOM_terminals] = CFUN_kCu_T_rho0_rho[$1, CFUN_rhoCu_T[$1]{0, 100.0}, CFUN_rhoCu_T_B[$1, Norm[$2]]{100.0}]{100.0};
    // Terminals thermal conductivity ends =============================================

    // Insulation thermal conductivity starts ==========================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionNoDta1b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona1b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta1b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona1b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta2b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona2b1[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};
    thermalMassFunctionNoDta2b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};

    thermalStiffnessFunctiona2b2[DOM_insulationSurface] = TSA_CFUN_kKapton_T_constantThickness_stiffness[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};
    // Insulation thermal conductivity ends ============================================

    // Transition layer thermal conductivity starts ====================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionNoDta1b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona1b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta1b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona1b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta2b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona2b1[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};
    
    thermalMassFunctionNoDta2b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};

    thermalStiffnessFunctiona2b2[DOM_terminalContactLayerSurface] = TSA_CFUN_kG10_T_constantThickness_stiffness[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};
    
    // Transition layer thermal conductivity ends ======================================
    
    // Cryocooler thermal conductivity starts ====================================
    // Linear:
    INPUT_cryocoolerThermalConductivity = 100.0; // thermal conductivity of the insulation, [W/*(m*K)]

    thermalMassFunctionNoDta1b1[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 1
        };

    thermalStiffnessFunctiona1b1[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 1
        };

    thermalMassFunctionNoDta1b2[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 2
        };

    thermalStiffnessFunctiona1b2[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 1, 2
        };

    thermalMassFunctionNoDta2b1[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 1
        };

    thermalStiffnessFunctiona2b1[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 1
        };

    thermalMassFunctionNoDta2b2[DOM_terminalSurfaces] = TSA_constantMaterial_mass[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 2
        };

    thermalStiffnessFunctiona2b2[DOM_terminalSurfaces] = TSA_constantMaterial_stiffness[th_cryocooler_k[]]{
            INPUT_cryocoolerThermalConductivity, 2, 2
        };

    // Cryocooler thermal conductivity ends ======================================

    // Winding specific heat capacity starts ===========================================
    // Nonlinear:
    CvCopper[] =  CFUN_CvCu_T[$1];
    CvHastelloy[] =  CFUN_CvHast_T[$1];
    CvSilver[] =  CFUN_CvAg_T[$1];

    Cv[DOM_allWindings] = 0.8727272727272727 * RuleOfMixtures[
        CvCopper[$1],
        CvHastelloy[$1],
        CvSilver[$1]
    ]{
        INPUT_relativeThicknessCopper,
        INPUT_relativeThicknessHastelloy,
        INPUT_relativeThicknessSilver
    };
    // Winding specific heat capacity ends =============================================

    // Terminals specific heat capacity starts =========================================
    Cv[DOM_transitionNotchVolumes] = 150.0;

    // Nonlinear:
    Cv[DOM_terminals] =  CFUN_CvCu_T[$1];
    // Terminals specific heat capacity ends ===========================================
    
    // Insulation specific heat capacity starts ========================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionDta1b1[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta1b2[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta2b1[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_insulation_k};

    thermalMassFunctionDta2b2[DOM_insulationSurface] = TSA_CFUN_CvKapton_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_insulation_k};

    // Insulation specific heat capacity ends ==========================================

    // Transition layer specific heat capacity starts ==================================
    // Nonlinear:
    // Thin-shell insulation:
    thermalMassFunctionDta1b1[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{1, 1, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta1b2[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{1, 2, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta2b1[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{2, 1, oneDGaussianOrder, th_terminal_k};
    thermalMassFunctionDta2b2[DOM_terminalContactLayerSurface] =  TSA_CFUN_CvG10_T_constantThickness_mass[$1, $2]{2, 2, oneDGaussianOrder, th_terminal_k};
    // Transition layer specific heat capacity ends ====================================
    
    // Cryocooler specific heat capacity starts ==================================
    // Nonlinear:
    thermalMassFunctionDta1b1[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{1, 1, oneDGaussianOrder};
    thermalMassFunctionDta1b2[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{1, 2, oneDGaussianOrder};
    thermalMassFunctionDta2b1[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{2, 1, oneDGaussianOrder};
    thermalMassFunctionDta2b2[DOM_terminalSurfaces] =  TSA_CFUN_CvCu_T_mass[$1, $2, th_cryocooler_k[]]{2, 2, oneDGaussianOrder};

    // Cryocooler specific heat capacity ends ====================================

    // Shell transformation parameters:
    INPUT_shellInnerRadius = 0.015; // inner radius of the shell, [m]
    INPUT_shellOuterRadius = 0.020999999999999998; // outer radius of the shell, [m]

    // EE circuit quantities
    Resistance[DOM_resistanceCrowbar] = 0.1;
    Resistance[DOM_resistanceEE] = 0.1;

    Resistance[DOM_switchCrowbar] = $quenchDetected == 0? 1000000.0: 1e-06;

    Resistance[DOM_switchEE] = $quenchDetected == 0? 1e-06: 1000000.0;

    Inductance[DOM_inductanceEE] = 0.001;
    // Cryocooler quantities

    // Division by area to compute Watts per meter squared
    // SurfaceArea function does not allow DOM_*** as argument, so we need to use
    // the actual ids
    cryocoolerCoolingPower[] = 
        (0.8 * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_T[$1] - 28.0)/ GetVariable[]{$areaCryocooler} ; 

    cryocoolerCoolingPowerDerivativeT[] =      
        0.8 * CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_dT_T[$1]/ GetVariable[]{$areaCryocooler}; 
}

//======================================================================================
// Jacobian and integration: ===========================================================
//======================================================================================
Jacobian{
    {
        Name JAC_vol; // volume Jacobian
        Case
        {
            {
                Region DOM_airInf;
                Jacobian VolCylShell {INPUT_shellInnerRadius, INPUT_shellOuterRadius};
            }
            {
                Region All;
                Jacobian Vol;
            }
        }
    }

    // surface Jacobian for TSA:
    {
        Name JAC_sur; // surface Jacobian
        Case
        {
            {
                Region All;
                Jacobian Sur;
            }
        }
    }
}

Integration{
    {
        Name Int; // Gauss integraion scheme
        Case{
            {
                Type Gauss;
                Case{
                    {
                        GeoElement Triangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Quadrangle;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Tetrahedron;
                        NumberOfPoints 4;
                    }
                    {
                        GeoElement Hexahedron;
                        NumberOfPoints 6;
                    }
                    {
                        GeoElement Prism;
                        NumberOfPoints 9;
                    }
                    {
                        GeoElement Pyramid;
                        NumberOfPoints 8;
                    }
                }
            }
        }
    }
}

//======================================================================================
// Constraints: ========================================================================
//======================================================================================
Constraint{
    {
        // Impose current:
        Name CONSTRAINT_current;
        Case{
            {
                Region DOM_currentSource;
                Type Assign;
                Value 1;


                TimeFunction current[$Time];
            }
        }
    }
    {
        // Impose current:
        Name CONSTRAINT_voltage;
        Case{
            {
                Region DOM_airHoleCut;
                Type Assign;
                Value 0;
            }
        }
    }
    
    {
        Name CONSTRAINT_zeroPhiAtOuterPoint;
        Case{
            {
                Region DOM_airPoints;
                Type Assign;
                Value 0;
            }
        }
    }
    {
        Name CONSTRAINT_initialTemperature;
        Case {
            {
                Region Region[
                            {
                                DOM_powered, 
                                DOM_allInsulationSurface
                            }
                        ];
                Type Init;
                Value 77.0;
            }
        }
    }

    { Name EECircuit_Netlist; Type Network;
        Case Circuit1 { // Describes node connection of branches
        // power source
        { Region DOM_currentSource;  Branch {1, 2} ; }

        // crowbar resistance and switch in series
        { Region DOM_resistanceCrowbar; Branch {2, 3} ; }
        { Region DOM_switchCrowbar; Branch {3, 1} ; }

        // energy extraction and switch in parallel
        { Region DOM_resistanceEE;  Branch {2, 4} ; }
        { Region DOM_switchEE;  Branch {2, 4} ; }

        // magnet current via terminal cut
        { Region DOM_terminalCut; Branch {4, 5} ; }

        // inductance in series with pancake coil
        { Region DOM_inductanceEE; Branch {5, 1} ; }
        }
    }
}

//======================================================================================
// Function spaces: ====================================================================
//======================================================================================
FunctionSpace{
    {
        Name SPACE_hPhi;
        Type Form1;
        BasisFunction{
            // gradient of nodal basis functions in DOM_Phi and on DOM_pancakeBoundary
            {
                Name BASISFUN_gradpsin;
                NameOfCoef phin;
                Function BF_GradNode;
                Support Region[{DOM_total, DOM_allInsulationSurface}];
                Entity NodesOf[{DOM_Phi, DOM_insulationSurface}, Not {DOM_insulationBoundaryCurvesAir}];
            }
            // edge basis functions in DOM_allConducting, and not on DOM_pancakeBoundary or DOM_allInsulationSurface
            {
                Name BASISFUN_psie;
                NameOfCoef he;
                Function BF_Edge;
                Support DOM_allConducting;
                Entity EdgesOf[All, Not {DOM_pancakeBoundary, DOM_allInsulationSurface}];
            }
            // edge-based cohomology basis functions on both cuts
            {
                Name BASISFUN_sc;
                NameOfCoef Ii;
                Function BF_GroupOfEdges;
                Support Region[{DOM_total, DOM_terminalContactLayerSurface}];
                Entity GroupsOfEdgesOf[DOM_airCuts];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_1,
                                DOM_terminalContactLayerSurface_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_0, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_1,
                                DOM_terminalContactLayerSurface_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_1,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_0, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_2,
                                DOM_terminalContactLayerSurface_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_1, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_2,
                                DOM_terminalContactLayerSurface_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_2,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_1, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_3,
                                DOM_terminalContactLayerSurface_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_2, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_3,
                                DOM_terminalContactLayerSurface_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_3,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_2, DOM_insulationSurface }
                    ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_4,
                                DOM_terminalContactLayerSurface_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_3, DOM_insulationSurface }
                    ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Edge;
                Support Region[
                            {   
                                DOM_terminalContactLayerSurface_4,
                                DOM_terminalContactLayerSurface_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity EdgesOf[
                    DOM_terminalContactLayerSurface_4,
                    Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal,  DOM_terminalContactLayerSurface_3, DOM_insulationSurface }
                    ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contribution on boundary of the thin layer
            {
                Name BASISFUN_gradpsinBnd;
                NameOfCoef phin_bnd;
                Function BF_GradNode;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_Phi,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity NodesOf[DOM_insulationBoundaryCurvesAir];
            }
            {
                Name BASISFUN_psieBnd;
                NameOfCoef psie_bnd;
                Function BF_Edge;
                Support Region[
                            {
                                DOM_allConducting,
                                DOM_allInsulationSurface
                            }
                        ];
                Entity EdgesOf[DOM_insulationBoundaryCurvesTerminal];
            }
            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef he~{i};
                    Function BF_Edge;
                    Support DOM_terminalContactLayerSurface;
                    Entity EdgesOf[ All, Not {DOM_insulationBoundaryCurvesAir, DOM_insulationBoundaryCurvesTerminal, DOM_insulationSurface } ];
                }
            EndFor
        }

        SubSpace{
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd,
                    BASISFUN_gradpsin
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_sc,
                    BASISFUN_gradpsinBnd,
                    BASISFUN_psieBnd,
                    BASISFUN_gradpsin
                };
            }



            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_sc,
                        BASISFUN_gradpsinBnd,
                        BASISFUN_psieBnd,
                        BASISFUN_gradpsin
                    };
                }

            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        // global quantities in order to impose/extract currents or voltages
        GlobalQuantity{
            {
                Name GLOBALQUANT_I;
                Type AliasOf;
                NameOfCoef Ii;
            }
            {
                Name GLOBALQUANT_V;
                Type AssociatedWith;
                NameOfCoef Ii;
            }
        }

        // imposing source current or voltage using global quantities
        Constraint
        {
            {
                NameOfCoef GLOBALQUANT_V;
                EntityType GroupsOfEdgesOf;
                NameOfConstraint CONSTRAINT_voltage;
            }
            {
                NameOfCoef phin;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_zeroPhiAtOuterPoint;
            }
        }
    }
    {
        Name SPACE_temperature;
        Type Form0;
        BasisFunction{
            {
                Name BASISFUN_sn;
                NameOfCoef Tn;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_terminalSurfaces 
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface
                }];
                Entity NodesOf[DOM_thermal, 
                    Not {DOM_allInsulationSurface}];
            }
            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
            {
                Name BASISFUN_snMinus_1;
                NameOfCoef BASISFUN_snMinus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingSurfaceMinus_1,
                                DOM_windingSurfaceMinus_2,
                                DOM_windingMinus_1,
                                DOM_windingMinus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_1;
                NameOfCoef BASISFUN_snPlus_coeff_1;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_1,
                                DOM_allInsulationSurface_2,
                                DOM_windingSurfacePlus_1,
                                DOM_windingSurfacePlus_2,
                                DOM_windingPlus_1,
                                DOM_windingPlus_2
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_1,
                        Not { DOM_allInsulationSurface_0, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_2;
                NameOfCoef BASISFUN_snMinus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingSurfaceMinus_2,
                                DOM_windingSurfaceMinus_3,
                                DOM_windingMinus_2,
                                DOM_windingMinus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_2;
                NameOfCoef BASISFUN_snPlus_coeff_2;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_2,
                                DOM_allInsulationSurface_3,
                                DOM_windingSurfacePlus_2,
                                DOM_windingSurfacePlus_3,
                                DOM_windingPlus_2,
                                DOM_windingPlus_3
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_2,
                        Not { DOM_allInsulationSurface_1, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_3;
                NameOfCoef BASISFUN_snMinus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingSurfaceMinus_3,
                                DOM_windingSurfaceMinus_4,
                                DOM_windingMinus_3,
                                DOM_windingMinus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_3;
                NameOfCoef BASISFUN_snPlus_coeff_3;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_3,
                                DOM_allInsulationSurface_4,
                                DOM_windingSurfacePlus_3,
                                DOM_windingSurfacePlus_4,
                                DOM_windingPlus_3,
                                DOM_windingPlus_4
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_3,
                        Not { DOM_allInsulationSurface_2, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }
            {
                Name BASISFUN_snMinus_4;
                NameOfCoef BASISFUN_snMinus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingSurfaceMinus_4,
                                DOM_windingSurfaceMinus_5,
                                DOM_windingMinus_4,
                                DOM_windingMinus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal 
                        }
                            ];
            }

            {
                Name BASISFUN_snPlus_4;
                NameOfCoef BASISFUN_snPlus_coeff_4;
                Function BF_Node;
                Support Region[
                            {   
                                DOM_allInsulationSurface_4,
                                DOM_allInsulationSurface_5,
                                DOM_windingSurfacePlus_4,
                                DOM_windingSurfacePlus_5,
                                DOM_windingPlus_4,
                                DOM_windingPlus_5
                            }
                        ];
                Entity NodesOf[
                        DOM_allInsulationSurface_4,
                        Not { DOM_allInsulationSurface_3, DOM_insulationBoundaryCurvesTerminal            
                    }
                        ];
            }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
                {
                    Name BASISFUN_snPlus_not_connected_1;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_1;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_1,
                                    DOM_insulationSurface_2
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_1,
                        Not DOM_insulationSurface_0
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_2;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_2;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_2,
                                    DOM_insulationSurface_3
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_2,
                        Not DOM_insulationSurface_1
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_3;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_3;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_3,
                                    DOM_insulationSurface_4
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_3,
                        Not DOM_insulationSurface_2
                        ];
                }
                {
                    Name BASISFUN_snPlus_not_connected_4;
                    NameOfCoef BASISFUN_snPlus_not_connected_coeff_4;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_4,
                                    DOM_insulationSurface_5
                                }
                            ];
                    Entity NodesOf[
                        DOM_insulationSurface_4,
                        Not DOM_insulationSurface_3
                        ];
                }

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA contributions following special indexing to restrict them to one side of the thin
            // layer split in plus and minus side.
                
                {
                    Name BASISFUN_snMinus_not_connected_1;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_1;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_1,
                                    DOM_insulationSurface_2
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_1,
                                Not DOM_insulationSurface_0
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_2;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_2;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_2,
                                    DOM_insulationSurface_3
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_2,
                                Not DOM_insulationSurface_1
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_3;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_3;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_3,
                                    DOM_insulationSurface_4
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_3,
                                Not DOM_insulationSurface_2
                                ];
                }
                
                
                {
                    Name BASISFUN_snMinus_not_connected_4;
                    NameOfCoef BASISFUN_snMinus_not_connected_coeff_4;
                    Function BF_Node;
                    Support Region[
                                {   
                                    DOM_insulationSurface_4,
                                    DOM_insulationSurface_5
                                }
                            ];
                    Entity NodesOf[
                                DOM_insulationSurface_4,
                                Not DOM_insulationSurface_3
                                ];
                }
                

            // TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            // Cryocooler TSA basis functions starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name BASISFUN_snOutside;
                NameOfCoef BASISFUN_snOutside_coeff;
                Function BF_Node;
                Support DOM_terminalSurfaces;
                Entity NodesOf[ DOM_terminalSurfaces ];
            }

            // Cryocooler TSA basis functions ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            {
                Name BASISFUN_snBndTerminal;
                NameOfCoef Tn_Bnd;
                Function BF_Node;
                Support Region[{DOM_thermal, DOM_allInsulationSurface
                    , DOM_innerTerminalAndTransitionNotch_surface, DOM_outerTerminalAndTransitionNotch_surface, DOM_allWindingSurface
                }];
                Entity NodesOf[DOM_insulationBoundaryCurvesTerminal];
            }

            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name BASISFUN_sn~{i};
                    NameOfCoef sn~{i};
                    Function BF_Node;
                    Support DOM_terminalContactLayerSurface;
                    Entity NodesOf[ All, Not DOM_insulationBoundaryCurvesTerminal ];
                }

                    {
                        Name BASISFUN_snMinus_adiabatic~{i};
                        NameOfCoef snMinus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
                    {
                        Name BASISFUN_snPlus_adiabatic~{i};
                        NameOfCoef snPlus_adiabatic~{i};
                        Function BF_Node;
                        Support DOM_insulationSurface;
                        Entity NodesOf[ All ];
                    }
            EndFor

            For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
                {
                    Name BASISFUN_cryocooler_sn~{i};
                    NameOfCoef sn_cryocooler~{i};
                    Function BF_Node;
                    Support DOM_terminalSurfaces;
                    Entity NodesOf[ All ];
                }
            EndFor
            
        }
        SubSpace {
            // TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the contact layer:
            {
                Name SUBSPACE_insulationSurface_down;
                NameOfBasisFunction{
                    BASISFUN_snMinus_1,
                    BASISFUN_snMinus_2,
                    BASISFUN_snMinus_3,
                    BASISFUN_snMinus_4,
                    BASISFUN_snBndTerminal
                };
            }
            {
                Name SUBSPACE_insulationSurface_up;
                NameOfBasisFunction{
                    BASISFUN_snPlus_1,
                    BASISFUN_snPlus_2,
                    BASISFUN_snPlus_3,
                    BASISFUN_snPlus_4,
                    BASISFUN_snBndTerminal
                };
            }

            {
                Name SUBSPACE_adiabatic_minus_up;
                NameOfBasisFunction{
                BASISFUN_snMinus_1,
                BASISFUN_snMinus_2,
                BASISFUN_snMinus_3,
                BASISFUN_snMinus_4
                };
            }
            {
                Name SUBSPACE_adiabatic_minus_down;
                NameOfBasisFunction{
                BASISFUN_snMinus_not_connected_1,
                BASISFUN_snMinus_not_connected_2,
                BASISFUN_snMinus_not_connected_3,
                BASISFUN_snMinus_not_connected_4
                };
            }

            {
                Name SUBSPACE_adiabatic_plus_up;
                NameOfBasisFunction{
                BASISFUN_snPlus_1,
                BASISFUN_snPlus_2,
                BASISFUN_snPlus_3,
                BASISFUN_snPlus_4
                };
            }
            {
                Name SUBSPACE_adiabatic_plus_down;
                NameOfBasisFunction{
                BASISFUN_snPlus_not_connected_1,
                BASISFUN_snPlus_not_connected_2,
                BASISFUN_snPlus_not_connected_3,
                BASISFUN_snPlus_not_connected_4
                };
            }


            For i In {1:INPUT_NumOfTSAElements - 1}
                {
                    Name SUBSPACE_tsa~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_sn~{i},
                        BASISFUN_snBndTerminal
                    };
                }

            {
                Name SUBSPACE_adiabatic_plus_tsa~{i}; 
                NameOfBasisFunction {
                    BASISFUN_snPlus_adiabatic~{i}
                };
            }
            {
                Name SUBSPACE_adiabatic_minus_tsa~{i}; 
                NameOfBasisFunction {
                    BASISFUN_snMinus_adiabatic~{i}
                };
            }
            EndFor

            // TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Cryocooler TSA subspaces starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // identification of the positive and negative side of the cryocooler lumped mass:
            {
                Name SUBSPACE_cryocooler_inside;
                NameOfBasisFunction{
                    BASISFUN_sn
                };
            }
            {
                Name SUBSPACE_cryocooler_outside;
                NameOfBasisFunction{
                    BASISFUN_snOutside
                };
            }

            For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
                {
                    Name SUBSPACE_cryocooler~{i}; 
                    NameOfBasisFunction {
                        BASISFUN_cryocooler_sn~{i}
                    };
                }
            EndFor

            // Cryocooler TSA subspaces ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        Constraint{
            {
                NameOfCoef Tn;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_1;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_2;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_3;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snMinus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    
            {
                NameOfCoef BASISFUN_snMinus_not_connected_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef BASISFUN_snPlus_not_connected_coeff_4;
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }

    For i In {1:INPUT_NumOfTSAElements - 1}
        {
            NameOfCoef sn~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }

            {
                NameOfCoef snMinus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
            {
                NameOfCoef snPlus_adiabatic~{i};
                EntityType NodesOf;
                NameOfConstraint CONSTRAINT_initialTemperature;
            }
    EndFor
    
    {
        NameOfCoef Tn_Bnd;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    {
        NameOfCoef BASISFUN_snOutside_coeff;
        EntityType NodesOf;
        NameOfConstraint CONSTRAINT_initialTemperature;
    }
    For i In {1:INPUT_NumOfCryocoolerTSAElements - 1}
        {
            NameOfCoef sn_cryocooler~{i};
            EntityType NodesOf;
            NameOfConstraint CONSTRAINT_initialTemperature;
        }
    EndFor
        }
    }

    { Name SPACE_circuit; Type Scalar;
        BasisFunction {
            {
                Name BASISFUN_ICircuit ; NameOfCoef ICircuit_coeff ; Function BF_Region ;
                    Support DOM_circuit ; Entity DOM_circuit ;
          }
        }

        GlobalQuantity {
            { Name ICircuit; Type AliasOf       ; NameOfCoef ICircuit_coeff; }
            { Name UCircuit; Type AssociatedWith; NameOfCoef ICircuit_coeff; }
        }

        Constraint {
             { NameOfCoef ICircuit; EntityType Region; NameOfConstraint CONSTRAINT_current; }
        }
    }

}


//======================================================================================
// Formulations: =======================================================================
//======================================================================================
Formulation{
    {
        Name FORMULATION_thermal;
        Type FemEquation;
        Quantity {
            // Volumetric quantities starts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_T;
                Type Local;
                NameOfSpace SPACE_temperature;
            }
            // Volumetric quantities ends ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_insulationSurface_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_minus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_minus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_minus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_minus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_TThinShell_plus~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_up];
            }

            For i In{1 : INPUT_NumOfTSAElements - 1}
                {
                    Name LOCALQUANT_TThinShell_plus~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_tsa~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_TThinShell_plus~{INPUT_NumOfTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_adiabatic_plus_down];
            }
            // TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Cryocooler TSA quantities starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            {
                Name LOCALQUANT_cryocooler_TThinShell~{0};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_inside];
            }

            For i In {1:INPUT_NumOfCryocoolerTSAElements-1}
                {
                    Name LOCALQUANT_cryocooler_TThinShell~{i};
                    Type Local;
                    NameOfSpace SPACE_temperature[SUBSPACE_cryocooler~{i}];
                }
            EndFor

            {
                Name LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements};
                Type Local;
                NameOfSpace SPACE_temperature[SUBSPACE_cryocooler_outside];
            }

            // Cryocooler TSA quantities ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        }
        
        Equation{
            Integral {
                [ kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0] * Dof{d LOCALQUANT_T}, {d LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral {
                DtDof[ Cv[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0] * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
                In DOM_thermal;
                Jacobian JAC_vol;
                Integration Int;
            }
            Integral {
                [ -imposedPowerDensity[XYZ[], $Time], {LOCALQUANT_T} ];
                In DOM_allWindings;
                Jacobian JAC_vol;
                Integration Int;
            }

            Integral {
                [ cryocoolerCoolingPower[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}], {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}} ];
                In DOM_terminalSurfaces;
                Jacobian JAC_sur;
                Integration Int;
            }

            // This Jacobian entry is extremely important for the convergence of the nonlinear solver
            Integral {
                JacNL [ cryocoolerCoolingPowerDerivativeT[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}] * Dof{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}, {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}} ];
                In DOM_terminalSurfaces;
                Jacobian JAC_sur;
                Integration Int;
            }

            Integral {
                [ 100000.0 * Dof{LOCALQUANT_T}, {LOCALQUANT_T} ];
                In DOM_allConvectiveSurface;
                Jacobian JAC_sur;
                Integration Int;
            }
            Integral {
                [ - 100000.0 * 70.0, {LOCALQUANT_T} ];
                In DOM_allConvectiveSurface;
                Jacobian JAC_sur;
                Integration Int;
            }


            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell~{i}},
                                {LOCALQUANT_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalContactLayerSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell_minus~{i}},
                                {LOCALQUANT_TThinShell_minus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_minus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_minus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfTSAElements-1}
                    Integral {
                        [                   thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 1 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 1 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [                   thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{d LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {d LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [                   thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_TThinShell_plus~{i}},
                                {LOCALQUANT_TThinShell_plus~{i+1}}
                            ] * Dof{LOCALQUANT_TThinShell_plus~{i + 2 - 1}},
                            {LOCALQUANT_TThinShell_plus~{i + 2 - 1}}
                        ];
                        In DOM_insulationSurface;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

            

            For i In {0:INPUT_NumOfCryocoolerTSAElements-1}
                    Integral {
                        [   
                            thermalMassFunctionNoDta1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta1b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b1[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 1 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
                    Integral {
                        [   
                            thermalMassFunctionNoDta2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {d LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        [   
                            thermalStiffnessFunctiona2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }

                    Integral {
                        DtDof[
                            thermalMassFunctionDta2b2[
                                {LOCALQUANT_cryocooler_TThinShell~{i}},
                                {LOCALQUANT_cryocooler_TThinShell~{i+1}}
                            ] * Dof{LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}},
                            {LOCALQUANT_cryocooler_TThinShell~{i + 2 - 1}}
                        ];
                        In DOM_terminalSurfaces;
                        Integration Int;
                        Jacobian JAC_sur;
                    }
            EndFor

        }
    }



}

//======================================================================================
// Resolution: =========================================================================
//======================================================================================
Resolution{
    {
        Name RESOLUTION_thermal;
        System{
            {
                Name SYSTEM_thermal;
                NameOfFormulation FORMULATION_thermal;
            }
        }

        Operation{
            Evaluate[SetVariable[SurfaceArea[]{2000013, 2000012 }]{$areaCryocooler}];
            InitSolution[SYSTEM_thermal];
            SaveSolution[SYSTEM_thermal];
            SetExtrapolationOrder[INPUT_extrapolationOrder];


            TimeLoopAdaptive[
                INPUT_tStart,
                INPUT_tEnd,
                INPUT_tAdaptiveInitStep,
                INPUT_tAdaptiveMinStep,
                INPUT_tAdaptiveMaxStep,
                "Euler",
                List[INPUT_tAdaptiveBreakPoints],
                PostOperation{
                    {
                        POSTOP_CONV_maximumTemperature,
                        0.1,
                        10.0,
                        LinfNorm
                    }
                }

            ]{
                // Nonlinear solver starts +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                IterativeLoopN[
                    INPUT_NLSMaxNumOfIter,
                    INPUT_NLSRelaxFactor,
                    PostOperation{
                        {
                            POSTOP_CONV_maximumTemperature,
                            0.1,
                            10.0,
                            LinfNorm
                        }
                    }

                ]{
                    GenerateJac SYSTEM_thermal;
                    SolveJac SYSTEM_thermal;

                }
                // Check if the solution is NaN and remove it
                Test[$KSPResidual != $KSPResidual]{
                    Print["Critical: Removing NaN solution from the solution vector."];
                    RemoveLastSolution[SYSTEM_thermal];
                }
                // Nonlinear solver ends +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            }{
                PostOperation[POSTOP_maximumTemperature];
                Test[#999 > 160.0] {
                    Print["Critical: stop simulation since maximum temperature surpassed threshold"];
                    Break[];
                }
                SaveSolution[SYSTEM_thermal];

            }

            PostOperation[POSTOP_temperature];
            PostOperation[POSTOP_timeSeriesPlot_maximumTemperature];
            PostOperation[POSTOP_timeSeriesPlot_cryocoolerAveragePower];
            PostOperation[POSTOP_timeSeriesPlot_cryocoolerAverageTemperature];



        }
    }
}

//======================================================================================
// Post-processing: ====================================================================
//======================================================================================
PostProcessing{
    {
        Name POSTPRO_thermal;
        NameOfFormulation FORMULATION_thermal;
        NameOfSystem SYSTEM_thermal;
        Quantity{
            {
                Name RESULT_temperature;
                Value {
                    Local{
                        [{LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_maximumTemperature; // maximum temperature
                Value{
                    Term {
                        Type Global;
                        [ #999 ];
                        In DOM_powered;
                    }
                }
            }

            {
                Name RESULT_heatFlux;
                Value {
                    Local{
                        [-kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0] * {d LOCALQUANT_T}];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_magnitudeOfHeatFlux;
                Value {
                    Local{
                        [Norm[-kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0] * {d LOCALQUANT_T}]];
                        In DOM_thermal;
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_thermalConductivity; // current density magnitude
                Value{
                    Local{
                        [kappa[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_specificHeatCapacity; // current density magnitude
                Value{
                    Local{
                        [Cv[{LOCALQUANT_T}, UnitVectorZ[] * 0.001, 0, 0]];
                        In Region[{ DOM_thermal }];
                        Jacobian JAC_vol;
                    }
                }
            }

            {
                Name RESULT_cryocoolerAveragePower; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;

                            [ cryocoolerCoolingPower[{LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}] ];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }

            {
                Name RESULT_cryocoolerAverageTemperature; // power of cryocooler computed as surface integral
                Value{
                    Integral{
                        Type Global;
                            // Division by area to compute Watts per meter squared
                            // SurfaceArea function does not allow DOM_*** as argument, so we need to use
                            // the actual ids

                            [  {LOCALQUANT_cryocooler_TThinShell~{INPUT_NumOfCryocoolerTSAElements}}/ GetVariable[]{$areaCryocooler}];
                        In DOM_terminalSurfaces;                        
                        Jacobian JAC_sur;
                        Integration Int;
                    }
                }
            }
 {
                Name RESULT_debug;
                Value{
                    Local{
                        [ 5 ];
                        In DOM_allInsulationSurface;
                        Jacobian JAC_sur;
                    }
                }
            }
        }
    }

}

//======================================================================================
// Post-operation: =====================================================================
//======================================================================================
PostOperation{
    {
        Name POSTOP_dummy;
        NameOfPostProcessing POSTPRO_thermal;
        Operation {
        }
    }
 {
        Name POSTOP_debug;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            // 3D magnetic field vector field:
            
            Print[
                RESULT_debug,
                OnElementsOf DOM_allInsulationSurface,
                File "debug-DefaultFormat.pos",
                Name "Debug Variable"
            ];

        }
    }
    {
        Name POSTOP_maximumTemperature;
        NameOfPostProcessing POSTPRO_thermal;
        Operation {
            Print[ RESULT_temperature, 
                    OnElementsOf DOM_powered, 
                    StoreMaxInRegister 999, 
                    Format Table,
                    LastTimeStepOnly 1, 
                    SendToServer "No",
                    File "maximumTemperature_dump.txt"
                ] ;
            // We can print the maximum temperature at any region that is part
            // of the thermal domain since the `StoreMaxInRegister` command
            // already searches all of the thermal region for the maximum and
            //populates the same value for all physical regions of the thermal 
            // domain.
            // Printing in just one domain makes the parsing of the output easier.
            
            Print[
                RESULT_maximumTemperature,
                OnRegion Region[1000000],
                File "maximumTemperature(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                NoTitle,
                Name "Maximum temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_temperature;
        NameOfPostProcessing POSTPRO_thermal;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_temperature,
                OnElementsOf DOM_thermal,
                File "Temperature-DefaultFormat.pos",
                Name "Temperature [K]"
            ];

        }
    }
    {
        Name POSTOP_heatFlux;
        NameOfPostProcessing POSTPRO_thermal;
        Operation {
            // 3D temperature scalar field:
            
            Print[
                RESULT_heatFlux,
                OnElementsOf DOM_thermal,
                File "HeatFlux-DefaultFormat.pos",
                Name "Heat Flux [W/m^2]"
            ];

        }
    }
    {
        Name POSTOP_thermalConductivity;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            // Thermal conductivity:
            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allWindings,
                AtGaussPoints 9,
                File "thermalConductivity_Windings-DefaultFormat.pos",
                Name "Thermal Conductivity Windings [W/(m*K)]"
            ];


            
            Print[
                RESULT_thermalConductivity,
                OnElementsOf DOM_allConductingWithoutWindings,
                File "thermalConductivity_ConductingWithoutWindings-DefaultFormat.pos",
                Name "Thermal Conductivity Without Windings [W/(m*K)]"
            ];

        }
    }
    {
        Name POSTOP_specificHeatCapacity;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            // Specific heat:
            
            Print[
                RESULT_specificHeatCapacity,
                OnElementsOf DOM_thermal,
                File "specificHeatCapacity-DefaultFormat.pos",
                Name "Specific Heat Capacity [J/(kg*K)]"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_maximumTemperature;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            
        }
    }
    {
        Name POSTOP_timeSeriesPlot_cryocoolerAveragePower;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            
            Print[
                RESULT_cryocoolerAveragePower,
                OnGlobal,
                File "cryocoolerAveragePower(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "cryocoolerAveragePower(TimeSeriesPlot)"
            ];

        }
    }
    {
        Name POSTOP_timeSeriesPlot_cryocoolerAverageTemperature;
        NameOfPostProcessing POSTPRO_thermal;
        Operation{
            
            Print[
                RESULT_cryocoolerAverageTemperature,
                OnGlobal,
                File "cryocoolerAverageTemperature(TimeSeriesPlot)-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                Name "cryocoolerAverageTemperature(TimeSeriesPlot)"
            ];

        }
    }
    // convergence criteria as postoperations:
    {
        Name POSTOP_CONV_maximumTemperature;
        NameOfPostProcessing POSTPRO_thermal;
        Operation {
            Print[ RESULT_temperature,
                OnElementsOf DOM_powered,
                StoreMaxInRegister 999,
                Format Table,
                LastTimeStepOnly 1,
                SendToServer "No"
             ] ;

            
            Print[
                RESULT_maximumTemperature,
                OnRegion DOM_powered,
                File "RESULT_maximumTemperature-TimeTableFormat.csv",
                Format TimeTable,
                Comma,
                LastTimeStepOnly 1,
                AppendToExistingFile 1,
                Name "Maximum temperature [K]"
            ];

        } 
    }

}