time:  # All the time related settings for transient analysis.
  start: 0.0  # Start time of the simulation.
  end: 10.0 # End time of the simulation.
  extrapolationOrder: 1 # Before solving for the next time steps, the previous solutions can be extrapolated for better convergence.
  timeSteppingType: adaptive
  adaptiveSteppingSettings:  # Adaptive time loop settings (only used if stepping type is adaptive).
    tolerances:  # Time steps or nonlinear iterations will be refined until the tolerances are satisfied.
      - quantity: voltageBetweenTerminals  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 0.05 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      - quantity: magnitudeOfCurrentDensity  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 16000000.0 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
        position: # Probing position of the quantity for tolerance.
          turnNumber: 3.7  # Winding turn number as a position input. It starts from 0 and it can be a float.
          whichPancakeCoil: 1 # The first pancake coil is 1, the second is 2, etc.
          x: -0.0018878758915098113
          y: -0.00581588253948834
          z: -0.009
      - quantity: magnitudeOfMagneticField  # Name of the quantity for tolerance.
        relative: 0.1 # Relative tolerance for the quantity.
        absolute: 0.002 # Absolute tolerance for the quantity
        normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
        position: # Probing position of the quantity for tolerance.
          x: 0.0  # x coordinate of the position.
          y: 0.0 # y coordinate of the position.
          z: 0.0 # z coordinate of the position.
    initialStep: 1.0 # Initial step for adaptive time stepping
    minimumStep: 0.01 # The simulation will be aborted if a finer time step is required than this minimum step value.
    maximumStep: 30.0 # Bigger steps than this won't be allowed
    integrationMethod: Euler # Integration method for transient analysis
    breakPoints_input: # Make sure to solve the system for these times.
      - 0
    postOperationTolerances:
      - quantity: voltageBetweenTerminals
        relative: 0.1
        absolute: 0.05
        normType: LinfNorm
      - quantity: magnitudeOfCurrentDensity
        relative: 0.1
        absolute: 16000000.0
        normType: LinfNorm
        position:
          turnNumber: 3.7
          whichPancakeCoil: 1
          x: -0.0018878758915098113
          y: -0.00581588253948834
          z: -0.009
      - quantity: magnitudeOfMagneticField
        relative: 0.1
        absolute: 0.002
        normType: LinfNorm
        position:
          x: 0.0
          y: 0.0
          z: 0.0
    systemTolerances: []
    breakPoints: []
nonlinearSolver: # All the nonlinear solver related settings.
  tolerances:  # Time steps or nonlinear iterations will be refined until the tolerances are satisfied.
    - quantity: voltageBetweenTerminals  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 0.05 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
    - quantity: magnitudeOfCurrentDensity  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 16000000.0 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      position: # Probing position of the quantity for tolerance.
        turnNumber: 3.7  # Winding turn number as a position input. It starts from 0 and it can be a float.
        whichPancakeCoil: 1 # The first pancake coil is 1, the second is 2, etc.
        x: -0.0018878758915098113
        y: -0.00581588253948834
        z: -0.009
    - quantity: magnitudeOfMagneticField  # Name of the quantity for tolerance.
      relative: 0.1 # Relative tolerance for the quantity.
      absolute: 0.002 # Absolute tolerance for the quantity
      normType: LinfNorm # Sometimes, tolerances return a vector instead of a scalar (ex, solutionVector). Then, the magnitude of the tolerance should be calculated with a method. Norm type selects this method.
      position: # Probing position of the quantity for tolerance.
        x: 0.0  # x coordinate of the position.
        y: 0.0 # y coordinate of the position.
        z: 0.0 # z coordinate of the position.
  maximumNumberOfIterations: 50 # Maximum number of iterations allowed for the nonlinear solver.
  relaxationFactor: 0.7 # Calculated step changes of the solution vector will be multiplied with this value for better convergence.
  postOperationTolerances:
    - quantity: voltageBetweenTerminals
      relative: 0.1
      absolute: 0.05
      normType: LinfNorm
    - quantity: magnitudeOfCurrentDensity
      relative: 0.1
      absolute: 16000000.0
      normType: LinfNorm
      position:
        turnNumber: 3.7
        whichPancakeCoil: 1
        x: -0.0018878758915098113
        y: -0.00581588253948834
        z: -0.009
    - quantity: magnitudeOfMagneticField
      relative: 0.1
      absolute: 0.002
      normType: LinfNorm
      position:
        x: 0.0
        y: 0.0
        z: 0.0
  systemTolerances: []
winding: # This dictionary contains the winding material properties.
  resistivity: 1e-12  # A scalar value. If this is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: # List of materials of HTS CC.
  shuntLayer: # Material properties of the shunt layer.
    resistivity:  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: # Material from STEAM material library.
      name: Copper
      RRR: 100 # Residual-resistivity ratio (also known as Residual-resistance ratio or just RRR) is the ratio of the resistivity of a material at reference temperature and at 0 K.
      RRRRefTemp: 295 # Reference temperature for residual resistance ratio
      relativeHeight: 0.0 # HTS 2G coated conductor are typically plated, usually  using copper. The relative height of the shunt layer is the  width of the shunt layer divided by the width of the tape.  0 means no shunt layer.
      resistivityMacroName: MATERIAL_Resistivity_Copper_T_B
      thermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_T_B
      heatCapacityMacroName: MATERIAL_SpecificHeatCapacity_Copper_T
      getdpTSAOnlyResistivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSAMassResistivityMacroName: MATERIAL_Resistivity_Copper_TSAMass_T
      getdpTSAStiffnessResistivityMacroName: MATERIAL_Resistivity_Copper_TSAStiffness_T
      getdpTSAMassThermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_TSAMass_T
      getdpTSAStiffnessThermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_TSAStiffness_T
      getdpTSAMassHeatCapacityMacroName: MATERIAL_SpecificHeatCapacity_Copper_TSAMass_T
      getdpTSARHSFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpTSATripleFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
      getdpNormalMaterialGetDPName: Copper
  isotropic: false # If True, resistivity and thermal conductivity are isotropic. If False, they are anisotropic. The default is anisotropic material.
  minimumPossibleResistivity: 1e-20 # The resistivity of the winding won't be lower than this value, no matter what.
  maximumPossibleResistivity: 0.01 # The resistivity of the winding won't be higher than this value, no matter what.
  relativeThicknessOfNormalConductor: 0
  relativeThicknessOfSuperConductor: 0
  normalConductors: []
  superConductor:
contactLayer: # This dictionary contains the contact layer material properties.
  resistivity: 0.001  # A scalar value or "perfectlyInsulating". If "perfectlyInsulating" is given, the contact layer will be perfectly insulating. If this value is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: # Material from STEAM material library.
  numberOfThinShellElements: 2 # Number of thin shell elements in the FE formulation (GetDP related, not physical and only used when TSA is set to True)
terminals: # This dictionary contains the terminals material properties and cooling condition.
  resistivity: 1e-12  # A scalar value. If this is given, material properties won't be used for resistivity.
  thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
  specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
  material: # Material from STEAM material library.
  cooling: adiabatic # Cooling condition of the terminal. It can be either adiabatic, fixed temperature, or cryocooler.
  cryocoolerOptions: # Additional inputs for the cryocooler boundary condition.
    coolingPowerMultiplier: 1  # Multiplier for the cooling power. It can be used to scale the cooling power given by the coldhead capacity map by a non-negative float factor.
    staticHeatLoadPower: 0 # Static heat load power in W. It can be used to add a static heat load to the cryocooler, i.e., decrease the power available for cooling.  The actual cooling power is P(t) = P_cryocooler(T) - P_staticLoad.
    lumpedMass: # Thermal lumped mass between second stage of the cryocooler and pancake coil modeled via TSA.
      resistivity:  # A scalar value. If this is given, material properties won't be used for resistivity.
      thermalConductivity: # A scalar value. If this is given, material properties won't be used for thermal conductivity.
      specificHeatCapacity: # A scalar value. If this is given, material properties won't be used for specific heat capacity.
      material: # Material from STEAM material library.
        name: Copper
        RRR: 295.0 # Residual-resistivity ratio (also known as Residual-resistance ratio or just RRR) is the ratio of the resistivity of a material at reference temperature and at 0 K.
        RRRRefTemp: 295 # Reference temperature for residual resistance ratio
        resistivityMacroName: MATERIAL_Resistivity_Copper_T_B
        thermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_T_B
        heatCapacityMacroName: MATERIAL_SpecificHeatCapacity_Copper_T
        getdpTSAOnlyResistivityFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
        getdpTSAMassResistivityMacroName: MATERIAL_Resistivity_Copper_TSAMass_T
        getdpTSAStiffnessResistivityMacroName: MATERIAL_Resistivity_Copper_TSAStiffness_T
        getdpTSAMassThermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_TSAMass_T
        getdpTSAStiffnessThermalConductivityMacroName: MATERIAL_ThermalConductivity_Copper_TSAStiffness_T
        getdpTSAMassHeatCapacityMacroName: MATERIAL_SpecificHeatCapacity_Copper_TSAMass_T
        getdpTSARHSFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
        getdpTSATripleFunction: NOT_DEFINED_IN_DATA_FIQUS_PANCAKE3D
        getdpNormalMaterialGetDPName: Copper
      volume: 0 # Volume of the lumped thermal mass between second stage of the cryocooler and pancake coil in m^3. A zero value effectively disables the lumped thermal mass between second stage of the cryocooler and pancake coil.
      numberOfThinShellElements: 1 # Number of thin shell elements in the FE formulation (GetDP related, not physical and only used when TSA is set to True)
  transitionNotch: # Material properties of the transition notch volume.
    resistivity: 0.01  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: # Material from STEAM material library.
  terminalContactLayer: # Material properties of the transition layer between terminals and windings.
    resistivity: 0.001  # A scalar value. If this is given, material properties won't be used for resistivity.
    thermalConductivity: 400.0 # A scalar value. If this is given, material properties won't be used for thermal conductivity.
    specificHeatCapacity: 400.0 # A scalar value. If this is given, material properties won't be used for specific heat capacity.
    material: # Material from STEAM material library.
air: # This dictionary contains the air material properties.
  permeability: 1.2566e-06  # Permeability of air.
initialConditions: # Initial conditions of the problem.
  temperature: 77.0  # Initial temperature of the pancake coils.
boundaryConditions: vanishingTangentialElectricField # Boundary conditions of the problem.
quantitiesToBeSaved: # List of quantities to be saved.
  - quantity: magneticField  # Name of the quantity to be saved.
    timesToBeSaved: # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_magneticField
    getdpPostOperationName: POSTOP_magneticField
  - quantity: currentDensity  # Name of the quantity to be saved.
    timesToBeSaved: # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_currentDensity
    getdpPostOperationName: POSTOP_currentDensity
  - quantity: temperature  # Name of the quantity to be saved.
    timesToBeSaved: # List of times that wanted to be saved. If not given, all the time steps will be saved.
    getdpQuantityName: RESULT_temperature
    getdpPostOperationName: POSTOP_temperature
type: weaklyCoupled # FiQuS/Pancake3D can solve only electromagnetic and thermal or electromagnetic and thermal coupled. In the weaklyCoupled setting, thermal and electromagnetics systems will be put into different matrices, whereas in the stronglyCoupled setting, they all will be combined into the same matrix. The solution should remain the same.
proTemplate: Pancake3D_template.pro # file name of the .pro template file
localDefects: # Local defects (like making a small part of the winding normal conductor at some time) can be introduced.
  criticalCurrentDensity:  # Set critical current density locally.
initFromPrevious: '' # The simulation is continued from an existing .res file.  The .res file is from a previous computation on the same geometry and mesh. The .res file is taken from the folder Solution_<<initFromPrevious>>. IMPORTANT: When the option is used, the start time should be identical to the last  time value for the <<initFromPrevious>> simulation.
isothermalInAxialDirection: false # If True, the DoF along the axial direction will be equated. This means that the temperature will be the same along the axial direction reducing the number of DoF. This is only valid for the thermal analysis.
voltageTapPositions: [] # List of voltage tap positions. The position can be given in the form of a list of [x, y, z] coordinates or as turnNumber and number of pancake coil.
EECircuit: # This dictionary contains the detection circuit settings.
  inductanceInSeriesWithPancakeCoil: 0  # A lumped inductance in series with the pancake coil to model a bigger coil. 
  enable: false # Enable the detection circuit for the pancake coil. 
  ResistanceEnergyExtractionOpenSwitch: 1000000.0 # The resistance of the energy extraction switch when modeled as open. 
  ResistanceEnergyExtractionClosedSwitch: 1e-06 # The resistance of the energy extraction switch when modeled as closed. 
  ResistanceCrowbarOpenSwitch: 1000000.0 # The resistance of the crowbar switch when modeled as open. 
  ResistanceCrowbarClosedSwitch: 1e-06 # The resistance of the crowbar switch when modeled as closed. 
  stopSimulationAtCurrent: 0 # If a quench is detected and the current reaches this value, the simulation will be stopped after. stopSimulationWaitingTime seconds.
  stopSimulationWaitingTime: 0 # The time to wait after a quench is detected and the current reaches stopSimulationAtCurrent before stopping the simulation.
  TurnOffDeltaTimePowerSupply: 0 # The time it takes for the power supply to be turned off after quench detection. A linear ramp-down is assumed between the time of quench detection and the time of power supply turn off.
noOfMPITasks: false # If integer, GetDP will be run in parallel using MPI. This is only valid if MPI is installed on the system and an MPI-enabled GetDP is used. If False, GetDP will be run in serial without invoking mpiexec.
resistiveHeatingTerminals: true # If True, terminals are subject to Joule heating. If False, terminal regions are not subject to Joule heating. In both cases, heat conduction through the terminal is  considered.
solveHeatEquationTerminalsTransitionNotch: true # If True, the heat equation is solved in the terminals and transition notch.If False, the heat equation is not solved in the terminals and transition notches.In the latter case, neither heat conduction nor generation are considered.In other words, the temperature is not an unknown of the problem in the terminals.
heatFlowBetweenTurns: true # If True, heat flow between turns is considered. If False, it is not considered. In the latter case, heat conduction is only considered to the middle of the winding in the thin shell approximation in order to keep the thermal mass of the insulation included. In the middle between the turns, an adiabatic condition is applied. Between the turns refers to the region between the winding turns, NOT to the region between terminals and the first and last turn. This feature is only implemented for the thin shell approximation.
convectiveCooling: # This dictionary contains the convective cooling settings.
  heatTransferCoefficient: 0  # The heat transfer coefficient for the heat transfer between the winding and the air. If zero, no heat transfer to the air is considered.This feature is only implemented for the thin shell approximation.At the moment, only constant values are supported.
  exteriorBathTemperature: 4.2 # The temperature of the exterior bath for convective cooling boundary condition. 
imposedPowerDensity: # The power density for an imposed power density in the winding.
materialParametersUseCoilField: true # If True, the total field (i.e., coil field plus potentially imposed field)will be used for the material (default).If False, only the imposed field (can be zero) will be used.
stopWhenTemperatureReaches: 0 # If the maximum temperature reaches this value, the simulation will be stopped.
systemsOfEquationsType: linear
